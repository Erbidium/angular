import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from "@angular/common";

@Pipe({
  name: 'customDate',
  pure: false
})
export class CustomDatePipe implements PipeTransform{
  transform(value: Date | undefined) {
    const datePipe = new DatePipe('uk');
    return datePipe.transform(value, 'd MMMM y');
  }
}
