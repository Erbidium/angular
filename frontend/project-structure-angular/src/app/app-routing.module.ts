import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsListComponent } from "./projects/components/projects-list/projects-list.component";
import { ProjectTasksListComponent } from "./project-tasks/components/project-tasks-list/project-tasks-list.component";
import { TeamsListComponent } from "./teams/components/teams-list/teams-list.component";
import { UsersListComponent } from "./users/components/users-list/users-list.component";
import { DeactivateGuard } from "./shared/guards/deactivate.guard";

const routes: Routes = [
  { path: 'projects', component: ProjectsListComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard]},
  { path: 'tasks', component: ProjectTasksListComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard]},
  { path: 'teams', component: TeamsListComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard]},
  { path: 'users', component: UsersListComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
