import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from "./components/project/project.component";
import { ProjectsListComponent } from "./components/projects-list/projects-list.component";
import { MatCardModule } from "@angular/material/card";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { HttpInternalService } from "../shared/services/http-internal.service";
import { BrowserModule } from "@angular/platform-browser";
import { SharedModule } from "../shared/shared.module";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatInputModule } from "@angular/material/input";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { SnackBarService } from "../shared/services/snack-bar.service";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ErrorInterceptor } from "../shared/helpers/error.interceptor";



@NgModule({
  declarations: [
    ProjectComponent,
    ProjectsListComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    BrowserModule,
    HttpClientModule,
    SharedModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatSnackBarModule
  ],
  providers: [
    HttpInternalService,
    SnackBarService,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class ProjectsModule { }
