export interface Project {
  id: number;
  authorId: number;
  teamId: number;
  name?: string;
  description?: string;
  deadline: Date;
  createdAt: Date;
}
