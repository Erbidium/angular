export interface EditProject {
  id: number;
  name?: string;
  description?: string;
}
