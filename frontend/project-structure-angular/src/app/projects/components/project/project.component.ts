import { Component, Input } from '@angular/core';
import { Project } from "../../models/project/project";
import { EditProject } from "../../models/project/edit-project";
import { ProjectService } from "../../services/project.service";
import { SnackBarService } from "../../../shared/services/snack-bar.service";
import { Subject, takeUntil } from "rxjs";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent {
  @Input() public project: Project | undefined;

  public isEdited = false;

  private unsubscribe$ = new Subject<void>();

  projectForm: FormGroup;

  public constructor(
    private projectService: ProjectService,
    private snackBarService: SnackBarService
  ) {
    this.projectForm = new FormGroup({
      name: new FormControl(this.project?.name ?? ''),
      description: new FormControl(this.project?.description ?? '')
    });
  }


  startEdit() {
    this.projectForm.setValue({
      name: this.project?.name ?? '',
      description: this.project?.description ?? ''
    });

    this.isEdited = true;
  }

  undoEdit() {
    this.isEdited = false;
    this.projectForm.reset();
  }

  sendEditedProject() {
    const editedProject: EditProject = {
      id: this.project?.id ?? 1
    };
    Object.assign(editedProject, this.projectForm.value);

    this.projectService
      .editProject(editedProject)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respProject) => {
        this.project = respProject.body ?? undefined;
        this.undoEdit();
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}
