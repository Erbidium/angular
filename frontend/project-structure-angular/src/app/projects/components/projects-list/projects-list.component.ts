import {Component, OnDestroy, OnInit} from '@angular/core';
import { Project } from "../../models/project/project";
import { ProjectService } from "../../services/project.service";
import { Subject, switchMap, takeUntil } from "rxjs";
import { NewProject } from "../../models/project/new-project";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { SnackBarService } from "../../../shared/services/snack-bar.service";

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit, OnDestroy {
  public projects: Project[] = [];

  newProject: NewProject = {
    authorId: 1,
    teamId: 1,
    deadline: new Date(),
    createdAt: new Date()
  }

  readonly initialValueProject: NewProject = {
    authorId: 1,
    teamId: 1,
    deadline: new Date(),
    createdAt: new Date()
  }

  projectForm = new FormGroup({
    authorId: new FormControl(this.newProject.authorId,
      [
        Validators.required
      ]),
    teamId: new FormControl(this.newProject.teamId, [
      Validators.required
    ]),
    name: new FormControl(this.newProject.name),
    description: new FormControl(this.newProject.description),
    deadline: new FormControl(this.newProject.deadline, [
      Validators.required
    ])
  });

  public loading = false;
  public loadingProjects = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private projectService: ProjectService, private snackBarService: SnackBarService) { }

  ngOnInit() {
    this.getProjects();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getProjects() {
    this.loadingProjects = true;
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (resp) => {
          this.loadingProjects = false;
          this.projects = resp.body ?? [];
          console.log(this.projects);
        },
        error: (error) => (this.loadingProjects = false)
      });
  }

  createProject() {
    Object.assign(this.newProject, this.projectForm.value);

    this.loading = true;

    this.projectService
      .createProject(this.newProject)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respPost) => {
        this.addNewProject(respPost.body);
        this.projectForm.reset();
        this.newProject = {...this.initialValueProject};
        this.loading = false;
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.loading = false;
      }
    );
  }

  public addNewProject(newProject: Project | null) {
    if (newProject != null && !this.projects.some((x) => x.id === newProject.id)) {
      this.projects = this.projects.concat(newProject);
    }
  }

  public canExit(): boolean {
    return this.projectForm.dirty
      ? window.confirm('You have unsaved changes.  Are you sure you want to leave the page?')
      : true;
  };
}
