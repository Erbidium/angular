import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../shared/services/http-internal.service";
import { Project } from "../models/project/project";
import { NewProject } from "../models/project/new-project";
import { EditProject } from "../models/project/edit-project";

@Injectable({ providedIn: 'root' })
export class ProjectService {
  public routePrefix = '/api/projects';

  constructor(private httpService: HttpInternalService) {}

  public getProjects() {
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  public createProject(project: NewProject) {
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
  }

  public editProject(editedProject: EditProject) {
    return this.httpService.putFullRequest<Project>(`${this.routePrefix}`, editedProject);
  }
}
