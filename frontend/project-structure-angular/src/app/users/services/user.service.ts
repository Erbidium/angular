import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../shared/services/http-internal.service";
import { User } from "../models/user/user";
import { NewUser } from "../models/user/new-user";
import { EditUser } from "../models/user/edit-user";

@Injectable({ providedIn: 'root' })
export class UserService {
  public routePrefix = '/api/users';

  constructor(private httpService: HttpInternalService) {}

  public getUsers() {
    return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }

  public createUser(user: NewUser) {
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`, user);
  }

  public editUser(editedUser: EditUser) {
    return this.httpService.putFullRequest<User>(`${this.routePrefix}`, editedUser);
  }
}
