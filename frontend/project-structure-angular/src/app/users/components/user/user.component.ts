import { Component, Input, OnInit } from '@angular/core';
import { User } from "../../models/user/user";
import { UserService } from "../../services/user.service";
import { SnackBarService } from "../../../shared/services/snack-bar.service";
import { Subject, takeUntil } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { EditUser } from "../../models/user/edit-user";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  @Input() public user: User | undefined;

  public isEdited = false;

  private unsubscribe$ = new Subject<void>();

  userForm: FormGroup;

  public constructor(
    private userService: UserService,
    private snackBarService: SnackBarService
  ) {
    this.userForm = new FormGroup({
      teamId: new FormControl(this.user?.teamId),
      firstName: new FormControl(this.user?.firstName ?? ''),
      lastName: new FormControl(this.user?.lastName ?? ''),
      email: new FormControl(this.user?.email, [
        Validators.email
      ]),
      birthDay: new FormControl(this.user?.birthDay, [
        Validators.required
      ])
    });
  }


  startEdit() {
    this.userForm.setValue({
      teamId: this.user?.teamId,
      firstName: this.user?.firstName ?? '',
      lastName: this.user?.lastName ?? '',
      email: this.user?.email,
      birthDay: this.user?.birthDay ?? new Date()
    });

    this.isEdited = true;
  }

  undoEdit() {
    this.isEdited = false;
    this.userForm.reset();
  }

  sendEditedUser() {
    const editedUser: EditUser = {
      id: this.user?.id ?? 1,
      birthDay: this.user?.birthDay ?? new Date()
    };
    Object.assign(editedUser, this.userForm.value);

    this.userService
      .editUser(editedUser)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respUser) => {
        this.user = respUser.body ?? undefined;
        this.undoEdit();
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}
