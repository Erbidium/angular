import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from "rxjs";
import { User } from "../../models/user/user";
import { UserService } from "../../services/user.service";
import { NewUser } from "../../models/user/new-user";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { SnackBarService } from "../../../shared/services/snack-bar.service";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, OnDestroy {
  public users: User[] = [];

  newUser: NewUser = {
    teamId: 1,
    birthDay: new Date()
  }

  readonly initialValueUser: NewUser = {
    teamId: 1,
    birthDay: new Date()
  }

  userForm = new FormGroup({
    teamId: new FormControl(this.newUser.teamId, [
      Validators.required
    ]),
    firstName: new FormControl(this.newUser.firstName),
    lastName: new FormControl(this.newUser.lastName),
    email: new FormControl(this.newUser.email, [
      Validators.email
    ]),
    birthDay: new FormControl(this.newUser.birthDay, [
      Validators.required
    ])
  });

  public loading = false;
  public loadingUsers = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private userService: UserService, private snackBarService: SnackBarService) { }

  ngOnInit() {
    this.getUsers();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getUsers() {
    this.loadingUsers = true;
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (resp) => {
          this.loadingUsers = false;
          this.users = resp.body ?? [];
          console.log(this.users);
        },
        error: (error) => (this.loadingUsers = false)
      });
  }

  createUser() {
    Object.assign(this.newUser, this.userForm.value);

    this.loading = true;

    this.userService
      .createUser(this.newUser)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respPost) => {
        this.addNewUser(respPost.body);
        this.userForm.reset();
        this.newUser = {...this.initialValueUser};

        this.loading = false;
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.loading = false;
      }
    );
  }

  public addNewUser(newUser: User | null) {
    if (newUser != null && !this.users.some((x) => x.id === newUser.id)) {
      this.users = this.users.concat(newUser);
    }
  }

  public canExit(): boolean {
    return this.userForm.dirty
      ? window.confirm('You have unsaved changes.  Are you sure you want to leave the page?')
      : true;
  };
}
