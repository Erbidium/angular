import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {HttpClientModule} from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { registerLocaleData } from "@angular/common";
import localeUk from '@angular/common/locales/uk'
import { ProjectsModule } from "./projects/projects.module";
import { ProjectTasksModule } from "./project-tasks/project-tasks.module";
import { TeamsModule } from "./teams/teams.module";
import { UsersModule } from "./users/users.module";
import { MatNativeDateModule } from "@angular/material/core";

registerLocaleData(localeUk, 'uk')

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonToggleModule,
    MatToolbarModule,
    RouterModule,
    ProjectsModule,
    ProjectTasksModule,
    TeamsModule,
    UsersModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
