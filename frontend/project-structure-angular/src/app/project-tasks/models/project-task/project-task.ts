import { TaskState } from "../../enums/task-state.enum";

export interface ProjectTask {
  id: number;
  projectId: number;
  performerId: number;
  name?: string;
  description?: string;
  state: TaskState,
  createdAt: Date;
  finishedAt?: Date;
}
