import { TaskState } from "../../enums/task-state.enum";

export interface NewProjectTask {
  projectId?: number;
  performerId?: number;
  name?: string;
  description?: string;
  state: TaskState,
}
