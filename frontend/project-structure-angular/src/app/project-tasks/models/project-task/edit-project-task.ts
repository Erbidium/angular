export interface EditProjectTask {
  id: number;
  name?: string;
  description?: string;
}
