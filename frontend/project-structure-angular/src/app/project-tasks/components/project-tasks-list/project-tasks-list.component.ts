import { ProjectTask } from "../../models/project-task/project-task";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ProjectTaskService } from "../../services/project-task.service";
import { Subject, takeUntil } from "rxjs";
import { TaskState } from "../../enums/task-state.enum";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NewProjectTask } from "../../models/project-task/new-project-task";
import { SnackBarService } from "../../../shared/services/snack-bar.service";

@Component({
  selector: 'app-project-tasks-list',
  templateUrl: './project-tasks-list.component.html',
  styleUrls: ['./project-tasks-list.component.css']
})
export class ProjectTasksListComponent implements OnInit, OnDestroy {
  public projectTasks: ProjectTask[] = [];

  newProjectTask: NewProjectTask = {
    state: TaskState.ToDo
  }

  readonly initialValueProject: NewProjectTask = {
    state: TaskState.ToDo
  }

  projectTaskForm = new FormGroup({
    projectId: new FormControl(this.newProjectTask.projectId),
    performerId: new FormControl(this.newProjectTask.performerId),
    name: new FormControl(this.newProjectTask.name),
    description: new FormControl(this.newProjectTask.description)
  });

  public loading = false;
  public loadingProjectTasks = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private projectTaskService: ProjectTaskService, private snackBarService: SnackBarService) { }

  ngOnInit() {
    this.getProjectTasks();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getProjectTasks() {
    this.loadingProjectTasks = true;
    this.projectTaskService
      .getProjectTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (resp) => {
          this.loadingProjectTasks = false;
          this.projectTasks = resp.body ?? [];
          console.log(this.projectTasks);
        },
        error: (error) => (this.loadingProjectTasks = false)
      });
  }

  createProject() {
    Object.assign(this.newProjectTask, this.projectTaskForm.value);

    this.loading = true;

    this.projectTaskService
      .createProjectTask(this.newProjectTask)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respPost) => {
        this.addNewProjectTask(respPost.body);
        this.projectTaskForm.reset();
        this.newProjectTask = {...this.initialValueProject};
        this.loading = false;
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.loading = false;
      }
    );
  }

  public addNewProjectTask(newProjectTask: ProjectTask | null) {
    if (newProjectTask != null && !this.projectTasks.some((x) => x.id === newProjectTask.id)) {
      this.projectTasks = this.projectTasks.concat(newProjectTask);
    }
  }

  public canExit(): boolean {
    return this.projectTaskForm.dirty
      ? window.confirm('You have unsaved changes.  Are you sure you want to leave the page?')
      : true;
  };
}
