import { Component, Input } from '@angular/core';
import { ProjectTask } from "../../models/project-task/project-task";
import { EditProjectTask } from "../../models/project-task/edit-project-task";
import { Subject, takeUntil } from "rxjs";
import { SnackBarService } from "../../../shared/services/snack-bar.service";
import { ProjectTaskService } from "../../services/project-task.service";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-project-task',
  templateUrl: './project-task.component.html',
  styleUrls: ['./project-task.component.css']
})
export class ProjectTaskComponent {
  @Input() public projectTask: ProjectTask | undefined;

  public isEdited = false;

  private unsubscribe$ = new Subject<void>();

  projectTaskForm: FormGroup;

  public constructor(
    private projectTaskService: ProjectTaskService,
    private snackBarService: SnackBarService
  ) {
    this.projectTaskForm = new FormGroup({
      name: new FormControl(this.projectTask?.name ?? ''),
      description: new FormControl(this.projectTask?.description ?? '')
    });
  }


  startEdit() {
    this.projectTaskForm.setValue({
      name: this.projectTask?.name ?? '',
      description: this.projectTask?.description ?? ''
    });

    this.isEdited = true;
  }

  undoEdit() {
    this.isEdited = false;
    this.projectTaskForm.reset();
  }

  sendEditedProjectTask() {
    const editedProjectTask: EditProjectTask = {
      id: this.projectTask?.id ?? 1
    };
    Object.assign(editedProjectTask, this.projectTaskForm.value);

    this.projectTaskService
      .editProjectTask(editedProjectTask)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respProjectTask) => {
        this.projectTask = respProjectTask.body ?? undefined;
        this.undoEdit();
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}
