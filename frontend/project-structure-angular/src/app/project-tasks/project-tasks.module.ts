import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectTasksListComponent } from './components/project-tasks-list/project-tasks-list.component';
import { ProjectTaskComponent } from './components/project-task/project-task.component';
import { MatCardModule } from "@angular/material/card";
import { SharedModule } from "../shared/shared.module";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatFormFieldModule } from "@angular/material/form-field";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { StatusDirective } from "./directives/status.directive";
import { MatTooltipModule } from "@angular/material/tooltip";



@NgModule({
  declarations: [
    ProjectTasksListComponent,
    ProjectTaskComponent,
    StatusDirective
  ],
    imports: [
        CommonModule,
        MatCardModule,
        SharedModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule
    ],
  exports: [
    StatusDirective
  ]
})
export class ProjectTasksModule { }
