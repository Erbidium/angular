import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { TaskState } from "../enums/task-state.enum";

@Directive({
  selector: '[status]'
})
export class StatusDirective implements OnInit{
  @Input('taskState') taskState: TaskState | undefined;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    const span = document.createElement('span');

    span.style.height = '8px';
    span.style.width = '8px';

    switch (this.taskState) {
      case TaskState.Done:
        span.style.backgroundColor = 'green';
        break;
      case TaskState.ToDo:
        span.style.backgroundColor = 'red';
        break;
      case TaskState.Canceled:
        span.style.backgroundColor = 'grey';
        break;
      case TaskState.InProgress:
        span.style.backgroundColor = 'yellow';
        break;
      default:
        span.style.backgroundColor = 'blue';
        break;
    }
    span.style.borderRadius = '50%';
    span.style.display = 'inline-block';

    const firstChild = this.el.nativeElement.firstChild;
    this.el.nativeElement.insertBefore(span, firstChild);
    this.el.nativeElement.style.alignItems = 'center';
  }
}
