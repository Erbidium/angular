import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../shared/services/http-internal.service";
import { ProjectTask } from "../models/project-task/project-task";
import { NewProjectTask } from "../models/project-task/new-project-task";
import { EditProjectTask } from "../models/project-task/edit-project-task";

@Injectable({ providedIn: 'root' })
export class ProjectTaskService {
  public routePrefix = '/api/tasks';

  constructor(private httpService: HttpInternalService) {}

  public getProjectTasks() {
    return this.httpService.getFullRequest<ProjectTask[]>(`${this.routePrefix}`);
  }

  public createProjectTask(projectTask: NewProjectTask) {
    return this.httpService.postFullRequest<ProjectTask>(`${this.routePrefix}`, projectTask);
  }

  public editProjectTask(editedProjectTask: EditProjectTask) {
    return this.httpService.putFullRequest<ProjectTask>(`${this.routePrefix}`, editedProjectTask);
  }
}
