import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project } from "../../../projects/models/project/project";
import { Subject, takeUntil } from "rxjs";
import { ProjectService } from "../../../projects/services/project.service";
import { Team } from "../../models/team/team";
import { TeamService } from "../../services/team.service";
import { NewTeam } from "../../models/team/new-team";
import { FormControl, FormGroup } from "@angular/forms";
import { SnackBarService } from "../../../shared/services/snack-bar.service";

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit, OnDestroy {
  public teams: Team[] = [];

  newTeam: NewTeam = {};

  teamForm = new FormGroup({
    name: new FormControl(this.newTeam.name),
  });

  public loading = false;
  public loadingTeams = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private teamService: TeamService, private snackBarService: SnackBarService) { }

  ngOnInit() {
    this.getTeams();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getTeams() {
    this.loadingTeams = true;
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (resp) => {
          this.loadingTeams = false;
          this.teams = resp.body ?? [];
          console.log(this.teams);
        },
        error: (error) => (this.loadingTeams = false)
      });
  }

  createTeam() {
    Object.assign(this.newTeam, this.teamForm.value);

    this.loading = true;

    this.teamService
      .createTeam(this.newTeam)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respPost) => {
        this.addNewTeam(respPost.body);
        this.teamForm.reset();
        this.newTeam = {};
        this.loading = false;
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.loading = false;
      }
    );
  }

  public addNewTeam(newTeam: Team | null) {
    if (newTeam != null && !this.teams.some((x) => x.id === newTeam.id)) {
      this.teams = this.teams.concat(newTeam);
    }
  }

  public canExit(): boolean {
    return this.teamForm.dirty
      ? window.confirm('You have unsaved changes.  Are you sure you want to leave the page?')
      : true;
  };
}
