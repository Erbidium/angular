import { Component, Input } from '@angular/core';
import { Team } from "../../models/team/team";
import { TeamService } from "../../services/team.service";
import { SnackBarService } from "../../../shared/services/snack-bar.service";
import { FormControl, FormGroup } from "@angular/forms";
import { EditProject } from "../../../projects/models/project/edit-project";
import { Subject, takeUntil } from "rxjs";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent {
  @Input() public team: Team | undefined;

  public isEdited = false;

  private unsubscribe$ = new Subject<void>();

  teamForm: FormGroup;

  public constructor(
    private teamService: TeamService,
    private snackBarService: SnackBarService
  ) {
    this.teamForm = new FormGroup({
      name: new FormControl(this.team?.name ?? '')
    });
  }

  startEdit() {
    this.teamForm.setValue({
      name: this.team?.name ?? ''
    });

    this.isEdited = true;
  }

  undoEdit() {
    this.isEdited = false;
    this.teamForm.reset();
  }

  sendEditedTeam() {
    const editedTeam: EditProject = {
      id: this.team?.id ?? 1
    };
    Object.assign(editedTeam, this.teamForm.value);

    this.teamService
      .editTeam(editedTeam)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respTeam) => {
        this.team = respTeam.body ?? undefined;
        this.undoEdit();
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}
