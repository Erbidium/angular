import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../shared/services/http-internal.service";
import { Team } from "../models/team/team";
import { NewTeam } from "../models/team/new-team";
import { EditTeam } from "../models/team/edit-team";

@Injectable({ providedIn: 'root' })
export class TeamService {
  public routePrefix = '/api/teams';

  constructor(private httpService: HttpInternalService) {}

  public getTeams() {
    return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  public createTeam(team: NewTeam) {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
  }

  public editTeam(editedTeam: EditTeam) {
    return this.httpService.putFullRequest<Team>(`${this.routePrefix}`, editedTeam);
  }
}
