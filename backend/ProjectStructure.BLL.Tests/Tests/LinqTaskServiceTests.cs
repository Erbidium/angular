using AutoMapper;
using LinqTask.Lib.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ProjectStructure.Common.DTO.LinqTask;
using ProjectStructure.Common.Enums;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;
using ProjectLinq = LinqTask.Lib.Entities.Project;
using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;
using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure.BLL.Tests.Tests;

public class LinqTaskServiceTests : IDisposable
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public LinqTaskServiceTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<ProjectTaskProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });

        _mapper = new Mapper(configuration);

        var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
    }

    public void Dispose()
    {
        _context.Dispose();
    }

    [Fact]
    public async void GetNumberUserTasks_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var expected = new NumberUserTasksDto
        {
            ProjectsTasksNumber = new Dictionary<ProjectLinq, int>
            {
                { _mapper.Map<ProjectLinq>(await _unitOfWork.ProjectRepository.GetById(1)), 2 }
            }.ToList()
        };

        var actual = await linqTaskService.GetNumberUserTasks(1);

        Assert.Equal(expected.ProjectsTasksNumber.Count, actual.ProjectsTasksNumber.Count);
        Assert.Equal(expected.ProjectsTasksNumber.First().Key.Id, actual.ProjectsTasksNumber.First().Key.Id);
        Assert.Equal(expected.ProjectsTasksNumber.First().Value, actual.ProjectsTasksNumber.First().Value);
    }

    [Fact]
    public async void GetTasksAssignedToUser_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            Name = "Name 1",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            Name = "Name 2",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var expected = new TasksAssignedToUserDto
        {
            Tasks = _mapper.Map<IEnumerable<ProjectTaskLinq>>(new[]
            {
                await _unitOfWork.ProjectTaskRepository.GetById(1),
                await _unitOfWork.ProjectTaskRepository.GetById(2),
            })
        };

        var actual = await linqTaskService.GetTasksAssignedToUser(1);

        Assert.Equal(expected.Tasks.Count(), actual.Tasks.Count());
        Assert.Equal(expected.Tasks.First().Id, actual.Tasks.First().Id);
        Assert.Equal(expected.Tasks.Last().Id, actual.Tasks.Last().Id);
    }

    [Fact]
    public async void GetTasksAssignedToUser_ReturnsTasksWithCorrectNameLength()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            Name = "Name 1",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            Name = "Name 2",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var actual = await linqTaskService.GetTasksAssignedToUser(1);

        Assert.All(actual.Tasks, task => Assert.True(task.Name.Length < 45));
    }

    [Fact]
    public async void GetFinishedTasksByUserInCurrentYear_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });

        int currentYear = 2022;
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1),
            FinishedAt = new DateTime(currentYear, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1),
            FinishedAt = new DateTime(currentYear, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var firstTask = await _unitOfWork.ProjectTaskRepository.GetById(1);
        var secondTask = await _unitOfWork.ProjectTaskRepository.GetById(2);
        var expected = new FinishedTaskByUserInCurrentYearDto[]
        {
            new()
            {
                Id = firstTask.Id,
                Name = firstTask.Name
            },
            new()
            {
                Id = secondTask.Id,
                Name = secondTask.Name
            }
        };

        var actual = await linqTaskService.GetFinishedTasksByUserInCurrentYear(1);

        Assert.Equal(expected.Length, actual.Count());
        Assert.Equal(expected.First().Id, actual.First().Id);
        Assert.Equal(expected.First().Name, actual.First().Name);
        Assert.Equal(expected.Last().Id, actual.Last().Id);
        Assert.Equal(expected.Last().Name, actual.Last().Name);
    }

    [Fact]
    public async void GetFinishedTasksByUserInCurrentYear_ReturnsTasksFinishedInCurrentYear()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });

        int currentYear = 2022;
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1),
            FinishedAt = new DateTime(currentYear, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1),
            FinishedAt = new DateTime(currentYear, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 3,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1),
            FinishedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var actual = await linqTaskService.GetFinishedTasksByUserInCurrentYear(1);

        Assert.All(actual, async dto =>
        {
            var projectTask = await _unitOfWork.ProjectTaskRepository.GetById(dto.Id);
            Assert.Equal(projectTask.FinishedAt.Value.Year, currentYear);
        });
    }

    [Fact]
    public async void GetTeamMembers_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            Name = "Name 1",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            Name = "Name2",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var teamMember = await _unitOfWork.UserRepository.GetById(1);
        var team = await _unitOfWork.TeamRepository.GetById(1);
        var expected = new TeamMembersDto[]
        {
            new()
            {
                Id = team.Id,
                TeamName = team.Name,
                TeamUsers = _mapper.Map<UserLinq[]>(new[] { teamMember }).OrderByDescending(u => u.RegisteredAt)
            }
        };

        var actual = (await linqTaskService.GetTeamMembers()).ToList();

        Assert.Equal(expected.Length, actual.Count());
        Assert.Equal(expected.First().Id, actual.First().Id);
        Assert.Equal(expected.First().TeamName, actual.First().TeamName);
        Assert.Equal(expected.First().TeamUsers.Count(), actual.First().TeamUsers.Count());
        Assert.Equal(expected.First().TeamUsers.First().Id, actual.First().TeamUsers.First().Id);
    }

    [Fact]
    public async void GetUsersAndTasks_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            FirstName = "User",
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);


        var user = _mapper.Map<UserLinq>(await _unitOfWork.UserRepository.GetById(1));
        var firstTask = _mapper.Map<ProjectTaskLinq>(await _unitOfWork.ProjectTaskRepository.GetById(1));
        var secondTask = _mapper.Map<ProjectTaskLinq>(await _unitOfWork.ProjectTaskRepository.GetById(2));
        var expected = new UserAndTasksDto[]
        {
            new()
            {
                User = user,
                UserTasks = new[] { firstTask, secondTask }.OrderByDescending(task => task.Name?.Length ?? 0)
            }
        };

        var actual = await linqTaskService.GetUsersAndTasks();

        Assert.Equal(expected.Length, actual.Count());
        Assert.Equal(expected.First().User.Id, actual.First().User.Id);
        Assert.Equal(expected.First().UserTasks.Count(), actual.First().UserTasks.Count());
        Assert.Equal(expected.First().UserTasks.First().Id, actual.First().UserTasks.First().Id);
        Assert.Equal(expected.First().UserTasks.Last().Id, actual.First().UserTasks.Last().Id);
    }

    [Fact]
    public async void GetUserStatistics_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1),
            FinishedAt = new DateTime(2022, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1),
            FinishedAt = new DateTime(2022, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var user = _mapper.Map<UserLinq>(await _unitOfWork.UserRepository.GetById(1));
        var project = _mapper.Map<ProjectLinq>(await _unitOfWork.ProjectRepository.GetById(1));
        var longestTask = _mapper.Map<ProjectTaskLinq>(await _unitOfWork.ProjectTaskRepository.GetById(2));
        var expected = new UserStatisticsDto
        {
            User = user,
            Project = project,
            TasksNumber = 2,
            NumberOfUnfinishedOrCanceledTasks = 0,
            LongestTask = longestTask
        };

        var actual = await linqTaskService.GetUserStatistics(1);

        Assert.Equal(expected.User.Id, actual.User.Id);
        Assert.Equal(expected.Project.Id, actual.Project.Id);
        Assert.Equal(expected.TasksNumber, actual.TasksNumber);
        Assert.Equal(expected.NumberOfUnfinishedOrCanceledTasks, actual.NumberOfUnfinishedOrCanceledTasks);
        Assert.Equal(expected.LongestTask.Id, actual.LongestTask.Id);
    }

    [Fact]
    public async void GetUserStatistics_WhenNotExistingUser_ThanNullResult()
    {
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);
        int notExistingUserId = 1;

        var actual = await linqTaskService.GetUserStatistics(notExistingUserId);

        Assert.Null(actual);
    }

    [Fact]
    public async void GetProjectStatistics_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            FirstName = "User",
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        IEnumerable<ProjectStatisticsDto> expected = new[]
        {
            new ProjectStatisticsDto
            {
                Project = _mapper.Map<ProjectLinq>(await _unitOfWork.ProjectRepository.GetById(1)),
                LongestTask = _mapper.Map<ProjectTaskLinq>(await _unitOfWork.ProjectTaskRepository.GetById(1)),
                ShortestTask = _mapper.Map<ProjectTaskLinq>(await _unitOfWork.ProjectTaskRepository.GetById(2)),
                NumberOfUsersInProjectTeam = 1
            }
        };

        var actual = (await linqTaskService.GetProjectStatistics()).ToList();

        Assert.Equal(expected.Count(), actual.Count);
        Assert.Equal(expected.First().Project.Id, actual.First().Project.Id);
        Assert.Equal(expected.First().NumberOfUsersInProjectTeam, actual.First().NumberOfUsersInProjectTeam);
        Assert.Equal(expected.First().LongestTask.Id, actual.First().LongestTask.Id);
        Assert.Equal(expected.First().ShortestTask.Id, actual.First().ShortestTask.Id);
    }

    [Fact]
    public async void
        GetProjectStatistics_WhenTasksNumberMoreOrEqualThanThreeAndDescriptionIsLessThanTwentySymbols_ThenNumberOfUsersInProjectTeamIsNull()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            FirstName = "User",
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 3,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var actual = (await linqTaskService.GetProjectStatistics()).ToList();

        Assert.Null(actual.First().NumberOfUsersInProjectTeam);
    }

    [Fact]
    public async void
        GetProjectStatistics_WhenDescriptionMoreThanTwentySymbols_ThenNumberOfUsersInProjectTeamIsNotNull()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            FirstName = "User",
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Description = "Very long description. More than 20 symbols.",
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 3,
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name name",
            Description = "Short description",
            State = TaskState.Done,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        var structureLinqQueriesCreator = new StructureLinqQueriesCreator(_unitOfWork, _mapper);
        LinqTaskService linqTaskService = new LinqTaskService(_unitOfWork, _mapper, structureLinqQueriesCreator);

        var actual = (await linqTaskService.GetProjectStatistics()).ToList();

        Assert.NotNull(actual.First().NumberOfUsersInProjectTeam);
    }
}