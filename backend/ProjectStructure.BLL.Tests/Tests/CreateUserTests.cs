﻿using AutoMapper;
using FakeItEasy;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;

namespace ProjectStructure.BLL.Tests.Tests;

public class CreateUserTests
{
    private readonly IMapper _mapper;

    public CreateUserTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<ProjectTaskProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        _mapper = new Mapper(configuration);
    }

    [Fact]
    public async void CreateUser_WhenNewUserPassed_ThenAddMethodInRepositoryCalled()
    {
        var newUser = new NewUserDto
        {
            FirstName = "Test",
            LastName = "Test",
            BirthDay = new DateTime(2000, 1, 1)
        };
        var expectedUser = new User
        {
            Id = 1,
            FirstName = "Test",
            LastName = "Test",
            BirthDay = new DateTime(2000, 1, 1),
            RegisteredAt = DateTime.Now
        };
        var fakeUnitOfWork = A.Fake<IUnitOfWork>();
        A.CallTo(() => fakeUnitOfWork.UserRepository.GetById(A<int>._)).Returns(Task.FromResult(expectedUser));
        UserService userService = new UserService(fakeUnitOfWork, _mapper);

        var createdUser = await userService.Create(newUser);

        Assert.NotNull(createdUser);
        Assert.Equal(newUser.BirthDay, createdUser.BirthDay);
        Assert.Equal(newUser.FirstName, createdUser.FirstName);
        Assert.Equal(newUser.LastName, createdUser.LastName);
        A.CallTo(() => fakeUnitOfWork.UserRepository.Add(A<User>.Ignored)).MustHaveHappened();
    }
}