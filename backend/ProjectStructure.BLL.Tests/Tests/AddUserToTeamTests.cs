﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;

namespace ProjectStructure.BLL.Tests.Tests;

public class AddUserToTeamTests
{
    private readonly UserService _userService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public AddUserToTeamTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<ProjectTaskProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        _mapper = new Mapper(configuration);

        var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _userService = new UserService(_unitOfWork, _mapper);
    }

    [Fact]
    public async void AddUserToTeam()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 2,
            CreatedAt = new DateTime(2021, 1, 1)
        });

        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();

        var expected = await _unitOfWork.UserRepository.GetById(1);
        expected.TeamId = 2;
        var updateUserDto = new UpdateUserDto
        {
            Id = 1,
            TeamId = 2
        };

        var actual = await _userService.Update(updateUserDto);
        await _unitOfWork.SaveChangesAsync();

        Assert.NotNull(actual);
        Assert.Equal(expected.Id, actual.Id);
        Assert.Equal(expected.TeamId, actual.TeamId);
    }

    [Fact]
    public async void AddUserToTeam_WhenNotExistingUser_ThenNotFoundException()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 2,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();

        var updateUserDto = new UpdateUserDto
        {
            Id = 1,
            TeamId = 2
        };

        await Assert.ThrowsAsync<NotFoundException>(() => _userService.Update(updateUserDto));
    }

    [Fact]
    public async void AddUserToTeam_WhenNotExistingTeam_ThenNotFoundException()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });

        var updateUserDto = new UpdateUserDto
        {
            Id = 1,
            TeamId = 5
        };

        await Assert.ThrowsAsync<NotFoundException>(() => _userService.Update(updateUserDto));
    }
}