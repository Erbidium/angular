﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.Enums;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;

namespace ProjectStructure.BLL.Tests.Tests;

public class UpdateProjectTaskTests
{
    private readonly ProjectTaskService _projectTaskService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public UpdateProjectTaskTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<ProjectTaskProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        _mapper = new Mapper(configuration);

        var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _projectTaskService = new ProjectTaskService(_unitOfWork, _mapper);
    }

    [Fact]
    public async void UpdateProjectTaskToFinished()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });

        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });

        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });

        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.InProgress,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();

        var expected = await _unitOfWork.ProjectTaskRepository.GetById(1);

        expected.State = TaskState.Done;
        var updateProjectTaskDto = new UpdateProjectTaskDto
        {
            Id = 1,
            State = TaskState.Done,
        };

        var actual = await _projectTaskService.Update(updateProjectTaskDto);
        await _unitOfWork.SaveChangesAsync();

        Assert.NotNull(actual);
        Assert.Equal(expected.Id, actual.Id);
        Assert.Equal(expected.State, actual.State);
    }

    [Fact]
    public async void UpdateProjectTaskToFinished_WhenNotExistingProjectTask_ThenNotFoundException()
    {
        var updateProjectTaskDto = new UpdateProjectTaskDto
        {
            Id = 1,
            State = TaskState.Done,
        };

        await Assert.ThrowsAsync<NotFoundException>(() => _projectTaskService.Update(updateProjectTaskDto));
    }
}