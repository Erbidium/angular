﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.Enums;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;

namespace ProjectStructure.BLL.Tests.Tests;

public class GetUnfinishedTasksByUserTests : IDisposable
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public GetUnfinishedTasksByUserTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<ProjectTaskProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        _mapper = new Mapper(configuration);

        var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
    }

    public void Dispose()
    {
        _context.Dispose();
    }

    [Fact]
    public async void GetUnfinishedTasksByUser_ReturnsCorrectResult()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            TeamId = 1,
            RegisteredAt = new DateTime(2020, 1, 1),
            BirthDay = new DateTime(2000, 1, 1)
        });
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            AuthorId = 1,
            TeamId = 1,
            Deadline = new DateTime(2020, 1, 1),
            CreatedAt = new DateTime(2021, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 1,
            Name = "Name 1",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.InProgress,
            CreatedAt = new DateTime(2020, 1, 1)
        });
        await _unitOfWork.ProjectTaskRepository.Add(new ProjectTask
        {
            Id = 2,
            Name = "Name 2",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.ToDo,
            CreatedAt = new DateTime(2019, 1, 1)
        });
        await _unitOfWork.SaveChangesAsync();
        ProjectTaskService projectTaskService = new ProjectTaskService(_unitOfWork, _mapper);

        IEnumerable<ProjectTaskDto> expected = _mapper.Map<IEnumerable<ProjectTaskDto>>(new[]
        {
            await _unitOfWork.ProjectTaskRepository.GetById(1),
            await _unitOfWork.ProjectTaskRepository.GetById(2)
        });

        var actual = await projectTaskService.GetUnfinishedTasksByUser(1);

        Assert.Equal(expected.Count(), actual.Count());
        Assert.All(expected,
            expectedProjectTask => actual.Any(actualProjectTask => actualProjectTask.Id == expectedProjectTask.Id));
    }

    [Fact]
    public async void GetUnfinishedTasksByUser_WhenNotExistingUser_ThanNotFoundException()
    {
        int notExistingUserId = 1;
        ProjectTaskService projectTaskService = new ProjectTaskService(_unitOfWork, _mapper);

        await Assert.ThrowsAsync<NotFoundException>(
            () => projectTaskService.GetUnfinishedTasksByUser(notExistingUserId));
    }
}