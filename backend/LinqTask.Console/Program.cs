using LinqTask.Console;

QueryResultsPrinter queryResultsPrinter = await QueryResultsPrinter.CreateAsync();

var menu = new Menu(queryResultsPrinter);
menu.ShowMenu();