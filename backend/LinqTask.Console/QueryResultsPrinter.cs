﻿using LinqTask.Lib;
using LinqTask.Lib.Entities;

namespace LinqTask.Console;

public class QueryResultsPrinter
{
    private readonly StructureLinqQueries _structureLinqQueries;
    private readonly ProjectTasksQueries _projectTasksQueries;

    public static async Task<QueryResultsPrinter> CreateAsync()
    {
        EntitiesApiReader entitiesApiReader = new();

        var projectsTask = entitiesApiReader.GetProjects();
        var projectTasksTask = entitiesApiReader.GetTasks();
        var teamsTask = entitiesApiReader.GetTeams();
        var usersTask = entitiesApiReader.GetUsers();

        await Task.WhenAll(projectsTask,
                projectTasksTask,
                teamsTask,
                usersTask);

        var structure = new Structure(await projectTasksTask, await projectsTask, await teamsTask, await usersTask);
        QueryResultsPrinter queryResultsPrinter = new(new StructureLinqQueries(structure));
        return queryResultsPrinter;
    }

    private QueryResultsPrinter(StructureLinqQueries structureLinqQueries)
    {
        _structureLinqQueries = structureLinqQueries;

        EntitiesApiReader entitiesApiReader = new();
        _projectTasksQueries = new ProjectTasksQueries(entitiesApiReader);
    }

    public void PrintQuery1Results(int userId)
    {
        var numberUserTasks = _structureLinqQueries.GetNumberUserTasks(userId);

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine($"Query 1. Number of tasks in each project of the user with id {userId}:");
        System.Console.ResetColor();

        foreach (var pair in numberUserTasks)
        {
            System.Console.WriteLine($"{pair.Key.Id} {pair.Key.Name}: {pair.Value}");
        }
    }

    public void PrintQuery2Results(int userId)
    {
        var tasksAssignedToUser = _structureLinqQueries.GetTasksAssignedToUser(userId);

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine($"Query 2. Tasks, assigned to user with id {userId}:");
        System.Console.ResetColor();

        foreach (var task in tasksAssignedToUser)
        {
            System.Console.WriteLine($"{task.Id} {task.Name}");
        }
    }

    public void PrintQuery3Results(int userId)
    {
        var finishedTasksByUserInCurrentYear = _structureLinqQueries.GetFinishedTasksByUserInCurrentYear(userId);

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine($"Query 3. Finished tasks by user with id {userId} in current year:");
        System.Console.ResetColor();

        foreach (var task in finishedTasksByUserInCurrentYear)
        {
            System.Console.WriteLine($"{task.id} {task.name}");
        }
    }

    public void PrintQuery4Results()
    {
        var teamMembers = _structureLinqQueries.GetTeamMembers();

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine("Query 4. Teams with members:");
        System.Console.ResetColor();

        foreach (var team in teamMembers)
        {
            System.Console.WriteLine($"{team.id} {team.teamName} Members number: {team.teamUsers.Count()}");
        }
    }

    public void PrintQuery5Results()
    {
        var usersAndTasks = _structureLinqQueries.GetUsersAndTasks();

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine("Query 5. Users and tasks:");
        System.Console.ResetColor();

        foreach (var userAndTasks in usersAndTasks)
        {
            System.Console.WriteLine(
                $"User: {userAndTasks.user.Id} {userAndTasks.user.FirstName} {userAndTasks.user.LastName}");
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var task in userAndTasks.userTasks)
            {
                System.Console.WriteLine($"Task Id: {task.Id} Name: {task.Name}");
            }

            System.Console.ResetColor();
        }
    }

    public void PrintQuery6Results(int userId)
    {
        var userStatistics = _structureLinqQueries.GetUserStatistics(userId);

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine("Query 6. User statistics:");
        System.Console.ResetColor();
        if (userStatistics is null)
        {
            System.Console.WriteLine("User is not found");
        }
        else
        {
            System.Console.WriteLine($"User {userStatistics.User.FirstName} {userStatistics.User.LastName}");
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            if (userStatistics.Project is not null)
            {
                System.Console.WriteLine($"Last project: ID {userStatistics.Project.Id} {userStatistics.Project.Name}");
            }

            if (userStatistics.TasksNumber is not null)
            {
                System.Console.WriteLine($"Number of tasks in last project: {userStatistics.TasksNumber}");
            }

            System.Console.WriteLine(
                $"Number of unfinished or canceled user tasks: {userStatistics.NumberOfUnfinishedOrCanceledTasks}");
            if (userStatistics.LongestTask is not null)
            {
                System.Console.WriteLine(
                    $"Longest user task by data: ID: {userStatistics.LongestTask.Id} Name: {userStatistics.LongestTask.Name}");
            }
        }
    }

    public void PrintQuery7Results()
    {
        var projectStatistics = _structureLinqQueries.GetProjectStatistics();

        System.Console.ForegroundColor = ConsoleColor.Cyan;
        System.Console.WriteLine("Query 7. Project statistics:");
        System.Console.ResetColor();

        foreach (var project in projectStatistics)
        {
            System.Console.WriteLine($"Project ID: {project.Project.Id} Name: {project.Project.Name}");
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            if (project.LongestTask is not null)
            {
                System.Console.WriteLine(
                    $"Longest project task by description: ID: {project.LongestTask.Id} Name: {project.LongestTask.Name}");
            }

            if (project.ShortestTask is not null)
            {
                System.Console.WriteLine(
                    $"Shortest project task by name: ID: {project.ShortestTask.Id} Name: {project.ShortestTask.Name}");
            }

            if (project.NumberOfUsersInProjectTeam is not null)
            {
                System.Console.WriteLine(
                    $"General number of users in project team: {project.NumberOfUsersInProjectTeam}");
            }

            System.Console.ResetColor();
        }
    }

    public async void PrintMarkedTaskId()
    {
        try
        {
            var markedTaskId = await _projectTasksQueries.MarkRandomTaskWithDelay(1000);
            System.Console.WriteLine($"Task with id: {markedTaskId} was marked successfully!");
        }
        catch (Exception e)
        {
            System.Console.WriteLine($"When tried to mark task, exception occured: {e.Message}");
        }
    }
}