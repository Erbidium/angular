﻿using LinqTask.Lib.Entities;
using Newtonsoft.Json;

namespace LinqTask.Console;

public class EntitiesApiReader
{
    private const string BaseUri = "https://localhost:7124/api";

    private readonly HttpClient _client = new(new HttpClientHandler
    {
        ClientCertificateOptions = ClientCertificateOption.Manual,
        ServerCertificateCustomValidationCallback = (_, _, _, _) => true
    });

    public async Task<List<Project>> GetProjects()
    {
        string getProjectsResponseBody = await _client.GetStringAsync(Path.Combine(BaseUri, "Projects"));

        return JsonConvert.DeserializeObject<List<Project>>(getProjectsResponseBody) ?? new List<Project>();
    }

    public async Task<List<ProjectTask>> GetTasks()
    {
        string getTasksResponseBody = await _client.GetStringAsync(Path.Combine(BaseUri, "Tasks"));

        return JsonConvert.DeserializeObject<List<ProjectTask>>(getTasksResponseBody) ?? new List<ProjectTask>();
    }

    public async Task<List<Team>> GetTeams()
    {
        string getTeamsResponseBody = await _client.GetStringAsync(Path.Combine(BaseUri, "Teams"));

        return JsonConvert.DeserializeObject<List<Team>>(getTeamsResponseBody) ?? new List<Team>();
    }

    public async Task<List<User>> GetUsers()
    {
        string getUsersResponseBody = await _client.GetStringAsync(Path.Combine(BaseUri, "Users"));

        return JsonConvert.DeserializeObject<List<User>>(getUsersResponseBody) ?? new List<User>();
    }
}