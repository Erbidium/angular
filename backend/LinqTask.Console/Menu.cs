﻿namespace LinqTask.Console;

public class Menu
{
    private readonly List<(string command, string action, Action function)> _menu = new();
    private int _userId = 1;

    public Menu(QueryResultsPrinter queryResultsPrinter)
    {
        _menu.AddRange(new List<(string command, string action, Action function)>
        {
            ("0", "Query 1", () => queryResultsPrinter.PrintQuery1Results(_userId)),
            ("1", "Query 2", () => queryResultsPrinter.PrintQuery2Results(_userId)),
            ("2", "Query 3", () => queryResultsPrinter.PrintQuery3Results(_userId)),
            ("3", "Query 4", queryResultsPrinter.PrintQuery4Results),
            ("4", "Query 5", queryResultsPrinter.PrintQuery5Results),
            ("5", "Query 6", () => queryResultsPrinter.PrintQuery6Results(_userId)),
            ("6", "Query 7", queryResultsPrinter.PrintQuery7Results),
            ("7", "Mark random task", queryResultsPrinter.PrintMarkedTaskId),
            ("8", "Enter new user id", ReadUserId),
            ("9", "Exit", Exit)
        });
    }

    private void ReadUserId()
    {
        System.Console.WriteLine("Please, enter user id:");
        bool userIdWasEnteredCorrectly = false;
        while (!userIdWasEnteredCorrectly)
        {
            var id = System.Console.ReadLine();
            if (!int.TryParse(id, out int parsedUserId))
            {
                System.Console.WriteLine("User id is in wrong format!");
                continue;
            }

            _userId = parsedUserId;
            userIdWasEnteredCorrectly = true;
        }

        System.Console.WriteLine("Id entered successfully!");
    }

    private bool _exit;

    public void ShowMenu()
    {
        ReadUserId();
        while (!_exit)
        {
            foreach (var menuItem in _menu)
            {
                System.Console.WriteLine($"({menuItem.command}) {menuItem.action}");
            }

            var key = System.Console.ReadLine();
            var currentMenuItem = _menu.FirstOrDefault(menuItem => menuItem.command == key);
            if (currentMenuItem.Equals(default))
            {
                System.Console.WriteLine("You have entered a wrong command");
            }
            else
            {
                System.Console.Clear();
                currentMenuItem.function.Invoke();
                System.Console.ReadLine();
                System.Console.Clear();
            }
        }
    }

    private void Exit()
    {
        System.Console.WriteLine("You have successfully exited");
        _exit = true;
    }
}