﻿using System.Net.Http.Json;
using LinqTask.Lib.Entities;
using LinqTask.Lib.Enums;

namespace LinqTask.Console;

public class ProjectTasksQueries
{
    private readonly EntitiesApiReader _entitiesApiReader;

    private const string BaseUri = "https://localhost:7124/api";

    private readonly HttpClient _client = new(new HttpClientHandler
    {
        ClientCertificateOptions = ClientCertificateOption.Manual,
        ServerCertificateCustomValidationCallback = (_, _, _, _) => true
    });

    public ProjectTasksQueries(EntitiesApiReader entitiesApiReader)
    {
        _entitiesApiReader = entitiesApiReader;
    }

    public Task<int> MarkRandomTaskWithDelay(int delay)
    {
        var taskCompletionSource = new TaskCompletionSource<int>();

        Timer timer = new Timer(async _ =>
        {
            try
            {
                var allTasks = await _entitiesApiReader.GetTasks();

                var taskToMark = SelectRandomProjectTask(allTasks);

                var httpResponseMessage = await PutRequestWithTaskMarkingAsDone(taskToMark);

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Request to mark was unsuccessful!");
                }

                taskCompletionSource.TrySetResult(taskToMark.Id);
            }
            catch (Exception e)
            {
                taskCompletionSource.SetException(e);
            }
        }, null, TimeSpan.FromMilliseconds(delay), TimeSpan.FromMilliseconds(Timeout.Infinite));

        taskCompletionSource.Task.ContinueWith(_ => timer.Dispose());

        return taskCompletionSource.Task;
    }

    private ProjectTask SelectRandomProjectTask(List<ProjectTask> projectTasks)
    {
        var random = new Random();
        int randomTaskIndex = random.Next(projectTasks.Count);

        return projectTasks[randomTaskIndex];
    }

    private Task<HttpResponseMessage> PutRequestWithTaskMarkingAsDone(ProjectTask projectTask)
    {
        var updateProjectTaskDto = new UpdateProjectTaskDto
        {
            Id = projectTask.Id,
            Name = projectTask.Name,
            Description = projectTask.Description,
            State = TaskState.Done,
            FinishedAt = projectTask.FinishedAt
        };

        return _client.PutAsJsonAsync(Path.Combine(BaseUri, "Tasks"), updateProjectTaskDto);
    }
}