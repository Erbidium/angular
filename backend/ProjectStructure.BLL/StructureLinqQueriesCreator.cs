﻿using AutoMapper;
using LinqTask.Lib;
using LinqTask.Lib.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectLinq = LinqTask.Lib.Entities.Project;
using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;
using TeamLinq = LinqTask.Lib.Entities.Team;
using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure_BLL;

public class StructureLinqQueriesCreator
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private StructureLinqQueries? _structureLinqQueries;

    public StructureLinqQueriesCreator(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task<StructureLinqQueries> Create()
    {
        return _structureLinqQueries ??= await CreateStructure();
    }

    private async Task<StructureLinqQueries> CreateStructure()
    {
        var projectsFromRepository = await _unitOfWork.ProjectRepository.GetAll();
        var projectTasksFromRepository = await _unitOfWork.ProjectTaskRepository.GetAll();
        var usersFromRepository = await _unitOfWork.UserRepository.GetAll();
        var teamsFromRepository = await _unitOfWork.TeamRepository.GetAll();

        var projectsLinq = _mapper.Map<List<ProjectLinq>>(projectsFromRepository);
        var projectTasksLinq = _mapper.Map<List<ProjectTaskLinq>>(projectTasksFromRepository);
        var usersLinq = _mapper.Map<List<UserLinq>>(usersFromRepository);
        var teamsLinq = _mapper.Map<List<TeamLinq>>(teamsFromRepository);
        return new StructureLinqQueries(new Structure(projectTasksLinq, projectsLinq, teamsLinq, usersLinq));
    }
}