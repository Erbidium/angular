﻿using AutoMapper;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.DAL.Entities;
using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;

namespace ProjectStructure_BLL.MappingProfiles;

public class ProjectTaskProfile : Profile
{
    public ProjectTaskProfile()
    {
        CreateMap<ProjectTask, ProjectTaskDto>();
        CreateMap<NewProjectTaskDto, ProjectTask>();
        CreateMap<ProjectTask, ProjectTaskLinq>();
    }
}