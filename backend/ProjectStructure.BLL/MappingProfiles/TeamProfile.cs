﻿using AutoMapper;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Entities;
using TeamLinq = LinqTask.Lib.Entities.Team;

namespace ProjectStructure_BLL.MappingProfiles;

public class TeamProfile : Profile
{
    public TeamProfile()
    {
        CreateMap<Team, TeamDto>();
        CreateMap<NewTeamDto, Team>();
        CreateMap<Team, TeamLinq>();
    }
}