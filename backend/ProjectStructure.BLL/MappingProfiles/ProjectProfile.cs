﻿using AutoMapper;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Entities;
using ProjectLinq = LinqTask.Lib.Entities.Project;

namespace ProjectStructure_BLL.MappingProfiles;

public class ProjectProfile : Profile
{
    public ProjectProfile()
    {
        CreateMap<Project, ProjectDto>();
        CreateMap<NewProjectDto, Project>();
        CreateMap<Project, ProjectLinq>();
    }
}