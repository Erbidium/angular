﻿using AutoMapper;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities;
using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure_BLL.MappingProfiles;

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<User, UserDto>();
        CreateMap<NewUserDto, User>();
        CreateMap<User, UserLinq>();
    }
}