﻿using AutoMapper;
using LinqTask.Lib.DTO;
using ProjectStructure_BLL.Services.Abstract;
using ProjectStructure.Common.DTO.LinqTask;
using ProjectStructure.DAL.Interfaces;
using ProjectLinq = LinqTask.Lib.Entities.Project;
using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;
using TeamLinq = LinqTask.Lib.Entities.Team;
using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure_BLL.Services;

public class LinqTaskService : BaseService
{
    private readonly StructureLinqQueriesCreator _structureLinqQueriesCreator;

    public LinqTaskService(IUnitOfWork unitOfWork, IMapper mapper,
        StructureLinqQueriesCreator structureLinqQueriesCreator) : base(unitOfWork, mapper)
    {
        _structureLinqQueriesCreator = structureLinqQueriesCreator;
    }


    public async Task<NumberUserTasksDto> GetNumberUserTasks(int userId)
    {
        return new NumberUserTasksDto
            { ProjectsTasksNumber = (await _structureLinqQueriesCreator.Create()).GetNumberUserTasks(userId).ToList() };
    }

    public async Task<TasksAssignedToUserDto> GetTasksAssignedToUser(int userId)
    {
        return new TasksAssignedToUserDto
            { Tasks = (await _structureLinqQueriesCreator.Create()).GetTasksAssignedToUser(userId) };
    }

    public async Task<IEnumerable<FinishedTaskByUserInCurrentYearDto>> GetFinishedTasksByUserInCurrentYear(int userId)
    {
        return (await _structureLinqQueriesCreator.Create())
            .GetFinishedTasksByUserInCurrentYear(userId)
            .Select(tuple => new FinishedTaskByUserInCurrentYearDto { Id = tuple.id, Name = tuple.name });
    }

    public async Task<IEnumerable<TeamMembersDto>> GetTeamMembers()
    {
        return (await _structureLinqQueriesCreator.Create())
            .GetTeamMembers()
            .Select(tuple => new TeamMembersDto
                { Id = tuple.id, TeamName = tuple.teamName, TeamUsers = tuple.teamUsers });
    }

    public async Task<IEnumerable<UserAndTasksDto>> GetUsersAndTasks()
    {
        return (await _structureLinqQueriesCreator.Create())
            .GetUsersAndTasks()
            .Select(tuple => new UserAndTasksDto { User = tuple.user, UserTasks = tuple.userTasks });
    }

    public async Task<UserStatisticsDto?> GetUserStatistics(int userId)
    {
        return (await _structureLinqQueriesCreator.Create())
            .GetUserStatistics(userId);
    }

    public async Task<IEnumerable<ProjectStatisticsDto>> GetProjectStatistics()
    {
        return (await _structureLinqQueriesCreator.Create())
            .GetProjectStatistics();
    }
}