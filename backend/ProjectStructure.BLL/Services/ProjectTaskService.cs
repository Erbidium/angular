﻿using AutoMapper;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.Services.Abstract;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.Enums;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure_BLL.Services;

public class ProjectTaskService : BaseService
{
    public ProjectTaskService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<ProjectTaskDto>> GetAll()
    {
        var projectTasks = await UnitOfWork.ProjectTaskRepository.GetAll();
        return Mapper.Map<ICollection<ProjectTaskDto>>(projectTasks);
    }

    public async Task<ProjectTaskDto> GetById(int id)
    {
        var projectTaskEntity = await UnitOfWork.ProjectTaskRepository.GetById(id) ??
                                throw new NotFoundException(nameof(ProjectTask), id);

        return Mapper.Map<ProjectTaskDto>(projectTaskEntity);
    }

    public async Task<ProjectTaskDto> Create(NewProjectTaskDto newProjectTask)
    {
        var projectTaskEntity = Mapper.Map<ProjectTask>(newProjectTask);
        projectTaskEntity.CreatedAt = DateTime.Now;

        await UnitOfWork.ProjectTaskRepository.Add(projectTaskEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectTaskDto>(projectTaskEntity);
    }

    public async Task<ProjectTaskDto> Update(UpdateProjectTaskDto updatedProjectTask)
    {
        var projectTaskEntity = await UnitOfWork.ProjectTaskRepository.GetById(updatedProjectTask.Id) ??
                                throw new NotFoundException(nameof(ProjectTask), updatedProjectTask.Id);

        projectTaskEntity.Name = updatedProjectTask.Name;
        projectTaskEntity.Description = updatedProjectTask.Description;
        projectTaskEntity.State = updatedProjectTask.State;
        projectTaskEntity.FinishedAt = updatedProjectTask.FinishedAt;

        await UnitOfWork.ProjectTaskRepository.Update(projectTaskEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectTaskDto>(projectTaskEntity);
    }

    public async Task Delete(int id)
    {
        var projectTaskEntity = await UnitOfWork.ProjectTaskRepository.GetById(id) ??
                                throw new NotFoundException(nameof(ProjectTask), id);

        await UnitOfWork.ProjectTaskRepository.Delete(projectTaskEntity);
        await UnitOfWork.SaveChangesAsync();
    }

    public async Task<IEnumerable<ProjectTaskDto>> GetUnfinishedTasksByUser(int id)
    {
        var userEntity = await UnitOfWork.UserRepository.GetById(id) ?? throw new NotFoundException(nameof(User), id);

        var projectTasks = Mapper.Map<IEnumerable<ProjectTaskDto>>(await UnitOfWork.ProjectTaskRepository.GetAll());
        return projectTasks.Where(t =>
            t.PerformerId == userEntity.Id && (t.State == TaskState.InProgress || t.State == TaskState.ToDo));
    }
}