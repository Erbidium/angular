﻿using AutoMapper;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure_BLL.Services;

public class TeamService : BaseService
{
    public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<TeamDto>> GetAll()
    {
        var teams = await UnitOfWork.TeamRepository.GetAll();
        return Mapper.Map<ICollection<TeamDto>>(teams);
    }

    public async Task<TeamDto> GetById(int id)
    {
        var teamEntity = await UnitOfWork.TeamRepository.GetById(id) ?? throw new NotFoundException(nameof(Team), id);

        return Mapper.Map<TeamDto>(teamEntity);
    }

    public async Task<TeamDto> Create(NewTeamDto newTeam)
    {
        var teamEntity = Mapper.Map<Team>(newTeam);
        teamEntity.CreatedAt = DateTime.Now;

        await UnitOfWork.TeamRepository.Add(teamEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<TeamDto>(teamEntity);
    }

    public async Task<TeamDto> Update(UpdateTeamDto updatedTeam)
    {
        var teamEntity = await UnitOfWork.TeamRepository.GetById(updatedTeam.Id) ??
                         throw new NotFoundException(nameof(Team), updatedTeam.Id);

        teamEntity.Name = updatedTeam.Name;

        await UnitOfWork.TeamRepository.Update(teamEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<TeamDto>(teamEntity);
    }

    public async Task Delete(int id)
    {
        var teamEntity = await UnitOfWork.TeamRepository.GetById(id) ?? throw new NotFoundException(nameof(Team), id);

        await UnitOfWork.TeamRepository.Delete(teamEntity);
        await UnitOfWork.SaveChangesAsync();
    }
}