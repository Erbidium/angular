﻿using AutoMapper;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure_BLL.Services;

public class ProjectService : BaseService
{
    public ProjectService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<ProjectDto>> GetAll()
    {
        var projects = await UnitOfWork.ProjectRepository.GetAll();
        return Mapper.Map<ICollection<ProjectDto>>(projects);
    }

    public async Task<ProjectDto> GetById(int id)
    {
        var projectEntity = await UnitOfWork.ProjectRepository.GetById(id) ??
                            throw new NotFoundException(nameof(Project), id);

        return Mapper.Map<ProjectDto>(projectEntity);
    }

    public async Task<ProjectDto> Create(NewProjectDto newProject)
    {
        var projectEntity = Mapper.Map<Project>(newProject);
        projectEntity.CreatedAt = DateTime.Now;

        await UnitOfWork.ProjectRepository.Add(projectEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectDto>(projectEntity);
    }

    public async Task<ProjectDto> Update(UpdateProjectDto updatedProject)
    {
        var projectEntity = await UnitOfWork.ProjectRepository.GetById(updatedProject.Id) ??
                            throw new NotFoundException(nameof(Project), updatedProject.Id);

        projectEntity.Name = updatedProject.Name;
        projectEntity.Description = updatedProject.Description;

        await UnitOfWork.ProjectRepository.Update(projectEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectDto>(projectEntity);
    }

    public async Task Delete(int id)
    {
        var projectEntity = await UnitOfWork.ProjectRepository.GetById(id) ??
                            throw new NotFoundException(nameof(Project), id);

        await UnitOfWork.ProjectRepository.Delete(projectEntity);
        await UnitOfWork.SaveChangesAsync();
    }
}