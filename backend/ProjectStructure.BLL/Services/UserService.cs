﻿using AutoMapper;
using ProjectStructure_BLL.Exceptions;
using ProjectStructure_BLL.Services.Abstract;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure_BLL.Services;

public class UserService : BaseService
{
    public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<UserDto>> GetAll()
    {
        var users = await UnitOfWork.UserRepository.GetAll();
        return Mapper.Map<ICollection<UserDto>>(users);
    }

    public async Task<UserDto> GetById(int id)
    {
        var userEntity = await UnitOfWork.UserRepository.GetById(id) ?? throw new NotFoundException(nameof(User), id);

        return Mapper.Map<UserDto>(userEntity);
    }

    public async Task<UserDto> Create(NewUserDto newUser)
    {
        var userEntity = Mapper.Map<User>(newUser);
        userEntity.RegisteredAt = DateTime.Now;

        await UnitOfWork.UserRepository.Add(userEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<UserDto>(userEntity);
    }

    public async Task<UserDto> Update(UpdateUserDto updatedUser)
    {
        var userEntity = await UnitOfWork.UserRepository.GetById(updatedUser.Id) ??
                         throw new NotFoundException(nameof(User), updatedUser.Id);

        if (updatedUser.TeamId is not null)
        {
            var team = await UnitOfWork.TeamRepository.GetById(updatedUser.TeamId.Value);
            if (team is null)
            {
                throw new NotFoundException("Team is not found");
            }

            userEntity.TeamId = updatedUser.TeamId;
        }

        userEntity.FirstName = updatedUser.FirstName;
        userEntity.LastName = updatedUser.LastName;
        userEntity.Email = updatedUser.Email;

        if (updatedUser.BirthDay is not null)
        {
            userEntity.BirthDay = updatedUser.BirthDay.Value;
        }

        await UnitOfWork.UserRepository.Update(userEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<UserDto>(userEntity);
    }

    public async Task Delete(int id)
    {
        var userEntity = await UnitOfWork.UserRepository.GetById(id) ?? throw new NotFoundException(nameof(User), id);

        await UnitOfWork.UserRepository.Delete(userEntity);
        await UnitOfWork.SaveChangesAsync();
    }
}