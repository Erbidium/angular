﻿using AutoMapper;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure_BLL.Services.Abstract;

public abstract class BaseService
{
    private protected readonly IUnitOfWork UnitOfWork;
    private protected readonly IMapper Mapper;

    public BaseService(IUnitOfWork unitOfWork, IMapper mapper)
    {
        UnitOfWork = unitOfWork;
        Mapper = mapper;
    }
}