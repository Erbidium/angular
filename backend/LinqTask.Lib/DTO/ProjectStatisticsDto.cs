﻿using LinqTask.Lib.Entities;

namespace LinqTask.Lib.DTO;

public class ProjectStatisticsDto
{
    public Project Project { get; set; }
    public ProjectTask? LongestTask { get; set; }
    public ProjectTask? ShortestTask { get; set; }
    public int? NumberOfUsersInProjectTeam { get; set; }
}