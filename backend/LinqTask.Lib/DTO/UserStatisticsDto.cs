﻿using LinqTask.Lib.Entities;

namespace LinqTask.Lib.DTO;

public class UserStatisticsDto
{
    public User User { get; set; }
    public Project? Project { get; set; }
    public int? TasksNumber { get; set; }
    public int NumberOfUnfinishedOrCanceledTasks { get; set; }
    public ProjectTask? LongestTask { get; set; }
}