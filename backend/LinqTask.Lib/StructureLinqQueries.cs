using LinqTask.Lib.DTO;
using LinqTask.Lib.Entities;
using LinqTask.Lib.Enums;

namespace LinqTask.Lib;

public class StructureLinqQueries
{
    private const int CurrentYear = 2022;

    private readonly Structure _structure;

    public StructureLinqQueries(Structure structure)
    {
        _structure = structure;
    }

    public Dictionary<Project, int> GetNumberUserTasks(int userId)
    {
        // Query 1
        return _structure.Projects
            .Where(p => p.Author?.Id == userId)
            .ToDictionary(p => p, p => p.Tasks?.Count() ?? 0);
    }

    public IEnumerable<ProjectTask> GetTasksAssignedToUser(int userId)
    {
        // Query 2
        return _structure.Projects
            .SelectMany(project => project.Tasks ?? Array.Empty<ProjectTask>())
            .Where(task => task.Performer?.Id == userId && (task.Name?.Length ?? 0) < 45);
    }

    public IEnumerable<(int id, string? name)> GetFinishedTasksByUserInCurrentYear(int userId)
    {
        // Query 3
        return _structure.Projects
            .SelectMany(project => project.Tasks ?? Array.Empty<ProjectTask>())
            .Where(task => task.Performer?.Id == userId && task.State == TaskState.Done &&
                           task.FinishedAt?.Year == CurrentYear)
            .Select(task => (id: task.Id, name: task.Name));
    }

    public IEnumerable<(int id, string? teamName, IOrderedEnumerable<User> teamUsers)> GetTeamMembers()
    {
        // Query 4
        return _structure.Projects
            .Where(project => project.Team is not null)
            .Select(project => (id: project.Team.Id, teamName: project.Team.Name,
                teamUsers: (project.Team.Members ?? Array.Empty<User>()).OrderByDescending(u => u.RegisteredAt)))
            .DistinctBy(tuple => tuple.id)
            .Where(tuple => tuple.teamUsers.All(u => CurrentYear - u.BirthDay.Year > 10));
    }

    public IOrderedEnumerable<(User user, IOrderedEnumerable<ProjectTask> userTasks)> GetUsersAndTasks()
    {
        // Query 5
        return _structure.Projects
            .SelectMany(project => (project.Tasks ?? Array.Empty<ProjectTask>()).Select(task => task.Performer))
            .OfType<User>()
            .DistinctBy(u => u.Id)
            .GroupJoin(_structure.Projects.SelectMany(project => project.Tasks ?? Array.Empty<ProjectTask>()),
                user => user.Id,
                task => task.PerformerId,
                (user, tasks) => (user, tasks.OrderByDescending(task => task.Name?.Length ?? 0)))
            .OrderBy(tuple => tuple.user.FirstName);
    }

    public UserStatisticsDto? GetUserStatistics(int userId)
    {
        // Query 6
        return _structure.Users
            .GroupJoin(_structure.Projects
                    .SelectMany(project => project.Tasks ?? Array.Empty<ProjectTask>()),
                user => user.Id,
                task => task.PerformerId,
                (user, tasks) => (user, tasks))
            .Select(tuple => (tuple.user,
                project: _structure.Projects
                    .Where(project => project.AuthorId == tuple.user.Id)
                    .MaxBy(p => p.CreatedAt),
                numberOfUnfinishedOrCanceledTasks: tuple.tasks
                    .Count(task => task.State is TaskState.Canceled or TaskState.InProgress),
                longestTask: tuple.tasks
                    .Where(task => task.FinishedAt is not null)
                    .MaxBy(task => (task.FinishedAt - task.CreatedAt).Value.TotalDays)))
            .Select(tuple => new UserStatisticsDto
            {
                User = tuple.user,
                Project = tuple.project,
                TasksNumber = tuple.project?.Tasks?.Count() ?? null,
                NumberOfUnfinishedOrCanceledTasks = tuple.numberOfUnfinishedOrCanceledTasks,
                LongestTask = tuple.longestTask
            })
            .FirstOrDefault(dto => dto.User.Id == userId);
    }

    public IEnumerable<ProjectStatisticsDto> GetProjectStatistics()
    {
        // Query 7
        return _structure.Projects
            .Select(project => new ProjectStatisticsDto
            {
                Project = project,
                LongestTask = project.Tasks
                    ?.MaxBy(task => task.Description?.Length ?? 0),
                ShortestTask = project.Tasks
                    ?.Where(task => task.Name is not null)
                    .MinBy(task => task.Name?.Length ?? 0),
                NumberOfUsersInProjectTeam = project.Description?.Length > 20 || project.Tasks?.Count() < 3
                    ? project.Team?.Members?.Count()
                    : null
            });
    }
}