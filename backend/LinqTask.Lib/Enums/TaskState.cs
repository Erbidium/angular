﻿namespace LinqTask.Lib.Enums;

public enum TaskState
{
    ToDo,
    InProgress,
    Done,
    Canceled
}