﻿namespace LinqTask.Lib.Entities;

public class Structure
{
    public Structure(List<ProjectTask> tasks, List<Project> projects, List<Team> teams, List<User> users)
    {
        Users = users;
        Projects = projects.Join(users,
                project => project.AuthorId,
                user => user.Id,
                (project, user) =>
                {
                    project.Author = user;
                    return project;
                })
            .Join(teams,
                project => project.TeamId,
                team => team.Id,
                (project, team) =>
                {
                    team.Members = users
                        .Where(u => u.TeamId == team.Id);
                    project.Team = team;
                    return project;
                })
            .GroupJoin(tasks,
                project => project.Id,
                task => task.ProjectId,
                (project, projectTasks) =>
                {
                    project.Tasks = projectTasks
                        .Join(users,
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) =>
                            {
                                task.Performer = user;
                                return task;
                            });
                    return project;
                });
    }

    public IEnumerable<Project> Projects { get; set; }
    public IEnumerable<User> Users { get; set; }
}