﻿namespace LinqTask.Lib.Entities;

public class Project
{
    public int Id { get; set; }
    public int AuthorId { get; set; }
    public int TeamId { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public DateTime Deadline { get; set; }
    public DateTime CreatedAt { get; set; }

    public IEnumerable<ProjectTask>? Tasks { get; set; }
    public User? Author { get; set; }
    public Team? Team { get; set; }
}