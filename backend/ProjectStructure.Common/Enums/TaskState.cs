﻿namespace ProjectStructure.Common.Enums;

public enum TaskState
{
    ToDo,
    InProgress,
    Done,
    Canceled
}