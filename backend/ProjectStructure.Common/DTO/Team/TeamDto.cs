﻿namespace ProjectStructure.Common.DTO.Team;

public class TeamDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public DateTime CreatedAt { get; set; }
}