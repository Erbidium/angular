﻿namespace ProjectStructure.Common.DTO.Team;

public class NewTeamDto
{
    public string? Name { get; set; }
}