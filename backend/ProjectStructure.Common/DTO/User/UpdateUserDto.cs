﻿namespace ProjectStructure.Common.DTO.User;

public class UpdateUserDto
{
    public int Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public DateTime? BirthDay { get; set; }
    public int? TeamId { get; set; }
}