﻿using ProjectStructure.Common.Enums;

namespace ProjectStructure.Common.DTO.ProjectTask;

public class UpdateProjectTaskDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public TaskState State { get; set; }
    public DateTime? FinishedAt { get; set; }
}