﻿using ProjectStructure.Common.Enums;

namespace ProjectStructure.Common.DTO.ProjectTask;

public class NewProjectTaskDto
{
    public int? ProjectId { get; set; }
    public int? PerformerId { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public TaskState State { get; set; }
}