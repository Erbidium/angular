﻿using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure.Common.DTO.LinqTask;

public class TeamMembersDto
{
    public int Id { get; set; }
    public string? TeamName { get; set; }
    public IOrderedEnumerable<UserLinq> TeamUsers { get; set; }
}