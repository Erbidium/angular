﻿using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;
using UserLinq = LinqTask.Lib.Entities.User;

namespace ProjectStructure.Common.DTO.LinqTask;

public class UserAndTasksDto
{
    public UserLinq User { get; set; }
    public IOrderedEnumerable<ProjectTaskLinq> UserTasks { get; set; }
}