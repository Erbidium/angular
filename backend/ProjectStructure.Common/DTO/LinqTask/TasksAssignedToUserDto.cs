﻿using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;

namespace ProjectStructure.Common.DTO.LinqTask;

public class TasksAssignedToUserDto
{
    public IEnumerable<ProjectTaskLinq>? Tasks { get; set; }
}