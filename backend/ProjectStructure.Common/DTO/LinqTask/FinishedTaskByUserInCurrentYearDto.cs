﻿namespace ProjectStructure.Common.DTO.LinqTask;

public class FinishedTaskByUserInCurrentYearDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
}