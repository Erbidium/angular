﻿using ProjectLinq = LinqTask.Lib.Entities.Project;

namespace ProjectStructure.Common.DTO.LinqTask;

public class NumberUserTasksDto
{
    public List<KeyValuePair<ProjectLinq, int>>? ProjectsTasksNumber { get; set; }
}