﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectStructure.DAL.Migrations
{
    public partial class Addvaliation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Task",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 29, new DateTime(2019, 6, 22, 10, 51, 20, 127, DateTimeKind.Unspecified).AddTicks(6528), new DateTime(2021, 12, 7, 10, 23, 53, 39, DateTimeKind.Unspecified).AddTicks(5962), "Ipsam ut recusandae aspernatur aut molestiae et aliquid.", "Handcrafted Steel Ball", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 8, 4, 23, 56, 37, 923, DateTimeKind.Unspecified).AddTicks(3377), new DateTime(2022, 4, 30, 11, 2, 4, 391, DateTimeKind.Unspecified).AddTicks(7048), "Enim labore rerum corporis qui provident qui libero.", "Sleek Metal Table", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 5, 22, 23, 35, 12, 12, DateTimeKind.Unspecified).AddTicks(6204), new DateTime(2022, 8, 28, 10, 52, 2, 901, DateTimeKind.Unspecified).AddTicks(8003), "Id itaque adipisci et impedit.", "Small Concrete Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 28, new DateTime(2019, 10, 9, 21, 30, 39, 404, DateTimeKind.Unspecified).AddTicks(2694), new DateTime(2021, 3, 27, 6, 9, 20, 637, DateTimeKind.Unspecified).AddTicks(55), "Inventore rem saepe est provident temporibus veritatis reiciendis est aut.", "Practical Rubber Chair", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 9, 5, 23, 18, 7, 86, DateTimeKind.Unspecified).AddTicks(2477), new DateTime(2021, 2, 11, 2, 54, 17, 464, DateTimeKind.Unspecified).AddTicks(4205), "Quo labore sit recusandae praesentium et quisquam quo vel.", "Intelligent Plastic Shirt", 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 31, 0, 1, 12, 276, DateTimeKind.Unspecified).AddTicks(3994), "Fugit officiis inventore optio sit odio.", new DateTime(2022, 4, 21, 10, 12, 29, 78, DateTimeKind.Unspecified).AddTicks(9841), "Alias et.", 13, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 16, 2, 56, 47, 830, DateTimeKind.Unspecified).AddTicks(1683), "Temporibus vel cupiditate.", new DateTime(2021, 7, 11, 14, 59, 42, 254, DateTimeKind.Unspecified).AddTicks(6125), "Quis fuga.", 13, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 7, 8, 39, 44, 906, DateTimeKind.Unspecified).AddTicks(9335), "Quia dolorem quia enim minus placeat ad perferendis nulla at.", new DateTime(2021, 11, 20, 5, 9, 24, 979, DateTimeKind.Unspecified).AddTicks(9901), "Aut velit.", 12, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 1, 1, 8, 4, 20, 681, DateTimeKind.Unspecified).AddTicks(1189), "Ut pariatur explicabo enim eveniet praesentium quas et.", new DateTime(2021, 6, 22, 0, 4, 49, 544, DateTimeKind.Unspecified).AddTicks(1857), "Qui molestiae.", 40 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 12, 12, 38, 23, 730, DateTimeKind.Unspecified).AddTicks(2462), "Eveniet perferendis unde id eum.", new DateTime(2021, 12, 18, 22, 0, 3, 563, DateTimeKind.Unspecified).AddTicks(5209), "Voluptatibus officiis.", 10, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 12, 19, 55, 624, DateTimeKind.Unspecified).AddTicks(840), "Dolorem est beatae vel iste voluptatem beatae quos placeat.", new DateTime(2022, 12, 24, 7, 5, 10, 253, DateTimeKind.Unspecified).AddTicks(207), "Mollitia laudantium.", 42, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 7, 1, 13, 28, 225, DateTimeKind.Unspecified).AddTicks(5728), "Velit assumenda nobis expedita porro omnis.", new DateTime(2021, 1, 4, 11, 46, 53, 360, DateTimeKind.Unspecified).AddTicks(7640), "Aut sunt.", 17, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 21, 3, 22, 21, 90, DateTimeKind.Unspecified).AddTicks(6162), "Odit enim nisi ea nobis perspiciatis aut.", new DateTime(2022, 10, 7, 1, 30, 25, 332, DateTimeKind.Unspecified).AddTicks(4856), "Aspernatur excepturi.", 28, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 9, 23, 22, 54, 71, DateTimeKind.Unspecified).AddTicks(6335), "Et sed voluptas ea possimus sit sint.", new DateTime(2021, 3, 13, 18, 50, 34, 725, DateTimeKind.Unspecified).AddTicks(6318), "Nemo voluptatem.", 42, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 7, 4, 0, 17, 828, DateTimeKind.Unspecified).AddTicks(8332), "Dolor excepturi nemo saepe ut at vel quia.", new DateTime(2021, 3, 5, 13, 34, 22, 80, DateTimeKind.Unspecified).AddTicks(6411), "Quo vitae.", 30, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 14, 23, 22, 55, 494, DateTimeKind.Unspecified).AddTicks(939), "Aut est est et.", new DateTime(2021, 5, 19, 8, 53, 12, 563, DateTimeKind.Unspecified).AddTicks(9831), "Eum eum.", 11, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 13, 28, 50, 813, DateTimeKind.Unspecified).AddTicks(4162), "Hic tenetur et eius non sint omnis culpa non.", new DateTime(2022, 2, 10, 7, 37, 35, 557, DateTimeKind.Unspecified).AddTicks(1825), "Sit sed.", 44, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 7, 11, 23, 6, 21, 741, DateTimeKind.Unspecified).AddTicks(5575), "Sit laborum esse maxime voluptatem necessitatibus voluptatibus autem.", new DateTime(2021, 1, 17, 19, 6, 18, 85, DateTimeKind.Unspecified).AddTicks(3369), "Pariatur enim.", 11 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 26, 14, 18, 50, 13, DateTimeKind.Unspecified).AddTicks(4433), "Quo non tempora ipsam ad nesciunt quis eum.", new DateTime(2022, 2, 14, 10, 47, 33, 31, DateTimeKind.Unspecified).AddTicks(8146), "Ut consequuntur.", 47, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 3, 16, 53, 41, 604, DateTimeKind.Unspecified).AddTicks(5152), "Sed repellat a veniam.", new DateTime(2022, 4, 16, 20, 11, 44, 332, DateTimeKind.Unspecified).AddTicks(667), "Sapiente iure.", 9, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 6, 37, 15, 173, DateTimeKind.Unspecified).AddTicks(1157), "Consequatur ab saepe molestias quidem odio enim.", new DateTime(2021, 6, 8, 8, 25, 41, 613, DateTimeKind.Unspecified).AddTicks(1130), "Explicabo quo.", 11, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 16, 7, 59, 57, 971, DateTimeKind.Unspecified).AddTicks(4854), "Eum quia laborum labore sed dolorum molestiae accusantium.", new DateTime(2022, 11, 16, 9, 51, 20, 503, DateTimeKind.Unspecified).AddTicks(5263), "Et eligendi.", 33, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 6, 6, 13, 12, 7, 309, DateTimeKind.Unspecified).AddTicks(9045), "Nam laudantium earum nulla dolor ducimus assumenda.", new DateTime(2021, 10, 17, 7, 6, 49, 868, DateTimeKind.Unspecified).AddTicks(2476), "Dicta nostrum.", 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 9, 18, 21, 59, 12, 682, DateTimeKind.Unspecified).AddTicks(3809), "Dignissimos quia culpa.", new DateTime(2021, 1, 3, 2, 45, 35, 592, DateTimeKind.Unspecified).AddTicks(6744), "Voluptatem modi.", 35 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 23, 17, 21, 42, 930, DateTimeKind.Unspecified).AddTicks(5473), "Ut voluptatem omnis qui ipsum.", new DateTime(2022, 10, 22, 3, 26, 25, 43, DateTimeKind.Unspecified).AddTicks(3922), "Cupiditate sapiente.", 32, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 7, 33, 51, 954, DateTimeKind.Unspecified).AddTicks(5866), "Architecto exercitationem repellat laudantium molestias tempore.", new DateTime(2022, 3, 20, 11, 55, 57, 325, DateTimeKind.Unspecified).AddTicks(2509), "Neque sit.", 49, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 18, 12, 18, 32, 826, DateTimeKind.Unspecified).AddTicks(3868), "Qui omnis enim amet eos doloremque perferendis aut.", new DateTime(2022, 7, 8, 7, 26, 56, 601, DateTimeKind.Unspecified).AddTicks(3251), "Quasi et.", 7, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 25, 9, 52, 25, 725, DateTimeKind.Unspecified).AddTicks(9620), "Ducimus aut nobis.", new DateTime(2021, 1, 7, 0, 51, 5, 643, DateTimeKind.Unspecified).AddTicks(4629), "Sed amet.", 19, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 22, 1, 37, 24, 971, DateTimeKind.Unspecified).AddTicks(5487), "Recusandae dolor beatae voluptas aliquid magni qui officiis.", new DateTime(2021, 8, 2, 7, 11, 2, 966, DateTimeKind.Unspecified).AddTicks(5369), "Fugiat praesentium.", 48, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 15, 11, 25, 35, 747, DateTimeKind.Unspecified).AddTicks(2273), "Ut nihil laborum magni et aspernatur consequatur et.", new DateTime(2021, 1, 8, 1, 59, 34, 317, DateTimeKind.Unspecified).AddTicks(4208), "Impedit delectus.", 27, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 4, 20, 31, 45, 400, DateTimeKind.Unspecified).AddTicks(4074), "Accusantium qui ut pariatur voluptatem repellat omnis.", new DateTime(2021, 7, 15, 23, 35, 15, 959, DateTimeKind.Unspecified).AddTicks(6486), "Iste ipsa.", 9, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 3, 13, 13, 31, 36, 32, DateTimeKind.Unspecified).AddTicks(6111), "Magni et nobis molestiae suscipit laboriosam tempore eveniet quod.", new DateTime(2021, 2, 15, 8, 16, 53, 725, DateTimeKind.Unspecified).AddTicks(6102), "Aut et.", 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 18, 43, 41, 791, DateTimeKind.Unspecified).AddTicks(1879), "Sint enim assumenda sunt libero.", new DateTime(2021, 5, 16, 5, 41, 35, 860, DateTimeKind.Unspecified).AddTicks(3826), "Blanditiis vero.", 36, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 21, 23, 6, 734, DateTimeKind.Unspecified).AddTicks(3328), "Hic nulla ut facilis libero accusamus quia est.", new DateTime(2022, 2, 18, 14, 3, 23, 144, DateTimeKind.Unspecified).AddTicks(8619), "Accusamus perspiciatis.", 38, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 17, 18, 5, 58, 562, DateTimeKind.Unspecified).AddTicks(4658), "Cum qui nihil laboriosam facere et dolorem porro.", new DateTime(2021, 9, 14, 15, 27, 20, 425, DateTimeKind.Unspecified).AddTicks(7705), "Veniam quo.", 2, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 22, 7, 32, 27, 908, DateTimeKind.Unspecified).AddTicks(3798), "Eaque enim officiis ad nihil cum ut.", new DateTime(2022, 12, 2, 13, 39, 7, 886, DateTimeKind.Unspecified).AddTicks(1963), "Et temporibus.", 27, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 21, 17, 26, 5, 661, DateTimeKind.Unspecified).AddTicks(1234), "Modi ut rerum velit.", new DateTime(2021, 6, 8, 18, 14, 40, 777, DateTimeKind.Unspecified).AddTicks(894), "Rerum distinctio.", 38, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 25, 18, 19, 59, 203, DateTimeKind.Unspecified).AddTicks(9169), "Sed dolorem corrupti.", new DateTime(2021, 5, 18, 22, 37, 48, 931, DateTimeKind.Unspecified).AddTicks(8707), "Enim blanditiis.", 2, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 20, 34, 48, 359, DateTimeKind.Unspecified).AddTicks(52), "Voluptas vel possimus facilis.", new DateTime(2022, 4, 7, 23, 41, 25, 238, DateTimeKind.Unspecified).AddTicks(1337), "Quia debitis.", 33, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 14, 7, 45, 7, 166, DateTimeKind.Unspecified).AddTicks(6988), "Illo et et atque ea velit vel.", new DateTime(2022, 3, 9, 0, 56, 49, 796, DateTimeKind.Unspecified).AddTicks(2976), "Vel omnis.", 27, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 26, 12, 35, 56, 920, DateTimeKind.Unspecified).AddTicks(7118), "Quae qui molestias sit veniam ad nemo.", new DateTime(2022, 4, 11, 4, 1, 18, 806, DateTimeKind.Unspecified).AddTicks(8363), "Qui ut.", 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 29, 16, 26, 14, 494, DateTimeKind.Unspecified).AddTicks(5818), "Vitae similique totam impedit.", new DateTime(2022, 9, 25, 9, 59, 36, 530, DateTimeKind.Unspecified).AddTicks(5063), "Tenetur eligendi.", 47, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 24, 15, 44, 44, 401, DateTimeKind.Unspecified).AddTicks(1775), "Vel voluptas vitae maxime veniam perspiciatis aut a voluptates esse.", new DateTime(2021, 8, 23, 18, 49, 8, 476, DateTimeKind.Unspecified).AddTicks(4254), "Nemo molestias.", 45, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 20, 2, 57, 672, DateTimeKind.Unspecified).AddTicks(7497), "Distinctio nobis numquam nostrum.", new DateTime(2022, 4, 16, 5, 44, 48, 382, DateTimeKind.Unspecified).AddTicks(2360), "Omnis veniam.", 9, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 4, 15, 9, 46, 55, 316, DateTimeKind.Unspecified).AddTicks(495), "Qui cumque inventore est veritatis praesentium unde sit.", new DateTime(2022, 9, 26, 5, 53, 18, 940, DateTimeKind.Unspecified).AddTicks(4322), "Incidunt id.", 19 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 4, 4, 15, 3, 4, 390, DateTimeKind.Unspecified).AddTicks(6213), "Qui assumenda blanditiis omnis nesciunt eum deleniti molestiae eos ut.", new DateTime(2022, 9, 10, 10, 4, 10, 924, DateTimeKind.Unspecified).AddTicks(1921), "Illum rerum.", 18 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 11, 18, 34, 28, 335, DateTimeKind.Unspecified).AddTicks(2207), "Eveniet distinctio dolorem dicta odit magnam accusamus nulla.", new DateTime(2022, 3, 3, 14, 6, 10, 498, DateTimeKind.Unspecified).AddTicks(8939), "Dolor omnis.", 32, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 13, 2, 4, 12, 495, DateTimeKind.Unspecified).AddTicks(5188), "Officia necessitatibus repellat delectus et.", new DateTime(2022, 2, 18, 7, 54, 33, 186, DateTimeKind.Unspecified).AddTicks(4010), "Perspiciatis ratione.", 37, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 6, 13, 18, 38, 135, DateTimeKind.Unspecified).AddTicks(6482), "Doloribus reiciendis veniam sint fugit sit assumenda.", new DateTime(2022, 2, 20, 12, 52, 40, 445, DateTimeKind.Unspecified).AddTicks(3179), "Nemo accusamus.", 17, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 18, 17, 53, 51, 88, DateTimeKind.Unspecified).AddTicks(2316), "Sapiente dolore sequi aut.", new DateTime(2022, 6, 24, 14, 36, 4, 521, DateTimeKind.Unspecified).AddTicks(2606), "In labore.", 13, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 20, 57, 58, 357, DateTimeKind.Unspecified).AddTicks(9864), "Sed consectetur nisi reprehenderit sapiente voluptatem.", new DateTime(2021, 4, 21, 14, 27, 44, 680, DateTimeKind.Unspecified).AddTicks(4286), "Ut facere.", 43, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 17, 13, 37, 203, DateTimeKind.Unspecified).AddTicks(4083), "Sit debitis occaecati voluptatibus expedita autem et ullam voluptatem at.", new DateTime(2021, 11, 22, 6, 57, 42, 376, DateTimeKind.Unspecified).AddTicks(5697), "Ipsam incidunt.", 36, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 7, 4, 21, 25, 513, DateTimeKind.Unspecified).AddTicks(1849), "Ipsum sapiente dignissimos illo ea molestiae asperiores facere quo ratione.", new DateTime(2021, 8, 13, 20, 22, 34, 30, DateTimeKind.Unspecified).AddTicks(8414), "Deleniti distinctio.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 14, 22, 11, 54, 918, DateTimeKind.Unspecified).AddTicks(1613), "Labore tempore laudantium.", new DateTime(2021, 4, 8, 16, 47, 40, 158, DateTimeKind.Unspecified).AddTicks(5380), "Atque quae.", 12, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 15, 22, 1, 11, 585, DateTimeKind.Unspecified).AddTicks(9624), "Qui hic delectus repellendus et deleniti ullam velit dicta omnis.", new DateTime(2021, 12, 6, 13, 4, 41, 736, DateTimeKind.Unspecified).AddTicks(903), "Tempora quia.", 1, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 23, 9, 52, 46, 581, DateTimeKind.Unspecified).AddTicks(8432), "Unde qui et possimus debitis dolorem alias enim facilis.", new DateTime(2022, 4, 12, 18, 49, 32, 666, DateTimeKind.Unspecified).AddTicks(5180), "Et recusandae.", 12, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 16, 3, 27, 8, 177, DateTimeKind.Unspecified).AddTicks(7142), "Nihil sit illum quo asperiores molestiae quam nesciunt illum quo.", new DateTime(2021, 5, 22, 1, 16, 27, 32, DateTimeKind.Unspecified).AddTicks(1189), "Assumenda voluptas.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 23, 49, 15, 566, DateTimeKind.Unspecified).AddTicks(7988), "Saepe ipsa tenetur assumenda aliquam adipisci fugit.", new DateTime(2022, 7, 7, 23, 17, 14, 659, DateTimeKind.Unspecified).AddTicks(8621), "Dolores non.", 25, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 25, 15, 12, 15, 271, DateTimeKind.Unspecified).AddTicks(8281), "Itaque dolorem molestias fugiat et odit nesciunt cumque architecto.", new DateTime(2021, 2, 26, 8, 54, 32, 487, DateTimeKind.Unspecified).AddTicks(8119), "Aliquid ut.", 24, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 25, 3, 40, 46, 587, DateTimeKind.Unspecified).AddTicks(9050), "Aut dolores velit sunt excepturi.", new DateTime(2021, 5, 6, 11, 14, 4, 53, DateTimeKind.Unspecified).AddTicks(856), "Quia aliquid.", 16, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 13, 16, 35, 50, 33, DateTimeKind.Unspecified).AddTicks(1344), "Ea doloremque consequatur tenetur adipisci labore porro non libero deserunt.", new DateTime(2022, 8, 4, 17, 2, 10, 698, DateTimeKind.Unspecified).AddTicks(5872), "Quo ducimus.", 27, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 21, 50, 15, 991, DateTimeKind.Unspecified).AddTicks(735), "Consequatur porro enim voluptas delectus qui aut maxime delectus ducimus.", new DateTime(2021, 9, 8, 17, 29, 7, 805, DateTimeKind.Unspecified).AddTicks(7237), "Aperiam et.", 12, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 3, 6, 41, 54, 437, DateTimeKind.Unspecified).AddTicks(332), "Soluta atque perferendis labore quo dolores sunt ducimus.", new DateTime(2022, 7, 17, 7, 0, 58, 776, DateTimeKind.Unspecified).AddTicks(7021), "Et iusto.", 10, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 16, 57, 29, 286, DateTimeKind.Unspecified).AddTicks(3066), "Atque quidem itaque illo vitae corrupti sit sit cupiditate perferendis.", new DateTime(2021, 2, 16, 17, 34, 44, 440, DateTimeKind.Unspecified).AddTicks(3101), "Dolores optio.", 40, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 11, 5, 44, 51, 405, DateTimeKind.Unspecified).AddTicks(2344), "In dicta error qui ipsam enim quia odio.", new DateTime(2022, 11, 25, 22, 6, 40, 815, DateTimeKind.Unspecified).AddTicks(1685), "Quos incidunt.", 7, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 13, 5, 16, 758, DateTimeKind.Unspecified).AddTicks(2979), "Rerum quia nesciunt ipsum deleniti eum dolores.", new DateTime(2022, 11, 21, 11, 36, 12, 662, DateTimeKind.Unspecified).AddTicks(139), "Alias reiciendis.", 16, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 20, 20, 45, 35, 533, DateTimeKind.Unspecified).AddTicks(9807), "Tempora dolores officiis velit doloribus aut id aperiam quia.", new DateTime(2021, 11, 30, 19, 26, 23, 58, DateTimeKind.Unspecified).AddTicks(2530), "Voluptas sint.", 39, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 23, 21, 3, 44, 530, DateTimeKind.Unspecified).AddTicks(5761), "Maiores tenetur et ipsa modi voluptatum.", new DateTime(2021, 3, 25, 20, 10, 4, 495, DateTimeKind.Unspecified).AddTicks(59), "Impedit quidem.", 38, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 4, 6, 53, 1, 250, DateTimeKind.Unspecified).AddTicks(5270), "In autem nostrum eligendi nostrum.", new DateTime(2022, 11, 10, 21, 3, 28, 704, DateTimeKind.Unspecified).AddTicks(9718), "Dolor consequatur.", 10, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 27, 18, 20, 42, 335, DateTimeKind.Unspecified).AddTicks(9084), "Commodi amet exercitationem iure assumenda nesciunt atque.", new DateTime(2022, 4, 23, 12, 12, 9, 572, DateTimeKind.Unspecified).AddTicks(5523), "Velit vel.", 39, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 5, 28, 11, 8, 58, 202, DateTimeKind.Unspecified).AddTicks(2799), "Est provident vel voluptas voluptatem rerum in.", new DateTime(2022, 7, 28, 2, 18, 11, 81, DateTimeKind.Unspecified).AddTicks(4538), "Eveniet quia.", 15, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 13, 21, 23, 9, 759, DateTimeKind.Unspecified).AddTicks(3438), "Facere est voluptatibus corrupti et quas autem eum similique excepturi.", new DateTime(2022, 2, 24, 12, 8, 35, 367, DateTimeKind.Unspecified).AddTicks(8547), "Et aut.", 48, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 10, 19, 59, 59, 17, DateTimeKind.Unspecified).AddTicks(8450), "Veniam officiis molestias ut vitae nemo labore quos vel maxime.", new DateTime(2022, 6, 16, 22, 40, 26, 490, DateTimeKind.Unspecified).AddTicks(1900), "Minima dolor.", 42, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 15, 3, 25, 35, 69, DateTimeKind.Unspecified).AddTicks(2287), "In quidem odit nulla veniam perspiciatis magni provident ducimus.", new DateTime(2022, 7, 1, 6, 32, 35, 717, DateTimeKind.Unspecified).AddTicks(3411), "Omnis velit.", 11, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 1, 17, 1, 12, 24, DateTimeKind.Unspecified).AddTicks(8249), "Totam autem et commodi cupiditate.", new DateTime(2021, 7, 20, 12, 30, 44, 115, DateTimeKind.Unspecified).AddTicks(6225), "Est nulla.", 16, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 27, 16, 46, 29, 117, DateTimeKind.Unspecified).AddTicks(2950), "Asperiores architecto quis ullam et.", new DateTime(2022, 11, 20, 15, 49, 41, 399, DateTimeKind.Unspecified).AddTicks(5259), "Vitae aliquam.", 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 18, 3, 54, 44, 860, DateTimeKind.Unspecified).AddTicks(6109), "Aut explicabo omnis quia inventore reiciendis qui eum debitis qui.", new DateTime(2022, 8, 28, 14, 10, 34, 46, DateTimeKind.Unspecified).AddTicks(4182), "Aut nam.", 27, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 25, 21, 59, 1, 460, DateTimeKind.Unspecified).AddTicks(6299), "Ut vel ut dolores.", new DateTime(2022, 8, 1, 3, 12, 10, 57, DateTimeKind.Unspecified).AddTicks(3628), "Est dolore.", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 23, 6, 47, 45, 737, DateTimeKind.Unspecified).AddTicks(5977), "Voluptatibus in explicabo.", new DateTime(2022, 3, 14, 8, 33, 50, 45, DateTimeKind.Unspecified).AddTicks(6766), "Vel dolorem.", 47, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 7, 2, 22, 33, 600, DateTimeKind.Unspecified).AddTicks(8987), "Eligendi et aut officia vel.", new DateTime(2021, 4, 3, 21, 37, 11, 172, DateTimeKind.Unspecified).AddTicks(9207), "Culpa laborum.", 44, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 6, 54, 54, 937, DateTimeKind.Unspecified).AddTicks(4179), "Beatae amet quia et et commodi accusamus.", new DateTime(2021, 9, 18, 2, 47, 11, 814, DateTimeKind.Unspecified).AddTicks(8012), "Autem impedit.", 28, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 3, 5, 27, 4, 209, DateTimeKind.Unspecified).AddTicks(5273), "Quam a repudiandae consequatur est sequi delectus itaque.", new DateTime(2021, 12, 2, 17, 4, 40, 775, DateTimeKind.Unspecified).AddTicks(6496), "Iste magni.", 27, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 3, 46, 19, 292, DateTimeKind.Unspecified).AddTicks(6569), "Voluptate repellat alias.", new DateTime(2022, 2, 12, 13, 25, 19, 473, DateTimeKind.Unspecified).AddTicks(8462), "Unde molestias.", 48, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 10, 24, 18, 46, 28, 714, DateTimeKind.Unspecified).AddTicks(7955), "Laborum dignissimos enim quo molestiae et voluptatem accusantium aspernatur possimus.", new DateTime(2021, 12, 30, 23, 14, 59, 807, DateTimeKind.Unspecified).AddTicks(1355), "Eligendi praesentium.", 27 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 27, 22, 46, 47, 392, DateTimeKind.Unspecified).AddTicks(2343), "Est ratione impedit quibusdam.", new DateTime(2021, 2, 5, 22, 8, 7, 770, DateTimeKind.Unspecified).AddTicks(8207), "Tempore hic.", 6, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 10, 48, 42, 447, DateTimeKind.Unspecified).AddTicks(7377), "Voluptatem ut autem voluptate ut iste perferendis omnis eaque.", new DateTime(2022, 3, 7, 22, 56, 6, 63, DateTimeKind.Unspecified).AddTicks(7734), "Rem architecto.", 48, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 18, 8, 4, 56, 881, DateTimeKind.Unspecified).AddTicks(2610), "Velit laborum distinctio laudantium deleniti quos natus odit quisquam voluptatem.", new DateTime(2021, 1, 2, 9, 35, 49, 933, DateTimeKind.Unspecified).AddTicks(5161), "In iusto.", 3, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 3, 5, 30, 39, 706, DateTimeKind.Unspecified).AddTicks(9477), "Est ratione et minima beatae neque.", new DateTime(2021, 4, 22, 8, 44, 21, 511, DateTimeKind.Unspecified).AddTicks(7063), "Vel quasi.", 21, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 10, 31, 57, 390, DateTimeKind.Unspecified).AddTicks(8308), "Excepturi et ipsum at.", new DateTime(2022, 5, 25, 10, 23, 55, 444, DateTimeKind.Unspecified).AddTicks(6004), "Quia sint.", 25, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 18, 9, 46, 42, 572, DateTimeKind.Unspecified).AddTicks(589), "Nulla maxime in sed exercitationem.", new DateTime(2022, 2, 22, 3, 8, 3, 554, DateTimeKind.Unspecified).AddTicks(6276), "Ut odit.", 24, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 5, 15, 26, 34, 202, DateTimeKind.Unspecified).AddTicks(6551), "Non voluptatem quae.", new DateTime(2021, 11, 2, 21, 55, 42, 584, DateTimeKind.Unspecified).AddTicks(155), "A labore.", 8, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 13, 21, 21, 13, 800, DateTimeKind.Unspecified).AddTicks(9431), "Sit nisi et quo.", new DateTime(2022, 11, 30, 15, 36, 21, 577, DateTimeKind.Unspecified).AddTicks(6822), "Repellat et.", 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 20, 50, 26, 770, DateTimeKind.Unspecified).AddTicks(762), "Earum rerum qui nam consequatur et fugiat commodi.", new DateTime(2021, 9, 12, 2, 13, 9, 470, DateTimeKind.Unspecified).AddTicks(1084), "Reprehenderit est.", 38, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 14, 23, 55, 20, 858, DateTimeKind.Unspecified).AddTicks(6013), "Asperiores explicabo labore nulla autem vel molestiae a.", new DateTime(2022, 2, 23, 11, 26, 27, 771, DateTimeKind.Unspecified).AddTicks(5457), "Sunt doloremque.", 5, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 30, 1, 13, 50, 121, DateTimeKind.Unspecified).AddTicks(7270), "Qui quod maxime quia at quaerat dolorem ut et.", new DateTime(2022, 5, 12, 9, 31, 22, 348, DateTimeKind.Unspecified).AddTicks(1287), "Perspiciatis eaque.", 25, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 9, 21, 33, 43, 616, DateTimeKind.Unspecified).AddTicks(5370), "Voluptas ut cumque.", new DateTime(2022, 11, 7, 11, 25, 45, 356, DateTimeKind.Unspecified).AddTicks(2559), "Tempora doloremque.", 33, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 19, 13, 50, 14, 860, DateTimeKind.Unspecified).AddTicks(9964), "Eveniet ea nihil voluptatem sit et.", new DateTime(2021, 1, 28, 11, 34, 51, 284, DateTimeKind.Unspecified).AddTicks(5650), "Natus ea.", 34, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 12, 31, 13, 401, DateTimeKind.Unspecified).AddTicks(9986), "Similique iusto ratione voluptate nesciunt.", new DateTime(2021, 10, 24, 6, 51, 36, 169, DateTimeKind.Unspecified).AddTicks(8546), "Aperiam sint.", 48, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 22, 0, 53, 28, 763, DateTimeKind.Unspecified).AddTicks(172), "Veritatis autem aut nulla voluptas veniam et commodi quam quo.", new DateTime(2021, 8, 29, 17, 44, 35, 659, DateTimeKind.Unspecified).AddTicks(4184), "Libero voluptates.", 16, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 7, 41, 42, 507, DateTimeKind.Unspecified).AddTicks(8405), "Optio temporibus dolores animi.", new DateTime(2021, 6, 11, 9, 38, 20, 918, DateTimeKind.Unspecified).AddTicks(4433), "Facilis dolore.", 21, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 4, 25, 13, 411, DateTimeKind.Unspecified).AddTicks(2134), "Eveniet maiores et eos aut eos voluptatem ducimus omnis ut.", new DateTime(2021, 10, 26, 3, 28, 29, 105, DateTimeKind.Unspecified).AddTicks(8926), "Non veritatis.", 20, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 16, 13, 58, 50, 941, DateTimeKind.Unspecified).AddTicks(7608), "Inventore iste quis.", new DateTime(2022, 8, 12, 8, 45, 30, 935, DateTimeKind.Unspecified).AddTicks(7903), "Similique eos.", 50, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 17, 0, 20, 42, 469, DateTimeKind.Unspecified).AddTicks(1806), "Voluptatem officia dolores eos ut sit totam.", new DateTime(2022, 6, 11, 20, 15, 18, 304, DateTimeKind.Unspecified).AddTicks(2160), "Tempora quia.", 7, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 5, 16, 47, 41, 617, DateTimeKind.Unspecified).AddTicks(7080), "Molestias ad voluptates ut est quasi nesciunt quia et sapiente.", new DateTime(2021, 3, 28, 0, 20, 44, 958, DateTimeKind.Unspecified).AddTicks(3666), "Velit necessitatibus.", 22, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 6, 18, 17, 53, 949, DateTimeKind.Unspecified).AddTicks(4882), "Velit voluptatum incidunt et molestiae sed qui placeat explicabo reprehenderit.", new DateTime(2022, 4, 18, 19, 11, 45, 973, DateTimeKind.Unspecified).AddTicks(649), "Ipsam enim.", 46, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 20, 56, 25, 904, DateTimeKind.Unspecified).AddTicks(6367), "Non ex iste laboriosam consequatur labore.", new DateTime(2021, 9, 20, 23, 23, 32, 123, DateTimeKind.Unspecified).AddTicks(5750), "Id consequuntur.", 33, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 22, 6, 46, 45, 872, DateTimeKind.Unspecified).AddTicks(8441), "Commodi nulla quidem dolorem sint explicabo aliquam ducimus.", new DateTime(2021, 9, 9, 7, 16, 7, 318, DateTimeKind.Unspecified).AddTicks(7153), "Eum pariatur.", 18, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 4, 3, 34, 25, 779, DateTimeKind.Unspecified).AddTicks(2261), "Et qui sint vero et.", new DateTime(2022, 9, 12, 10, 0, 7, 530, DateTimeKind.Unspecified).AddTicks(2826), "Nisi ipsam.", 17, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 11, 12, 56, 18, 329, DateTimeKind.Unspecified).AddTicks(1586), "Iste sed ipsam aliquam aut nisi eum aut quia consectetur.", new DateTime(2022, 2, 20, 3, 40, 8, 48, DateTimeKind.Unspecified).AddTicks(311), "Soluta molestiae.", 28, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 26, 1, 35, 25, 205, DateTimeKind.Unspecified).AddTicks(7966), "Facilis odit ut in in voluptatem tempora.", new DateTime(2021, 8, 23, 10, 7, 32, 649, DateTimeKind.Unspecified).AddTicks(9351), "Ratione voluptates.", 6, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 22, 5, 50, 11, 870, DateTimeKind.Unspecified).AddTicks(404), "Sit exercitationem reiciendis quis illum.", new DateTime(2022, 2, 5, 10, 2, 46, 351, DateTimeKind.Unspecified).AddTicks(7352), "Cum corporis.", 37, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 29, 0, 35, 35, 159, DateTimeKind.Unspecified).AddTicks(5811), "Nisi unde in minus modi dolores repudiandae.", new DateTime(2022, 5, 22, 16, 2, 6, 213, DateTimeKind.Unspecified).AddTicks(3542), "Ut sed.", 48, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 21, 10, 52, 57, 896, DateTimeKind.Unspecified).AddTicks(2778), "Eum nesciunt et ut.", new DateTime(2022, 6, 8, 11, 33, 44, 385, DateTimeKind.Unspecified).AddTicks(7453), "Necessitatibus dolorem.", 12, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 25, 7, 23, 4, 790, DateTimeKind.Unspecified).AddTicks(7335), "Aut quae autem.", new DateTime(2022, 10, 27, 18, 8, 23, 475, DateTimeKind.Unspecified).AddTicks(870), "Laudantium autem.", 38, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 4, 1, 3, 52, 13, 996, DateTimeKind.Unspecified).AddTicks(7007), "Sapiente molestiae voluptatem qui consectetur ab dolore qui ducimus sed.", new DateTime(2022, 11, 4, 2, 2, 2, 74, DateTimeKind.Unspecified).AddTicks(7670), "Magni delectus.", 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 23, 6, 8, 885, DateTimeKind.Unspecified).AddTicks(2503), "Cumque dolor nihil aperiam aut aperiam omnis sequi expedita mollitia.", new DateTime(2022, 4, 17, 9, 33, 3, 785, DateTimeKind.Unspecified).AddTicks(1621), "Illum dolorem.", 29, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 2, 16, 25, 32, 653, DateTimeKind.Unspecified).AddTicks(1522), "Nam repellat provident.", new DateTime(2022, 5, 30, 4, 40, 52, 762, DateTimeKind.Unspecified).AddTicks(4776), "In perspiciatis.", 21, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 15, 12, 59, 14, 200, DateTimeKind.Unspecified).AddTicks(8433), "Voluptatem reprehenderit consequuntur ut tempora dolorem numquam quia provident aspernatur.", new DateTime(2022, 10, 28, 5, 17, 14, 847, DateTimeKind.Unspecified).AddTicks(2767), "Debitis ab.", 44, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 23, 17, 59, 17, 673, DateTimeKind.Unspecified).AddTicks(4471), "Animi velit sint reprehenderit dolores ad quaerat iusto fugit libero.", new DateTime(2021, 6, 24, 11, 15, 33, 940, DateTimeKind.Unspecified).AddTicks(8862), "Quibusdam ea.", 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 8, 18, 4, 26, 40, 384, DateTimeKind.Unspecified).AddTicks(4518), "Est est sint nostrum aut itaque rerum cumque odio.", new DateTime(2021, 9, 19, 13, 3, 54, 988, DateTimeKind.Unspecified).AddTicks(1644), "Commodi vero.", 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 16, 7, 42, 17, 895, DateTimeKind.Unspecified).AddTicks(9706), "Nulla voluptas rerum pariatur autem quo qui praesentium sit.", new DateTime(2021, 10, 14, 16, 33, 21, 210, DateTimeKind.Unspecified).AddTicks(9711), "Quis dolores.", 36, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 7, 10, 9, 47, 503, DateTimeKind.Unspecified).AddTicks(2195), "Quidem voluptas nihil distinctio suscipit neque expedita beatae consequatur.", new DateTime(2022, 5, 15, 18, 40, 24, 117, DateTimeKind.Unspecified).AddTicks(167), "Rerum labore.", 43, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 24, 4, 55, 6, 520, DateTimeKind.Unspecified).AddTicks(6305), "Aperiam quam ducimus.", new DateTime(2021, 2, 14, 1, 39, 22, 295, DateTimeKind.Unspecified).AddTicks(447), "Animi veniam.", 29, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 12, 20, 39, 39, 363, DateTimeKind.Unspecified).AddTicks(4705), "Maxime ea qui quasi molestiae perspiciatis mollitia quo alias magni.", new DateTime(2021, 3, 12, 21, 38, 51, 819, DateTimeKind.Unspecified).AddTicks(1687), "Possimus recusandae.", 43, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 14, 10, 42, 30, 477, DateTimeKind.Unspecified).AddTicks(2414), "Et doloremque aperiam iusto velit voluptatum.", new DateTime(2021, 6, 29, 17, 31, 58, 282, DateTimeKind.Unspecified).AddTicks(9131), "Et velit.", 37, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 12, 44, 51, 805, DateTimeKind.Unspecified).AddTicks(6460), "Accusantium eveniet corrupti id autem quam beatae recusandae qui vel.", new DateTime(2021, 11, 4, 5, 17, 50, 678, DateTimeKind.Unspecified).AddTicks(6385), "Iusto a.", 47, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 4, 31, 26, 126, DateTimeKind.Unspecified).AddTicks(2489), "Eos corrupti ea.", new DateTime(2021, 7, 12, 17, 2, 46, 156, DateTimeKind.Unspecified).AddTicks(3803), "Et quis.", 32, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 4, 17, 33, 49, 198, DateTimeKind.Unspecified).AddTicks(3919), "Et necessitatibus itaque qui est corrupti illo.", new DateTime(2022, 11, 26, 11, 32, 29, 409, DateTimeKind.Unspecified).AddTicks(6245), "Nemo facere.", 35, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 19, 15, 31, 32, 713, DateTimeKind.Unspecified).AddTicks(46), "Facilis nihil laboriosam sapiente iusto magni officiis labore ea fugiat.", new DateTime(2022, 9, 25, 10, 46, 29, 358, DateTimeKind.Unspecified).AddTicks(1554), "Atque magnam.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 18, 28, 47, 358, DateTimeKind.Unspecified).AddTicks(5855), "Quam iste dolore enim veniam.", new DateTime(2021, 9, 1, 11, 1, 26, 412, DateTimeKind.Unspecified).AddTicks(9775), "Est omnis.", 6, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 6, 17, 17, 18, 33, 934, DateTimeKind.Unspecified).AddTicks(6327), "Est quidem a et illo mollitia aut et.", new DateTime(2022, 8, 26, 5, 13, 35, 733, DateTimeKind.Unspecified).AddTicks(1900), "Est doloribus.", 23, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 30, 20, 25, 22, 876, DateTimeKind.Unspecified).AddTicks(1891), "Ut necessitatibus distinctio.", new DateTime(2022, 2, 27, 5, 7, 32, 599, DateTimeKind.Unspecified).AddTicks(8816), "Omnis iusto.", 36, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 19, 39, 33, 469, DateTimeKind.Unspecified).AddTicks(8503), "Velit est autem recusandae mollitia illum sed explicabo et rerum.", new DateTime(2021, 6, 24, 23, 41, 38, 790, DateTimeKind.Unspecified).AddTicks(7902), "Ut consequatur.", 6, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 26, 3, 54, 32, 413, DateTimeKind.Unspecified).AddTicks(8684), "Sapiente in minima.", new DateTime(2021, 11, 4, 18, 35, 29, 532, DateTimeKind.Unspecified).AddTicks(6375), "Enim et.", 8, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 22, 8, 21, 2, 814, DateTimeKind.Unspecified).AddTicks(4164), "Ipsum incidunt laudantium et exercitationem reprehenderit unde quasi aut architecto.", new DateTime(2021, 1, 15, 6, 5, 39, 478, DateTimeKind.Unspecified).AddTicks(7800), "Numquam et.", 8, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 0, 15, 31, 489, DateTimeKind.Unspecified).AddTicks(5531), "Cupiditate explicabo dolor debitis modi quos eum quia.", new DateTime(2021, 11, 16, 0, 25, 37, 481, DateTimeKind.Unspecified).AddTicks(7918), "Repudiandae nulla.", 11, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 27, 2, 26, 22, 981, DateTimeKind.Unspecified).AddTicks(4130), "Nobis nam corporis debitis.", new DateTime(2021, 2, 3, 3, 27, 10, 91, DateTimeKind.Unspecified).AddTicks(1827), "Eius est.", 14, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 21, 47, 28, 582, DateTimeKind.Unspecified).AddTicks(9976), "Ex autem quibusdam qui vitae qui voluptas.", new DateTime(2022, 7, 20, 22, 29, 44, 127, DateTimeKind.Unspecified).AddTicks(5467), "Qui aut.", 31, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 13, 26, 10, 441, DateTimeKind.Unspecified).AddTicks(7094), "Id quidem asperiores vel.", new DateTime(2022, 11, 15, 23, 13, 40, 983, DateTimeKind.Unspecified).AddTicks(8053), "Consequatur nihil.", 14, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 11, 5, 10, 24, 348, DateTimeKind.Unspecified).AddTicks(6514), "Qui deserunt sit sunt eos aut aut.", new DateTime(2021, 12, 25, 23, 30, 26, 105, DateTimeKind.Unspecified).AddTicks(3990), "Non repudiandae.", 24, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 13, 14, 39, 5, 666, DateTimeKind.Unspecified).AddTicks(4270), "Praesentium rem dolores qui eius blanditiis inventore ex.", new DateTime(2021, 1, 26, 1, 35, 21, 168, DateTimeKind.Unspecified).AddTicks(6143), "Qui eveniet.", 27, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 21, 7, 12, 44, 946, DateTimeKind.Unspecified).AddTicks(5409), "Error dolorum eum ex neque voluptatem culpa nam fuga ipsam.", new DateTime(2022, 12, 8, 3, 54, 33, 893, DateTimeKind.Unspecified).AddTicks(2420), "Ut temporibus.", 19, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 7, 5, 6, 49, 647, DateTimeKind.Unspecified).AddTicks(2096), "Tempore mollitia quo sit dolorem accusantium placeat error et.", new DateTime(2022, 10, 22, 19, 37, 41, 236, DateTimeKind.Unspecified).AddTicks(6309), "Qui et.", 25, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 21, 25, 43, 48, DateTimeKind.Unspecified).AddTicks(6004), "Porro pariatur quasi quis minima et sapiente omnis.", new DateTime(2021, 9, 8, 17, 51, 34, 663, DateTimeKind.Unspecified).AddTicks(6239), "Et natus.", 41, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 23, 20, 44, 7, 202, DateTimeKind.Unspecified).AddTicks(1361), "Necessitatibus nihil quis unde reiciendis dolorem praesentium eaque similique.", new DateTime(2022, 8, 20, 3, 25, 15, 56, DateTimeKind.Unspecified).AddTicks(6358), "Optio velit.", 13, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 19, 3, 50, 6, 167, DateTimeKind.Unspecified).AddTicks(4593), "Facilis dolores iusto sit non enim.", new DateTime(2021, 11, 29, 12, 11, 9, 863, DateTimeKind.Unspecified).AddTicks(8957), "Facilis eius.", 40, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 17, 13, 31, 23, 324, DateTimeKind.Unspecified).AddTicks(7330), "Neque aperiam vel est corrupti nihil.", new DateTime(2022, 4, 10, 3, 55, 44, 526, DateTimeKind.Unspecified).AddTicks(3756), "Voluptatem error.", 44, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 7, 19, 48, 810, DateTimeKind.Unspecified).AddTicks(310), "Architecto molestias natus quo soluta officia.", new DateTime(2021, 10, 23, 0, 57, 6, 25, DateTimeKind.Unspecified).AddTicks(2801), "Pariatur animi.", 40, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 11, 21, 27, 45, 840, DateTimeKind.Unspecified).AddTicks(2000), "Ducimus vitae saepe molestias vel rerum minus nesciunt placeat.", new DateTime(2022, 3, 11, 5, 45, 9, 517, DateTimeKind.Unspecified).AddTicks(4587), "Perferendis illum.", 31, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 29, 2, 58, 22, 351, DateTimeKind.Unspecified).AddTicks(2700), "Ullam molestias et et nihil aut quaerat quam molestias.", new DateTime(2022, 11, 17, 5, 57, 39, 65, DateTimeKind.Unspecified).AddTicks(5174), "Architecto numquam.", 6, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 7, 13, 59, 38, 580, DateTimeKind.Unspecified).AddTicks(8584), "Veritatis officia laudantium velit doloribus sit.", new DateTime(2022, 8, 7, 18, 3, 56, 11, DateTimeKind.Unspecified).AddTicks(5930), "Delectus quo.", 6, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 19, 16, 33, 55, 682, DateTimeKind.Unspecified).AddTicks(7265), "Ipsam excepturi aut eum incidunt reiciendis dolores explicabo.", new DateTime(2022, 4, 26, 3, 57, 43, 633, DateTimeKind.Unspecified).AddTicks(8179), "Quia corporis.", 18, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 6, 14, 58, 25, 361, DateTimeKind.Unspecified).AddTicks(7082), "Quia ut aperiam ipsam quia similique.", new DateTime(2022, 3, 7, 4, 58, 17, 143, DateTimeKind.Unspecified).AddTicks(4472), "Fuga est.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 21, 10, 16, 17, 799, DateTimeKind.Unspecified).AddTicks(3542), "Non iusto autem qui.", new DateTime(2021, 8, 4, 0, 52, 33, 868, DateTimeKind.Unspecified).AddTicks(5372), "Mollitia ut.", 50, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 13, 13, 55, 39, 134, DateTimeKind.Unspecified).AddTicks(14), "Dolorem qui excepturi earum.", new DateTime(2021, 1, 19, 2, 29, 57, 489, DateTimeKind.Unspecified).AddTicks(8010), "Adipisci atque.", 28, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 20, 5, 19, 0, 154, DateTimeKind.Unspecified).AddTicks(7184), "Eos qui culpa.", new DateTime(2022, 1, 24, 22, 54, 14, 643, DateTimeKind.Unspecified).AddTicks(4550), "Sunt sed.", 17, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 9, 24, 2, 3, DateTimeKind.Unspecified).AddTicks(506), "Eos non ut et iste recusandae delectus.", new DateTime(2022, 3, 1, 1, 25, 11, 930, DateTimeKind.Unspecified).AddTicks(366), "Occaecati ut.", 19, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 8, 18, 23, 30, 31, 360, DateTimeKind.Unspecified).AddTicks(6603), "Possimus voluptatem quod voluptatem.", new DateTime(2022, 6, 16, 14, 42, 18, 802, DateTimeKind.Unspecified).AddTicks(8832), "Dicta dolorem.", 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 22, 3, 38, 42, 57, DateTimeKind.Unspecified).AddTicks(3177), "Vero velit corrupti labore sunt et.", new DateTime(2021, 4, 2, 10, 10, 10, 919, DateTimeKind.Unspecified).AddTicks(683), "Et et.", 49, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 18, 8, 23, 49, 770, DateTimeKind.Unspecified).AddTicks(2166), "Ex voluptate omnis fugit maiores labore quam et.", new DateTime(2021, 12, 30, 22, 52, 59, 831, DateTimeKind.Unspecified).AddTicks(6431), "Dolores sint.", 29, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 16, 10, 57, 46, 701, DateTimeKind.Unspecified).AddTicks(1587), "Officiis ut modi et assumenda dolores voluptate.", new DateTime(2021, 7, 5, 21, 34, 56, 867, DateTimeKind.Unspecified).AddTicks(8765), "Et temporibus.", 30, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 4, 21, 39, 22, 628, DateTimeKind.Unspecified).AddTicks(345), "Quis ducimus aut consequatur ipsa velit dolor adipisci adipisci reprehenderit.", new DateTime(2021, 7, 26, 19, 2, 19, 146, DateTimeKind.Unspecified).AddTicks(5082), "At tempore.", 16, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 0, 6, 7, 301, DateTimeKind.Unspecified).AddTicks(6864), "Harum voluptatum perspiciatis consequatur amet nesciunt aut et est.", new DateTime(2021, 12, 17, 19, 59, 24, 568, DateTimeKind.Unspecified).AddTicks(5425), "Possimus qui.", 28, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 28, 18, 2, 50, 423, DateTimeKind.Unspecified).AddTicks(5780), "Quam sit eveniet velit aut et cum.", new DateTime(2022, 9, 28, 1, 10, 59, 195, DateTimeKind.Unspecified).AddTicks(5720), "Similique vel.", 30, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 24, 3, 31, 22, 916, DateTimeKind.Unspecified).AddTicks(1701), "Est vel quis rem velit exercitationem id unde.", new DateTime(2022, 7, 8, 23, 20, 29, 81, DateTimeKind.Unspecified).AddTicks(1897), "Est repellat.", 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 27, 4, 7, 55, 634, DateTimeKind.Unspecified).AddTicks(1278), "Nobis fugit illo non omnis ut doloremque illo.", new DateTime(2022, 4, 15, 3, 21, 13, 589, DateTimeKind.Unspecified).AddTicks(7965), "Quaerat repudiandae.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 14, 39, 46, 602, DateTimeKind.Unspecified).AddTicks(4558), "Omnis et qui saepe.", new DateTime(2022, 5, 8, 3, 9, 54, 829, DateTimeKind.Unspecified).AddTicks(1250), "Harum repellat.", 40, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 3, 11, 5, 11, 831, DateTimeKind.Unspecified).AddTicks(1500), "Deserunt dolores ab expedita eum veritatis repellat.", new DateTime(2022, 7, 1, 0, 16, 16, 219, DateTimeKind.Unspecified).AddTicks(5389), "Minima et.", 16, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 19, 23, 43, 55, 440, DateTimeKind.Unspecified).AddTicks(8110), "Alias sunt sed.", new DateTime(2022, 3, 1, 6, 59, 18, 457, DateTimeKind.Unspecified).AddTicks(3468), "Quia aspernatur.", 34, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 25, 2, 46, 54, 612, DateTimeKind.Unspecified).AddTicks(8038), "Placeat dolore magni voluptatem.", new DateTime(2021, 8, 15, 11, 44, 12, 584, DateTimeKind.Unspecified).AddTicks(8966), "Id molestiae.", 38, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 14, 1, 59, 26, 63, DateTimeKind.Unspecified).AddTicks(4835), "Ratione est in accusamus sed impedit illo.", new DateTime(2022, 1, 19, 15, 58, 52, 18, DateTimeKind.Unspecified).AddTicks(1340), "Ut aspernatur.", 29, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 7, 11, 22, 19, 847, DateTimeKind.Unspecified).AddTicks(4419), "Nihil nihil non.", new DateTime(2021, 9, 5, 14, 42, 15, 946, DateTimeKind.Unspecified).AddTicks(6277), "Architecto vel.", 30, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 22, 9, 11, 16, 57, DateTimeKind.Unspecified).AddTicks(3059), "Suscipit voluptas voluptatem deleniti.", new DateTime(2022, 3, 15, 19, 25, 4, 976, DateTimeKind.Unspecified).AddTicks(1674), "Delectus velit.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 20, 12, 9, 981, DateTimeKind.Unspecified).AddTicks(7872), "A commodi et consequatur iusto perferendis nobis et.", new DateTime(2021, 9, 9, 4, 50, 47, 681, DateTimeKind.Unspecified).AddTicks(2821), "Odio consequatur.", 19, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 18, 55, 23, 579, DateTimeKind.Unspecified).AddTicks(1163), "Excepturi ex vero et eos labore et nobis adipisci soluta.", new DateTime(2021, 5, 7, 8, 47, 14, 343, DateTimeKind.Unspecified).AddTicks(7345), "Aut nostrum.", 50, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 7, 29, 30, 544, DateTimeKind.Unspecified).AddTicks(8585), "Dolor laboriosam illo aliquid.", new DateTime(2021, 10, 5, 11, 13, 52, 831, DateTimeKind.Unspecified).AddTicks(2242), "Odit aut.", 19, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 8, 21, 58, 36, 441, DateTimeKind.Unspecified).AddTicks(2689), "Cum doloribus eligendi quia itaque id sed.", new DateTime(2022, 12, 22, 9, 45, 20, 157, DateTimeKind.Unspecified).AddTicks(3228), "Accusantium corrupti.", 11, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 19, 19, 9, 266, DateTimeKind.Unspecified).AddTicks(1044), "Quidem modi sunt itaque architecto corrupti harum tempora et.", new DateTime(2021, 7, 18, 21, 50, 50, 64, DateTimeKind.Unspecified).AddTicks(1261), "Est maiores.", 30, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 2, 20, 49, 31, 388, DateTimeKind.Unspecified).AddTicks(1639), "Deserunt soluta libero eligendi omnis et voluptatem aspernatur.", new DateTime(2022, 5, 5, 11, 26, 53, 490, DateTimeKind.Unspecified).AddTicks(6861), "Nesciunt voluptatibus.", 25, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 17, 7, 19, 306, DateTimeKind.Unspecified).AddTicks(2417), "Aspernatur distinctio dolore et est adipisci ut libero deleniti quisquam.", new DateTime(2022, 6, 30, 2, 45, 18, 864, DateTimeKind.Unspecified).AddTicks(4170), "Iste nihil.", 42, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 20, 34, 37, 179, DateTimeKind.Unspecified).AddTicks(8564), "Non omnis harum provident ea nisi.", new DateTime(2022, 3, 8, 1, 13, 32, 847, DateTimeKind.Unspecified).AddTicks(3876), "Consequatur in.", 45, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 10, 3, 22, 969, DateTimeKind.Unspecified).AddTicks(3814), "Voluptatum vel rem molestiae nostrum officia magnam itaque mollitia.", new DateTime(2021, 7, 8, 7, 9, 55, 169, DateTimeKind.Unspecified).AddTicks(7558), "Deleniti molestiae.", 30, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 26, 5, 39, 38, 414, DateTimeKind.Unspecified).AddTicks(3387), "Quia sed aut aperiam et mollitia ea non.", new DateTime(2021, 10, 26, 10, 8, 10, 877, DateTimeKind.Unspecified).AddTicks(2608), "Animi fugit.", 1, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 1, 1, 40, 1, 593, DateTimeKind.Unspecified).AddTicks(504), "Cumque officiis ut sapiente et quibusdam sint.", new DateTime(2021, 9, 20, 11, 24, 42, 862, DateTimeKind.Unspecified).AddTicks(137), "Eum amet.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 9, 8, 10, 15, 130, DateTimeKind.Unspecified).AddTicks(8826), "Sint et impedit.", new DateTime(2022, 3, 7, 4, 37, 1, 418, DateTimeKind.Unspecified).AddTicks(9127), "Sit et.", 28, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 25, 3, 51, 33, 853, DateTimeKind.Unspecified).AddTicks(7869), "Sit pariatur deserunt voluptatum mollitia facere architecto sed.", new DateTime(2022, 9, 30, 15, 9, 22, 822, DateTimeKind.Unspecified).AddTicks(6780), "Ea laborum.", 19, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 6, 14, 6, 29, 396, DateTimeKind.Unspecified).AddTicks(804), "Laborum officiis deleniti minima quod.", new DateTime(2021, 4, 13, 16, 45, 42, 87, DateTimeKind.Unspecified).AddTicks(7051), "Ipsa sed.", 38, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 2, 50, 54, 527, DateTimeKind.Unspecified).AddTicks(3071), "Tempore dolore ullam eveniet optio distinctio harum.", new DateTime(2021, 4, 8, 8, 46, 11, 879, DateTimeKind.Unspecified).AddTicks(819), "Asperiores consequatur.", 17, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 30, 21, 44, 10, 222, DateTimeKind.Unspecified).AddTicks(2018), "Expedita ipsa expedita dolor rerum voluptas in quis quaerat.", new DateTime(2022, 5, 8, 13, 51, 53, 860, DateTimeKind.Unspecified).AddTicks(3865), "Aut et.", 40, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 10, 18, 15, 29, 467, DateTimeKind.Unspecified).AddTicks(3315), "Alias velit minima quasi eveniet facilis.", new DateTime(2022, 5, 30, 6, 31, 26, 714, DateTimeKind.Unspecified).AddTicks(2715), "Aut nam.", 48, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 7, 20, 56, 226, DateTimeKind.Unspecified).AddTicks(8000), "Exercitationem earum et inventore.", new DateTime(2021, 3, 24, 10, 49, 55, 902, DateTimeKind.Unspecified).AddTicks(6616), "Ea aperiam.", 32, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 1, 17, 9, 46, 38, 979, DateTimeKind.Unspecified).AddTicks(4713), "Recusandae numquam autem enim.", new DateTime(2021, 3, 7, 4, 11, 2, 608, DateTimeKind.Unspecified).AddTicks(9676), "Rem est.", 8 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 29, 23, 54, 53, 9, DateTimeKind.Unspecified).AddTicks(2239), "Facilis molestiae rerum inventore at dolorem.", new DateTime(2022, 2, 27, 2, 31, 39, 418, DateTimeKind.Unspecified).AddTicks(4222), "Modi et.", 34, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 9, 5, 39, 8, 450, DateTimeKind.Unspecified).AddTicks(2509), "Inventore magni qui quae nam sit quia fugit dolor excepturi.", new DateTime(2022, 6, 16, 13, 27, 17, 531, DateTimeKind.Unspecified).AddTicks(7593), "Et saepe.", 1, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 18, 21, 10, 54, 561, DateTimeKind.Unspecified).AddTicks(4481), "Rerum adipisci incidunt veritatis accusantium occaecati cum cum voluptatem id.", new DateTime(2021, 3, 26, 16, 46, 50, 621, DateTimeKind.Unspecified).AddTicks(4688), "Tenetur quidem.", 19, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 7, 0, 37, 38, 981, DateTimeKind.Unspecified).AddTicks(3289), "Delectus iusto totam ipsam temporibus.", new DateTime(2022, 6, 25, 11, 9, 58, 751, DateTimeKind.Unspecified).AddTicks(3714), "Consequatur tempora.", 46, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 6, 20, 2, 55, 2, 206, DateTimeKind.Unspecified).AddTicks(5243), "Iste repellendus rerum voluptate autem eveniet doloremque rerum magni ad.", new DateTime(2021, 9, 18, 1, 30, 6, 32, DateTimeKind.Unspecified).AddTicks(6235), "Odit laudantium.", 42, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 29, 12, 17, 53, 547, DateTimeKind.Unspecified).AddTicks(5761), "Similique qui quam.", new DateTime(2022, 1, 17, 9, 1, 11, 974, DateTimeKind.Unspecified).AddTicks(1148), "Hic odio.", 16, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 17, 11, 11, 5, 231, DateTimeKind.Unspecified).AddTicks(8973), "Recusandae eius dignissimos qui sunt repudiandae aperiam animi magni.", new DateTime(2022, 9, 20, 10, 43, 22, 407, DateTimeKind.Unspecified).AddTicks(822), "Quo minus.", 30, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 5, 13, 7, 42, 40, 830, DateTimeKind.Unspecified).AddTicks(2318), "Nisi est dolor in cumque suscipit et ea.", new DateTime(2021, 6, 25, 7, 51, 0, 104, DateTimeKind.Unspecified).AddTicks(6413), "Quae velit.", 41 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 7, 7, 23, 56, 579, DateTimeKind.Unspecified).AddTicks(2586), "Sit animi modi vel sequi.", new DateTime(2022, 9, 18, 13, 32, 36, 284, DateTimeKind.Unspecified).AddTicks(4359), "Repudiandae est.", 14, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 28, 5, 53, 8, 502, DateTimeKind.Unspecified).AddTicks(3064), "Amet rerum qui iste aut.", new DateTime(2022, 10, 29, 12, 25, 35, 949, DateTimeKind.Unspecified).AddTicks(7152), "Molestiae ut.", 12, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 6, 7, 55, 30, 33, DateTimeKind.Unspecified).AddTicks(583), "In doloremque quibusdam.", new DateTime(2022, 10, 13, 1, 52, 26, 631, DateTimeKind.Unspecified).AddTicks(6521), "Accusantium voluptatum.", 12, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 31, 8, 17, 43, 761, DateTimeKind.Unspecified).AddTicks(9363), "Repudiandae aperiam labore.", new DateTime(2021, 4, 16, 14, 54, 55, 628, DateTimeKind.Unspecified).AddTicks(7826), "Culpa quia.", 31, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 19, 0, 9, 6, 934, DateTimeKind.Unspecified).AddTicks(2138), "Repellat dignissimos provident ut rerum rem dolore magnam eum.", new DateTime(2022, 6, 22, 10, 57, 48, 756, DateTimeKind.Unspecified).AddTicks(1551), "Omnis error.", 30, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 10, 12, 7, 39, 41, 192, DateTimeKind.Unspecified).AddTicks(252), "Schmidt, Schroeder and Yost" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 12, 6, 14, 7, 24, 323, DateTimeKind.Unspecified).AddTicks(9937), "Lakin LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 7, 16, 0, 31, 52, 238, DateTimeKind.Unspecified).AddTicks(5472), "Beahan LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 5, 24, 18, 22, 34, 521, DateTimeKind.Unspecified).AddTicks(2542), "Collins, Blick and Thompson" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 5, 30, 17, 39, 1, 438, DateTimeKind.Unspecified).AddTicks(9948), "Wilderman, Spencer and Bailey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 12, 25, 23, 10, 44, 143, DateTimeKind.Unspecified).AddTicks(3560), "Wisozk - Crist" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 9, 27, 20, 0, 49, 4, DateTimeKind.Unspecified).AddTicks(6959), "Hyatt Inc" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 11, 6, 11, 45, 35, 702, DateTimeKind.Unspecified).AddTicks(7188), "Wolff, Dietrich and Feest" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 8, 10, 22, 38, 11, 620, DateTimeKind.Unspecified).AddTicks(3341), "Gerhold LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 7, 11, 17, 16, 42, 925, DateTimeKind.Unspecified).AddTicks(5716), "Bode, Ratke and Keeling" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 6, 1, 0, 1, 4, 959, DateTimeKind.Unspecified).AddTicks(6055), "Audreanne_Wintheiser@hotmail.com", "Jayden", "Trantow", new DateTime(2015, 12, 30, 16, 32, 19, 280, DateTimeKind.Unspecified).AddTicks(1484), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 3, 20, 15, 26, 706, DateTimeKind.Unspecified).AddTicks(1494), "Nellie_Veum@yahoo.com", "Watson", "Cronin", new DateTime(2017, 2, 16, 22, 16, 41, 20, DateTimeKind.Unspecified).AddTicks(4600), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 9, 21, 11, 8, 30, 266, DateTimeKind.Unspecified).AddTicks(5389), "Cullen_Hand54@hotmail.com", "Napoleon", "Schaden", new DateTime(2015, 1, 17, 8, 59, 34, 193, DateTimeKind.Unspecified).AddTicks(3706) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 7, 21, 3, 9, 37, 796, DateTimeKind.Unspecified).AddTicks(1867), "Nicholaus_Paucek82@hotmail.com", "Darrin", "Jaskolski", new DateTime(2015, 6, 25, 19, 36, 0, 201, DateTimeKind.Unspecified).AddTicks(6079), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 21, 23, 59, 56, 24, DateTimeKind.Unspecified).AddTicks(6701), "Grayce.Koepp@gmail.com", "Chanelle", "Huel", new DateTime(2011, 2, 24, 16, 52, 42, 552, DateTimeKind.Unspecified).AddTicks(1052), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 6, 2, 3, 20, 6, 29, DateTimeKind.Unspecified).AddTicks(1206), "Dagmar.Trantow@yahoo.com", "Norbert", "Murray", new DateTime(2010, 12, 5, 0, 45, 15, 575, DateTimeKind.Unspecified).AddTicks(4941), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 1, 10, 11, 57, 38, 2, DateTimeKind.Unspecified).AddTicks(6734), "Erin_Boyle93@hotmail.com", "Herman", "Roob", new DateTime(2016, 12, 5, 21, 44, 45, 954, DateTimeKind.Unspecified).AddTicks(7710), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 15, 18, 8, 5, 289, DateTimeKind.Unspecified).AddTicks(6692), "Gilbert.Kunze94@gmail.com", "Olen", "Wisoky", new DateTime(2010, 6, 8, 21, 21, 18, 399, DateTimeKind.Unspecified).AddTicks(9814), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 9, 18, 14, 54, 36, 795, DateTimeKind.Unspecified).AddTicks(9632), "Sadye.Yost@hotmail.com", "Deon", "Adams", new DateTime(2014, 7, 19, 19, 34, 0, 712, DateTimeKind.Unspecified).AddTicks(6996), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 8, 18, 2, 46, 2, 627, DateTimeKind.Unspecified).AddTicks(4904), "Wilton.Dibbert@hotmail.com", "Genevieve", "Dooley", new DateTime(2011, 3, 25, 19, 45, 3, 548, DateTimeKind.Unspecified).AddTicks(4988), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 6, 3, 30, 30, 395, DateTimeKind.Unspecified).AddTicks(7937), "Sallie.Kemmer94@hotmail.com", "Brycen", "Weissnat", new DateTime(2010, 9, 3, 16, 29, 36, 216, DateTimeKind.Unspecified).AddTicks(2097), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 7, 27, 6, 37, 1, 631, DateTimeKind.Unspecified).AddTicks(4836), "Wendell85@hotmail.com", "Jon", "Ruecker", new DateTime(2016, 5, 28, 20, 30, 22, 526, DateTimeKind.Unspecified).AddTicks(7003) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 30, 18, 42, 18, 241, DateTimeKind.Unspecified).AddTicks(5962), "Dulce82@yahoo.com", "Elda", "Gerlach", new DateTime(2014, 8, 20, 7, 4, 26, 741, DateTimeKind.Unspecified).AddTicks(1752), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1995, 6, 7, 15, 1, 44, 388, DateTimeKind.Unspecified).AddTicks(1517), "Cornelius_Spinka54@gmail.com", "Maximillia", "Watsica", new DateTime(2017, 12, 9, 12, 45, 54, 982, DateTimeKind.Unspecified).AddTicks(9860) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 29, 20, 58, 52, 805, DateTimeKind.Unspecified).AddTicks(6368), "Aaron_Littel69@hotmail.com", "Lelia", "Wisoky", new DateTime(2010, 3, 26, 7, 27, 39, 700, DateTimeKind.Unspecified).AddTicks(7544), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 1, 28, 13, 18, 41, 836, DateTimeKind.Unspecified).AddTicks(8146), "Imelda37@gmail.com", "Genoveva", "Hamill", new DateTime(2016, 11, 30, 9, 22, 54, 618, DateTimeKind.Unspecified).AddTicks(9415), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 12, 12, 14, 55, 49, 69, DateTimeKind.Unspecified).AddTicks(8159), "Anastacio.Ziemann@yahoo.com", "Josefa", "Kuphal", new DateTime(2010, 3, 14, 16, 8, 32, 543, DateTimeKind.Unspecified).AddTicks(1494), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 11, 25, 3, 3, 34, 407, DateTimeKind.Unspecified).AddTicks(7610), "Emmie.Collins@hotmail.com", "Bruce", "Kohler", new DateTime(2013, 5, 26, 0, 17, 14, 953, DateTimeKind.Unspecified).AddTicks(3384), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 5, 8, 39, 12, 557, DateTimeKind.Unspecified).AddTicks(6003), "Nathanial.Kuhlman@gmail.com", "Juston", "Roberts", new DateTime(2013, 3, 18, 10, 40, 0, 988, DateTimeKind.Unspecified).AddTicks(5022), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 26, 22, 4, 52, 323, DateTimeKind.Unspecified).AddTicks(7570), "Bailey19@hotmail.com", "Barrett", "Stroman", new DateTime(2017, 12, 31, 17, 56, 56, 145, DateTimeKind.Unspecified).AddTicks(2776), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 3, 22, 17, 4, 15, 152, DateTimeKind.Unspecified).AddTicks(952), "Jadon.Fadel@yahoo.com", "Jacinthe", "Schiller", new DateTime(2015, 1, 7, 8, 14, 11, 588, DateTimeKind.Unspecified).AddTicks(6813) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 5, 19, 13, 20, 20, 332, DateTimeKind.Unspecified).AddTicks(8106), "Valerie.Bergstrom@hotmail.com", "Tillman", "Koelpin", new DateTime(2012, 4, 21, 17, 55, 15, 427, DateTimeKind.Unspecified).AddTicks(3254), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1994, 3, 6, 23, 6, 27, 10, DateTimeKind.Unspecified).AddTicks(5616), "Allene_Sanford@hotmail.com", "Emmalee", "Treutel", new DateTime(2012, 7, 8, 10, 5, 44, 312, DateTimeKind.Unspecified).AddTicks(8590) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 4, 20, 22, 22, 48, 324, DateTimeKind.Unspecified).AddTicks(4412), "Jeremy_Wiza5@yahoo.com", "Ara", "Labadie", new DateTime(2012, 12, 12, 2, 45, 3, 89, DateTimeKind.Unspecified).AddTicks(2343), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 9, 15, 9, 22, 14, 28, DateTimeKind.Unspecified).AddTicks(7740), "Eladio66@yahoo.com", "Daphne", "Hyatt", new DateTime(2012, 3, 2, 0, 51, 13, 56, DateTimeKind.Unspecified).AddTicks(5750) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 12, 17, 44, 8, 208, DateTimeKind.Unspecified).AddTicks(4290), "Katelynn14@gmail.com", "Eugenia", "Ledner", new DateTime(2014, 6, 24, 7, 7, 35, 75, DateTimeKind.Unspecified).AddTicks(3511), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 31, 10, 55, 29, 604, DateTimeKind.Unspecified).AddTicks(7688), "Camren.Emmerich91@gmail.com", "Itzel", "Sanford", new DateTime(2011, 11, 1, 21, 40, 36, 794, DateTimeKind.Unspecified).AddTicks(8899), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 10, 5, 28, 41, 528, DateTimeKind.Unspecified).AddTicks(8497), "Trinity.Monahan76@yahoo.com", "Clemens", "Upton", new DateTime(2014, 11, 10, 20, 32, 58, 273, DateTimeKind.Unspecified).AddTicks(2900), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 12, 12, 12, 54, 29, 421, DateTimeKind.Unspecified).AddTicks(2329), "Onie_Boehm41@hotmail.com", "Luz", "Beer", new DateTime(2016, 5, 14, 0, 28, 19, 666, DateTimeKind.Unspecified).AddTicks(4911), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 1, 9, 16, 58, 38, 433, DateTimeKind.Unspecified).AddTicks(7961), "Terrill.Keebler@hotmail.com", "Madisyn", "Stark", new DateTime(2012, 1, 2, 15, 27, 26, 30, DateTimeKind.Unspecified).AddTicks(3389), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 10, 16, 16, 37, 47, 439, DateTimeKind.Unspecified).AddTicks(9826), "Abigayle_Huel@hotmail.com", "Natalia", "Lakin", new DateTime(2012, 9, 11, 23, 45, 24, 281, DateTimeKind.Unspecified).AddTicks(7926), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 5, 10, 22, 28, 5, 572, DateTimeKind.Unspecified).AddTicks(4091), "Carlee_Streich@gmail.com", "Lora", "Rice", new DateTime(2014, 4, 9, 9, 54, 56, 521, DateTimeKind.Unspecified).AddTicks(3624), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1994, 7, 24, 2, 8, 7, 872, DateTimeKind.Unspecified).AddTicks(2964), "Omari.DAmore@hotmail.com", "Leta", "Howe", new DateTime(2012, 1, 7, 20, 3, 1, 756, DateTimeKind.Unspecified).AddTicks(7552) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 12, 8, 22, 39, 27, 499, DateTimeKind.Unspecified).AddTicks(918), "Hester.OReilly@hotmail.com", "Claude", "Tromp", new DateTime(2011, 9, 6, 2, 34, 22, 795, DateTimeKind.Unspecified).AddTicks(474), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 2, 2, 48, 0, 28, DateTimeKind.Unspecified).AddTicks(9237), "Evelyn40@hotmail.com", "Lester", "Muller", new DateTime(2013, 5, 5, 11, 39, 8, 277, DateTimeKind.Unspecified).AddTicks(187), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 3, 3, 7, 42, 50, 372, DateTimeKind.Unspecified).AddTicks(8955), "Fausto10@yahoo.com", "Oma", "Quitzon", new DateTime(2010, 4, 23, 14, 39, 55, 871, DateTimeKind.Unspecified).AddTicks(6162), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 7, 21, 3, 59, 17, 735, DateTimeKind.Unspecified).AddTicks(5208), "Jaiden11@hotmail.com", "Kennedy", "Ferry", new DateTime(2012, 8, 6, 22, 2, 22, 474, DateTimeKind.Unspecified).AddTicks(8679), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 10, 3, 14, 55, 203, DateTimeKind.Unspecified).AddTicks(5717), "Raleigh.Bode@yahoo.com", "Emmalee", "Stracke", new DateTime(2015, 2, 2, 18, 48, 33, 177, DateTimeKind.Unspecified).AddTicks(5350), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 5, 15, 2, 19, 16, 724, DateTimeKind.Unspecified).AddTicks(4108), "Mario_OConnell@hotmail.com", "Daphney", "Gibson", new DateTime(2011, 10, 19, 9, 22, 52, 622, DateTimeKind.Unspecified).AddTicks(7511) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 6, 14, 23, 17, 14, 446, DateTimeKind.Unspecified).AddTicks(2595), "Alan_Dibbert45@hotmail.com", "Carter", "Anderson", new DateTime(2014, 1, 9, 20, 13, 39, 290, DateTimeKind.Unspecified).AddTicks(802), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 23, 1, 12, 16, 406, DateTimeKind.Unspecified).AddTicks(4954), "Hailee.Romaguera9@hotmail.com", "Mariela", "West", new DateTime(2017, 4, 12, 10, 14, 2, 895, DateTimeKind.Unspecified).AddTicks(3291), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 1, 25, 9, 40, 9, 436, DateTimeKind.Unspecified).AddTicks(4478), "Kim_Larkin@yahoo.com", "Kylee", "VonRueden", new DateTime(2015, 12, 16, 19, 39, 50, 235, DateTimeKind.Unspecified).AddTicks(7815), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 10, 14, 14, 6, 768, DateTimeKind.Unspecified).AddTicks(5128), "Sheridan.Pouros@hotmail.com", "Rusty", "Simonis", new DateTime(2010, 6, 22, 15, 21, 40, 999, DateTimeKind.Unspecified).AddTicks(7421), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 4, 14, 18, 51, 900, DateTimeKind.Unspecified).AddTicks(8430), "Blair_Dietrich@hotmail.com", "Gwendolyn", "Huel", new DateTime(2016, 9, 13, 14, 23, 14, 543, DateTimeKind.Unspecified).AddTicks(3635), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 5, 17, 49, 51, 695, DateTimeKind.Unspecified).AddTicks(3982), "Alana_Johnson@hotmail.com", "Pink", "Gutkowski", new DateTime(2016, 4, 30, 12, 59, 15, 65, DateTimeKind.Unspecified).AddTicks(5554), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 12, 4, 17, 43, 2, 251, DateTimeKind.Unspecified).AddTicks(8498), "Clovis58@gmail.com", "Elyssa", "Frami", new DateTime(2016, 1, 8, 14, 15, 27, 415, DateTimeKind.Unspecified).AddTicks(3730), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 29, 2, 19, 24, 32, DateTimeKind.Unspecified).AddTicks(3483), "Adaline.Prohaska73@gmail.com", "Markus", "Mitchell", new DateTime(2010, 7, 25, 4, 0, 52, 503, DateTimeKind.Unspecified).AddTicks(1025), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 15, 14, 14, 15, 2, DateTimeKind.Unspecified).AddTicks(1329), "Parker.Hessel@gmail.com", "Ruby", "Botsford", new DateTime(2011, 5, 23, 8, 9, 38, 557, DateTimeKind.Unspecified).AddTicks(1173), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 21, 22, 19, 58, 201, DateTimeKind.Unspecified).AddTicks(190), "Troy.Gerlach@yahoo.com", "Rickey", "Wolf", new DateTime(2014, 7, 28, 11, 43, 0, 399, DateTimeKind.Unspecified).AddTicks(2138), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 7, 17, 35, 26, 446, DateTimeKind.Unspecified).AddTicks(5008), "Deondre.Dietrich87@yahoo.com", "Madonna", "Hane", new DateTime(2017, 11, 29, 16, 8, 3, 828, DateTimeKind.Unspecified).AddTicks(6722), 9 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Task",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 41, new DateTime(2019, 1, 16, 7, 42, 55, 912, DateTimeKind.Unspecified).AddTicks(2303), new DateTime(2022, 8, 7, 16, 0, 36, 290, DateTimeKind.Unspecified).AddTicks(7596), "Et iste sed et asperiores quia.", "Rustic Rubber Fish", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 1, 23, 1, 29, 47, 658, DateTimeKind.Unspecified).AddTicks(8066), new DateTime(2022, 9, 15, 15, 53, 31, 973, DateTimeKind.Unspecified).AddTicks(7692), "Repudiandae placeat sapiente.", "Rustic Frozen Bacon", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 10, 27, 17, 14, 32, 765, DateTimeKind.Unspecified).AddTicks(5602), new DateTime(2022, 3, 18, 15, 48, 29, 138, DateTimeKind.Unspecified).AddTicks(88), "Libero qui doloremque quia fugiat nemo dignissimos fugit fugiat.", "Licensed Granite Soap", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 40, new DateTime(2019, 6, 27, 4, 2, 12, 777, DateTimeKind.Unspecified).AddTicks(1911), new DateTime(2021, 9, 27, 3, 22, 13, 581, DateTimeKind.Unspecified).AddTicks(1424), "Cumque sed dolores nam doloremque.", "Ergonomic Rubber Salad", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 10, 11, 16, 47, 57, 547, DateTimeKind.Unspecified).AddTicks(6640), new DateTime(2021, 11, 6, 20, 56, 53, 50, DateTimeKind.Unspecified).AddTicks(6960), "Recusandae quidem atque vel cumque ut.", "Handcrafted Rubber Gloves", 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 26, 2, 49, 56, 158, DateTimeKind.Unspecified).AddTicks(3581), "Quos et exercitationem dolores laborum.", new DateTime(2022, 3, 1, 20, 53, 15, 428, DateTimeKind.Unspecified).AddTicks(3535), "Architecto nihil.", 44, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 4, 14, 36, 9, 703, DateTimeKind.Unspecified).AddTicks(5834), "Porro et est.", new DateTime(2021, 6, 23, 10, 48, 4, 261, DateTimeKind.Unspecified).AddTicks(2580), "Dicta atque.", 36, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 8, 7, 10, 45, 36, DateTimeKind.Unspecified).AddTicks(3988), "Quidem harum est qui velit neque esse dolor non.", new DateTime(2022, 4, 26, 2, 29, 5, 889, DateTimeKind.Unspecified).AddTicks(9124), "Ut velit.", 35, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 10, 12, 22, 2, 33, 881, DateTimeKind.Unspecified).AddTicks(2254), "Non mollitia voluptatum.", new DateTime(2021, 1, 7, 14, 39, 14, 119, DateTimeKind.Unspecified).AddTicks(5123), "Deserunt eligendi.", 47 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 12, 7, 6, 34, 13, 101, DateTimeKind.Unspecified).AddTicks(4228), "Asperiores quasi rerum ullam.", new DateTime(2021, 6, 23, 22, 19, 9, 868, DateTimeKind.Unspecified).AddTicks(8906), "Voluptatem fugiat.", 44, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 4, 20, 15, 3, 245, DateTimeKind.Unspecified).AddTicks(6368), "Ipsam aspernatur dolore vitae.", new DateTime(2022, 5, 5, 4, 40, 12, 814, DateTimeKind.Unspecified).AddTicks(5846), "Expedita ex.", 7, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 24, 13, 28, 48, 112, DateTimeKind.Unspecified).AddTicks(2962), "Nihil fuga magnam ipsam.", new DateTime(2022, 2, 3, 9, 20, 28, 842, DateTimeKind.Unspecified).AddTicks(2726), "Consectetur ut.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 24, 21, 47, 29, 40, DateTimeKind.Unspecified).AddTicks(1646), "Eum provident nisi.", new DateTime(2022, 4, 29, 17, 22, 27, 867, DateTimeKind.Unspecified).AddTicks(9031), "Ut unde.", 4, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 8, 14, 47, 44, 459, DateTimeKind.Unspecified).AddTicks(8855), "Similique blanditiis blanditiis quia voluptate dicta qui.", new DateTime(2022, 9, 23, 11, 13, 47, 152, DateTimeKind.Unspecified).AddTicks(7799), "Maiores quibusdam.", 13, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 1, 30, 50, 269, DateTimeKind.Unspecified).AddTicks(6950), "Deserunt sed vel provident qui.", new DateTime(2021, 10, 24, 5, 39, 40, 612, DateTimeKind.Unspecified).AddTicks(8562), "Animi repellendus.", 42, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 8, 12, 46, 655, DateTimeKind.Unspecified).AddTicks(5228), "Saepe ab culpa omnis.", new DateTime(2021, 3, 4, 18, 23, 25, 58, DateTimeKind.Unspecified).AddTicks(6018), "In in.", 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 17, 58, 8, 685, DateTimeKind.Unspecified).AddTicks(1260), "Quia eum omnis sint quia dolor rerum neque temporibus qui.", new DateTime(2022, 2, 8, 21, 23, 45, 314, DateTimeKind.Unspecified).AddTicks(3224), "Nulla rerum.", 8, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 7, 30, 2, 47, 44, 142, DateTimeKind.Unspecified).AddTicks(4567), "Qui quo porro nisi fugit autem aliquid.", new DateTime(2021, 4, 25, 16, 30, 33, 69, DateTimeKind.Unspecified).AddTicks(8962), "Porro expedita.", 24 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 13, 8, 42, 136, DateTimeKind.Unspecified).AddTicks(6027), "Provident rerum voluptatem possimus.", new DateTime(2022, 6, 9, 20, 28, 4, 711, DateTimeKind.Unspecified).AddTicks(7908), "Qui qui.", 41, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 26, 14, 41, 51, 906, DateTimeKind.Unspecified).AddTicks(5939), "Sit atque perspiciatis vitae corporis eius et voluptas quos quidem.", new DateTime(2021, 4, 2, 20, 5, 1, 318, DateTimeKind.Unspecified).AddTicks(388), "Aliquam ut.", 30, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 16, 7, 7, 33, 250, DateTimeKind.Unspecified).AddTicks(2532), "Hic corrupti excepturi et dolores molestiae.", new DateTime(2022, 11, 8, 5, 50, 1, 722, DateTimeKind.Unspecified).AddTicks(7278), "Dolorem qui.", 47, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 8, 2, 16, 19, 601, DateTimeKind.Unspecified).AddTicks(5781), "Voluptatem architecto vel odio consequatur sit dignissimos.", new DateTime(2022, 10, 24, 1, 4, 37, 247, DateTimeKind.Unspecified).AddTicks(8202), "Est excepturi.", 4, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 15, 14, 20, 18, 549, DateTimeKind.Unspecified).AddTicks(4692), "Deserunt aut fugiat cum et sit est sapiente quasi sunt.", new DateTime(2022, 2, 13, 3, 50, 10, 83, DateTimeKind.Unspecified).AddTicks(7028), "Nisi quaerat.", 35, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 11, 11, 12, 50, 15, 631, DateTimeKind.Unspecified).AddTicks(3902), "Impedit voluptatem omnis enim est dignissimos velit.", new DateTime(2022, 4, 24, 10, 59, 30, 911, DateTimeKind.Unspecified).AddTicks(4227), "Debitis iste.", 40 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 16, 10, 57, 13, 535, DateTimeKind.Unspecified).AddTicks(944), "Sequi totam necessitatibus quia voluptas enim eveniet est.", new DateTime(2022, 7, 6, 4, 56, 57, 167, DateTimeKind.Unspecified).AddTicks(7645), "Dolore qui.", 37, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 21, 51, 29, 984, DateTimeKind.Unspecified).AddTicks(137), "Sint magni rerum blanditiis deserunt quam in rerum est rem.", new DateTime(2021, 8, 27, 15, 24, 21, 570, DateTimeKind.Unspecified).AddTicks(3725), "Laborum iure.", 39, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 9, 18, 4, 19, 348, DateTimeKind.Unspecified).AddTicks(6001), "Commodi officia sed occaecati voluptatem iste est.", new DateTime(2021, 5, 15, 6, 15, 26, 901, DateTimeKind.Unspecified).AddTicks(8559), "Laudantium vel.", 9, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 11, 22, 19, 26, 311, DateTimeKind.Unspecified).AddTicks(1622), "Provident quisquam nihil.", new DateTime(2022, 9, 14, 11, 45, 44, 720, DateTimeKind.Unspecified).AddTicks(7216), "Aspernatur est.", 33, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 2, 14, 34, 275, DateTimeKind.Unspecified).AddTicks(482), "Qui in iste labore.", new DateTime(2021, 1, 24, 10, 53, 28, 860, DateTimeKind.Unspecified).AddTicks(9215), "Ratione magnam.", 14, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 10, 4, 30, 38, 817, DateTimeKind.Unspecified).AddTicks(4730), "Magni accusamus cumque omnis nisi et.", new DateTime(2021, 1, 14, 9, 12, 22, 864, DateTimeKind.Unspecified).AddTicks(9829), "Cum amet.", 19, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 2, 54, 17, 1, DateTimeKind.Unspecified).AddTicks(4925), "Numquam natus aut quo qui vel ex nulla.", new DateTime(2022, 9, 17, 20, 26, 58, 559, DateTimeKind.Unspecified).AddTicks(550), "Possimus voluptatem.", 24, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 9, 17, 23, 43, 40, 237, DateTimeKind.Unspecified).AddTicks(227), "Minima odit iure ducimus modi non sint.", new DateTime(2021, 2, 28, 2, 23, 10, 890, DateTimeKind.Unspecified).AddTicks(8282), "Voluptatem quia.", 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 17, 36, 36, 654, DateTimeKind.Unspecified).AddTicks(9358), "Nihil facilis voluptatem facere pariatur animi quaerat eos deserunt.", new DateTime(2022, 1, 24, 14, 37, 26, 90, DateTimeKind.Unspecified).AddTicks(7846), "Perspiciatis autem.", 29, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 16, 13, 13, 23, 142, DateTimeKind.Unspecified).AddTicks(7619), "Blanditiis maxime asperiores quo nisi ab autem repudiandae.", new DateTime(2021, 3, 15, 17, 21, 39, 405, DateTimeKind.Unspecified).AddTicks(7389), "Nisi quia.", 46, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 12, 43, 56, 206, DateTimeKind.Unspecified).AddTicks(5495), "Aspernatur quaerat aut non at incidunt earum provident.", new DateTime(2021, 3, 1, 19, 28, 26, 785, DateTimeKind.Unspecified).AddTicks(4513), "Consequatur inventore.", 49, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 6, 15, 15, 28, 13, 348, DateTimeKind.Unspecified).AddTicks(3711), "Temporibus optio quasi repellendus.", new DateTime(2022, 6, 30, 15, 51, 43, 169, DateTimeKind.Unspecified).AddTicks(8121), "Enim et.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 26, 8, 39, 41, 843, DateTimeKind.Unspecified).AddTicks(7798), "Dicta rerum rem deleniti velit aut dignissimos omnis exercitationem rerum.", new DateTime(2021, 12, 12, 13, 22, 10, 292, DateTimeKind.Unspecified).AddTicks(9695), "Quia rerum.", 7, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 27, 16, 14, 28, 516, DateTimeKind.Unspecified).AddTicks(1082), "Officiis non sint quia ipsa soluta.", new DateTime(2021, 2, 1, 7, 56, 34, 1, DateTimeKind.Unspecified).AddTicks(3688), "Laudantium aut.", 28, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 23, 54, 26, 625, DateTimeKind.Unspecified).AddTicks(430), "Molestiae saepe quod.", new DateTime(2022, 10, 3, 5, 3, 50, 206, DateTimeKind.Unspecified).AddTicks(1179), "Saepe ipsam.", 4, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 6, 21, 58, 16, 134, DateTimeKind.Unspecified).AddTicks(6106), "Aut illo similique nesciunt iusto eum sint explicabo.", new DateTime(2022, 9, 30, 22, 2, 24, 295, DateTimeKind.Unspecified).AddTicks(2006), "Veniam delectus.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 21, 21, 41, 16, 153, DateTimeKind.Unspecified).AddTicks(44), "Molestiae minus ratione.", new DateTime(2021, 9, 26, 13, 47, 55, 151, DateTimeKind.Unspecified).AddTicks(5236), "Minima tempora.", 44, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 23, 36, 4, 909, DateTimeKind.Unspecified).AddTicks(3335), "Dignissimos in tenetur neque.", new DateTime(2021, 2, 3, 19, 5, 14, 127, DateTimeKind.Unspecified).AddTicks(9477), "Voluptate voluptatem.", 40, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 21, 4, 20, 15, 391, DateTimeKind.Unspecified).AddTicks(7680), "Ea dolore facere quos reprehenderit id.", new DateTime(2022, 1, 31, 11, 3, 44, 816, DateTimeKind.Unspecified).AddTicks(4717), "Quia magni.", 49, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 17, 6, 53, 4, 940, DateTimeKind.Unspecified).AddTicks(196), "Ea rem rerum voluptas consequatur perferendis velit.", new DateTime(2022, 12, 31, 20, 45, 24, 679, DateTimeKind.Unspecified).AddTicks(5671), "Dolorum a.", 42, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 4, 8, 8, 55, 27, 188, DateTimeKind.Unspecified).AddTicks(2643), "Quasi aliquid ut.", new DateTime(2021, 1, 19, 23, 55, 26, 35, DateTimeKind.Unspecified).AddTicks(6506), "Sed dolores.", 43 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 2, 3, 19, 7, 42, 24, DateTimeKind.Unspecified).AddTicks(7462), "Quia et sit assumenda est pariatur facere autem.", new DateTime(2022, 2, 15, 0, 12, 35, 791, DateTimeKind.Unspecified).AddTicks(7990), "Repudiandae et.", 17 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 8, 11, 58, 353, DateTimeKind.Unspecified).AddTicks(4697), "Dolore commodi nesciunt libero natus inventore.", new DateTime(2022, 10, 19, 18, 7, 44, 519, DateTimeKind.Unspecified).AddTicks(1109), "Dolor vel.", 1, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 18, 15, 32, 42, 96, DateTimeKind.Unspecified).AddTicks(2443), "Adipisci nesciunt iure id amet iusto architecto nisi exercitationem.", new DateTime(2021, 11, 14, 5, 59, 57, 379, DateTimeKind.Unspecified).AddTicks(3012), "Et nihil.", 5, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 15, 15, 6, 46, 373, DateTimeKind.Unspecified).AddTicks(1315), "Quibusdam animi dolorem ipsa sed.", new DateTime(2022, 3, 27, 7, 24, 44, 448, DateTimeKind.Unspecified).AddTicks(5594), "Reiciendis labore.", 46, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 9, 16, 13, 31, 793, DateTimeKind.Unspecified).AddTicks(1442), "A nisi qui.", new DateTime(2022, 9, 20, 21, 16, 10, 161, DateTimeKind.Unspecified).AddTicks(8037), "Quis aut.", 15, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 25, 22, 20, 36, 965, DateTimeKind.Unspecified).AddTicks(5915), "Est autem ratione provident veniam quasi omnis voluptatem quaerat minus.", new DateTime(2021, 12, 24, 7, 43, 31, 53, DateTimeKind.Unspecified).AddTicks(7698), "Aut et.", 29, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 27, 14, 54, 27, 99, DateTimeKind.Unspecified).AddTicks(2466), "Aut odit cumque qui tempora veritatis assumenda dignissimos quos amet.", new DateTime(2021, 11, 1, 6, 9, 52, 265, DateTimeKind.Unspecified).AddTicks(5530), "Fuga atque.", 26, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 20, 12, 11, 22, 100, DateTimeKind.Unspecified).AddTicks(836), "Quod iste id magnam deserunt adipisci quis et aliquam.", new DateTime(2021, 1, 27, 10, 40, 42, 17, DateTimeKind.Unspecified).AddTicks(7477), "Velit aut.", 29, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 24, 7, 43, 57, 394, DateTimeKind.Unspecified).AddTicks(4513), "Blanditiis omnis cum similique.", new DateTime(2022, 7, 11, 7, 21, 29, 581, DateTimeKind.Unspecified).AddTicks(10), "Velit sit.", 15, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 20, 15, 13, 636, DateTimeKind.Unspecified).AddTicks(9123), "Unde ex unde.", new DateTime(2022, 10, 10, 10, 21, 26, 433, DateTimeKind.Unspecified).AddTicks(1520), "Reiciendis dolorum.", 36, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 5, 4, 8, 847, DateTimeKind.Unspecified).AddTicks(9180), "Ullam et dolores est quia sit perferendis debitis.", new DateTime(2022, 3, 28, 11, 1, 42, 394, DateTimeKind.Unspecified).AddTicks(6372), "Natus eligendi.", 19, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 28, 12, 0, 4, 116, DateTimeKind.Unspecified).AddTicks(5784), "Dolor reprehenderit fuga nobis dolore.", new DateTime(2022, 9, 28, 2, 42, 49, 306, DateTimeKind.Unspecified).AddTicks(7357), "Sit sit.", 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 17, 37, 53, 831, DateTimeKind.Unspecified).AddTicks(5094), "Excepturi quia id nesciunt.", new DateTime(2022, 4, 29, 19, 41, 9, 781, DateTimeKind.Unspecified).AddTicks(3855), "Occaecati velit.", 26, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 15, 10, 4, 17, 350, DateTimeKind.Unspecified).AddTicks(6486), "Voluptatem voluptates animi rem.", new DateTime(2022, 7, 20, 8, 50, 14, 618, DateTimeKind.Unspecified).AddTicks(4610), "Sint eius.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 14, 19, 18, 0, 15, DateTimeKind.Unspecified).AddTicks(2302), "Et ab sunt.", new DateTime(2022, 10, 17, 2, 24, 28, 211, DateTimeKind.Unspecified).AddTicks(3967), "Eligendi omnis.", 37, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 7, 11, 26, 54, 337, DateTimeKind.Unspecified).AddTicks(4298), "Optio officiis facere error a architecto consequuntur.", new DateTime(2021, 12, 3, 17, 48, 51, 990, DateTimeKind.Unspecified).AddTicks(2460), "Delectus eum.", 36, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 6, 38, 44, 933, DateTimeKind.Unspecified).AddTicks(3367), "Magnam quia dolorem vel in ullam magnam animi.", new DateTime(2021, 2, 3, 21, 48, 23, 491, DateTimeKind.Unspecified).AddTicks(407), "Aspernatur ipsam.", 35, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 9, 5, 59, 989, DateTimeKind.Unspecified).AddTicks(3258), "Quis voluptatibus iste in praesentium voluptates.", new DateTime(2022, 6, 9, 18, 56, 21, 150, DateTimeKind.Unspecified).AddTicks(4927), "Ad quia.", 16, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 16, 23, 28, 2, 976, DateTimeKind.Unspecified).AddTicks(8095), "Eos molestiae consequatur temporibus.", new DateTime(2022, 3, 21, 18, 52, 45, 952, DateTimeKind.Unspecified).AddTicks(8471), "Est sed.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 12, 4, 43, 50, DateTimeKind.Unspecified).AddTicks(7371), "Rerum rem aut esse accusantium laudantium qui.", new DateTime(2021, 8, 18, 0, 46, 27, 286, DateTimeKind.Unspecified).AddTicks(7059), "Dolor alias.", 48, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 18, 11, 24, 28, 836, DateTimeKind.Unspecified).AddTicks(5971), "Rerum qui omnis labore molestiae.", new DateTime(2022, 6, 18, 21, 53, 34, 310, DateTimeKind.Unspecified).AddTicks(5253), "Fugiat iure.", 3, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 0, 19, 2, 175, DateTimeKind.Unspecified).AddTicks(6451), "Eaque quia iste soluta.", new DateTime(2021, 4, 7, 23, 15, 9, 161, DateTimeKind.Unspecified).AddTicks(708), "Fugiat quia.", 8, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 2, 14, 19, 41, 852, DateTimeKind.Unspecified).AddTicks(6612), "Sint aut maiores sed rem nulla similique.", new DateTime(2022, 7, 2, 4, 8, 59, 684, DateTimeKind.Unspecified).AddTicks(8423), "Minima consequuntur.", 6, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 20, 0, 13, 25, 771, DateTimeKind.Unspecified).AddTicks(4455), "Error quidem aut nisi nisi quia debitis numquam voluptatibus.", new DateTime(2022, 5, 15, 16, 18, 17, 322, DateTimeKind.Unspecified).AddTicks(166), "Reiciendis itaque.", 35, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 0, 10, 6, 636, DateTimeKind.Unspecified).AddTicks(3432), "Provident consequatur delectus aut quia quaerat ipsa sint placeat est.", new DateTime(2021, 8, 5, 7, 10, 14, 313, DateTimeKind.Unspecified).AddTicks(4785), "Laudantium sint.", 31, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 7, 11, 12, 967, DateTimeKind.Unspecified).AddTicks(7704), "Eos necessitatibus sit beatae ut.", new DateTime(2021, 10, 9, 5, 48, 48, 593, DateTimeKind.Unspecified).AddTicks(9437), "Nam dolorem.", 43, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 7, 23, 48, 605, DateTimeKind.Unspecified).AddTicks(2335), "Laudantium nam rem.", new DateTime(2022, 6, 6, 0, 20, 6, 177, DateTimeKind.Unspecified).AddTicks(9862), "Officiis culpa.", 41, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 8, 7, 37, 341, DateTimeKind.Unspecified).AddTicks(607), "Debitis officia cumque aliquid.", new DateTime(2022, 4, 18, 21, 7, 44, 962, DateTimeKind.Unspecified).AddTicks(7885), "Iure quis.", 1, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 7, 14, 16, 26, 459, DateTimeKind.Unspecified).AddTicks(3783), "Aut autem repudiandae atque sunt nemo.", new DateTime(2021, 12, 19, 7, 55, 35, 748, DateTimeKind.Unspecified).AddTicks(7261), "Vero consequuntur.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 30, 4, 47, 44, 150, DateTimeKind.Unspecified).AddTicks(573), "Aut modi perferendis exercitationem voluptatem.", new DateTime(2022, 3, 20, 14, 58, 40, 507, DateTimeKind.Unspecified).AddTicks(3515), "Laboriosam et.", 11, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 30, 20, 4, 31, 557, DateTimeKind.Unspecified).AddTicks(5796), "Ipsam expedita reprehenderit ab omnis adipisci maxime incidunt et.", new DateTime(2021, 11, 13, 11, 59, 13, 659, DateTimeKind.Unspecified).AddTicks(9965), "Hic ut.", 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 4, 11, 58, 29, 194, DateTimeKind.Unspecified).AddTicks(5330), "Aut esse et.", new DateTime(2021, 10, 29, 20, 30, 50, 634, DateTimeKind.Unspecified).AddTicks(9714), "Excepturi veritatis.", 40, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 22, 9, 26, 1, 241, DateTimeKind.Unspecified).AddTicks(5014), "Libero velit consequatur et.", new DateTime(2021, 2, 27, 11, 25, 5, 470, DateTimeKind.Unspecified).AddTicks(8664), "Et et.", 19, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 23, 7, 52, 447, DateTimeKind.Unspecified).AddTicks(2905), "Illum est voluptatem.", new DateTime(2022, 3, 6, 1, 13, 50, 257, DateTimeKind.Unspecified).AddTicks(6517), "Consequatur qui.", 22, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 18, 59, 52, 994, DateTimeKind.Unspecified).AddTicks(353), "Officia doloremque eius quo quod animi ipsa.", new DateTime(2022, 1, 23, 23, 30, 42, 314, DateTimeKind.Unspecified).AddTicks(1030), "Tenetur nesciunt.", 50, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 7, 26, 30, 88, DateTimeKind.Unspecified).AddTicks(494), "Distinctio saepe ducimus natus maxime fuga et accusantium iusto.", new DateTime(2021, 4, 3, 10, 38, 42, 207, DateTimeKind.Unspecified).AddTicks(4553), "Consequatur vel.", 42, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 29, 15, 40, 55, 166, DateTimeKind.Unspecified).AddTicks(8889), "Odit tempora facere vel debitis.", new DateTime(2022, 1, 21, 2, 0, 14, 803, DateTimeKind.Unspecified).AddTicks(3454), "Tempore enim.", 29, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 0, 38, 3, 216, DateTimeKind.Unspecified).AddTicks(7593), "Itaque earum officiis numquam fugiat laudantium voluptate.", new DateTime(2022, 5, 16, 13, 43, 16, 113, DateTimeKind.Unspecified).AddTicks(3670), "Qui qui.", 23, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 11, 6, 9, 42, 10, 548, DateTimeKind.Unspecified).AddTicks(8305), "Ullam distinctio magni nemo reprehenderit cumque earum.", new DateTime(2021, 1, 25, 18, 47, 19, 907, DateTimeKind.Unspecified).AddTicks(7253), "Ipsam eum.", 23 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 11, 2, 59, 13, 532, DateTimeKind.Unspecified).AddTicks(6037), "Provident et reprehenderit ad harum.", new DateTime(2022, 11, 18, 1, 44, 37, 312, DateTimeKind.Unspecified).AddTicks(2539), "Et animi.", 42, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 3, 15, 43, 40, 164, DateTimeKind.Unspecified).AddTicks(488), "In blanditiis error autem et quod.", new DateTime(2021, 11, 26, 0, 45, 47, 860, DateTimeKind.Unspecified).AddTicks(6197), "Sit omnis.", 14, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 5, 6, 52, 11, 79, DateTimeKind.Unspecified).AddTicks(7722), "Expedita id id assumenda sed autem quidem iste quaerat.", new DateTime(2022, 10, 23, 10, 11, 22, 437, DateTimeKind.Unspecified).AddTicks(8375), "Et harum.", 39, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 20, 6, 20, 423, DateTimeKind.Unspecified).AddTicks(4830), "Et non et nam minus aut suscipit enim.", new DateTime(2021, 1, 7, 20, 28, 26, 426, DateTimeKind.Unspecified).AddTicks(7430), "Velit mollitia.", 18, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 27, 15, 47, 42, 732, DateTimeKind.Unspecified).AddTicks(3280), "Ut laboriosam itaque et non et ducimus earum eius.", new DateTime(2022, 10, 22, 5, 38, 28, 926, DateTimeKind.Unspecified).AddTicks(1051), "Fugiat quia.", 1, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 19, 14, 17, 54, 614, DateTimeKind.Unspecified).AddTicks(3528), "Libero id non tenetur molestiae commodi.", new DateTime(2022, 8, 19, 13, 5, 45, 481, DateTimeKind.Unspecified).AddTicks(40), "A sapiente.", 50, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 11, 22, 38, 7, 21, DateTimeKind.Unspecified).AddTicks(4643), "Illo aperiam deleniti id.", new DateTime(2021, 2, 11, 14, 8, 52, 636, DateTimeKind.Unspecified).AddTicks(7235), "In dolore.", 20, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 9, 10, 5, 852, DateTimeKind.Unspecified).AddTicks(2232), "Rerum qui eveniet dolores et.", new DateTime(2021, 9, 12, 0, 13, 4, 166, DateTimeKind.Unspecified).AddTicks(302), "Veritatis tempore.", 49, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 0, 55, 16, 868, DateTimeKind.Unspecified).AddTicks(9851), "Et itaque temporibus.", new DateTime(2022, 9, 10, 23, 14, 30, 488, DateTimeKind.Unspecified).AddTicks(1866), "Consequatur voluptatem.", 21, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 23, 9, 19, 744, DateTimeKind.Unspecified).AddTicks(276), "Quia praesentium et dolorem qui iure.", new DateTime(2022, 2, 19, 20, 45, 8, 580, DateTimeKind.Unspecified).AddTicks(1893), "Blanditiis culpa.", 13, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 23, 0, 15, 37, 452, DateTimeKind.Unspecified).AddTicks(6233), "Occaecati ut veniam.", new DateTime(2022, 8, 25, 16, 5, 7, 115, DateTimeKind.Unspecified).AddTicks(9265), "Magnam odio.", 28, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 9, 0, 5, 14, 91, DateTimeKind.Unspecified).AddTicks(5989), "Sed voluptates exercitationem enim nesciunt animi porro.", new DateTime(2021, 6, 26, 12, 1, 36, 714, DateTimeKind.Unspecified).AddTicks(2690), "Hic officia.", 34, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 14, 13, 37, 58, 254, DateTimeKind.Unspecified).AddTicks(7167), "Temporibus perferendis nam eum provident iure repudiandae officia.", new DateTime(2022, 2, 10, 20, 36, 23, 989, DateTimeKind.Unspecified).AddTicks(9858), "Quidem aut.", 20, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 10, 10, 40, 43, 975, DateTimeKind.Unspecified).AddTicks(4341), "Distinctio quis tenetur.", new DateTime(2022, 3, 29, 18, 33, 30, 926, DateTimeKind.Unspecified).AddTicks(692), "Similique itaque.", 16, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 5, 15, 14, 11, 636, DateTimeKind.Unspecified).AddTicks(9348), "Eaque impedit excepturi ea.", new DateTime(2022, 9, 30, 10, 56, 8, 818, DateTimeKind.Unspecified).AddTicks(5656), "Accusantium asperiores.", 34, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 14, 55, 33, 39, DateTimeKind.Unspecified).AddTicks(3185), "Vel qui dignissimos quisquam nobis ullam voluptatibus corrupti libero libero.", new DateTime(2021, 9, 1, 23, 54, 16, 927, DateTimeKind.Unspecified).AddTicks(3239), "Delectus et.", 32, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 16, 15, 10, 405, DateTimeKind.Unspecified).AddTicks(4293), "Ducimus qui nesciunt omnis consequatur modi et ducimus odio minima.", new DateTime(2021, 12, 6, 8, 31, 23, 238, DateTimeKind.Unspecified).AddTicks(6382), "Ratione illum.", 17, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 21, 12, 36, 7, 951, DateTimeKind.Unspecified).AddTicks(3208), "Ut at voluptatem.", new DateTime(2021, 5, 7, 5, 3, 52, 341, DateTimeKind.Unspecified).AddTicks(5568), "Nulla dignissimos.", 33, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 12, 23, 53, 43, 957, DateTimeKind.Unspecified).AddTicks(8303), "Molestiae blanditiis asperiores doloribus est amet minima ut aut voluptas.", new DateTime(2022, 11, 24, 2, 26, 21, 758, DateTimeKind.Unspecified).AddTicks(1096), "Sequi quasi.", 2, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 10, 11, 11, 38, 244, DateTimeKind.Unspecified).AddTicks(4445), "Ut dolore doloremque expedita suscipit ut.", new DateTime(2022, 9, 17, 10, 25, 23, 273, DateTimeKind.Unspecified).AddTicks(5062), "Odit et.", 36, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 13, 4, 44, 25, 503, DateTimeKind.Unspecified).AddTicks(2851), "At aperiam qui.", new DateTime(2021, 1, 30, 19, 18, 49, 612, DateTimeKind.Unspecified).AddTicks(6224), "Fugiat id.", 25, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 23, 13, 58, 7, 10, DateTimeKind.Unspecified).AddTicks(2198), "Maxime quisquam voluptates unde quia sunt saepe in.", new DateTime(2022, 8, 17, 14, 48, 58, 621, DateTimeKind.Unspecified).AddTicks(5908), "Itaque beatae.", 26, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 9, 9, 29, 8, 676, DateTimeKind.Unspecified).AddTicks(6297), "Sit minus quidem nesciunt neque adipisci qui dolorum eligendi doloribus.", new DateTime(2022, 10, 2, 10, 53, 57, 576, DateTimeKind.Unspecified).AddTicks(6867), "Dolorem molestiae.", 11, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 4, 9, 22, 2, DateTimeKind.Unspecified).AddTicks(6957), "Perferendis nobis quo.", new DateTime(2021, 5, 25, 19, 17, 38, 672, DateTimeKind.Unspecified).AddTicks(1207), "Et expedita.", 49, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 8, 23, 20, 33, 240, DateTimeKind.Unspecified).AddTicks(2815), "Delectus recusandae modi eum et illum suscipit.", new DateTime(2021, 6, 5, 11, 17, 41, 782, DateTimeKind.Unspecified).AddTicks(5567), "Repellendus dicta.", 24, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 16, 5, 57, 33, 65, DateTimeKind.Unspecified).AddTicks(3879), "Voluptatem et labore consequatur enim soluta tenetur.", new DateTime(2021, 2, 6, 21, 22, 26, 737, DateTimeKind.Unspecified).AddTicks(5664), "Sit ut.", 40, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 6, 12, 53, 975, DateTimeKind.Unspecified).AddTicks(8377), "Laudantium dignissimos et.", new DateTime(2022, 5, 22, 23, 47, 41, 296, DateTimeKind.Unspecified).AddTicks(8524), "Consequatur fuga.", 24, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 19, 45, 52, 380, DateTimeKind.Unspecified).AddTicks(7609), "Autem consequatur rerum ut praesentium sed.", new DateTime(2021, 2, 7, 22, 6, 50, 239, DateTimeKind.Unspecified).AddTicks(897), "Velit corrupti.", 44, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 23, 0, 38, 7, 529, DateTimeKind.Unspecified).AddTicks(8851), "Maiores ut dolor exercitationem rerum molestias at adipisci voluptas.", new DateTime(2022, 5, 13, 15, 41, 29, 820, DateTimeKind.Unspecified).AddTicks(37), "Exercitationem vel.", 37, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 8, 27, 13, 495, DateTimeKind.Unspecified).AddTicks(4530), "Quaerat sapiente libero modi ut voluptatem officia voluptatem dolor.", new DateTime(2022, 8, 14, 9, 49, 7, 108, DateTimeKind.Unspecified).AddTicks(7731), "Et sit.", 2, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 10, 18, 22, 13, 1, 450, DateTimeKind.Unspecified).AddTicks(4875), "Et beatae ut quia qui.", new DateTime(2021, 7, 14, 14, 11, 4, 476, DateTimeKind.Unspecified).AddTicks(8248), "Optio enim.", 48 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 13, 16, 26, 17, 581, DateTimeKind.Unspecified).AddTicks(8263), "Ex et ea omnis accusamus aut eum impedit rerum et.", new DateTime(2022, 10, 6, 2, 30, 57, 264, DateTimeKind.Unspecified).AddTicks(5740), "Commodi delectus.", 33, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 18, 7, 36, 48, 992, DateTimeKind.Unspecified).AddTicks(146), "Voluptas recusandae et est eius facilis odit.", new DateTime(2021, 6, 11, 4, 21, 26, 989, DateTimeKind.Unspecified).AddTicks(2387), "Quas fuga.", 4, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 9, 23, 35, 656, DateTimeKind.Unspecified).AddTicks(5109), "Hic veniam quo iure rerum quisquam atque cumque autem accusantium.", new DateTime(2021, 12, 2, 16, 52, 16, 992, DateTimeKind.Unspecified).AddTicks(3032), "Repellendus neque.", 17, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 22, 23, 4, 8, 620, DateTimeKind.Unspecified).AddTicks(5189), "Qui vel odio voluptas.", new DateTime(2021, 7, 27, 11, 7, 6, 362, DateTimeKind.Unspecified).AddTicks(1310), "Rerum ut.", 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 8, 8, 23, 46, 18, 593, DateTimeKind.Unspecified).AddTicks(6327), "Quae et labore facilis alias.", new DateTime(2021, 4, 7, 4, 27, 8, 251, DateTimeKind.Unspecified).AddTicks(6688), "Cumque et.", 6 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 25, 13, 43, 44, 298, DateTimeKind.Unspecified).AddTicks(3772), "Ullam quae sed qui enim omnis dolorem libero quia.", new DateTime(2021, 9, 30, 0, 7, 49, 792, DateTimeKind.Unspecified).AddTicks(4946), "Quia veritatis.", 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 27, 21, 39, 43, 313, DateTimeKind.Unspecified).AddTicks(9687), "Est totam temporibus numquam quas iusto necessitatibus.", new DateTime(2022, 5, 29, 0, 3, 25, 686, DateTimeKind.Unspecified).AddTicks(5250), "Modi corrupti.", 50, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 13, 17, 49, 23, 114, DateTimeKind.Unspecified).AddTicks(1832), "Est voluptatem maiores at.", new DateTime(2022, 3, 31, 9, 16, 51, 698, DateTimeKind.Unspecified).AddTicks(3447), "Quo adipisci.", 32, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 3, 6, 11, 20, 248, DateTimeKind.Unspecified).AddTicks(8434), "Illum asperiores earum.", new DateTime(2022, 11, 23, 22, 45, 30, 68, DateTimeKind.Unspecified).AddTicks(4545), "Dolorem repellendus.", 48, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 27, 21, 8, 55, 397, DateTimeKind.Unspecified).AddTicks(4092), "Ea ducimus incidunt non deleniti nihil vitae.", new DateTime(2021, 2, 8, 7, 39, 50, 885, DateTimeKind.Unspecified).AddTicks(4922), "Neque sed.", 13, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 26, 1, 51, 31, 572, DateTimeKind.Unspecified).AddTicks(1811), "Aut reprehenderit asperiores sit esse.", new DateTime(2022, 10, 2, 12, 48, 48, 844, DateTimeKind.Unspecified).AddTicks(5055), "Blanditiis quae.", 19, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 6, 13, 52, 43, 671, DateTimeKind.Unspecified).AddTicks(4712), "Non esse occaecati assumenda dolores quasi aut nostrum ex impedit.", new DateTime(2022, 11, 12, 9, 26, 35, 522, DateTimeKind.Unspecified).AddTicks(9377), "Velit voluptate.", 17, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 17, 21, 47, 749, DateTimeKind.Unspecified).AddTicks(8749), "Eaque ipsum distinctio itaque beatae totam sunt.", new DateTime(2021, 11, 16, 20, 22, 34, 373, DateTimeKind.Unspecified).AddTicks(7044), "Consequatur occaecati.", 5, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 12, 12, 5, 5, 413, DateTimeKind.Unspecified).AddTicks(107), "Corrupti consectetur voluptas provident omnis officiis.", new DateTime(2022, 7, 12, 14, 11, 53, 833, DateTimeKind.Unspecified).AddTicks(2302), "In nemo.", 35, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 3, 17, 38, 46, 108, DateTimeKind.Unspecified).AddTicks(2679), "Exercitationem doloribus expedita quam ab est iste velit alias.", new DateTime(2022, 2, 23, 11, 35, 53, 705, DateTimeKind.Unspecified).AddTicks(9845), "Ab corporis.", 33, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 5, 12, 8, 3, 34, 277, DateTimeKind.Unspecified).AddTicks(7638), "Commodi molestias eos et repellat dignissimos magnam temporibus.", new DateTime(2021, 4, 4, 18, 4, 9, 891, DateTimeKind.Unspecified).AddTicks(4459), "Qui ut.", 50, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 2, 10, 32, 473, DateTimeKind.Unspecified).AddTicks(1033), "Vitae eos voluptatem enim deleniti maxime id rem voluptatum sunt.", new DateTime(2021, 8, 8, 22, 57, 27, 217, DateTimeKind.Unspecified).AddTicks(7403), "Illum qui.", 50, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 15, 18, 59, 55, 897, DateTimeKind.Unspecified).AddTicks(4663), "Impedit suscipit et sit consequatur quam sint non quam perferendis.", new DateTime(2021, 8, 10, 6, 28, 55, 213, DateTimeKind.Unspecified).AddTicks(3672), "Possimus sint.", 4, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 5, 20, 29, 729, DateTimeKind.Unspecified).AddTicks(8511), "Officia tempore facilis et id.", new DateTime(2022, 8, 6, 15, 34, 45, 875, DateTimeKind.Unspecified).AddTicks(925), "Quia veritatis.", 15, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 1, 9, 6, 9, 78, DateTimeKind.Unspecified).AddTicks(9449), "Enim excepturi laudantium provident maxime eligendi in et voluptatem consectetur.", new DateTime(2022, 11, 18, 17, 10, 1, 284, DateTimeKind.Unspecified).AddTicks(713), "Ut aliquam.", 46, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 29, 21, 48, 39, 679, DateTimeKind.Unspecified).AddTicks(909), "Quia ex rerum similique neque.", new DateTime(2022, 10, 29, 16, 26, 12, 321, DateTimeKind.Unspecified).AddTicks(3671), "Sit nihil.", 45, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 11, 10, 14, 48, 125, DateTimeKind.Unspecified).AddTicks(3760), "Quo repellendus officiis minima.", new DateTime(2022, 11, 2, 20, 44, 8, 196, DateTimeKind.Unspecified).AddTicks(737), "Sit doloribus.", 9, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 22, 55, 49, 294, DateTimeKind.Unspecified).AddTicks(6199), "Similique harum in fugiat odio.", new DateTime(2021, 7, 16, 17, 57, 16, 55, DateTimeKind.Unspecified).AddTicks(8459), "Recusandae ut.", 35, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 2, 18, 39, 26, 535, DateTimeKind.Unspecified).AddTicks(2583), "Accusantium eligendi quisquam illo minus exercitationem.", new DateTime(2022, 3, 21, 16, 22, 1, 146, DateTimeKind.Unspecified).AddTicks(5500), "Enim voluptas.", 23, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 5, 17, 50, 2, 444, DateTimeKind.Unspecified).AddTicks(3701), "Velit atque delectus quas dolor est qui enim dolor est.", new DateTime(2021, 2, 9, 14, 17, 24, 339, DateTimeKind.Unspecified).AddTicks(1297), "Pariatur et.", 21, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 11, 12, 30, 59, 340, DateTimeKind.Unspecified).AddTicks(9092), "Et eum rerum dolor alias quam incidunt.", new DateTime(2021, 12, 8, 1, 19, 16, 943, DateTimeKind.Unspecified).AddTicks(5018), "Consequatur amet.", 20, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 7, 15, 47, 9, 64, DateTimeKind.Unspecified).AddTicks(5252), "Ut vero fugiat vel id molestiae unde nemo veritatis aut.", new DateTime(2022, 5, 17, 15, 17, 22, 27, DateTimeKind.Unspecified).AddTicks(9121), "Est nesciunt.", 31, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 12, 0, 6, 41, 496, DateTimeKind.Unspecified).AddTicks(2149), "Placeat sint itaque suscipit sunt perspiciatis qui tempore incidunt occaecati.", new DateTime(2021, 8, 13, 19, 8, 39, 897, DateTimeKind.Unspecified).AddTicks(6052), "Eligendi quo.", 6, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 22, 30, 40, 359, DateTimeKind.Unspecified).AddTicks(5999), "Sit dicta ut amet assumenda eum.", new DateTime(2021, 5, 30, 17, 32, 34, 285, DateTimeKind.Unspecified).AddTicks(4310), "Omnis maiores.", 3, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 4, 4, 4, 18, 374, DateTimeKind.Unspecified).AddTicks(9484), "Et placeat id natus sunt aliquam vero non consequatur numquam.", new DateTime(2022, 4, 17, 12, 29, 52, 104, DateTimeKind.Unspecified).AddTicks(8386), "Molestias cumque.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 4, 16, 38, 29, 390, DateTimeKind.Unspecified).AddTicks(6892), "Et vero dicta.", new DateTime(2021, 1, 23, 13, 13, 54, 504, DateTimeKind.Unspecified).AddTicks(4209), "Maxime a.", 42, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 27, 5, 34, 18, 422, DateTimeKind.Unspecified).AddTicks(5837), "Sunt explicabo sed explicabo beatae incidunt minima facilis aliquid iure.", new DateTime(2021, 1, 18, 4, 16, 33, 166, DateTimeKind.Unspecified).AddTicks(6957), "Blanditiis aperiam.", 47, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 3, 20, 17, 13, 234, DateTimeKind.Unspecified).AddTicks(7576), "Deleniti sit et necessitatibus.", new DateTime(2022, 11, 16, 17, 13, 8, 580, DateTimeKind.Unspecified).AddTicks(7830), "Voluptatibus mollitia.", 41, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 6, 14, 27, 700, DateTimeKind.Unspecified).AddTicks(5091), "Eius ab eum doloribus voluptatem cum reprehenderit neque omnis maxime.", new DateTime(2021, 6, 2, 4, 47, 16, 104, DateTimeKind.Unspecified).AddTicks(6211), "Impedit magni.", 1, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 28, 13, 1, 43, 276, DateTimeKind.Unspecified).AddTicks(9349), "Ullam dolor fugit sequi eveniet culpa eum voluptatum quo repellendus.", new DateTime(2022, 7, 7, 3, 7, 24, 546, DateTimeKind.Unspecified).AddTicks(8522), "Omnis ea.", 18, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 10, 8, 8, 0, 174, DateTimeKind.Unspecified).AddTicks(6970), "Sit et quo cum ab ullam eos.", new DateTime(2021, 10, 20, 15, 0, 12, 573, DateTimeKind.Unspecified).AddTicks(1156), "Sint eaque.", 5, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 29, 23, 54, 13, 39, DateTimeKind.Unspecified).AddTicks(3647), "Repellendus nihil eos.", new DateTime(2021, 5, 20, 14, 7, 32, 357, DateTimeKind.Unspecified).AddTicks(119), "Itaque delectus.", 23, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 18, 3, 32, 38, 659, DateTimeKind.Unspecified).AddTicks(4985), "Molestiae quia qui aliquid omnis perspiciatis culpa id voluptatem molestiae.", new DateTime(2022, 1, 20, 12, 19, 56, 110, DateTimeKind.Unspecified).AddTicks(9192), "Dolor quidem.", 46, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 19, 8, 16, 47, 557, DateTimeKind.Unspecified).AddTicks(995), "Impedit ad optio dolor laudantium sit quia asperiores pariatur.", new DateTime(2021, 3, 29, 6, 19, 19, 161, DateTimeKind.Unspecified).AddTicks(6926), "Et occaecati.", 26, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 0, 20, 21, 192, DateTimeKind.Unspecified).AddTicks(2801), "Quasi nesciunt tempora non praesentium aspernatur tempore eaque eveniet dolorem.", new DateTime(2022, 3, 25, 0, 8, 26, 755, DateTimeKind.Unspecified).AddTicks(9816), "Exercitationem maiores.", 21, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 4, 23, 44, 240, DateTimeKind.Unspecified).AddTicks(1696), "Dolor delectus dolorem ea deleniti.", new DateTime(2021, 11, 14, 14, 38, 33, 441, DateTimeKind.Unspecified).AddTicks(4588), "In quis.", 47, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 16, 23, 43, 18, 932, DateTimeKind.Unspecified).AddTicks(9975), "Iure excepturi quas repellat sunt.", new DateTime(2021, 10, 27, 13, 32, 25, 576, DateTimeKind.Unspecified).AddTicks(8571), "Est porro.", 41, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 1, 10, 7, 38, 43, 607, DateTimeKind.Unspecified).AddTicks(8658), "In ab perferendis voluptates veritatis in.", new DateTime(2022, 2, 25, 4, 3, 42, 988, DateTimeKind.Unspecified).AddTicks(6736), "Ut id.", 37 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 19, 4, 42, 2, 160, DateTimeKind.Unspecified).AddTicks(8696), "Autem est odit sint et libero accusamus.", new DateTime(2021, 2, 20, 11, 8, 48, 966, DateTimeKind.Unspecified).AddTicks(4710), "Illo accusantium.", 42, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 24, 7, 49, 59, 405, DateTimeKind.Unspecified).AddTicks(7312), "Nesciunt placeat nisi maxime.", new DateTime(2022, 7, 23, 20, 24, 11, 689, DateTimeKind.Unspecified).AddTicks(4915), "Commodi reiciendis.", 7, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 17, 44, 2, 888, DateTimeKind.Unspecified).AddTicks(494), "Similique facilis ipsa.", new DateTime(2022, 8, 30, 8, 40, 34, 73, DateTimeKind.Unspecified).AddTicks(1560), "Commodi accusantium.", 33, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 15, 23, 33, 43, 224, DateTimeKind.Unspecified).AddTicks(9090), "Est incidunt harum facilis.", new DateTime(2022, 7, 25, 1, 42, 35, 522, DateTimeKind.Unspecified).AddTicks(6267), "Quae in.", 24, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 19, 28, 31, 713, DateTimeKind.Unspecified).AddTicks(7766), "Necessitatibus non sit.", new DateTime(2021, 5, 26, 19, 1, 6, 154, DateTimeKind.Unspecified).AddTicks(4930), "Voluptatem molestias.", 30, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 23, 12, 53, 252, DateTimeKind.Unspecified).AddTicks(8417), "Aut dolorem unde.", new DateTime(2021, 7, 25, 3, 12, 25, 586, DateTimeKind.Unspecified).AddTicks(8314), "Eum et.", 3, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 12, 6, 6, 0, 236, DateTimeKind.Unspecified).AddTicks(1107), "Excepturi omnis rerum voluptas ab rerum non quod voluptatem.", new DateTime(2022, 6, 3, 16, 2, 53, 172, DateTimeKind.Unspecified).AddTicks(256), "Voluptas ut.", 16, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 25, 4, 21, 9, 355, DateTimeKind.Unspecified).AddTicks(4270), "Ea quam ipsa.", new DateTime(2021, 5, 24, 8, 15, 5, 139, DateTimeKind.Unspecified).AddTicks(603), "Provident repellat.", 15, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 2, 5, 49, 47, 622, DateTimeKind.Unspecified).AddTicks(1378), "Quis voluptatum voluptatem ea aut ullam.", new DateTime(2021, 1, 6, 23, 9, 59, 357, DateTimeKind.Unspecified).AddTicks(7077), "Quia sunt.", 25, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 22, 35, 2, 971, DateTimeKind.Unspecified).AddTicks(3704), "Quia doloremque cumque.", new DateTime(2021, 11, 23, 11, 5, 32, 841, DateTimeKind.Unspecified).AddTicks(8750), "Pariatur fugit.", 5, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 20, 10, 58, 893, DateTimeKind.Unspecified).AddTicks(5494), "Provident ipsam quo nihil nihil.", new DateTime(2021, 4, 11, 13, 19, 13, 369, DateTimeKind.Unspecified).AddTicks(2843), "Omnis culpa.", 7, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 1, 4, 19, 14, 119, DateTimeKind.Unspecified).AddTicks(2772), "Quibusdam totam ab et aut neque eaque ut velit ab.", new DateTime(2021, 5, 1, 4, 49, 29, 563, DateTimeKind.Unspecified).AddTicks(6648), "Qui neque.", 41, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 3, 30, 59, 2, DateTimeKind.Unspecified).AddTicks(4581), "Nobis excepturi quia laboriosam iusto qui.", new DateTime(2022, 1, 23, 2, 50, 29, 963, DateTimeKind.Unspecified).AddTicks(7621), "Asperiores ut.", 3, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 24, 11, 56, 13, 769, DateTimeKind.Unspecified).AddTicks(4430), "Illo consequuntur accusamus.", new DateTime(2022, 4, 30, 6, 25, 44, 527, DateTimeKind.Unspecified).AddTicks(3039), "Maxime assumenda.", 34, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 15, 1, 36, 14, 154, DateTimeKind.Unspecified).AddTicks(6185), "Sit minima occaecati fugit quo.", new DateTime(2022, 9, 22, 10, 24, 28, 228, DateTimeKind.Unspecified).AddTicks(8149), "Labore voluptates.", 20, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 17, 23, 49, 24, 782, DateTimeKind.Unspecified).AddTicks(67), "Molestiae odit dolorem et rerum rem impedit fugit officiis odit.", new DateTime(2022, 10, 14, 19, 53, 8, 341, DateTimeKind.Unspecified).AddTicks(884), "Sed et.", 6, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 5, 2, 19, 16, 622, DateTimeKind.Unspecified).AddTicks(4404), "Ea ab debitis repellendus nostrum quo et hic.", new DateTime(2022, 12, 24, 14, 24, 50, 766, DateTimeKind.Unspecified).AddTicks(1859), "Velit repudiandae.", 31, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 17, 39, 19, 954, DateTimeKind.Unspecified).AddTicks(1611), "Numquam earum sit repellat aut et laborum.", new DateTime(2021, 11, 15, 22, 14, 49, 425, DateTimeKind.Unspecified).AddTicks(5054), "Sint molestias.", 8, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 11, 23, 33, 43, 501, DateTimeKind.Unspecified).AddTicks(8397), "Accusantium sed iure tenetur itaque est.", new DateTime(2022, 3, 2, 23, 37, 31, 156, DateTimeKind.Unspecified).AddTicks(6361), "Sed id.", 37, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 1, 4, 52, 1, 781, DateTimeKind.Unspecified).AddTicks(7876), "Repudiandae sapiente harum rerum nesciunt sint in quae voluptatem voluptas.", new DateTime(2022, 12, 15, 11, 56, 46, 254, DateTimeKind.Unspecified).AddTicks(1463), "Velit saepe.", 38, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 1, 2, 3, 346, DateTimeKind.Unspecified).AddTicks(5150), "Autem accusamus rerum eum asperiores animi autem ipsum natus.", new DateTime(2021, 10, 21, 20, 15, 40, 709, DateTimeKind.Unspecified).AddTicks(1668), "Voluptas eum.", 44, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 19, 0, 48, 16, DateTimeKind.Unspecified).AddTicks(3893), "Autem illo voluptas distinctio perferendis adipisci ullam.", new DateTime(2021, 10, 31, 2, 29, 21, 867, DateTimeKind.Unspecified).AddTicks(1004), "Enim laboriosam.", 37, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 7, 0, 31, 1, 260, DateTimeKind.Unspecified).AddTicks(8559), "Id tenetur ipsam et sunt est cum quo quia.", new DateTime(2021, 10, 11, 8, 57, 36, 257, DateTimeKind.Unspecified).AddTicks(8628), "Provident est.", 25, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 14, 39, 11, 46, DateTimeKind.Unspecified).AddTicks(5620), "Et itaque quod libero sit iusto incidunt.", new DateTime(2021, 5, 29, 21, 32, 19, 246, DateTimeKind.Unspecified).AddTicks(9319), "Voluptatem qui.", 24, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 21, 7, 19, 43, 210, DateTimeKind.Unspecified).AddTicks(9399), "Consequatur deleniti praesentium quia nisi.", new DateTime(2022, 11, 27, 0, 16, 45, 807, DateTimeKind.Unspecified).AddTicks(5932), "Neque laboriosam.", 47, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 6, 16, 42, 41, 999, DateTimeKind.Unspecified).AddTicks(9387), "Occaecati dolore aliquam sapiente similique sed.", new DateTime(2022, 2, 24, 17, 36, 27, 181, DateTimeKind.Unspecified).AddTicks(410), "Esse sunt.", 32, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 12, 0, 16, 561, DateTimeKind.Unspecified).AddTicks(3348), "Consequuntur ad quibusdam odio consequatur explicabo ut.", new DateTime(2021, 6, 1, 3, 40, 12, 139, DateTimeKind.Unspecified).AddTicks(6005), "Consequuntur eum.", 46, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 13, 15, 48, 7, 453, DateTimeKind.Unspecified).AddTicks(5451), "Ut qui qui deserunt illo quae molestiae recusandae aliquid sapiente.", new DateTime(2022, 12, 4, 9, 54, 37, 349, DateTimeKind.Unspecified).AddTicks(2273), "Eos qui.", 22, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 1, 7, 6, 42, 20, DateTimeKind.Unspecified).AddTicks(6398), "Maxime mollitia omnis repellendus eos facilis vel sed.", new DateTime(2022, 1, 7, 15, 11, 14, 837, DateTimeKind.Unspecified).AddTicks(635), "Maiores occaecati.", 10, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 2, 0, 17, 109, DateTimeKind.Unspecified).AddTicks(4401), "Id quia molestias consequatur corporis.", new DateTime(2022, 2, 25, 1, 28, 6, 187, DateTimeKind.Unspecified).AddTicks(3538), "Quo similique.", 29, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 6, 2, 24, 34, 562, DateTimeKind.Unspecified).AddTicks(3253), "Nobis sed voluptatem maiores non vel deserunt voluptate molestiae.", new DateTime(2021, 11, 21, 6, 31, 44, 528, DateTimeKind.Unspecified).AddTicks(3185), "Quasi magnam.", 10, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 7, 47, 17, 92, DateTimeKind.Unspecified).AddTicks(8432), "Accusamus aut eum deserunt.", new DateTime(2022, 6, 29, 12, 23, 26, 750, DateTimeKind.Unspecified).AddTicks(2401), "Ad ut.", 45, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 13, 24, 15, 863, DateTimeKind.Unspecified).AddTicks(6842), "Soluta aliquid totam distinctio quo dolor iure ut aut odio.", new DateTime(2022, 8, 22, 2, 8, 35, 526, DateTimeKind.Unspecified).AddTicks(2711), "Dolore magni.", 26, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 5, 30, 15, 24, 6, 174, DateTimeKind.Unspecified).AddTicks(781), "Dolore quia qui dolorem molestiae ut cum.", new DateTime(2022, 5, 1, 0, 55, 9, 654, DateTimeKind.Unspecified).AddTicks(3112), "Pariatur eos.", 14 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 17, 56, 45, 27, DateTimeKind.Unspecified).AddTicks(8704), "Repudiandae in eos optio.", new DateTime(2021, 7, 9, 2, 38, 58, 398, DateTimeKind.Unspecified).AddTicks(9804), "Voluptatem quibusdam.", 25, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 17, 33, 22, 953, DateTimeKind.Unspecified).AddTicks(1610), "Qui nostrum dolores sunt.", new DateTime(2022, 8, 15, 11, 50, 39, 363, DateTimeKind.Unspecified).AddTicks(2707), "Quis neque.", 25, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 6, 41, 7, 365, DateTimeKind.Unspecified).AddTicks(8625), "Est odit placeat tempore autem ipsum enim quasi labore blanditiis.", new DateTime(2021, 11, 16, 0, 2, 10, 848, DateTimeKind.Unspecified).AddTicks(8531), "Commodi iusto.", 16, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 6, 5, 7, 40, 39, 85, DateTimeKind.Unspecified).AddTicks(5582), "Rem hic quisquam numquam veniam consequatur quis neque rerum quia.", new DateTime(2022, 7, 27, 1, 27, 22, 331, DateTimeKind.Unspecified).AddTicks(8882), "Amet autem.", 24, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 6, 22, 5, 42, 911, DateTimeKind.Unspecified).AddTicks(2343), "Totam eum excepturi.", new DateTime(2022, 6, 2, 8, 53, 47, 388, DateTimeKind.Unspecified).AddTicks(809), "Id nemo.", 12, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 4, 5, 21, 15, 563, DateTimeKind.Unspecified).AddTicks(1519), "Ratione cumque illum perspiciatis et.", new DateTime(2021, 5, 31, 5, 59, 11, 92, DateTimeKind.Unspecified).AddTicks(7686), "Non aut.", 41, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 2, 27, 59, 632, DateTimeKind.Unspecified).AddTicks(3662), "Deserunt eligendi officiis distinctio necessitatibus modi eaque porro consequatur.", new DateTime(2021, 12, 12, 22, 29, 43, 76, DateTimeKind.Unspecified).AddTicks(111), "Illo assumenda.", 38, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 3, 17, 7, 34, 0, 494, DateTimeKind.Unspecified).AddTicks(2644), "Eum velit a sint nostrum assumenda accusamus consectetur aliquid qui.", new DateTime(2022, 5, 16, 7, 49, 48, 399, DateTimeKind.Unspecified).AddTicks(2801), "Et magnam.", 45 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 9, 2, 20, 10, 307, DateTimeKind.Unspecified).AddTicks(4397), "Dolorem minus consequuntur fuga illo nostrum nihil reiciendis sit quod.", new DateTime(2022, 6, 4, 3, 4, 3, 840, DateTimeKind.Unspecified).AddTicks(6729), "Libero itaque.", 19, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 10, 7, 27, 55, 53, DateTimeKind.Unspecified).AddTicks(7858), "Velit cum eos.", new DateTime(2021, 11, 19, 12, 57, 16, 258, DateTimeKind.Unspecified).AddTicks(1513), "Ipsum nemo.", 28, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 1, 26, 59, 556, DateTimeKind.Unspecified).AddTicks(7485), "Nesciunt enim sed maxime quas est.", new DateTime(2021, 9, 16, 1, 53, 7, 587, DateTimeKind.Unspecified).AddTicks(3766), "Voluptatum ducimus.", 20, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 22, 7, 18, 39, 553, DateTimeKind.Unspecified).AddTicks(4311), "Voluptas vitae dignissimos reprehenderit quos laborum.", new DateTime(2022, 2, 21, 6, 6, 23, 397, DateTimeKind.Unspecified).AddTicks(7609), "Optio qui.", 21, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 8, 21, 41, 41, 252, DateTimeKind.Unspecified).AddTicks(2081), "Dolores animi est est cum cum pariatur eligendi excepturi perspiciatis.", new DateTime(2022, 7, 26, 15, 41, 16, 829, DateTimeKind.Unspecified).AddTicks(2235), "Ratione sit.", 9, 3 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 2, 7, 21, 1, 39, 417, DateTimeKind.Unspecified).AddTicks(8545), "Roberts - Kunze" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 7, 19, 1, 22, 43, 193, DateTimeKind.Unspecified).AddTicks(9861), "Reynolds LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 1, 30, 14, 40, 2, 565, DateTimeKind.Unspecified).AddTicks(5750), "Ledner, Schroeder and Sawayn" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 7, 29, 20, 27, 15, 591, DateTimeKind.Unspecified).AddTicks(9066), "Jacobi, Pfeffer and Pacocha" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2010, 12, 15, 2, 45, 19, 102, DateTimeKind.Unspecified).AddTicks(4000), "Heathcote Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 3, 22, 1, 39, 15, 703, DateTimeKind.Unspecified).AddTicks(390), "Senger - Hoeger" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 11, 6, 13, 32, 6, 90, DateTimeKind.Unspecified).AddTicks(2363), "Quigley LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 4, 2, 10, 21, 13, 190, DateTimeKind.Unspecified).AddTicks(2483), "Beahan - Raynor" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 12, 4, 10, 44, 18, 848, DateTimeKind.Unspecified).AddTicks(154), "Koelpin, Kihn and Adams" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 11, 19, 17, 14, 2, 428, DateTimeKind.Unspecified).AddTicks(8850), "O'Connell, Stiedemann and Stiedemann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 4, 6, 0, 3, 1, 357, DateTimeKind.Unspecified).AddTicks(2436), "Caitlyn.OKeefe49@yahoo.com", "Jack", "Carter", new DateTime(2016, 11, 15, 13, 13, 0, 992, DateTimeKind.Unspecified).AddTicks(7411), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 7, 1, 15, 27, 15, 658, DateTimeKind.Unspecified).AddTicks(317), "Freddie_Reichert@hotmail.com", "Gracie", "Medhurst", new DateTime(2017, 7, 24, 11, 33, 55, 662, DateTimeKind.Unspecified).AddTicks(5090), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1995, 9, 14, 20, 8, 43, 214, DateTimeKind.Unspecified).AddTicks(266), "Elody_Mertz@hotmail.com", "Humberto", "Botsford", new DateTime(2015, 6, 20, 9, 22, 36, 617, DateTimeKind.Unspecified).AddTicks(8674) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 1, 6, 3, 7, 43, 302, DateTimeKind.Unspecified).AddTicks(943), "Carmela.Fritsch@gmail.com", "Adeline", "Stoltenberg", new DateTime(2012, 12, 4, 13, 35, 16, 650, DateTimeKind.Unspecified).AddTicks(2705), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 9, 15, 12, 9, 15, 700, DateTimeKind.Unspecified).AddTicks(7023), "Petra49@gmail.com", "Marjorie", "Thiel", new DateTime(2014, 9, 9, 9, 54, 50, 333, DateTimeKind.Unspecified).AddTicks(6075), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 12, 30, 3, 58, 4, 212, DateTimeKind.Unspecified).AddTicks(9780), "Rocky.Schamberger23@hotmail.com", "Christop", "Wisoky", new DateTime(2014, 7, 13, 4, 22, 46, 481, DateTimeKind.Unspecified).AddTicks(6262), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 12, 19, 8, 59, 36, 911, DateTimeKind.Unspecified).AddTicks(7110), "Juliet19@gmail.com", "Reyna", "Erdman", new DateTime(2016, 12, 27, 11, 11, 28, 462, DateTimeKind.Unspecified).AddTicks(1348), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 9, 1, 41, 38, 4, DateTimeKind.Unspecified).AddTicks(4860), "Lizzie_Russel@hotmail.com", "Asia", "Stracke", new DateTime(2015, 10, 16, 14, 4, 14, 56, DateTimeKind.Unspecified).AddTicks(8475), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 31, 16, 16, 19, 454, DateTimeKind.Unspecified).AddTicks(4987), "Elvis.Glover60@gmail.com", "Daisha", "Hermiston", new DateTime(2016, 1, 26, 3, 19, 49, 376, DateTimeKind.Unspecified).AddTicks(1553), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 16, 4, 12, 4, 473, DateTimeKind.Unspecified).AddTicks(528), "Elvera0@gmail.com", "Orpha", "Marvin", new DateTime(2017, 3, 5, 9, 25, 10, 897, DateTimeKind.Unspecified).AddTicks(7887), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 11, 8, 19, 33, 23, 902, DateTimeKind.Unspecified).AddTicks(3996), "Tomasa_Beer@hotmail.com", "Ashtyn", "Denesik", new DateTime(2012, 2, 25, 18, 50, 12, 740, DateTimeKind.Unspecified).AddTicks(3741), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 7, 29, 13, 25, 56, 97, DateTimeKind.Unspecified).AddTicks(5575), "Priscilla_Welch@gmail.com", "Chelsea", "Wehner", new DateTime(2017, 3, 13, 3, 48, 46, 90, DateTimeKind.Unspecified).AddTicks(5755) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 7, 21, 40, 22, 755, DateTimeKind.Unspecified).AddTicks(1382), "Porter35@hotmail.com", "Mandy", "Kovacek", new DateTime(2016, 7, 9, 16, 8, 53, 358, DateTimeKind.Unspecified).AddTicks(5219), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1995, 1, 31, 5, 10, 30, 751, DateTimeKind.Unspecified).AddTicks(1634), "Loy_Pagac@gmail.com", "Paxton", "Denesik", new DateTime(2011, 12, 16, 21, 30, 55, 149, DateTimeKind.Unspecified).AddTicks(3257) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 27, 7, 13, 13, 937, DateTimeKind.Unspecified).AddTicks(7575), "Elinor_Zboncak64@gmail.com", "John", "Koepp", new DateTime(2012, 5, 15, 8, 26, 12, 749, DateTimeKind.Unspecified).AddTicks(3708), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 2, 10, 9, 20, 56, 144, DateTimeKind.Unspecified).AddTicks(2022), "Russel56@hotmail.com", "Marcelino", "Waelchi", new DateTime(2012, 7, 14, 3, 55, 36, 290, DateTimeKind.Unspecified).AddTicks(3218), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 4, 1, 4, 9, 392, DateTimeKind.Unspecified).AddTicks(8231), "Verla_Kiehn@gmail.com", "Tyrell", "Walker", new DateTime(2013, 8, 14, 0, 58, 30, 916, DateTimeKind.Unspecified).AddTicks(8236), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 9, 19, 44, 57, 638, DateTimeKind.Unspecified).AddTicks(9970), "Caitlyn.Borer27@yahoo.com", "Judah", "Spinka", new DateTime(2014, 4, 8, 10, 45, 50, 214, DateTimeKind.Unspecified).AddTicks(9250), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 12, 11, 6, 28, 38, 644, DateTimeKind.Unspecified).AddTicks(9901), "Maribel24@gmail.com", "Beth", "Marvin", new DateTime(2016, 7, 27, 3, 31, 28, 624, DateTimeKind.Unspecified).AddTicks(1009), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 8, 24, 19, 11, 0, 572, DateTimeKind.Unspecified).AddTicks(7389), "Frederique_Purdy@gmail.com", "Lea", "Crist", new DateTime(2017, 4, 23, 18, 30, 53, 215, DateTimeKind.Unspecified).AddTicks(5715), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1995, 6, 5, 0, 18, 10, 55, DateTimeKind.Unspecified).AddTicks(9967), "Taurean_Swift35@gmail.com", "Maci", "Konopelski", new DateTime(2015, 3, 20, 1, 54, 5, 645, DateTimeKind.Unspecified).AddTicks(180) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 1, 18, 11, 53, 893, DateTimeKind.Unspecified).AddTicks(2554), "Jermain.Cummings78@gmail.com", "Jake", "Huel", new DateTime(2012, 7, 12, 16, 58, 35, 404, DateTimeKind.Unspecified).AddTicks(4556), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 6, 6, 13, 47, 7, 327, DateTimeKind.Unspecified).AddTicks(1790), "Trenton_Dach22@gmail.com", "Henri", "Daniel", new DateTime(2012, 8, 31, 4, 53, 10, 957, DateTimeKind.Unspecified).AddTicks(1142) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 20, 5, 48, 8, 275, DateTimeKind.Unspecified).AddTicks(723), "Queen.Streich@gmail.com", "Lester", "Hane", new DateTime(2012, 3, 9, 2, 1, 54, 431, DateTimeKind.Unspecified).AddTicks(5876), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1990, 5, 9, 7, 47, 9, 226, DateTimeKind.Unspecified).AddTicks(7514), "Madilyn.Feest@gmail.com", "Antwan", "Schumm", new DateTime(2013, 6, 17, 12, 48, 15, 106, DateTimeKind.Unspecified).AddTicks(5944) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 3, 18, 16, 57, 1, 772, DateTimeKind.Unspecified).AddTicks(8877), "Charlie.Monahan@hotmail.com", "Flavie", "Schmidt", new DateTime(2011, 8, 19, 20, 16, 55, 940, DateTimeKind.Unspecified).AddTicks(1547), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 4, 21, 21, 7, 38, 824, DateTimeKind.Unspecified).AddTicks(840), "Rosalia10@gmail.com", "Freddy", "O'Kon", new DateTime(2010, 10, 25, 17, 26, 59, 276, DateTimeKind.Unspecified).AddTicks(6217), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 26, 14, 35, 39, 507, DateTimeKind.Unspecified).AddTicks(3222), "Lucy_Dietrich91@yahoo.com", "Maia", "Zulauf", new DateTime(2013, 7, 6, 20, 58, 31, 468, DateTimeKind.Unspecified).AddTicks(995), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 1, 6, 18, 34, 56, 85, DateTimeKind.Unspecified).AddTicks(6666), "Lenora_Beahan@hotmail.com", "Claudine", "Connelly", new DateTime(2014, 2, 11, 17, 21, 42, 768, DateTimeKind.Unspecified).AddTicks(364), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 27, 18, 16, 54, 520, DateTimeKind.Unspecified).AddTicks(9207), "Else.Satterfield3@hotmail.com", "Dewayne", "Mraz", new DateTime(2013, 7, 26, 0, 31, 13, 953, DateTimeKind.Unspecified).AddTicks(2126), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 10, 15, 9, 41, 845, DateTimeKind.Unspecified).AddTicks(4192), "Marta.Gaylord@hotmail.com", "Kamren", "Greenfelder", new DateTime(2013, 12, 27, 3, 29, 44, 35, DateTimeKind.Unspecified).AddTicks(6316), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 6, 18, 12, 21, 4, 881, DateTimeKind.Unspecified).AddTicks(8801), "Amparo_Schulist@gmail.com", "Kirsten", "Steuber", new DateTime(2017, 7, 8, 12, 11, 8, 391, DateTimeKind.Unspecified).AddTicks(4545), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1996, 7, 12, 20, 43, 28, 517, DateTimeKind.Unspecified).AddTicks(4660), "Jessyca_Bashirian16@hotmail.com", "Kayden", "Walker", new DateTime(2012, 7, 3, 21, 20, 33, 998, DateTimeKind.Unspecified).AddTicks(1730) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 31, 14, 24, 3, 453, DateTimeKind.Unspecified).AddTicks(7239), "Zack.Haag14@hotmail.com", "Nikko", "MacGyver", new DateTime(2014, 1, 5, 22, 56, 31, 212, DateTimeKind.Unspecified).AddTicks(1534), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 2, 0, 3, 16, 384, DateTimeKind.Unspecified).AddTicks(3073), "Rosendo.Cronin75@yahoo.com", "Abbey", "Flatley", new DateTime(2016, 8, 29, 7, 54, 3, 288, DateTimeKind.Unspecified).AddTicks(547), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 30, 20, 55, 45, 864, DateTimeKind.Unspecified).AddTicks(1722), "Malachi_Smith@gmail.com", "Zita", "Robel", new DateTime(2017, 1, 1, 8, 49, 4, 614, DateTimeKind.Unspecified).AddTicks(2280), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 12, 14, 4, 24, 3, 480, DateTimeKind.Unspecified).AddTicks(6784), "Rosetta_Bauch6@gmail.com", "Kraig", "Crooks", new DateTime(2013, 9, 14, 20, 51, 37, 406, DateTimeKind.Unspecified).AddTicks(2586), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 1, 9, 5, 53, 28, 829, DateTimeKind.Unspecified).AddTicks(9226), "Magnolia_Schultz@gmail.com", "Emelie", "Feil", new DateTime(2010, 4, 29, 10, 6, 52, 863, DateTimeKind.Unspecified).AddTicks(1678), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 10, 14, 3, 39, 18, 359, DateTimeKind.Unspecified).AddTicks(1083), "Glen_Skiles@hotmail.com", "Josie", "Waters", new DateTime(2016, 7, 16, 9, 52, 54, 933, DateTimeKind.Unspecified).AddTicks(1962) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 5, 18, 13, 51, 19, 301, DateTimeKind.Unspecified).AddTicks(1798), "Orion11@yahoo.com", "Ansley", "Bashirian", new DateTime(2012, 8, 14, 11, 24, 30, 498, DateTimeKind.Unspecified).AddTicks(4193), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 28, 12, 10, 13, 130, DateTimeKind.Unspecified).AddTicks(1298), "Lilliana87@gmail.com", "Veda", "Runte", new DateTime(2016, 11, 10, 4, 53, 24, 636, DateTimeKind.Unspecified).AddTicks(7457), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 11, 5, 22, 4, 58, 631, DateTimeKind.Unspecified).AddTicks(8836), "Marquise82@hotmail.com", "Maudie", "Moore", new DateTime(2011, 9, 10, 4, 55, 46, 943, DateTimeKind.Unspecified).AddTicks(2542), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 8, 22, 4, 8, 8, 745, DateTimeKind.Unspecified).AddTicks(1874), "Rigoberto_Hoeger@yahoo.com", "Rubie", "Williamson", new DateTime(2015, 7, 27, 0, 53, 54, 998, DateTimeKind.Unspecified).AddTicks(2391), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 23, 15, 33, 55, 791, DateTimeKind.Unspecified).AddTicks(7614), "Jayne19@hotmail.com", "Colten", "Hammes", new DateTime(2011, 10, 31, 15, 40, 1, 52, DateTimeKind.Unspecified).AddTicks(5637), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 29, 12, 49, 56, 285, DateTimeKind.Unspecified).AddTicks(4676), "Mossie_Daugherty@gmail.com", "Casimir", "Adams", new DateTime(2010, 8, 21, 18, 46, 47, 835, DateTimeKind.Unspecified).AddTicks(1818), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 23, 1, 51, 33, 439, DateTimeKind.Unspecified).AddTicks(2643), "Maritza.Pouros@yahoo.com", "Linnie", "Dickens", new DateTime(2013, 3, 3, 9, 12, 52, 808, DateTimeKind.Unspecified).AddTicks(2970), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 1, 14, 16, 22, 58, 823, DateTimeKind.Unspecified).AddTicks(3720), "Kris18@gmail.com", "Eryn", "Buckridge", new DateTime(2015, 12, 28, 16, 9, 47, 823, DateTimeKind.Unspecified).AddTicks(3668), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 12, 28, 8, 35, 31, 760, DateTimeKind.Unspecified).AddTicks(4587), "Alana16@yahoo.com", "Eileen", "Bruen", new DateTime(2013, 11, 16, 22, 45, 49, 856, DateTimeKind.Unspecified).AddTicks(1324), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 3, 29, 6, 35, 12, 529, DateTimeKind.Unspecified).AddTicks(7087), "Granville_Kunze@hotmail.com", "Oscar", "Leannon", new DateTime(2010, 9, 19, 23, 56, 4, 990, DateTimeKind.Unspecified).AddTicks(8002), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 11, 13, 3, 38, 40, 451, DateTimeKind.Unspecified).AddTicks(5174), "Baby5@hotmail.com", "Randall", "Littel", new DateTime(2013, 1, 26, 2, 42, 21, 540, DateTimeKind.Unspecified).AddTicks(1545), 1 });
        }
    }
}
