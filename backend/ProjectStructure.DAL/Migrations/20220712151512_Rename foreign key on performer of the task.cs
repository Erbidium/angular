﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectStructure.DAL.Migrations
{
    public partial class Renameforeignkeyonperformerofthetask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Task_Users_PerformerId",
                table: "Task");

            migrationBuilder.RenameColumn(
                name: "PerformerId",
                table: "Task",
                newName: "PerformerUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Task_PerformerId",
                table: "Task",
                newName: "IX_Task_PerformerUserId");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 41, new DateTime(2019, 1, 16, 7, 42, 55, 912, DateTimeKind.Unspecified).AddTicks(2303), new DateTime(2022, 8, 7, 16, 0, 36, 290, DateTimeKind.Unspecified).AddTicks(7596), "Et iste sed et asperiores quia.", "Rustic Rubber Fish", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 1, 23, 1, 29, 47, 658, DateTimeKind.Unspecified).AddTicks(8066), new DateTime(2022, 9, 15, 15, 53, 31, 973, DateTimeKind.Unspecified).AddTicks(7692), "Repudiandae placeat sapiente.", "Rustic Frozen Bacon", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 10, 27, 17, 14, 32, 765, DateTimeKind.Unspecified).AddTicks(5602), new DateTime(2022, 3, 18, 15, 48, 29, 138, DateTimeKind.Unspecified).AddTicks(88), "Libero qui doloremque quia fugiat nemo dignissimos fugit fugiat.", "Licensed Granite Soap", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 40, new DateTime(2019, 6, 27, 4, 2, 12, 777, DateTimeKind.Unspecified).AddTicks(1911), new DateTime(2021, 9, 27, 3, 22, 13, 581, DateTimeKind.Unspecified).AddTicks(1424), "Cumque sed dolores nam doloremque.", "Ergonomic Rubber Salad", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 10, 11, 16, 47, 57, 547, DateTimeKind.Unspecified).AddTicks(6640), new DateTime(2021, 11, 6, 20, 56, 53, 50, DateTimeKind.Unspecified).AddTicks(6960), "Recusandae quidem atque vel cumque ut.", "Handcrafted Rubber Gloves", 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 26, 2, 49, 56, 158, DateTimeKind.Unspecified).AddTicks(3581), "Quos et exercitationem dolores laborum.", new DateTime(2022, 3, 1, 20, 53, 15, 428, DateTimeKind.Unspecified).AddTicks(3535), "Architecto nihil.", 44, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 4, 14, 36, 9, 703, DateTimeKind.Unspecified).AddTicks(5834), "Porro et est.", new DateTime(2021, 6, 23, 10, 48, 4, 261, DateTimeKind.Unspecified).AddTicks(2580), "Dicta atque.", 36, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 8, 7, 10, 45, 36, DateTimeKind.Unspecified).AddTicks(3988), "Quidem harum est qui velit neque esse dolor non.", new DateTime(2022, 4, 26, 2, 29, 5, 889, DateTimeKind.Unspecified).AddTicks(9124), "Ut velit.", 35, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 22, 2, 33, 881, DateTimeKind.Unspecified).AddTicks(2254), "Non mollitia voluptatum.", new DateTime(2021, 1, 7, 14, 39, 14, 119, DateTimeKind.Unspecified).AddTicks(5123), "Deserunt eligendi.", 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 7, 6, 34, 13, 101, DateTimeKind.Unspecified).AddTicks(4228), "Asperiores quasi rerum ullam.", new DateTime(2021, 6, 23, 22, 19, 9, 868, DateTimeKind.Unspecified).AddTicks(8906), "Voluptatem fugiat.", 44, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 4, 20, 15, 3, 245, DateTimeKind.Unspecified).AddTicks(6368), "Ipsam aspernatur dolore vitae.", new DateTime(2022, 5, 5, 4, 40, 12, 814, DateTimeKind.Unspecified).AddTicks(5846), "Expedita ex.", 7, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 24, 13, 28, 48, 112, DateTimeKind.Unspecified).AddTicks(2962), "Nihil fuga magnam ipsam.", new DateTime(2022, 2, 3, 9, 20, 28, 842, DateTimeKind.Unspecified).AddTicks(2726), "Consectetur ut.", 3, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 24, 21, 47, 29, 40, DateTimeKind.Unspecified).AddTicks(1646), "Eum provident nisi.", new DateTime(2022, 4, 29, 17, 22, 27, 867, DateTimeKind.Unspecified).AddTicks(9031), "Ut unde.", 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 8, 14, 47, 44, 459, DateTimeKind.Unspecified).AddTicks(8855), "Similique blanditiis blanditiis quia voluptate dicta qui.", new DateTime(2022, 9, 23, 11, 13, 47, 152, DateTimeKind.Unspecified).AddTicks(7799), "Maiores quibusdam.", 13, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 1, 30, 50, 269, DateTimeKind.Unspecified).AddTicks(6950), "Deserunt sed vel provident qui.", new DateTime(2021, 10, 24, 5, 39, 40, 612, DateTimeKind.Unspecified).AddTicks(8562), "Animi repellendus.", 42, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 8, 12, 46, 655, DateTimeKind.Unspecified).AddTicks(5228), "Saepe ab culpa omnis.", new DateTime(2021, 3, 4, 18, 23, 25, 58, DateTimeKind.Unspecified).AddTicks(6018), "In in.", 3, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 17, 58, 8, 685, DateTimeKind.Unspecified).AddTicks(1260), "Quia eum omnis sint quia dolor rerum neque temporibus qui.", new DateTime(2022, 2, 8, 21, 23, 45, 314, DateTimeKind.Unspecified).AddTicks(3224), "Nulla rerum.", 8, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 30, 2, 47, 44, 142, DateTimeKind.Unspecified).AddTicks(4567), "Qui quo porro nisi fugit autem aliquid.", new DateTime(2021, 4, 25, 16, 30, 33, 69, DateTimeKind.Unspecified).AddTicks(8962), "Porro expedita.", 24, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 13, 8, 42, 136, DateTimeKind.Unspecified).AddTicks(6027), "Provident rerum voluptatem possimus.", new DateTime(2022, 6, 9, 20, 28, 4, 711, DateTimeKind.Unspecified).AddTicks(7908), "Qui qui.", 41, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 26, 14, 41, 51, 906, DateTimeKind.Unspecified).AddTicks(5939), "Sit atque perspiciatis vitae corporis eius et voluptas quos quidem.", new DateTime(2021, 4, 2, 20, 5, 1, 318, DateTimeKind.Unspecified).AddTicks(388), "Aliquam ut.", 30, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 16, 7, 7, 33, 250, DateTimeKind.Unspecified).AddTicks(2532), "Hic corrupti excepturi et dolores molestiae.", new DateTime(2022, 11, 8, 5, 50, 1, 722, DateTimeKind.Unspecified).AddTicks(7278), "Dolorem qui.", 47, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 8, 2, 16, 19, 601, DateTimeKind.Unspecified).AddTicks(5781), "Voluptatem architecto vel odio consequatur sit dignissimos.", new DateTime(2022, 10, 24, 1, 4, 37, 247, DateTimeKind.Unspecified).AddTicks(8202), "Est excepturi.", 4, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 15, 14, 20, 18, 549, DateTimeKind.Unspecified).AddTicks(4692), "Deserunt aut fugiat cum et sit est sapiente quasi sunt.", new DateTime(2022, 2, 13, 3, 50, 10, 83, DateTimeKind.Unspecified).AddTicks(7028), "Nisi quaerat.", 35, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 11, 12, 50, 15, 631, DateTimeKind.Unspecified).AddTicks(3902), "Impedit voluptatem omnis enim est dignissimos velit.", new DateTime(2022, 4, 24, 10, 59, 30, 911, DateTimeKind.Unspecified).AddTicks(4227), "Debitis iste.", 40, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 16, 10, 57, 13, 535, DateTimeKind.Unspecified).AddTicks(944), "Sequi totam necessitatibus quia voluptas enim eveniet est.", new DateTime(2022, 7, 6, 4, 56, 57, 167, DateTimeKind.Unspecified).AddTicks(7645), "Dolore qui.", 37, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 21, 51, 29, 984, DateTimeKind.Unspecified).AddTicks(137), "Sint magni rerum blanditiis deserunt quam in rerum est rem.", new DateTime(2021, 8, 27, 15, 24, 21, 570, DateTimeKind.Unspecified).AddTicks(3725), "Laborum iure.", 39, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 9, 18, 4, 19, 348, DateTimeKind.Unspecified).AddTicks(6001), "Commodi officia sed occaecati voluptatem iste est.", new DateTime(2021, 5, 15, 6, 15, 26, 901, DateTimeKind.Unspecified).AddTicks(8559), "Laudantium vel.", 9, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 11, 22, 19, 26, 311, DateTimeKind.Unspecified).AddTicks(1622), "Provident quisquam nihil.", new DateTime(2022, 9, 14, 11, 45, 44, 720, DateTimeKind.Unspecified).AddTicks(7216), "Aspernatur est.", 33, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 2, 14, 34, 275, DateTimeKind.Unspecified).AddTicks(482), "Qui in iste labore.", new DateTime(2021, 1, 24, 10, 53, 28, 860, DateTimeKind.Unspecified).AddTicks(9215), "Ratione magnam.", 14, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 5, 10, 4, 30, 38, 817, DateTimeKind.Unspecified).AddTicks(4730), "Magni accusamus cumque omnis nisi et.", new DateTime(2021, 1, 14, 9, 12, 22, 864, DateTimeKind.Unspecified).AddTicks(9829), "Cum amet.", 19 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 12, 2, 54, 17, 1, DateTimeKind.Unspecified).AddTicks(4925), "Numquam natus aut quo qui vel ex nulla.", new DateTime(2022, 9, 17, 20, 26, 58, 559, DateTimeKind.Unspecified).AddTicks(550), "Possimus voluptatem.", 24, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 17, 23, 43, 40, 237, DateTimeKind.Unspecified).AddTicks(227), "Minima odit iure ducimus modi non sint.", new DateTime(2021, 2, 28, 2, 23, 10, 890, DateTimeKind.Unspecified).AddTicks(8282), "Voluptatem quia.", 16, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 27, 17, 36, 36, 654, DateTimeKind.Unspecified).AddTicks(9358), "Nihil facilis voluptatem facere pariatur animi quaerat eos deserunt.", new DateTime(2022, 1, 24, 14, 37, 26, 90, DateTimeKind.Unspecified).AddTicks(7846), "Perspiciatis autem.", 29, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 16, 13, 13, 23, 142, DateTimeKind.Unspecified).AddTicks(7619), "Blanditiis maxime asperiores quo nisi ab autem repudiandae.", new DateTime(2021, 3, 15, 17, 21, 39, 405, DateTimeKind.Unspecified).AddTicks(7389), "Nisi quia.", 46, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 12, 43, 56, 206, DateTimeKind.Unspecified).AddTicks(5495), "Aspernatur quaerat aut non at incidunt earum provident.", new DateTime(2021, 3, 1, 19, 28, 26, 785, DateTimeKind.Unspecified).AddTicks(4513), "Consequatur inventore.", 49, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 15, 15, 28, 13, 348, DateTimeKind.Unspecified).AddTicks(3711), "Temporibus optio quasi repellendus.", new DateTime(2022, 6, 30, 15, 51, 43, 169, DateTimeKind.Unspecified).AddTicks(8121), "Enim et.", 8, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 26, 8, 39, 41, 843, DateTimeKind.Unspecified).AddTicks(7798), "Dicta rerum rem deleniti velit aut dignissimos omnis exercitationem rerum.", new DateTime(2021, 12, 12, 13, 22, 10, 292, DateTimeKind.Unspecified).AddTicks(9695), "Quia rerum.", 7, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 27, 16, 14, 28, 516, DateTimeKind.Unspecified).AddTicks(1082), "Officiis non sint quia ipsa soluta.", new DateTime(2021, 2, 1, 7, 56, 34, 1, DateTimeKind.Unspecified).AddTicks(3688), "Laudantium aut.", 28, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 23, 54, 26, 625, DateTimeKind.Unspecified).AddTicks(430), "Molestiae saepe quod.", new DateTime(2022, 10, 3, 5, 3, 50, 206, DateTimeKind.Unspecified).AddTicks(1179), "Saepe ipsam.", 4, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 6, 21, 58, 16, 134, DateTimeKind.Unspecified).AddTicks(6106), "Aut illo similique nesciunt iusto eum sint explicabo.", new DateTime(2022, 9, 30, 22, 2, 24, 295, DateTimeKind.Unspecified).AddTicks(2006), "Veniam delectus.", 36, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 21, 41, 16, 153, DateTimeKind.Unspecified).AddTicks(44), "Molestiae minus ratione.", new DateTime(2021, 9, 26, 13, 47, 55, 151, DateTimeKind.Unspecified).AddTicks(5236), "Minima tempora.", 44, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 23, 36, 4, 909, DateTimeKind.Unspecified).AddTicks(3335), "Dignissimos in tenetur neque.", new DateTime(2021, 2, 3, 19, 5, 14, 127, DateTimeKind.Unspecified).AddTicks(9477), "Voluptate voluptatem.", 40, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 4, 20, 15, 391, DateTimeKind.Unspecified).AddTicks(7680), "Ea dolore facere quos reprehenderit id.", new DateTime(2022, 1, 31, 11, 3, 44, 816, DateTimeKind.Unspecified).AddTicks(4717), "Quia magni.", 49, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 17, 6, 53, 4, 940, DateTimeKind.Unspecified).AddTicks(196), "Ea rem rerum voluptas consequatur perferendis velit.", new DateTime(2022, 12, 31, 20, 45, 24, 679, DateTimeKind.Unspecified).AddTicks(5671), "Dolorum a.", 42, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 8, 8, 55, 27, 188, DateTimeKind.Unspecified).AddTicks(2643), "Quasi aliquid ut.", new DateTime(2021, 1, 19, 23, 55, 26, 35, DateTimeKind.Unspecified).AddTicks(6506), "Sed dolores.", 43, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 3, 19, 7, 42, 24, DateTimeKind.Unspecified).AddTicks(7462), "Quia et sit assumenda est pariatur facere autem.", new DateTime(2022, 2, 15, 0, 12, 35, 791, DateTimeKind.Unspecified).AddTicks(7990), "Repudiandae et.", 17, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 8, 11, 58, 353, DateTimeKind.Unspecified).AddTicks(4697), "Dolore commodi nesciunt libero natus inventore.", new DateTime(2022, 10, 19, 18, 7, 44, 519, DateTimeKind.Unspecified).AddTicks(1109), "Dolor vel.", 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 18, 15, 32, 42, 96, DateTimeKind.Unspecified).AddTicks(2443), "Adipisci nesciunt iure id amet iusto architecto nisi exercitationem.", new DateTime(2021, 11, 14, 5, 59, 57, 379, DateTimeKind.Unspecified).AddTicks(3012), "Et nihil.", 5, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 15, 15, 6, 46, 373, DateTimeKind.Unspecified).AddTicks(1315), "Quibusdam animi dolorem ipsa sed.", new DateTime(2022, 3, 27, 7, 24, 44, 448, DateTimeKind.Unspecified).AddTicks(5594), "Reiciendis labore.", 46, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 9, 16, 13, 31, 793, DateTimeKind.Unspecified).AddTicks(1442), "A nisi qui.", new DateTime(2022, 9, 20, 21, 16, 10, 161, DateTimeKind.Unspecified).AddTicks(8037), "Quis aut.", 15, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 25, 22, 20, 36, 965, DateTimeKind.Unspecified).AddTicks(5915), "Est autem ratione provident veniam quasi omnis voluptatem quaerat minus.", new DateTime(2021, 12, 24, 7, 43, 31, 53, DateTimeKind.Unspecified).AddTicks(7698), "Aut et.", 29, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 27, 14, 54, 27, 99, DateTimeKind.Unspecified).AddTicks(2466), "Aut odit cumque qui tempora veritatis assumenda dignissimos quos amet.", new DateTime(2021, 11, 1, 6, 9, 52, 265, DateTimeKind.Unspecified).AddTicks(5530), "Fuga atque.", 26, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 20, 12, 11, 22, 100, DateTimeKind.Unspecified).AddTicks(836), "Quod iste id magnam deserunt adipisci quis et aliquam.", new DateTime(2021, 1, 27, 10, 40, 42, 17, DateTimeKind.Unspecified).AddTicks(7477), "Velit aut.", 29, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 24, 7, 43, 57, 394, DateTimeKind.Unspecified).AddTicks(4513), "Blanditiis omnis cum similique.", new DateTime(2022, 7, 11, 7, 21, 29, 581, DateTimeKind.Unspecified).AddTicks(10), "Velit sit.", 15, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 20, 15, 13, 636, DateTimeKind.Unspecified).AddTicks(9123), "Unde ex unde.", new DateTime(2022, 10, 10, 10, 21, 26, 433, DateTimeKind.Unspecified).AddTicks(1520), "Reiciendis dolorum.", 36, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 5, 4, 8, 847, DateTimeKind.Unspecified).AddTicks(9180), "Ullam et dolores est quia sit perferendis debitis.", new DateTime(2022, 3, 28, 11, 1, 42, 394, DateTimeKind.Unspecified).AddTicks(6372), "Natus eligendi.", 19, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 28, 12, 0, 4, 116, DateTimeKind.Unspecified).AddTicks(5784), "Dolor reprehenderit fuga nobis dolore.", new DateTime(2022, 9, 28, 2, 42, 49, 306, DateTimeKind.Unspecified).AddTicks(7357), "Sit sit.", 9, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 17, 37, 53, 831, DateTimeKind.Unspecified).AddTicks(5094), "Excepturi quia id nesciunt.", new DateTime(2022, 4, 29, 19, 41, 9, 781, DateTimeKind.Unspecified).AddTicks(3855), "Occaecati velit.", 26, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 15, 10, 4, 17, 350, DateTimeKind.Unspecified).AddTicks(6486), "Voluptatem voluptates animi rem.", new DateTime(2022, 7, 20, 8, 50, 14, 618, DateTimeKind.Unspecified).AddTicks(4610), "Sint eius.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 14, 19, 18, 0, 15, DateTimeKind.Unspecified).AddTicks(2302), "Et ab sunt.", new DateTime(2022, 10, 17, 2, 24, 28, 211, DateTimeKind.Unspecified).AddTicks(3967), "Eligendi omnis.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 7, 11, 26, 54, 337, DateTimeKind.Unspecified).AddTicks(4298), "Optio officiis facere error a architecto consequuntur.", new DateTime(2021, 12, 3, 17, 48, 51, 990, DateTimeKind.Unspecified).AddTicks(2460), "Delectus eum.", 36, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 26, 6, 38, 44, 933, DateTimeKind.Unspecified).AddTicks(3367), "Magnam quia dolorem vel in ullam magnam animi.", new DateTime(2021, 2, 3, 21, 48, 23, 491, DateTimeKind.Unspecified).AddTicks(407), "Aspernatur ipsam.", 35, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 9, 5, 59, 989, DateTimeKind.Unspecified).AddTicks(3258), "Quis voluptatibus iste in praesentium voluptates.", new DateTime(2022, 6, 9, 18, 56, 21, 150, DateTimeKind.Unspecified).AddTicks(4927), "Ad quia.", 16, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 16, 23, 28, 2, 976, DateTimeKind.Unspecified).AddTicks(8095), "Eos molestiae consequatur temporibus.", new DateTime(2022, 3, 21, 18, 52, 45, 952, DateTimeKind.Unspecified).AddTicks(8471), "Est sed.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 12, 4, 43, 50, DateTimeKind.Unspecified).AddTicks(7371), "Rerum rem aut esse accusantium laudantium qui.", new DateTime(2021, 8, 18, 0, 46, 27, 286, DateTimeKind.Unspecified).AddTicks(7059), "Dolor alias.", 48, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 18, 11, 24, 28, 836, DateTimeKind.Unspecified).AddTicks(5971), "Rerum qui omnis labore molestiae.", new DateTime(2022, 6, 18, 21, 53, 34, 310, DateTimeKind.Unspecified).AddTicks(5253), "Fugiat iure.", 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 0, 19, 2, 175, DateTimeKind.Unspecified).AddTicks(6451), "Eaque quia iste soluta.", new DateTime(2021, 4, 7, 23, 15, 9, 161, DateTimeKind.Unspecified).AddTicks(708), "Fugiat quia.", 8, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 2, 14, 19, 41, 852, DateTimeKind.Unspecified).AddTicks(6612), "Sint aut maiores sed rem nulla similique.", new DateTime(2022, 7, 2, 4, 8, 59, 684, DateTimeKind.Unspecified).AddTicks(8423), "Minima consequuntur.", 6, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 20, 0, 13, 25, 771, DateTimeKind.Unspecified).AddTicks(4455), "Error quidem aut nisi nisi quia debitis numquam voluptatibus.", new DateTime(2022, 5, 15, 16, 18, 17, 322, DateTimeKind.Unspecified).AddTicks(166), "Reiciendis itaque.", 35, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 12, 26, 0, 10, 6, 636, DateTimeKind.Unspecified).AddTicks(3432), "Provident consequatur delectus aut quia quaerat ipsa sint placeat est.", new DateTime(2021, 8, 5, 7, 10, 14, 313, DateTimeKind.Unspecified).AddTicks(4785), "Laudantium sint.", 31 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 7, 11, 12, 967, DateTimeKind.Unspecified).AddTicks(7704), "Eos necessitatibus sit beatae ut.", new DateTime(2021, 10, 9, 5, 48, 48, 593, DateTimeKind.Unspecified).AddTicks(9437), "Nam dolorem.", 43, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 7, 23, 48, 605, DateTimeKind.Unspecified).AddTicks(2335), "Laudantium nam rem.", new DateTime(2022, 6, 6, 0, 20, 6, 177, DateTimeKind.Unspecified).AddTicks(9862), "Officiis culpa.", 41, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 8, 7, 37, 341, DateTimeKind.Unspecified).AddTicks(607), "Debitis officia cumque aliquid.", new DateTime(2022, 4, 18, 21, 7, 44, 962, DateTimeKind.Unspecified).AddTicks(7885), "Iure quis.", 1, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 7, 14, 16, 26, 459, DateTimeKind.Unspecified).AddTicks(3783), "Aut autem repudiandae atque sunt nemo.", new DateTime(2021, 12, 19, 7, 55, 35, 748, DateTimeKind.Unspecified).AddTicks(7261), "Vero consequuntur.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 30, 4, 47, 44, 150, DateTimeKind.Unspecified).AddTicks(573), "Aut modi perferendis exercitationem voluptatem.", new DateTime(2022, 3, 20, 14, 58, 40, 507, DateTimeKind.Unspecified).AddTicks(3515), "Laboriosam et.", 11, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 30, 20, 4, 31, 557, DateTimeKind.Unspecified).AddTicks(5796), "Ipsam expedita reprehenderit ab omnis adipisci maxime incidunt et.", new DateTime(2021, 11, 13, 11, 59, 13, 659, DateTimeKind.Unspecified).AddTicks(9965), "Hic ut.", 26, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 4, 11, 58, 29, 194, DateTimeKind.Unspecified).AddTicks(5330), "Aut esse et.", new DateTime(2021, 10, 29, 20, 30, 50, 634, DateTimeKind.Unspecified).AddTicks(9714), "Excepturi veritatis.", 40, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 22, 9, 26, 1, 241, DateTimeKind.Unspecified).AddTicks(5014), "Libero velit consequatur et.", new DateTime(2021, 2, 27, 11, 25, 5, 470, DateTimeKind.Unspecified).AddTicks(8664), "Et et.", 19, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 23, 7, 52, 447, DateTimeKind.Unspecified).AddTicks(2905), "Illum est voluptatem.", new DateTime(2022, 3, 6, 1, 13, 50, 257, DateTimeKind.Unspecified).AddTicks(6517), "Consequatur qui.", 22, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 18, 59, 52, 994, DateTimeKind.Unspecified).AddTicks(353), "Officia doloremque eius quo quod animi ipsa.", new DateTime(2022, 1, 23, 23, 30, 42, 314, DateTimeKind.Unspecified).AddTicks(1030), "Tenetur nesciunt.", 50, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 7, 26, 30, 88, DateTimeKind.Unspecified).AddTicks(494), "Distinctio saepe ducimus natus maxime fuga et accusantium iusto.", new DateTime(2021, 4, 3, 10, 38, 42, 207, DateTimeKind.Unspecified).AddTicks(4553), "Consequatur vel.", 42, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 29, 15, 40, 55, 166, DateTimeKind.Unspecified).AddTicks(8889), "Odit tempora facere vel debitis.", new DateTime(2022, 1, 21, 2, 0, 14, 803, DateTimeKind.Unspecified).AddTicks(3454), "Tempore enim.", 29, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 0, 38, 3, 216, DateTimeKind.Unspecified).AddTicks(7593), "Itaque earum officiis numquam fugiat laudantium voluptate.", new DateTime(2022, 5, 16, 13, 43, 16, 113, DateTimeKind.Unspecified).AddTicks(3670), "Qui qui.", 23, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 6, 9, 42, 10, 548, DateTimeKind.Unspecified).AddTicks(8305), "Ullam distinctio magni nemo reprehenderit cumque earum.", new DateTime(2021, 1, 25, 18, 47, 19, 907, DateTimeKind.Unspecified).AddTicks(7253), "Ipsam eum.", 23, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 11, 2, 59, 13, 532, DateTimeKind.Unspecified).AddTicks(6037), "Provident et reprehenderit ad harum.", new DateTime(2022, 11, 18, 1, 44, 37, 312, DateTimeKind.Unspecified).AddTicks(2539), "Et animi.", 42, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 3, 15, 43, 40, 164, DateTimeKind.Unspecified).AddTicks(488), "In blanditiis error autem et quod.", new DateTime(2021, 11, 26, 0, 45, 47, 860, DateTimeKind.Unspecified).AddTicks(6197), "Sit omnis.", 14, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 5, 6, 52, 11, 79, DateTimeKind.Unspecified).AddTicks(7722), "Expedita id id assumenda sed autem quidem iste quaerat.", new DateTime(2022, 10, 23, 10, 11, 22, 437, DateTimeKind.Unspecified).AddTicks(8375), "Et harum.", 39, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 20, 6, 20, 423, DateTimeKind.Unspecified).AddTicks(4830), "Et non et nam minus aut suscipit enim.", new DateTime(2021, 1, 7, 20, 28, 26, 426, DateTimeKind.Unspecified).AddTicks(7430), "Velit mollitia.", 18, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 27, 15, 47, 42, 732, DateTimeKind.Unspecified).AddTicks(3280), "Ut laboriosam itaque et non et ducimus earum eius.", new DateTime(2022, 10, 22, 5, 38, 28, 926, DateTimeKind.Unspecified).AddTicks(1051), "Fugiat quia.", 1, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 19, 14, 17, 54, 614, DateTimeKind.Unspecified).AddTicks(3528), "Libero id non tenetur molestiae commodi.", new DateTime(2022, 8, 19, 13, 5, 45, 481, DateTimeKind.Unspecified).AddTicks(40), "A sapiente.", 50, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 11, 22, 38, 7, 21, DateTimeKind.Unspecified).AddTicks(4643), "Illo aperiam deleniti id.", new DateTime(2021, 2, 11, 14, 8, 52, 636, DateTimeKind.Unspecified).AddTicks(7235), "In dolore.", 20, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 9, 10, 5, 852, DateTimeKind.Unspecified).AddTicks(2232), "Rerum qui eveniet dolores et.", new DateTime(2021, 9, 12, 0, 13, 4, 166, DateTimeKind.Unspecified).AddTicks(302), "Veritatis tempore.", 49, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 0, 55, 16, 868, DateTimeKind.Unspecified).AddTicks(9851), "Et itaque temporibus.", new DateTime(2022, 9, 10, 23, 14, 30, 488, DateTimeKind.Unspecified).AddTicks(1866), "Consequatur voluptatem.", 21, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 23, 9, 19, 744, DateTimeKind.Unspecified).AddTicks(276), "Quia praesentium et dolorem qui iure.", new DateTime(2022, 2, 19, 20, 45, 8, 580, DateTimeKind.Unspecified).AddTicks(1893), "Blanditiis culpa.", 13, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 23, 0, 15, 37, 452, DateTimeKind.Unspecified).AddTicks(6233), "Occaecati ut veniam.", new DateTime(2022, 8, 25, 16, 5, 7, 115, DateTimeKind.Unspecified).AddTicks(9265), "Magnam odio.", 28, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 9, 0, 5, 14, 91, DateTimeKind.Unspecified).AddTicks(5989), "Sed voluptates exercitationem enim nesciunt animi porro.", new DateTime(2021, 6, 26, 12, 1, 36, 714, DateTimeKind.Unspecified).AddTicks(2690), "Hic officia.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 14, 13, 37, 58, 254, DateTimeKind.Unspecified).AddTicks(7167), "Temporibus perferendis nam eum provident iure repudiandae officia.", new DateTime(2022, 2, 10, 20, 36, 23, 989, DateTimeKind.Unspecified).AddTicks(9858), "Quidem aut.", 20, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 10, 10, 40, 43, 975, DateTimeKind.Unspecified).AddTicks(4341), "Distinctio quis tenetur.", new DateTime(2022, 3, 29, 18, 33, 30, 926, DateTimeKind.Unspecified).AddTicks(692), "Similique itaque.", 16, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 5, 15, 14, 11, 636, DateTimeKind.Unspecified).AddTicks(9348), "Eaque impedit excepturi ea.", new DateTime(2022, 9, 30, 10, 56, 8, 818, DateTimeKind.Unspecified).AddTicks(5656), "Accusantium asperiores.", 34, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 14, 55, 33, 39, DateTimeKind.Unspecified).AddTicks(3185), "Vel qui dignissimos quisquam nobis ullam voluptatibus corrupti libero libero.", new DateTime(2021, 9, 1, 23, 54, 16, 927, DateTimeKind.Unspecified).AddTicks(3239), "Delectus et.", 32, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 16, 15, 10, 405, DateTimeKind.Unspecified).AddTicks(4293), "Ducimus qui nesciunt omnis consequatur modi et ducimus odio minima.", new DateTime(2021, 12, 6, 8, 31, 23, 238, DateTimeKind.Unspecified).AddTicks(6382), "Ratione illum.", 17, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 21, 12, 36, 7, 951, DateTimeKind.Unspecified).AddTicks(3208), "Ut at voluptatem.", new DateTime(2021, 5, 7, 5, 3, 52, 341, DateTimeKind.Unspecified).AddTicks(5568), "Nulla dignissimos.", 33, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 12, 23, 53, 43, 957, DateTimeKind.Unspecified).AddTicks(8303), "Molestiae blanditiis asperiores doloribus est amet minima ut aut voluptas.", new DateTime(2022, 11, 24, 2, 26, 21, 758, DateTimeKind.Unspecified).AddTicks(1096), "Sequi quasi.", 2, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 10, 11, 11, 38, 244, DateTimeKind.Unspecified).AddTicks(4445), "Ut dolore doloremque expedita suscipit ut.", new DateTime(2022, 9, 17, 10, 25, 23, 273, DateTimeKind.Unspecified).AddTicks(5062), "Odit et.", 36, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 13, 4, 44, 25, 503, DateTimeKind.Unspecified).AddTicks(2851), "At aperiam qui.", new DateTime(2021, 1, 30, 19, 18, 49, 612, DateTimeKind.Unspecified).AddTicks(6224), "Fugiat id.", 25, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 23, 13, 58, 7, 10, DateTimeKind.Unspecified).AddTicks(2198), "Maxime quisquam voluptates unde quia sunt saepe in.", new DateTime(2022, 8, 17, 14, 48, 58, 621, DateTimeKind.Unspecified).AddTicks(5908), "Itaque beatae.", 26, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 9, 9, 29, 8, 676, DateTimeKind.Unspecified).AddTicks(6297), "Sit minus quidem nesciunt neque adipisci qui dolorum eligendi doloribus.", new DateTime(2022, 10, 2, 10, 53, 57, 576, DateTimeKind.Unspecified).AddTicks(6867), "Dolorem molestiae.", 11, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 3, 27, 4, 9, 22, 2, DateTimeKind.Unspecified).AddTicks(6957), "Perferendis nobis quo.", new DateTime(2021, 5, 25, 19, 17, 38, 672, DateTimeKind.Unspecified).AddTicks(1207), "Et expedita.", 49 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 8, 23, 20, 33, 240, DateTimeKind.Unspecified).AddTicks(2815), "Delectus recusandae modi eum et illum suscipit.", new DateTime(2021, 6, 5, 11, 17, 41, 782, DateTimeKind.Unspecified).AddTicks(5567), "Repellendus dicta.", 24, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 16, 5, 57, 33, 65, DateTimeKind.Unspecified).AddTicks(3879), "Voluptatem et labore consequatur enim soluta tenetur.", new DateTime(2021, 2, 6, 21, 22, 26, 737, DateTimeKind.Unspecified).AddTicks(5664), "Sit ut.", 40, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 6, 12, 53, 975, DateTimeKind.Unspecified).AddTicks(8377), "Laudantium dignissimos et.", new DateTime(2022, 5, 22, 23, 47, 41, 296, DateTimeKind.Unspecified).AddTicks(8524), "Consequatur fuga.", 24, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 19, 45, 52, 380, DateTimeKind.Unspecified).AddTicks(7609), "Autem consequatur rerum ut praesentium sed.", new DateTime(2021, 2, 7, 22, 6, 50, 239, DateTimeKind.Unspecified).AddTicks(897), "Velit corrupti.", 44, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 23, 0, 38, 7, 529, DateTimeKind.Unspecified).AddTicks(8851), "Maiores ut dolor exercitationem rerum molestias at adipisci voluptas.", new DateTime(2022, 5, 13, 15, 41, 29, 820, DateTimeKind.Unspecified).AddTicks(37), "Exercitationem vel.", 37, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 3, 8, 27, 13, 495, DateTimeKind.Unspecified).AddTicks(4530), "Quaerat sapiente libero modi ut voluptatem officia voluptatem dolor.", new DateTime(2022, 8, 14, 9, 49, 7, 108, DateTimeKind.Unspecified).AddTicks(7731), "Et sit.", 2, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 18, 22, 13, 1, 450, DateTimeKind.Unspecified).AddTicks(4875), "Et beatae ut quia qui.", new DateTime(2021, 7, 14, 14, 11, 4, 476, DateTimeKind.Unspecified).AddTicks(8248), "Optio enim.", 48, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 13, 16, 26, 17, 581, DateTimeKind.Unspecified).AddTicks(8263), "Ex et ea omnis accusamus aut eum impedit rerum et.", new DateTime(2022, 10, 6, 2, 30, 57, 264, DateTimeKind.Unspecified).AddTicks(5740), "Commodi delectus.", 33, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 4, 18, 7, 36, 48, 992, DateTimeKind.Unspecified).AddTicks(146), "Voluptas recusandae et est eius facilis odit.", new DateTime(2021, 6, 11, 4, 21, 26, 989, DateTimeKind.Unspecified).AddTicks(2387), "Quas fuga.", 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 9, 23, 35, 656, DateTimeKind.Unspecified).AddTicks(5109), "Hic veniam quo iure rerum quisquam atque cumque autem accusantium.", new DateTime(2021, 12, 2, 16, 52, 16, 992, DateTimeKind.Unspecified).AddTicks(3032), "Repellendus neque.", 17, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 22, 23, 4, 8, 620, DateTimeKind.Unspecified).AddTicks(5189), "Qui vel odio voluptas.", new DateTime(2021, 7, 27, 11, 7, 6, 362, DateTimeKind.Unspecified).AddTicks(1310), "Rerum ut.", 45, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 23, 46, 18, 593, DateTimeKind.Unspecified).AddTicks(6327), "Quae et labore facilis alias.", new DateTime(2021, 4, 7, 4, 27, 8, 251, DateTimeKind.Unspecified).AddTicks(6688), "Cumque et.", 6, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 25, 13, 43, 44, 298, DateTimeKind.Unspecified).AddTicks(3772), "Ullam quae sed qui enim omnis dolorem libero quia.", new DateTime(2021, 9, 30, 0, 7, 49, 792, DateTimeKind.Unspecified).AddTicks(4946), "Quia veritatis.", 3, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 27, 21, 39, 43, 313, DateTimeKind.Unspecified).AddTicks(9687), "Est totam temporibus numquam quas iusto necessitatibus.", new DateTime(2022, 5, 29, 0, 3, 25, 686, DateTimeKind.Unspecified).AddTicks(5250), "Modi corrupti.", 50, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 13, 17, 49, 23, 114, DateTimeKind.Unspecified).AddTicks(1832), "Est voluptatem maiores at.", new DateTime(2022, 3, 31, 9, 16, 51, 698, DateTimeKind.Unspecified).AddTicks(3447), "Quo adipisci.", 32, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 3, 6, 11, 20, 248, DateTimeKind.Unspecified).AddTicks(8434), "Illum asperiores earum.", new DateTime(2022, 11, 23, 22, 45, 30, 68, DateTimeKind.Unspecified).AddTicks(4545), "Dolorem repellendus.", 48, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 27, 21, 8, 55, 397, DateTimeKind.Unspecified).AddTicks(4092), "Ea ducimus incidunt non deleniti nihil vitae.", new DateTime(2021, 2, 8, 7, 39, 50, 885, DateTimeKind.Unspecified).AddTicks(4922), "Neque sed.", 13, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 26, 1, 51, 31, 572, DateTimeKind.Unspecified).AddTicks(1811), "Aut reprehenderit asperiores sit esse.", new DateTime(2022, 10, 2, 12, 48, 48, 844, DateTimeKind.Unspecified).AddTicks(5055), "Blanditiis quae.", 19, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 6, 13, 52, 43, 671, DateTimeKind.Unspecified).AddTicks(4712), "Non esse occaecati assumenda dolores quasi aut nostrum ex impedit.", new DateTime(2022, 11, 12, 9, 26, 35, 522, DateTimeKind.Unspecified).AddTicks(9377), "Velit voluptate.", 17, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 17, 21, 47, 749, DateTimeKind.Unspecified).AddTicks(8749), "Eaque ipsum distinctio itaque beatae totam sunt.", new DateTime(2021, 11, 16, 20, 22, 34, 373, DateTimeKind.Unspecified).AddTicks(7044), "Consequatur occaecati.", 5, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 12, 5, 5, 413, DateTimeKind.Unspecified).AddTicks(107), "Corrupti consectetur voluptas provident omnis officiis.", new DateTime(2022, 7, 12, 14, 11, 53, 833, DateTimeKind.Unspecified).AddTicks(2302), "In nemo.", 35, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 3, 17, 38, 46, 108, DateTimeKind.Unspecified).AddTicks(2679), "Exercitationem doloribus expedita quam ab est iste velit alias.", new DateTime(2022, 2, 23, 11, 35, 53, 705, DateTimeKind.Unspecified).AddTicks(9845), "Ab corporis.", 33, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 12, 8, 3, 34, 277, DateTimeKind.Unspecified).AddTicks(7638), "Commodi molestias eos et repellat dignissimos magnam temporibus.", new DateTime(2021, 4, 4, 18, 4, 9, 891, DateTimeKind.Unspecified).AddTicks(4459), "Qui ut.", 50, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 2, 10, 32, 473, DateTimeKind.Unspecified).AddTicks(1033), "Vitae eos voluptatem enim deleniti maxime id rem voluptatum sunt.", new DateTime(2021, 8, 8, 22, 57, 27, 217, DateTimeKind.Unspecified).AddTicks(7403), "Illum qui.", 50, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 15, 18, 59, 55, 897, DateTimeKind.Unspecified).AddTicks(4663), "Impedit suscipit et sit consequatur quam sint non quam perferendis.", new DateTime(2021, 8, 10, 6, 28, 55, 213, DateTimeKind.Unspecified).AddTicks(3672), "Possimus sint.", 4, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 29, 5, 20, 29, 729, DateTimeKind.Unspecified).AddTicks(8511), "Officia tempore facilis et id.", new DateTime(2022, 8, 6, 15, 34, 45, 875, DateTimeKind.Unspecified).AddTicks(925), "Quia veritatis.", 15, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 1, 9, 6, 9, 78, DateTimeKind.Unspecified).AddTicks(9449), "Enim excepturi laudantium provident maxime eligendi in et voluptatem consectetur.", new DateTime(2022, 11, 18, 17, 10, 1, 284, DateTimeKind.Unspecified).AddTicks(713), "Ut aliquam.", 46, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 29, 21, 48, 39, 679, DateTimeKind.Unspecified).AddTicks(909), "Quia ex rerum similique neque.", new DateTime(2022, 10, 29, 16, 26, 12, 321, DateTimeKind.Unspecified).AddTicks(3671), "Sit nihil.", 45, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 11, 10, 14, 48, 125, DateTimeKind.Unspecified).AddTicks(3760), "Quo repellendus officiis minima.", new DateTime(2022, 11, 2, 20, 44, 8, 196, DateTimeKind.Unspecified).AddTicks(737), "Sit doloribus.", 9, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 22, 55, 49, 294, DateTimeKind.Unspecified).AddTicks(6199), "Similique harum in fugiat odio.", new DateTime(2021, 7, 16, 17, 57, 16, 55, DateTimeKind.Unspecified).AddTicks(8459), "Recusandae ut.", 35, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 2, 18, 39, 26, 535, DateTimeKind.Unspecified).AddTicks(2583), "Accusantium eligendi quisquam illo minus exercitationem.", new DateTime(2022, 3, 21, 16, 22, 1, 146, DateTimeKind.Unspecified).AddTicks(5500), "Enim voluptas.", 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 5, 17, 50, 2, 444, DateTimeKind.Unspecified).AddTicks(3701), "Velit atque delectus quas dolor est qui enim dolor est.", new DateTime(2021, 2, 9, 14, 17, 24, 339, DateTimeKind.Unspecified).AddTicks(1297), "Pariatur et.", 21, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 11, 12, 30, 59, 340, DateTimeKind.Unspecified).AddTicks(9092), "Et eum rerum dolor alias quam incidunt.", new DateTime(2021, 12, 8, 1, 19, 16, 943, DateTimeKind.Unspecified).AddTicks(5018), "Consequatur amet.", 20, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 7, 15, 47, 9, 64, DateTimeKind.Unspecified).AddTicks(5252), "Ut vero fugiat vel id molestiae unde nemo veritatis aut.", new DateTime(2022, 5, 17, 15, 17, 22, 27, DateTimeKind.Unspecified).AddTicks(9121), "Est nesciunt.", 31, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 12, 0, 6, 41, 496, DateTimeKind.Unspecified).AddTicks(2149), "Placeat sint itaque suscipit sunt perspiciatis qui tempore incidunt occaecati.", new DateTime(2021, 8, 13, 19, 8, 39, 897, DateTimeKind.Unspecified).AddTicks(6052), "Eligendi quo.", 6, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 22, 30, 40, 359, DateTimeKind.Unspecified).AddTicks(5999), "Sit dicta ut amet assumenda eum.", new DateTime(2021, 5, 30, 17, 32, 34, 285, DateTimeKind.Unspecified).AddTicks(4310), "Omnis maiores.", 3, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 4, 4, 4, 18, 374, DateTimeKind.Unspecified).AddTicks(9484), "Et placeat id natus sunt aliquam vero non consequatur numquam.", new DateTime(2022, 4, 17, 12, 29, 52, 104, DateTimeKind.Unspecified).AddTicks(8386), "Molestias cumque.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 4, 16, 38, 29, 390, DateTimeKind.Unspecified).AddTicks(6892), "Et vero dicta.", new DateTime(2021, 1, 23, 13, 13, 54, 504, DateTimeKind.Unspecified).AddTicks(4209), "Maxime a.", 42, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 27, 5, 34, 18, 422, DateTimeKind.Unspecified).AddTicks(5837), "Sunt explicabo sed explicabo beatae incidunt minima facilis aliquid iure.", new DateTime(2021, 1, 18, 4, 16, 33, 166, DateTimeKind.Unspecified).AddTicks(6957), "Blanditiis aperiam.", 47, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 3, 20, 17, 13, 234, DateTimeKind.Unspecified).AddTicks(7576), "Deleniti sit et necessitatibus.", new DateTime(2022, 11, 16, 17, 13, 8, 580, DateTimeKind.Unspecified).AddTicks(7830), "Voluptatibus mollitia.", 41, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 6, 14, 27, 700, DateTimeKind.Unspecified).AddTicks(5091), "Eius ab eum doloribus voluptatem cum reprehenderit neque omnis maxime.", new DateTime(2021, 6, 2, 4, 47, 16, 104, DateTimeKind.Unspecified).AddTicks(6211), "Impedit magni.", 1, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 28, 13, 1, 43, 276, DateTimeKind.Unspecified).AddTicks(9349), "Ullam dolor fugit sequi eveniet culpa eum voluptatum quo repellendus.", new DateTime(2022, 7, 7, 3, 7, 24, 546, DateTimeKind.Unspecified).AddTicks(8522), "Omnis ea.", 18, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 11, 10, 8, 8, 0, 174, DateTimeKind.Unspecified).AddTicks(6970), "Sit et quo cum ab ullam eos.", new DateTime(2021, 10, 20, 15, 0, 12, 573, DateTimeKind.Unspecified).AddTicks(1156), "Sint eaque.", 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 29, 23, 54, 13, 39, DateTimeKind.Unspecified).AddTicks(3647), "Repellendus nihil eos.", new DateTime(2021, 5, 20, 14, 7, 32, 357, DateTimeKind.Unspecified).AddTicks(119), "Itaque delectus.", 23, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 18, 3, 32, 38, 659, DateTimeKind.Unspecified).AddTicks(4985), "Molestiae quia qui aliquid omnis perspiciatis culpa id voluptatem molestiae.", new DateTime(2022, 1, 20, 12, 19, 56, 110, DateTimeKind.Unspecified).AddTicks(9192), "Dolor quidem.", 46, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 10, 19, 8, 16, 47, 557, DateTimeKind.Unspecified).AddTicks(995), "Impedit ad optio dolor laudantium sit quia asperiores pariatur.", new DateTime(2021, 3, 29, 6, 19, 19, 161, DateTimeKind.Unspecified).AddTicks(6926), "Et occaecati.", 26 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 0, 20, 21, 192, DateTimeKind.Unspecified).AddTicks(2801), "Quasi nesciunt tempora non praesentium aspernatur tempore eaque eveniet dolorem.", new DateTime(2022, 3, 25, 0, 8, 26, 755, DateTimeKind.Unspecified).AddTicks(9816), "Exercitationem maiores.", 21, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 4, 23, 44, 240, DateTimeKind.Unspecified).AddTicks(1696), "Dolor delectus dolorem ea deleniti.", new DateTime(2021, 11, 14, 14, 38, 33, 441, DateTimeKind.Unspecified).AddTicks(4588), "In quis.", 47, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 16, 23, 43, 18, 932, DateTimeKind.Unspecified).AddTicks(9975), "Iure excepturi quas repellat sunt.", new DateTime(2021, 10, 27, 13, 32, 25, 576, DateTimeKind.Unspecified).AddTicks(8571), "Est porro.", 41, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 10, 7, 38, 43, 607, DateTimeKind.Unspecified).AddTicks(8658), "In ab perferendis voluptates veritatis in.", new DateTime(2022, 2, 25, 4, 3, 42, 988, DateTimeKind.Unspecified).AddTicks(6736), "Ut id.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 11, 19, 4, 42, 2, 160, DateTimeKind.Unspecified).AddTicks(8696), "Autem est odit sint et libero accusamus.", new DateTime(2021, 2, 20, 11, 8, 48, 966, DateTimeKind.Unspecified).AddTicks(4710), "Illo accusantium.", 42 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 24, 7, 49, 59, 405, DateTimeKind.Unspecified).AddTicks(7312), "Nesciunt placeat nisi maxime.", new DateTime(2022, 7, 23, 20, 24, 11, 689, DateTimeKind.Unspecified).AddTicks(4915), "Commodi reiciendis.", 7, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 17, 44, 2, 888, DateTimeKind.Unspecified).AddTicks(494), "Similique facilis ipsa.", new DateTime(2022, 8, 30, 8, 40, 34, 73, DateTimeKind.Unspecified).AddTicks(1560), "Commodi accusantium.", 33, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 9, 15, 23, 33, 43, 224, DateTimeKind.Unspecified).AddTicks(9090), "Est incidunt harum facilis.", new DateTime(2022, 7, 25, 1, 42, 35, 522, DateTimeKind.Unspecified).AddTicks(6267), "Quae in.", 24 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 19, 28, 31, 713, DateTimeKind.Unspecified).AddTicks(7766), "Necessitatibus non sit.", new DateTime(2021, 5, 26, 19, 1, 6, 154, DateTimeKind.Unspecified).AddTicks(4930), "Voluptatem molestias.", 30, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 23, 12, 53, 252, DateTimeKind.Unspecified).AddTicks(8417), "Aut dolorem unde.", new DateTime(2021, 7, 25, 3, 12, 25, 586, DateTimeKind.Unspecified).AddTicks(8314), "Eum et.", 3, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 6, 6, 0, 236, DateTimeKind.Unspecified).AddTicks(1107), "Excepturi omnis rerum voluptas ab rerum non quod voluptatem.", new DateTime(2022, 6, 3, 16, 2, 53, 172, DateTimeKind.Unspecified).AddTicks(256), "Voluptas ut.", 16, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 25, 4, 21, 9, 355, DateTimeKind.Unspecified).AddTicks(4270), "Ea quam ipsa.", new DateTime(2021, 5, 24, 8, 15, 5, 139, DateTimeKind.Unspecified).AddTicks(603), "Provident repellat.", 15, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 2, 5, 49, 47, 622, DateTimeKind.Unspecified).AddTicks(1378), "Quis voluptatum voluptatem ea aut ullam.", new DateTime(2021, 1, 6, 23, 9, 59, 357, DateTimeKind.Unspecified).AddTicks(7077), "Quia sunt.", 25, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 22, 35, 2, 971, DateTimeKind.Unspecified).AddTicks(3704), "Quia doloremque cumque.", new DateTime(2021, 11, 23, 11, 5, 32, 841, DateTimeKind.Unspecified).AddTicks(8750), "Pariatur fugit.", 5, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 7, 20, 10, 58, 893, DateTimeKind.Unspecified).AddTicks(5494), "Provident ipsam quo nihil nihil.", new DateTime(2021, 4, 11, 13, 19, 13, 369, DateTimeKind.Unspecified).AddTicks(2843), "Omnis culpa.", 7, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 1, 4, 19, 14, 119, DateTimeKind.Unspecified).AddTicks(2772), "Quibusdam totam ab et aut neque eaque ut velit ab.", new DateTime(2021, 5, 1, 4, 49, 29, 563, DateTimeKind.Unspecified).AddTicks(6648), "Qui neque.", 41, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 3, 30, 59, 2, DateTimeKind.Unspecified).AddTicks(4581), "Nobis excepturi quia laboriosam iusto qui.", new DateTime(2022, 1, 23, 2, 50, 29, 963, DateTimeKind.Unspecified).AddTicks(7621), "Asperiores ut.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 24, 11, 56, 13, 769, DateTimeKind.Unspecified).AddTicks(4430), "Illo consequuntur accusamus.", new DateTime(2022, 4, 30, 6, 25, 44, 527, DateTimeKind.Unspecified).AddTicks(3039), "Maxime assumenda.", 34, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 15, 1, 36, 14, 154, DateTimeKind.Unspecified).AddTicks(6185), "Sit minima occaecati fugit quo.", new DateTime(2022, 9, 22, 10, 24, 28, 228, DateTimeKind.Unspecified).AddTicks(8149), "Labore voluptates.", 20, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 17, 23, 49, 24, 782, DateTimeKind.Unspecified).AddTicks(67), "Molestiae odit dolorem et rerum rem impedit fugit officiis odit.", new DateTime(2022, 10, 14, 19, 53, 8, 341, DateTimeKind.Unspecified).AddTicks(884), "Sed et.", 6, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 5, 2, 19, 16, 622, DateTimeKind.Unspecified).AddTicks(4404), "Ea ab debitis repellendus nostrum quo et hic.", new DateTime(2022, 12, 24, 14, 24, 50, 766, DateTimeKind.Unspecified).AddTicks(1859), "Velit repudiandae.", 31, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 17, 39, 19, 954, DateTimeKind.Unspecified).AddTicks(1611), "Numquam earum sit repellat aut et laborum.", new DateTime(2021, 11, 15, 22, 14, 49, 425, DateTimeKind.Unspecified).AddTicks(5054), "Sint molestias.", 8, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 11, 23, 33, 43, 501, DateTimeKind.Unspecified).AddTicks(8397), "Accusantium sed iure tenetur itaque est.", new DateTime(2022, 3, 2, 23, 37, 31, 156, DateTimeKind.Unspecified).AddTicks(6361), "Sed id.", 37, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 1, 4, 52, 1, 781, DateTimeKind.Unspecified).AddTicks(7876), "Repudiandae sapiente harum rerum nesciunt sint in quae voluptatem voluptas.", new DateTime(2022, 12, 15, 11, 56, 46, 254, DateTimeKind.Unspecified).AddTicks(1463), "Velit saepe.", 38, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 21, 1, 2, 3, 346, DateTimeKind.Unspecified).AddTicks(5150), "Autem accusamus rerum eum asperiores animi autem ipsum natus.", new DateTime(2021, 10, 21, 20, 15, 40, 709, DateTimeKind.Unspecified).AddTicks(1668), "Voluptas eum.", 44, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 19, 0, 48, 16, DateTimeKind.Unspecified).AddTicks(3893), "Autem illo voluptas distinctio perferendis adipisci ullam.", new DateTime(2021, 10, 31, 2, 29, 21, 867, DateTimeKind.Unspecified).AddTicks(1004), "Enim laboriosam.", 37, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 7, 0, 31, 1, 260, DateTimeKind.Unspecified).AddTicks(8559), "Id tenetur ipsam et sunt est cum quo quia.", new DateTime(2021, 10, 11, 8, 57, 36, 257, DateTimeKind.Unspecified).AddTicks(8628), "Provident est.", 25, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 14, 39, 11, 46, DateTimeKind.Unspecified).AddTicks(5620), "Et itaque quod libero sit iusto incidunt.", new DateTime(2021, 5, 29, 21, 32, 19, 246, DateTimeKind.Unspecified).AddTicks(9319), "Voluptatem qui.", 24, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 21, 7, 19, 43, 210, DateTimeKind.Unspecified).AddTicks(9399), "Consequatur deleniti praesentium quia nisi.", new DateTime(2022, 11, 27, 0, 16, 45, 807, DateTimeKind.Unspecified).AddTicks(5932), "Neque laboriosam.", 47, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 6, 16, 42, 41, 999, DateTimeKind.Unspecified).AddTicks(9387), "Occaecati dolore aliquam sapiente similique sed.", new DateTime(2022, 2, 24, 17, 36, 27, 181, DateTimeKind.Unspecified).AddTicks(410), "Esse sunt.", 32, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 12, 0, 16, 561, DateTimeKind.Unspecified).AddTicks(3348), "Consequuntur ad quibusdam odio consequatur explicabo ut.", new DateTime(2021, 6, 1, 3, 40, 12, 139, DateTimeKind.Unspecified).AddTicks(6005), "Consequuntur eum.", 46, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 13, 15, 48, 7, 453, DateTimeKind.Unspecified).AddTicks(5451), "Ut qui qui deserunt illo quae molestiae recusandae aliquid sapiente.", new DateTime(2022, 12, 4, 9, 54, 37, 349, DateTimeKind.Unspecified).AddTicks(2273), "Eos qui.", 22, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 1, 7, 6, 42, 20, DateTimeKind.Unspecified).AddTicks(6398), "Maxime mollitia omnis repellendus eos facilis vel sed.", new DateTime(2022, 1, 7, 15, 11, 14, 837, DateTimeKind.Unspecified).AddTicks(635), "Maiores occaecati.", 10, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 2, 0, 17, 109, DateTimeKind.Unspecified).AddTicks(4401), "Id quia molestias consequatur corporis.", new DateTime(2022, 2, 25, 1, 28, 6, 187, DateTimeKind.Unspecified).AddTicks(3538), "Quo similique.", 29, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 6, 2, 24, 34, 562, DateTimeKind.Unspecified).AddTicks(3253), "Nobis sed voluptatem maiores non vel deserunt voluptate molestiae.", new DateTime(2021, 11, 21, 6, 31, 44, 528, DateTimeKind.Unspecified).AddTicks(3185), "Quasi magnam.", 10, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 7, 47, 17, 92, DateTimeKind.Unspecified).AddTicks(8432), "Accusamus aut eum deserunt.", new DateTime(2022, 6, 29, 12, 23, 26, 750, DateTimeKind.Unspecified).AddTicks(2401), "Ad ut.", 45, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 3, 13, 24, 15, 863, DateTimeKind.Unspecified).AddTicks(6842), "Soluta aliquid totam distinctio quo dolor iure ut aut odio.", new DateTime(2022, 8, 22, 2, 8, 35, 526, DateTimeKind.Unspecified).AddTicks(2711), "Dolore magni.", 26, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 30, 15, 24, 6, 174, DateTimeKind.Unspecified).AddTicks(781), "Dolore quia qui dolorem molestiae ut cum.", new DateTime(2022, 5, 1, 0, 55, 9, 654, DateTimeKind.Unspecified).AddTicks(3112), "Pariatur eos.", 14, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 17, 56, 45, 27, DateTimeKind.Unspecified).AddTicks(8704), "Repudiandae in eos optio.", new DateTime(2021, 7, 9, 2, 38, 58, 398, DateTimeKind.Unspecified).AddTicks(9804), "Voluptatem quibusdam.", 25, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 17, 33, 22, 953, DateTimeKind.Unspecified).AddTicks(1610), "Qui nostrum dolores sunt.", new DateTime(2022, 8, 15, 11, 50, 39, 363, DateTimeKind.Unspecified).AddTicks(2707), "Quis neque.", 25, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 6, 41, 7, 365, DateTimeKind.Unspecified).AddTicks(8625), "Est odit placeat tempore autem ipsum enim quasi labore blanditiis.", new DateTime(2021, 11, 16, 0, 2, 10, 848, DateTimeKind.Unspecified).AddTicks(8531), "Commodi iusto.", 16, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 5, 7, 40, 39, 85, DateTimeKind.Unspecified).AddTicks(5582), "Rem hic quisquam numquam veniam consequatur quis neque rerum quia.", new DateTime(2022, 7, 27, 1, 27, 22, 331, DateTimeKind.Unspecified).AddTicks(8882), "Amet autem.", 24, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 6, 22, 5, 42, 911, DateTimeKind.Unspecified).AddTicks(2343), "Totam eum excepturi.", new DateTime(2022, 6, 2, 8, 53, 47, 388, DateTimeKind.Unspecified).AddTicks(809), "Id nemo.", 12, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 4, 5, 21, 15, 563, DateTimeKind.Unspecified).AddTicks(1519), "Ratione cumque illum perspiciatis et.", new DateTime(2021, 5, 31, 5, 59, 11, 92, DateTimeKind.Unspecified).AddTicks(7686), "Non aut.", 41, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 2, 27, 59, 632, DateTimeKind.Unspecified).AddTicks(3662), "Deserunt eligendi officiis distinctio necessitatibus modi eaque porro consequatur.", new DateTime(2021, 12, 12, 22, 29, 43, 76, DateTimeKind.Unspecified).AddTicks(111), "Illo assumenda.", 38, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 7, 34, 0, 494, DateTimeKind.Unspecified).AddTicks(2644), "Eum velit a sint nostrum assumenda accusamus consectetur aliquid qui.", new DateTime(2022, 5, 16, 7, 49, 48, 399, DateTimeKind.Unspecified).AddTicks(2801), "Et magnam.", 45, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 9, 2, 20, 10, 307, DateTimeKind.Unspecified).AddTicks(4397), "Dolorem minus consequuntur fuga illo nostrum nihil reiciendis sit quod.", new DateTime(2022, 6, 4, 3, 4, 3, 840, DateTimeKind.Unspecified).AddTicks(6729), "Libero itaque.", 19, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 1, 10, 7, 27, 55, 53, DateTimeKind.Unspecified).AddTicks(7858), "Velit cum eos.", new DateTime(2021, 11, 19, 12, 57, 16, 258, DateTimeKind.Unspecified).AddTicks(1513), "Ipsum nemo.", 28, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 1, 26, 59, 556, DateTimeKind.Unspecified).AddTicks(7485), "Nesciunt enim sed maxime quas est.", new DateTime(2021, 9, 16, 1, 53, 7, 587, DateTimeKind.Unspecified).AddTicks(3766), "Voluptatum ducimus.", 20, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId" },
                values: new object[] { new DateTime(2019, 4, 22, 7, 18, 39, 553, DateTimeKind.Unspecified).AddTicks(4311), "Voluptas vitae dignissimos reprehenderit quos laborum.", new DateTime(2022, 2, 21, 6, 6, 23, 397, DateTimeKind.Unspecified).AddTicks(7609), "Optio qui.", 21 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 8, 21, 41, 41, 252, DateTimeKind.Unspecified).AddTicks(2081), "Dolores animi est est cum cum pariatur eligendi excepturi perspiciatis.", new DateTime(2022, 7, 26, 15, 41, 16, 829, DateTimeKind.Unspecified).AddTicks(2235), "Ratione sit.", 9, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 2, 7, 21, 1, 39, 417, DateTimeKind.Unspecified).AddTicks(8545), "Roberts - Kunze" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 7, 19, 1, 22, 43, 193, DateTimeKind.Unspecified).AddTicks(9861), "Reynolds LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 1, 30, 14, 40, 2, 565, DateTimeKind.Unspecified).AddTicks(5750), "Ledner, Schroeder and Sawayn" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 7, 29, 20, 27, 15, 591, DateTimeKind.Unspecified).AddTicks(9066), "Jacobi, Pfeffer and Pacocha" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2010, 12, 15, 2, 45, 19, 102, DateTimeKind.Unspecified).AddTicks(4000), "Heathcote Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 3, 22, 1, 39, 15, 703, DateTimeKind.Unspecified).AddTicks(390), "Senger - Hoeger" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 11, 6, 13, 32, 6, 90, DateTimeKind.Unspecified).AddTicks(2363), "Quigley LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 4, 2, 10, 21, 13, 190, DateTimeKind.Unspecified).AddTicks(2483), "Beahan - Raynor" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 12, 4, 10, 44, 18, 848, DateTimeKind.Unspecified).AddTicks(154), "Koelpin, Kihn and Adams" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 11, 19, 17, 14, 2, 428, DateTimeKind.Unspecified).AddTicks(8850), "O'Connell, Stiedemann and Stiedemann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 4, 6, 0, 3, 1, 357, DateTimeKind.Unspecified).AddTicks(2436), "Caitlyn.OKeefe49@yahoo.com", "Jack", "Carter", new DateTime(2016, 11, 15, 13, 13, 0, 992, DateTimeKind.Unspecified).AddTicks(7411), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 7, 1, 15, 27, 15, 658, DateTimeKind.Unspecified).AddTicks(317), "Freddie_Reichert@hotmail.com", "Gracie", "Medhurst", new DateTime(2017, 7, 24, 11, 33, 55, 662, DateTimeKind.Unspecified).AddTicks(5090), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 9, 14, 20, 8, 43, 214, DateTimeKind.Unspecified).AddTicks(266), "Elody_Mertz@hotmail.com", "Humberto", "Botsford", new DateTime(2015, 6, 20, 9, 22, 36, 617, DateTimeKind.Unspecified).AddTicks(8674), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 1, 6, 3, 7, 43, 302, DateTimeKind.Unspecified).AddTicks(943), "Carmela.Fritsch@gmail.com", "Adeline", "Stoltenberg", new DateTime(2012, 12, 4, 13, 35, 16, 650, DateTimeKind.Unspecified).AddTicks(2705), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1994, 9, 15, 12, 9, 15, 700, DateTimeKind.Unspecified).AddTicks(7023), "Petra49@gmail.com", "Marjorie", "Thiel", new DateTime(2014, 9, 9, 9, 54, 50, 333, DateTimeKind.Unspecified).AddTicks(6075) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 12, 30, 3, 58, 4, 212, DateTimeKind.Unspecified).AddTicks(9780), "Rocky.Schamberger23@hotmail.com", "Christop", "Wisoky", new DateTime(2014, 7, 13, 4, 22, 46, 481, DateTimeKind.Unspecified).AddTicks(6262), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 12, 19, 8, 59, 36, 911, DateTimeKind.Unspecified).AddTicks(7110), "Juliet19@gmail.com", "Reyna", "Erdman", new DateTime(2016, 12, 27, 11, 11, 28, 462, DateTimeKind.Unspecified).AddTicks(1348), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 9, 1, 41, 38, 4, DateTimeKind.Unspecified).AddTicks(4860), "Lizzie_Russel@hotmail.com", "Asia", "Stracke", new DateTime(2015, 10, 16, 14, 4, 14, 56, DateTimeKind.Unspecified).AddTicks(8475), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 31, 16, 16, 19, 454, DateTimeKind.Unspecified).AddTicks(4987), "Elvis.Glover60@gmail.com", "Daisha", "Hermiston", new DateTime(2016, 1, 26, 3, 19, 49, 376, DateTimeKind.Unspecified).AddTicks(1553), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 16, 4, 12, 4, 473, DateTimeKind.Unspecified).AddTicks(528), "Elvera0@gmail.com", "Orpha", "Marvin", new DateTime(2017, 3, 5, 9, 25, 10, 897, DateTimeKind.Unspecified).AddTicks(7887), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1999, 11, 8, 19, 33, 23, 902, DateTimeKind.Unspecified).AddTicks(3996), "Tomasa_Beer@hotmail.com", "Ashtyn", "Denesik", new DateTime(2012, 2, 25, 18, 50, 12, 740, DateTimeKind.Unspecified).AddTicks(3741) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 29, 13, 25, 56, 97, DateTimeKind.Unspecified).AddTicks(5575), "Priscilla_Welch@gmail.com", "Chelsea", "Wehner", new DateTime(2017, 3, 13, 3, 48, 46, 90, DateTimeKind.Unspecified).AddTicks(5755), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 7, 21, 40, 22, 755, DateTimeKind.Unspecified).AddTicks(1382), "Porter35@hotmail.com", "Mandy", "Kovacek", new DateTime(2016, 7, 9, 16, 8, 53, 358, DateTimeKind.Unspecified).AddTicks(5219), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 1, 31, 5, 10, 30, 751, DateTimeKind.Unspecified).AddTicks(1634), "Loy_Pagac@gmail.com", "Paxton", "Denesik", new DateTime(2011, 12, 16, 21, 30, 55, 149, DateTimeKind.Unspecified).AddTicks(3257), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 27, 7, 13, 13, 937, DateTimeKind.Unspecified).AddTicks(7575), "Elinor_Zboncak64@gmail.com", "John", "Koepp", new DateTime(2012, 5, 15, 8, 26, 12, 749, DateTimeKind.Unspecified).AddTicks(3708), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 2, 10, 9, 20, 56, 144, DateTimeKind.Unspecified).AddTicks(2022), "Russel56@hotmail.com", "Marcelino", "Waelchi", new DateTime(2012, 7, 14, 3, 55, 36, 290, DateTimeKind.Unspecified).AddTicks(3218), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 4, 1, 4, 9, 392, DateTimeKind.Unspecified).AddTicks(8231), "Verla_Kiehn@gmail.com", "Tyrell", "Walker", new DateTime(2013, 8, 14, 0, 58, 30, 916, DateTimeKind.Unspecified).AddTicks(8236), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 9, 19, 44, 57, 638, DateTimeKind.Unspecified).AddTicks(9970), "Caitlyn.Borer27@yahoo.com", "Judah", "Spinka", new DateTime(2014, 4, 8, 10, 45, 50, 214, DateTimeKind.Unspecified).AddTicks(9250), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 12, 11, 6, 28, 38, 644, DateTimeKind.Unspecified).AddTicks(9901), "Maribel24@gmail.com", "Beth", "Marvin", new DateTime(2016, 7, 27, 3, 31, 28, 624, DateTimeKind.Unspecified).AddTicks(1009), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 8, 24, 19, 11, 0, 572, DateTimeKind.Unspecified).AddTicks(7389), "Frederique_Purdy@gmail.com", "Lea", "Crist", new DateTime(2017, 4, 23, 18, 30, 53, 215, DateTimeKind.Unspecified).AddTicks(5715), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 5, 0, 18, 10, 55, DateTimeKind.Unspecified).AddTicks(9967), "Taurean_Swift35@gmail.com", "Maci", "Konopelski", new DateTime(2015, 3, 20, 1, 54, 5, 645, DateTimeKind.Unspecified).AddTicks(180), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 1, 18, 11, 53, 893, DateTimeKind.Unspecified).AddTicks(2554), "Jermain.Cummings78@gmail.com", "Jake", "Huel", new DateTime(2012, 7, 12, 16, 58, 35, 404, DateTimeKind.Unspecified).AddTicks(4556), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 6, 6, 13, 47, 7, 327, DateTimeKind.Unspecified).AddTicks(1790), "Trenton_Dach22@gmail.com", "Henri", "Daniel", new DateTime(2012, 8, 31, 4, 53, 10, 957, DateTimeKind.Unspecified).AddTicks(1142), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 20, 5, 48, 8, 275, DateTimeKind.Unspecified).AddTicks(723), "Queen.Streich@gmail.com", "Lester", "Hane", new DateTime(2012, 3, 9, 2, 1, 54, 431, DateTimeKind.Unspecified).AddTicks(5876), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 5, 9, 7, 47, 9, 226, DateTimeKind.Unspecified).AddTicks(7514), "Madilyn.Feest@gmail.com", "Antwan", "Schumm", new DateTime(2013, 6, 17, 12, 48, 15, 106, DateTimeKind.Unspecified).AddTicks(5944), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1993, 3, 18, 16, 57, 1, 772, DateTimeKind.Unspecified).AddTicks(8877), "Charlie.Monahan@hotmail.com", "Flavie", "Schmidt", new DateTime(2011, 8, 19, 20, 16, 55, 940, DateTimeKind.Unspecified).AddTicks(1547) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1999, 4, 21, 21, 7, 38, 824, DateTimeKind.Unspecified).AddTicks(840), "Rosalia10@gmail.com", "Freddy", "O'Kon", new DateTime(2010, 10, 25, 17, 26, 59, 276, DateTimeKind.Unspecified).AddTicks(6217) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 26, 14, 35, 39, 507, DateTimeKind.Unspecified).AddTicks(3222), "Lucy_Dietrich91@yahoo.com", "Maia", "Zulauf", new DateTime(2013, 7, 6, 20, 58, 31, 468, DateTimeKind.Unspecified).AddTicks(995), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 1, 6, 18, 34, 56, 85, DateTimeKind.Unspecified).AddTicks(6666), "Lenora_Beahan@hotmail.com", "Claudine", "Connelly", new DateTime(2014, 2, 11, 17, 21, 42, 768, DateTimeKind.Unspecified).AddTicks(364), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 27, 18, 16, 54, 520, DateTimeKind.Unspecified).AddTicks(9207), "Else.Satterfield3@hotmail.com", "Dewayne", "Mraz", new DateTime(2013, 7, 26, 0, 31, 13, 953, DateTimeKind.Unspecified).AddTicks(2126), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 10, 15, 9, 41, 845, DateTimeKind.Unspecified).AddTicks(4192), "Marta.Gaylord@hotmail.com", "Kamren", "Greenfelder", new DateTime(2013, 12, 27, 3, 29, 44, 35, DateTimeKind.Unspecified).AddTicks(6316), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 6, 18, 12, 21, 4, 881, DateTimeKind.Unspecified).AddTicks(8801), "Amparo_Schulist@gmail.com", "Kirsten", "Steuber", new DateTime(2017, 7, 8, 12, 11, 8, 391, DateTimeKind.Unspecified).AddTicks(4545), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 12, 20, 43, 28, 517, DateTimeKind.Unspecified).AddTicks(4660), "Jessyca_Bashirian16@hotmail.com", "Kayden", "Walker", new DateTime(2012, 7, 3, 21, 20, 33, 998, DateTimeKind.Unspecified).AddTicks(1730), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 31, 14, 24, 3, 453, DateTimeKind.Unspecified).AddTicks(7239), "Zack.Haag14@hotmail.com", "Nikko", "MacGyver", new DateTime(2014, 1, 5, 22, 56, 31, 212, DateTimeKind.Unspecified).AddTicks(1534), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 2, 0, 3, 16, 384, DateTimeKind.Unspecified).AddTicks(3073), "Rosendo.Cronin75@yahoo.com", "Abbey", "Flatley", new DateTime(2016, 8, 29, 7, 54, 3, 288, DateTimeKind.Unspecified).AddTicks(547), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 30, 20, 55, 45, 864, DateTimeKind.Unspecified).AddTicks(1722), "Malachi_Smith@gmail.com", "Zita", "Robel", new DateTime(2017, 1, 1, 8, 49, 4, 614, DateTimeKind.Unspecified).AddTicks(2280), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 12, 14, 4, 24, 3, 480, DateTimeKind.Unspecified).AddTicks(6784), "Rosetta_Bauch6@gmail.com", "Kraig", "Crooks", new DateTime(2013, 9, 14, 20, 51, 37, 406, DateTimeKind.Unspecified).AddTicks(2586), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 1, 9, 5, 53, 28, 829, DateTimeKind.Unspecified).AddTicks(9226), "Magnolia_Schultz@gmail.com", "Emelie", "Feil", new DateTime(2010, 4, 29, 10, 6, 52, 863, DateTimeKind.Unspecified).AddTicks(1678), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 14, 3, 39, 18, 359, DateTimeKind.Unspecified).AddTicks(1083), "Glen_Skiles@hotmail.com", "Josie", "Waters", new DateTime(2016, 7, 16, 9, 52, 54, 933, DateTimeKind.Unspecified).AddTicks(1962), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 5, 18, 13, 51, 19, 301, DateTimeKind.Unspecified).AddTicks(1798), "Orion11@yahoo.com", "Ansley", "Bashirian", new DateTime(2012, 8, 14, 11, 24, 30, 498, DateTimeKind.Unspecified).AddTicks(4193), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 28, 12, 10, 13, 130, DateTimeKind.Unspecified).AddTicks(1298), "Lilliana87@gmail.com", "Veda", "Runte", new DateTime(2016, 11, 10, 4, 53, 24, 636, DateTimeKind.Unspecified).AddTicks(7457), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 11, 5, 22, 4, 58, 631, DateTimeKind.Unspecified).AddTicks(8836), "Marquise82@hotmail.com", "Maudie", "Moore", new DateTime(2011, 9, 10, 4, 55, 46, 943, DateTimeKind.Unspecified).AddTicks(2542), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 8, 22, 4, 8, 8, 745, DateTimeKind.Unspecified).AddTicks(1874), "Rigoberto_Hoeger@yahoo.com", "Rubie", "Williamson", new DateTime(2015, 7, 27, 0, 53, 54, 998, DateTimeKind.Unspecified).AddTicks(2391), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 23, 15, 33, 55, 791, DateTimeKind.Unspecified).AddTicks(7614), "Jayne19@hotmail.com", "Colten", "Hammes", new DateTime(2011, 10, 31, 15, 40, 1, 52, DateTimeKind.Unspecified).AddTicks(5637), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 29, 12, 49, 56, 285, DateTimeKind.Unspecified).AddTicks(4676), "Mossie_Daugherty@gmail.com", "Casimir", "Adams", new DateTime(2010, 8, 21, 18, 46, 47, 835, DateTimeKind.Unspecified).AddTicks(1818), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 23, 1, 51, 33, 439, DateTimeKind.Unspecified).AddTicks(2643), "Maritza.Pouros@yahoo.com", "Linnie", "Dickens", new DateTime(2013, 3, 3, 9, 12, 52, 808, DateTimeKind.Unspecified).AddTicks(2970), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 1, 14, 16, 22, 58, 823, DateTimeKind.Unspecified).AddTicks(3720), "Kris18@gmail.com", "Eryn", "Buckridge", new DateTime(2015, 12, 28, 16, 9, 47, 823, DateTimeKind.Unspecified).AddTicks(3668), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 12, 28, 8, 35, 31, 760, DateTimeKind.Unspecified).AddTicks(4587), "Alana16@yahoo.com", "Eileen", "Bruen", new DateTime(2013, 11, 16, 22, 45, 49, 856, DateTimeKind.Unspecified).AddTicks(1324), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 3, 29, 6, 35, 12, 529, DateTimeKind.Unspecified).AddTicks(7087), "Granville_Kunze@hotmail.com", "Oscar", "Leannon", new DateTime(2010, 9, 19, 23, 56, 4, 990, DateTimeKind.Unspecified).AddTicks(8002), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 11, 13, 3, 38, 40, 451, DateTimeKind.Unspecified).AddTicks(5174), "Baby5@hotmail.com", "Randall", "Littel", new DateTime(2013, 1, 26, 2, 42, 21, 540, DateTimeKind.Unspecified).AddTicks(1545), 1 });

            migrationBuilder.AddForeignKey(
                name: "FK_Task_Users_PerformerUserId",
                table: "Task",
                column: "PerformerUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Task_Users_PerformerUserId",
                table: "Task");

            migrationBuilder.RenameColumn(
                name: "PerformerUserId",
                table: "Task",
                newName: "PerformerId");

            migrationBuilder.RenameIndex(
                name: "IX_Task_PerformerUserId",
                table: "Task",
                newName: "IX_Task_PerformerId");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 11, new DateTime(2019, 12, 6, 5, 59, 28, 923, DateTimeKind.Unspecified).AddTicks(4974), new DateTime(2022, 10, 10, 19, 9, 12, 812, DateTimeKind.Unspecified).AddTicks(2543), "Non eius labore dolorem assumenda sunt aut qui quae.", "Refined Rubber Computer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 11, 3, 4, 50, 35, 159, DateTimeKind.Unspecified).AddTicks(86), new DateTime(2021, 4, 28, 14, 5, 20, 500, DateTimeKind.Unspecified).AddTicks(3086), "Asperiores dignissimos voluptatem voluptatibus saepe dolor ut omnis asperiores nihil.", "Handmade Frozen Bike", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 43, new DateTime(2019, 8, 2, 6, 41, 44, 926, DateTimeKind.Unspecified).AddTicks(7214), new DateTime(2022, 5, 1, 4, 1, 37, 454, DateTimeKind.Unspecified).AddTicks(7716), "Id vero tenetur et omnis rerum est est aperiam.", "Small Wooden Table", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 8, 28, 19, 49, 26, 128, DateTimeKind.Unspecified).AddTicks(3879), new DateTime(2021, 3, 9, 8, 21, 23, 102, DateTimeKind.Unspecified).AddTicks(1885), "Ut et fugit asperiores enim iure excepturi corrupti dolores.", "Tasty Soft Chair", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 34, new DateTime(2019, 6, 22, 0, 35, 17, 952, DateTimeKind.Unspecified).AddTicks(883), new DateTime(2021, 5, 22, 18, 7, 34, 743, DateTimeKind.Unspecified).AddTicks(9702), "Tenetur et repellat et sit sint eum.", "Practical Soft Chair", 10 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 17, 18, 5, 802, DateTimeKind.Unspecified).AddTicks(5373), "Repudiandae et tenetur autem rerum est.", new DateTime(2022, 4, 16, 10, 11, 12, 377, DateTimeKind.Unspecified).AddTicks(7762), "Atque nobis.", 22, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 17, 9, 21, 21, 601, DateTimeKind.Unspecified).AddTicks(600), "Eos modi recusandae quae debitis.", new DateTime(2021, 5, 2, 4, 31, 11, 860, DateTimeKind.Unspecified).AddTicks(6197), "Tenetur nihil.", 18, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 28, 9, 35, 16, 566, DateTimeKind.Unspecified).AddTicks(335), "Qui distinctio ut.", new DateTime(2021, 6, 5, 20, 53, 55, 836, DateTimeKind.Unspecified).AddTicks(7886), "Occaecati rerum.", 8, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 23, 13, 31, 28, 342, DateTimeKind.Unspecified).AddTicks(9920), "Et laudantium optio asperiores fugit.", new DateTime(2021, 7, 7, 21, 53, 46, 805, DateTimeKind.Unspecified).AddTicks(6183), "Laboriosam nihil.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 17, 7, 23, 40, 828, DateTimeKind.Unspecified).AddTicks(5552), "Laudantium id ea.", new DateTime(2022, 3, 29, 23, 12, 47, 837, DateTimeKind.Unspecified).AddTicks(3318), "Nisi et.", 33, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 16, 48, 23, 480, DateTimeKind.Unspecified).AddTicks(5070), "Voluptatem ratione veritatis fugiat.", new DateTime(2021, 2, 27, 10, 14, 29, 37, DateTimeKind.Unspecified).AddTicks(5186), "Non aut.", 32, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 19, 6, 22, 48, 554, DateTimeKind.Unspecified).AddTicks(4359), "Molestiae provident veniam nam ducimus recusandae eaque.", new DateTime(2021, 8, 9, 21, 16, 42, 660, DateTimeKind.Unspecified).AddTicks(4904), "Fugiat et.", 28, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 18, 1, 2, 16, 820, DateTimeKind.Unspecified).AddTicks(4029), "Aliquid aut beatae cupiditate sed consectetur ab suscipit recusandae.", new DateTime(2021, 8, 7, 7, 32, 38, 801, DateTimeKind.Unspecified).AddTicks(3236), "Quas vero.", 14, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 2, 0, 4, 20, 248, DateTimeKind.Unspecified).AddTicks(5367), "Omnis quasi neque et.", new DateTime(2022, 1, 8, 13, 36, 14, 479, DateTimeKind.Unspecified).AddTicks(2473), "Quisquam explicabo.", 23, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 23, 17, 26, 56, 236, DateTimeKind.Unspecified).AddTicks(5325), "Id corrupti corrupti nemo quas aliquid.", new DateTime(2022, 10, 21, 1, 4, 5, 778, DateTimeKind.Unspecified).AddTicks(9279), "Voluptatem asperiores.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 21, 8, 5, 121, DateTimeKind.Unspecified).AddTicks(8192), "Minus id deserunt qui.", new DateTime(2021, 7, 30, 2, 55, 34, 506, DateTimeKind.Unspecified).AddTicks(7751), "A itaque.", 19, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 4, 10, 27, 22, 882, DateTimeKind.Unspecified).AddTicks(1618), "Vel voluptatem voluptate et est aut.", new DateTime(2022, 3, 29, 5, 53, 25, 784, DateTimeKind.Unspecified).AddTicks(3456), "Exercitationem numquam.", 7, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 9, 6, 36, 57, 505, DateTimeKind.Unspecified).AddTicks(4262), "Dolores asperiores et distinctio temporibus ut.", new DateTime(2021, 12, 1, 8, 56, 59, 604, DateTimeKind.Unspecified).AddTicks(340), "Est voluptatem.", 23, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 3, 0, 32, 50, 653, DateTimeKind.Unspecified).AddTicks(2077), "Aliquid maxime vel.", new DateTime(2021, 12, 23, 12, 43, 14, 395, DateTimeKind.Unspecified).AddTicks(6379), "Rem corrupti.", 21, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 18, 21, 35, 54, 508, DateTimeKind.Unspecified).AddTicks(8293), "Aliquid voluptas deleniti quas.", new DateTime(2021, 12, 11, 4, 38, 42, 635, DateTimeKind.Unspecified).AddTicks(4668), "Et omnis.", 44, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 28, 0, 42, 45, 251, DateTimeKind.Unspecified).AddTicks(6655), "Placeat sed recusandae vero sit aliquam nesciunt aut est dolore.", new DateTime(2022, 1, 21, 13, 57, 58, 750, DateTimeKind.Unspecified).AddTicks(9132), "Voluptas ad.", 15, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 7, 23, 26, 33, 169, DateTimeKind.Unspecified).AddTicks(8245), "Itaque repellat illo id impedit quibusdam culpa.", new DateTime(2022, 4, 9, 7, 26, 51, 818, DateTimeKind.Unspecified).AddTicks(4435), "Beatae in.", 42, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 19, 25, 41, 938, DateTimeKind.Unspecified).AddTicks(6087), "Eligendi voluptas beatae fugiat.", new DateTime(2022, 1, 21, 17, 4, 8, 522, DateTimeKind.Unspecified).AddTicks(7993), "Saepe recusandae.", 34, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 23, 2, 23, 46, 933, DateTimeKind.Unspecified).AddTicks(3294), "Quas consequuntur ut quibusdam.", new DateTime(2021, 5, 17, 17, 56, 13, 789, DateTimeKind.Unspecified).AddTicks(2923), "Ut saepe.", 29, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 30, 18, 44, 24, 361, DateTimeKind.Unspecified).AddTicks(5646), "Illum et eos perferendis placeat quaerat quia mollitia esse iure.", new DateTime(2021, 8, 1, 16, 18, 28, 655, DateTimeKind.Unspecified).AddTicks(504), "Illum eaque.", 8, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 25, 18, 19, 12, 949, DateTimeKind.Unspecified).AddTicks(7070), "At et omnis tempora pariatur porro harum rem.", new DateTime(2022, 7, 22, 1, 28, 0, 185, DateTimeKind.Unspecified).AddTicks(561), "Est qui.", 45, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 12, 8, 30, 23, 998, DateTimeKind.Unspecified).AddTicks(6126), "Repellendus commodi dolor omnis nobis inventore ut impedit reprehenderit.", new DateTime(2022, 1, 25, 15, 48, 19, 609, DateTimeKind.Unspecified).AddTicks(3111), "Aut debitis.", 5, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 23, 14, 42, 20, 240, DateTimeKind.Unspecified).AddTicks(404), "Sint voluptate ea ullam delectus voluptatibus rerum expedita praesentium.", new DateTime(2022, 9, 29, 0, 33, 12, 392, DateTimeKind.Unspecified).AddTicks(6114), "Occaecati rerum.", 47, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 10, 1, 18, 323, DateTimeKind.Unspecified).AddTicks(3405), "Quia ratione omnis et alias.", new DateTime(2022, 8, 30, 17, 15, 29, 839, DateTimeKind.Unspecified).AddTicks(3888), "Reiciendis veniam.", 12, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 11, 1, 18, 3, 20, 832, DateTimeKind.Unspecified).AddTicks(4115), "Iusto sint qui et vitae minus rerum quo placeat.", new DateTime(2022, 5, 26, 14, 0, 42, 873, DateTimeKind.Unspecified).AddTicks(3949), "Adipisci modi.", 18 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 4, 9, 4, 2, 548, DateTimeKind.Unspecified).AddTicks(1831), "Aut hic ab et voluptatum.", new DateTime(2022, 5, 7, 15, 29, 40, 138, DateTimeKind.Unspecified).AddTicks(3788), "Nostrum beatae.", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 26, 9, 46, 22, 675, DateTimeKind.Unspecified).AddTicks(3985), "Nihil eum facilis quia reprehenderit dignissimos.", new DateTime(2022, 1, 11, 5, 10, 49, 332, DateTimeKind.Unspecified).AddTicks(1360), "Est beatae.", 21, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 26, 13, 38, 44, 927, DateTimeKind.Unspecified).AddTicks(2390), "In voluptas non incidunt ratione odio deleniti a consequuntur consequatur.", new DateTime(2021, 4, 4, 9, 56, 54, 823, DateTimeKind.Unspecified).AddTicks(8531), "Adipisci reprehenderit.", 36, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 15, 16, 37, 989, DateTimeKind.Unspecified).AddTicks(4471), "Ut dolores necessitatibus error dignissimos voluptates.", new DateTime(2021, 12, 20, 6, 2, 6, 566, DateTimeKind.Unspecified).AddTicks(7669), "Odio qui.", 8, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 15, 13, 49, 16, 35, DateTimeKind.Unspecified).AddTicks(1290), "Minima est reprehenderit molestiae ut.", new DateTime(2021, 9, 25, 15, 34, 38, 723, DateTimeKind.Unspecified).AddTicks(9358), "Et et.", 27, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 14, 26, 13, 832, DateTimeKind.Unspecified).AddTicks(2954), "Pariatur molestias autem rerum exercitationem sit sed voluptates exercitationem.", new DateTime(2022, 11, 12, 16, 35, 11, 615, DateTimeKind.Unspecified).AddTicks(3311), "Non sint.", 23, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 0, 12, 23, 855, DateTimeKind.Unspecified).AddTicks(2796), "Quo commodi et qui impedit voluptate et ex similique.", new DateTime(2022, 7, 18, 2, 24, 59, 687, DateTimeKind.Unspecified).AddTicks(7936), "Labore aliquam.", 37, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 6, 16, 35, 37, 800, DateTimeKind.Unspecified).AddTicks(9239), "Officia id consequuntur ipsam eum maiores vel dolorem deleniti.", new DateTime(2021, 12, 9, 16, 56, 22, 399, DateTimeKind.Unspecified).AddTicks(3573), "Odit aperiam.", 29, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 1, 2, 50, 22, 6, DateTimeKind.Unspecified).AddTicks(203), "A deleniti quia ut quibusdam molestiae fuga.", new DateTime(2022, 3, 17, 2, 59, 53, 845, DateTimeKind.Unspecified).AddTicks(5234), "Voluptatum ratione.", 9, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 23, 55, 18, 761, DateTimeKind.Unspecified).AddTicks(7354), "Quaerat et qui.", new DateTime(2021, 2, 2, 22, 21, 17, 174, DateTimeKind.Unspecified).AddTicks(5124), "Dolore et.", 24, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 5, 13, 43, 204, DateTimeKind.Unspecified).AddTicks(5175), "Ut ut tempora dolores.", new DateTime(2021, 7, 22, 12, 56, 56, 837, DateTimeKind.Unspecified).AddTicks(1845), "Magnam porro.", 34, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 16, 20, 32, 54, 648, DateTimeKind.Unspecified).AddTicks(7147), "Voluptatem ipsa dolorum.", new DateTime(2021, 8, 31, 10, 12, 39, 577, DateTimeKind.Unspecified).AddTicks(8512), "Distinctio harum.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 30, 14, 16, 23, 48, DateTimeKind.Unspecified).AddTicks(9266), "Repellat eius placeat et.", new DateTime(2022, 2, 23, 18, 28, 33, 541, DateTimeKind.Unspecified).AddTicks(9341), "Mollitia sunt.", 33, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 3, 4, 45, 7, 175, DateTimeKind.Unspecified).AddTicks(9324), "Aspernatur voluptatibus totam.", new DateTime(2022, 11, 7, 9, 35, 25, 722, DateTimeKind.Unspecified).AddTicks(8107), "Rem excepturi.", 32, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 8, 13, 26, 57, 241, DateTimeKind.Unspecified).AddTicks(7891), "Quo labore numquam et sapiente nesciunt similique doloribus est voluptas.", new DateTime(2022, 5, 15, 5, 14, 55, 299, DateTimeKind.Unspecified).AddTicks(3228), "Quia velit.", 28, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 18, 5, 23, 55, 327, DateTimeKind.Unspecified).AddTicks(5319), "A sed odio.", new DateTime(2021, 2, 3, 14, 46, 23, 370, DateTimeKind.Unspecified).AddTicks(8115), "Saepe alias.", 30, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 12, 42, 52, 159, DateTimeKind.Unspecified).AddTicks(3763), "Assumenda in quod nisi.", new DateTime(2021, 10, 20, 18, 12, 42, 745, DateTimeKind.Unspecified).AddTicks(1586), "Et aut.", 50, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 5, 43, 3, 564, DateTimeKind.Unspecified).AddTicks(211), "Dolorem sunt consectetur id.", new DateTime(2021, 11, 10, 2, 21, 23, 552, DateTimeKind.Unspecified).AddTicks(8195), "Eos libero.", 14, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 6, 20, 55, 10, 621, DateTimeKind.Unspecified).AddTicks(4376), "Libero eligendi soluta.", new DateTime(2022, 4, 21, 16, 26, 47, 594, DateTimeKind.Unspecified).AddTicks(1547), "Omnis qui.", 11, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 30, 13, 19, 44, 34, DateTimeKind.Unspecified).AddTicks(5454), "Dolorum qui rerum quia eos neque cum.", new DateTime(2022, 4, 22, 19, 31, 52, 906, DateTimeKind.Unspecified).AddTicks(9803), "Doloremque autem.", 26, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 11, 5, 12, 25, 348, DateTimeKind.Unspecified).AddTicks(4362), "Incidunt iste est voluptatibus dicta sit.", new DateTime(2022, 2, 1, 3, 32, 46, 399, DateTimeKind.Unspecified).AddTicks(9259), "Saepe quas.", 19, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 20, 46, 8, 646, DateTimeKind.Unspecified).AddTicks(9415), "Autem ea quam possimus.", new DateTime(2021, 8, 31, 14, 13, 12, 600, DateTimeKind.Unspecified).AddTicks(5285), "Architecto sit.", 33, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 13, 8, 14, 35, 348, DateTimeKind.Unspecified).AddTicks(2598), "Quia similique non et nisi nihil nulla nihil doloribus.", new DateTime(2022, 6, 14, 15, 40, 39, 291, DateTimeKind.Unspecified).AddTicks(3554), "Velit sed.", 34, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 16, 14, 42, 35, 958, DateTimeKind.Unspecified).AddTicks(8630), "Et autem natus porro.", new DateTime(2022, 3, 17, 7, 46, 48, 254, DateTimeKind.Unspecified).AddTicks(2804), "Id iure.", 34, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 29, 9, 45, 19, 476, DateTimeKind.Unspecified).AddTicks(8453), "Iusto maxime harum nemo qui itaque est et excepturi expedita.", new DateTime(2022, 2, 23, 16, 45, 14, 856, DateTimeKind.Unspecified).AddTicks(2289), "In consequatur.", 47, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 17, 13, 37, 46, 233, DateTimeKind.Unspecified).AddTicks(3987), "Ad odio qui ut odit.", new DateTime(2022, 5, 16, 22, 12, 17, 364, DateTimeKind.Unspecified).AddTicks(8062), "Et dolores.", 16, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 20, 10, 43, 40, DateTimeKind.Unspecified).AddTicks(8264), "Consequatur ipsum quod delectus perspiciatis voluptatem reprehenderit facere aut.", new DateTime(2022, 11, 30, 16, 31, 49, 443, DateTimeKind.Unspecified).AddTicks(6451), "Voluptatem ipsum.", 39, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 14, 11, 2, 4, 264, DateTimeKind.Unspecified).AddTicks(2309), "Est iste reprehenderit eius corporis in beatae officiis voluptatibus.", new DateTime(2022, 1, 30, 5, 29, 40, 1, DateTimeKind.Unspecified).AddTicks(7628), "Quod nobis.", 47, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 7, 5, 14, 40, 20, DateTimeKind.Unspecified).AddTicks(9841), "Iste qui ea quasi minus.", new DateTime(2021, 2, 13, 23, 27, 20, 647, DateTimeKind.Unspecified).AddTicks(9194), "Inventore et.", 50, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 27, 11, 17, 11, 47, DateTimeKind.Unspecified).AddTicks(8982), "Porro possimus explicabo in eos aut iusto deleniti ipsam ipsa.", new DateTime(2022, 8, 4, 17, 39, 23, 736, DateTimeKind.Unspecified).AddTicks(5264), "Nesciunt et.", 18, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 3, 12, 50, 47, 383, DateTimeKind.Unspecified).AddTicks(827), "Sed tempora quis id sapiente ducimus nemo consequatur sunt.", new DateTime(2022, 9, 25, 3, 44, 52, 693, DateTimeKind.Unspecified).AddTicks(5536), "Iure ducimus.", 22, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 29, 6, 55, 52, 67, DateTimeKind.Unspecified).AddTicks(9046), "A et ut placeat dolores alias aut.", new DateTime(2021, 12, 8, 15, 19, 19, 359, DateTimeKind.Unspecified).AddTicks(409), "Est possimus.", 21, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 4, 57, 1, 265, DateTimeKind.Unspecified).AddTicks(4896), "Consequatur perferendis sunt eos aut praesentium sunt blanditiis cum.", new DateTime(2021, 5, 29, 7, 28, 24, 781, DateTimeKind.Unspecified).AddTicks(5309), "In modi.", 32, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 4, 45, 20, 264, DateTimeKind.Unspecified).AddTicks(4903), "Asperiores ut eligendi dolore est laborum animi fuga eveniet quo.", new DateTime(2021, 1, 6, 12, 55, 13, 795, DateTimeKind.Unspecified).AddTicks(9685), "Voluptates quia.", 26, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 9, 22, 51, 79, DateTimeKind.Unspecified).AddTicks(2368), "In consequatur saepe.", new DateTime(2022, 11, 15, 12, 0, 3, 321, DateTimeKind.Unspecified).AddTicks(3698), "Accusantium accusantium.", 9, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 22, 18, 13, 21, 542, DateTimeKind.Unspecified).AddTicks(2199), "Maxime ipsum natus et quod aut molestiae.", new DateTime(2021, 7, 12, 21, 7, 38, 333, DateTimeKind.Unspecified).AddTicks(6373), "Non aperiam.", 36, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 17, 20, 55, 51, 368, DateTimeKind.Unspecified).AddTicks(5691), "Repudiandae velit veniam odit voluptatum sed non rerum nemo accusantium.", new DateTime(2021, 6, 12, 0, 49, 43, 779, DateTimeKind.Unspecified).AddTicks(7562), "Sed aut.", 44, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 3, 15, 33, 49, 435, DateTimeKind.Unspecified).AddTicks(5049), "Maxime sint doloremque voluptate explicabo veritatis sunt exercitationem.", new DateTime(2021, 5, 16, 20, 46, 40, 528, DateTimeKind.Unspecified).AddTicks(7226), "Et unde.", 19, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 8, 12, 48, 17, 789, DateTimeKind.Unspecified).AddTicks(6312), "Aliquam sit laudantium dolor.", new DateTime(2022, 4, 17, 5, 44, 14, 92, DateTimeKind.Unspecified).AddTicks(7680), "Totam exercitationem.", 22, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 5, 23, 14, 58, 44, 536, DateTimeKind.Unspecified).AddTicks(9528), "Rerum quas eaque voluptatem eum saepe.", new DateTime(2021, 10, 22, 22, 51, 45, 378, DateTimeKind.Unspecified).AddTicks(6807), "Architecto qui.", 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 14, 34, 44, 156, DateTimeKind.Unspecified).AddTicks(4710), "Ducimus deserunt enim sapiente repudiandae.", new DateTime(2021, 11, 7, 0, 43, 20, 288, DateTimeKind.Unspecified).AddTicks(7329), "Provident dolor.", 45, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 15, 20, 48, 27, 784, DateTimeKind.Unspecified).AddTicks(3606), "Molestias et accusamus quas provident sequi consectetur.", new DateTime(2021, 10, 17, 0, 37, 11, 217, DateTimeKind.Unspecified).AddTicks(3337), "Sed est.", 8, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 10, 11, 56, 12, 574, DateTimeKind.Unspecified).AddTicks(3783), "Repudiandae assumenda qui quod sint aut cumque.", new DateTime(2022, 2, 19, 5, 37, 29, 559, DateTimeKind.Unspecified).AddTicks(7845), "Doloribus earum.", 17, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 1, 34, 18, 921, DateTimeKind.Unspecified).AddTicks(7332), "Quia et dolor magnam nesciunt dolorem quia doloremque qui.", new DateTime(2021, 10, 6, 16, 25, 32, 496, DateTimeKind.Unspecified).AddTicks(9250), "Fugit tenetur.", 21, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 5, 20, 29, 34, 964, DateTimeKind.Unspecified).AddTicks(6274), "Ut sit molestiae voluptates.", new DateTime(2022, 8, 10, 14, 50, 18, 131, DateTimeKind.Unspecified).AddTicks(4108), "Nesciunt explicabo.", 43, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 25, 5, 20, 31, 10, DateTimeKind.Unspecified).AddTicks(6422), "Quis perspiciatis voluptatem quae voluptatem laudantium sed.", new DateTime(2021, 12, 11, 17, 36, 2, 442, DateTimeKind.Unspecified).AddTicks(3604), "Dolorem aut.", 50, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 5, 15, 1, 649, DateTimeKind.Unspecified).AddTicks(1330), "Sit tenetur cum quia ab voluptas consequatur doloremque quisquam.", new DateTime(2021, 6, 5, 21, 9, 0, 79, DateTimeKind.Unspecified).AddTicks(7571), "Sed excepturi.", 23, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 12, 17, 23, 32, 190, DateTimeKind.Unspecified).AddTicks(9662), "Et expedita accusantium illo reprehenderit qui delectus quod ex vel.", new DateTime(2022, 11, 26, 2, 9, 52, 169, DateTimeKind.Unspecified).AddTicks(3706), "Id neque.", 45, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 19, 15, 5, 258, DateTimeKind.Unspecified).AddTicks(1412), "Reiciendis sit vel impedit quo error aut pariatur earum.", new DateTime(2021, 12, 19, 2, 10, 41, 317, DateTimeKind.Unspecified).AddTicks(7756), "Quia voluptas.", 35, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 21, 3, 43, 56, 771, DateTimeKind.Unspecified).AddTicks(4826), "Repellendus eveniet natus odio et ea repudiandae eum possimus.", new DateTime(2021, 4, 20, 2, 28, 44, 375, DateTimeKind.Unspecified).AddTicks(1443), "Adipisci illo.", 38, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 11, 10, 28, 39, 370, DateTimeKind.Unspecified).AddTicks(2441), "Facere ab veritatis et consequatur ab.", new DateTime(2021, 6, 30, 1, 42, 23, 356, DateTimeKind.Unspecified).AddTicks(4530), "Nostrum est.", 16, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 18, 0, 15, 33, 42, DateTimeKind.Unspecified).AddTicks(4186), "Minus fuga vel saepe.", new DateTime(2022, 7, 29, 23, 14, 31, 873, DateTimeKind.Unspecified).AddTicks(5090), "Magni mollitia.", 31, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 2, 13, 11, 57, 524, DateTimeKind.Unspecified).AddTicks(4928), "Distinctio quam ut nesciunt nisi quia enim quia.", new DateTime(2022, 2, 2, 1, 48, 6, 243, DateTimeKind.Unspecified).AddTicks(6287), "Omnis ipsum.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 3, 17, 46, 57, 363, DateTimeKind.Unspecified).AddTicks(7577), "Accusantium rem modi nesciunt quia quia quidem ut ut.", new DateTime(2022, 5, 29, 11, 54, 23, 511, DateTimeKind.Unspecified).AddTicks(2494), "Esse quibusdam.", 18, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 24, 9, 46, 10, 809, DateTimeKind.Unspecified).AddTicks(8668), "Non amet deserunt minus culpa tenetur consectetur.", new DateTime(2021, 10, 12, 5, 16, 13, 567, DateTimeKind.Unspecified).AddTicks(5977), "Repudiandae consectetur.", 10, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 9, 2, 15, 147, DateTimeKind.Unspecified).AddTicks(4879), "Illo cumque sunt deserunt incidunt adipisci in.", new DateTime(2022, 5, 29, 8, 3, 28, 291, DateTimeKind.Unspecified).AddTicks(2722), "Quo aut.", 27, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 29, 20, 50, 40, 618, DateTimeKind.Unspecified).AddTicks(3514), "Consequuntur ut voluptatem laborum.", new DateTime(2021, 11, 28, 9, 13, 11, 991, DateTimeKind.Unspecified).AddTicks(4418), "Quo sint.", 24, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 17, 0, 52, 36, 187, DateTimeKind.Unspecified).AddTicks(191), "Recusandae repellendus ut vel itaque.", new DateTime(2021, 2, 16, 15, 34, 6, 807, DateTimeKind.Unspecified).AddTicks(4242), "Sapiente dolorem.", 35, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 4, 37, 29, 433, DateTimeKind.Unspecified).AddTicks(3574), "Voluptas autem cum et sunt impedit.", new DateTime(2021, 3, 19, 21, 42, 26, 798, DateTimeKind.Unspecified).AddTicks(762), "Dolores sunt.", 22, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 19, 8, 45, 19, 538, DateTimeKind.Unspecified).AddTicks(2927), "Rerum vero impedit voluptatibus aperiam architecto.", new DateTime(2022, 7, 6, 22, 47, 54, 737, DateTimeKind.Unspecified).AddTicks(8694), "Enim voluptatem.", 38, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 28, 4, 29, 36, 697, DateTimeKind.Unspecified).AddTicks(8501), "Voluptatibus officiis cupiditate nisi earum voluptate corporis.", new DateTime(2022, 8, 1, 7, 52, 17, 171, DateTimeKind.Unspecified).AddTicks(4494), "Harum quibusdam.", 2, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 23, 31, 2, 699, DateTimeKind.Unspecified).AddTicks(98), "Culpa hic veniam fugit ipsam a.", new DateTime(2021, 1, 30, 8, 2, 55, 103, DateTimeKind.Unspecified).AddTicks(2895), "Non atque.", 34, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 23, 49, 34, 125, DateTimeKind.Unspecified).AddTicks(4036), "Sapiente qui corrupti vitae labore delectus id eius dolorum ea.", new DateTime(2021, 4, 25, 6, 9, 40, 503, DateTimeKind.Unspecified).AddTicks(8704), "Aut saepe.", 32, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 16, 5, 56, 48, 641, DateTimeKind.Unspecified).AddTicks(5702), "Sequi ipsa adipisci.", new DateTime(2021, 3, 6, 6, 35, 51, 775, DateTimeKind.Unspecified).AddTicks(2434), "Similique dicta.", 45, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 16, 15, 20, 13, 402, DateTimeKind.Unspecified).AddTicks(4387), "Libero aliquid minus sequi et perspiciatis omnis fugit voluptas.", new DateTime(2021, 12, 24, 9, 10, 39, 700, DateTimeKind.Unspecified).AddTicks(7574), "Vero at.", 25, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 2, 0, 16, 21, 550, DateTimeKind.Unspecified).AddTicks(7933), "Et eligendi ea veniam eveniet et similique.", new DateTime(2022, 8, 19, 22, 45, 37, 967, DateTimeKind.Unspecified).AddTicks(5290), "Consequatur veritatis.", 29, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 5, 22, 22, 3, 345, DateTimeKind.Unspecified).AddTicks(6271), "Occaecati qui natus ut eos.", new DateTime(2022, 10, 9, 23, 40, 4, 541, DateTimeKind.Unspecified).AddTicks(5206), "Qui enim.", 48, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 1, 15, 19, 4, 141, DateTimeKind.Unspecified).AddTicks(7629), "Amet et corporis.", new DateTime(2021, 3, 26, 3, 45, 9, 450, DateTimeKind.Unspecified).AddTicks(4211), "Ut hic.", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 23, 14, 37, 58, 735, DateTimeKind.Unspecified).AddTicks(691), "Odit ullam nobis est quae facere.", new DateTime(2022, 12, 28, 4, 44, 30, 480, DateTimeKind.Unspecified).AddTicks(6550), "Quas mollitia.", 26, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 14, 55, 32, 693, DateTimeKind.Unspecified).AddTicks(5925), "Hic quaerat fugit culpa labore rerum saepe provident at.", new DateTime(2022, 5, 31, 6, 26, 56, 356, DateTimeKind.Unspecified).AddTicks(2591), "Natus consectetur.", 31, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 3, 7, 3, 52, 806, DateTimeKind.Unspecified).AddTicks(3981), "Odio natus minima asperiores voluptatem et quae rerum saepe.", new DateTime(2021, 5, 3, 13, 46, 52, 248, DateTimeKind.Unspecified).AddTicks(3031), "Voluptas eos.", 1, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 24, 8, 52, 28, 279, DateTimeKind.Unspecified).AddTicks(6330), "Minima numquam consequatur sed qui tempora earum dolorem ea blanditiis.", new DateTime(2022, 9, 3, 13, 26, 24, 139, DateTimeKind.Unspecified).AddTicks(8955), "Omnis non.", 22, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 21, 22, 17, 11, 750, DateTimeKind.Unspecified).AddTicks(812), "Odit voluptatem dignissimos est officia ea id quia natus et.", new DateTime(2022, 2, 19, 15, 42, 32, 247, DateTimeKind.Unspecified).AddTicks(6520), "Ipsam quia.", 13, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 20, 4, 34, 47, 414, DateTimeKind.Unspecified).AddTicks(7784), "Dignissimos aspernatur possimus eius rem excepturi commodi animi debitis.", new DateTime(2022, 5, 17, 15, 6, 15, 763, DateTimeKind.Unspecified).AddTicks(4463), "Voluptatem qui.", 27, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 19, 12, 23, 20, 319, DateTimeKind.Unspecified).AddTicks(1486), "Nobis at sit earum nobis vero necessitatibus ea.", new DateTime(2022, 8, 31, 17, 16, 37, 177, DateTimeKind.Unspecified).AddTicks(3104), "Ab voluptate.", 43, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 19, 35, 14, 682, DateTimeKind.Unspecified).AddTicks(2177), "Aspernatur repudiandae natus voluptatem animi dolorem perferendis id.", new DateTime(2022, 4, 24, 21, 49, 24, 800, DateTimeKind.Unspecified).AddTicks(3607), "Omnis quaerat.", 31, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 14, 15, 0, 1, 827, DateTimeKind.Unspecified).AddTicks(8236), "Quos illo maxime ipsa cupiditate amet aut.", new DateTime(2022, 8, 11, 4, 5, 28, 432, DateTimeKind.Unspecified).AddTicks(4724), "Possimus distinctio.", 25, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 6, 18, 12, 25, 25, 549, DateTimeKind.Unspecified).AddTicks(2394), "Ipsa error quod doloremque sint.", new DateTime(2021, 5, 19, 15, 52, 59, 611, DateTimeKind.Unspecified).AddTicks(5970), "Ea occaecati.", 13 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 11, 11, 57, 4, 359, DateTimeKind.Unspecified).AddTicks(3977), "Corporis laboriosam sint repudiandae soluta commodi nihil impedit modi.", new DateTime(2021, 8, 19, 2, 5, 35, 602, DateTimeKind.Unspecified).AddTicks(1588), "Maxime deserunt.", 40, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 19, 20, 5, 59, 233, DateTimeKind.Unspecified).AddTicks(9923), "Ut autem voluptate eum soluta.", new DateTime(2021, 9, 22, 23, 55, 48, 705, DateTimeKind.Unspecified).AddTicks(5748), "Provident error.", 38, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 15, 57, 25, 854, DateTimeKind.Unspecified).AddTicks(5334), "Sed incidunt eaque.", new DateTime(2021, 9, 26, 0, 30, 15, 734, DateTimeKind.Unspecified).AddTicks(7394), "Qui et.", 37, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 3, 19, 35, 25, 597, DateTimeKind.Unspecified).AddTicks(3273), "Quis corrupti aperiam et qui aut vitae.", new DateTime(2021, 6, 3, 5, 14, 26, 764, DateTimeKind.Unspecified).AddTicks(11), "Quisquam quia.", 10, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 20, 9, 44, 255, DateTimeKind.Unspecified).AddTicks(96), "Autem harum nostrum qui.", new DateTime(2022, 12, 24, 17, 28, 8, 820, DateTimeKind.Unspecified).AddTicks(231), "Est beatae.", 21, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 5, 10, 36, 15, 295, DateTimeKind.Unspecified).AddTicks(566), "Quae eos perspiciatis et quasi consequatur aut reprehenderit omnis.", new DateTime(2021, 3, 13, 6, 33, 45, 458, DateTimeKind.Unspecified).AddTicks(1083), "Soluta animi.", 30, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 12, 19, 52, 38, 956, DateTimeKind.Unspecified).AddTicks(238), "Cupiditate amet cum consectetur.", new DateTime(2022, 10, 8, 0, 8, 51, 629, DateTimeKind.Unspecified).AddTicks(3788), "Aspernatur totam.", 6, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 2, 24, 59, 133, DateTimeKind.Unspecified).AddTicks(1037), "Dolor eum iure sed porro rerum aliquam quo.", new DateTime(2022, 11, 17, 19, 56, 22, 285, DateTimeKind.Unspecified).AddTicks(9396), "Ut suscipit.", 36, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 18, 15, 25, 53, 133, DateTimeKind.Unspecified).AddTicks(491), "Tempore numquam sint.", new DateTime(2021, 4, 28, 7, 38, 34, 806, DateTimeKind.Unspecified).AddTicks(5181), "Maxime totam.", 14, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 16, 58, 44, 973, DateTimeKind.Unspecified).AddTicks(7242), "Voluptates facilis deleniti esse voluptas.", new DateTime(2022, 8, 19, 22, 26, 8, 612, DateTimeKind.Unspecified).AddTicks(556), "Aut id.", 33, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 21, 41, 9, 586, DateTimeKind.Unspecified).AddTicks(148), "Animi aliquid non.", new DateTime(2022, 1, 28, 17, 47, 56, 331, DateTimeKind.Unspecified).AddTicks(561), "Consequuntur dignissimos.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 5, 16, 18, 13, 122, DateTimeKind.Unspecified).AddTicks(5928), "Error veritatis at temporibus incidunt rerum aut sit officiis.", new DateTime(2021, 11, 30, 20, 13, 36, 408, DateTimeKind.Unspecified).AddTicks(8169), "Eaque eius.", 38, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 1, 14, 1, 20, 847, DateTimeKind.Unspecified).AddTicks(7329), "Dolor molestiae iste temporibus repudiandae est sit natus.", new DateTime(2021, 9, 16, 20, 1, 46, 738, DateTimeKind.Unspecified).AddTicks(4892), "Hic et.", 34, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 12, 41, 58, 430, DateTimeKind.Unspecified).AddTicks(6743), "Voluptate eum in consequatur nostrum molestiae.", new DateTime(2022, 6, 19, 4, 24, 49, 994, DateTimeKind.Unspecified).AddTicks(4785), "Distinctio aspernatur.", 35, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 34, 56, 810, DateTimeKind.Unspecified).AddTicks(1742), "Qui optio illum perspiciatis nemo veniam at tempore soluta.", new DateTime(2021, 1, 6, 9, 56, 52, 194, DateTimeKind.Unspecified).AddTicks(5302), "Cum tenetur.", 41, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 29, 5, 49, 55, 61, DateTimeKind.Unspecified).AddTicks(8079), "Voluptatem qui deserunt soluta aut aspernatur.", new DateTime(2022, 6, 21, 12, 22, 32, 306, DateTimeKind.Unspecified).AddTicks(1828), "Dolores et.", 24, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 23, 7, 57, 288, DateTimeKind.Unspecified).AddTicks(9237), "Occaecati eius qui.", new DateTime(2022, 10, 6, 21, 36, 27, 938, DateTimeKind.Unspecified).AddTicks(5853), "Quibusdam dolorem.", 39, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 13, 5, 21, 415, DateTimeKind.Unspecified).AddTicks(9844), "Accusamus quia cupiditate officiis optio consectetur fugiat.", new DateTime(2022, 10, 26, 23, 42, 17, 384, DateTimeKind.Unspecified).AddTicks(2536), "Autem reprehenderit.", 41, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 14, 18, 8, 21, 191, DateTimeKind.Unspecified).AddTicks(8701), "Fugit quis soluta nihil quo voluptatem.", new DateTime(2022, 6, 29, 9, 25, 29, 527, DateTimeKind.Unspecified).AddTicks(1907), "Iusto amet.", 10, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 0, 16, 7, 992, DateTimeKind.Unspecified).AddTicks(5022), "Aut quam dolorum et.", new DateTime(2022, 9, 9, 0, 11, 11, 377, DateTimeKind.Unspecified).AddTicks(133), "Hic incidunt.", 45, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 20, 17, 9, 54, 687, DateTimeKind.Unspecified).AddTicks(6782), "Temporibus rerum et consequatur sit numquam odio recusandae molestiae omnis.", new DateTime(2021, 1, 25, 11, 38, 50, 99, DateTimeKind.Unspecified).AddTicks(816), "Eos asperiores.", 31, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 13, 9, 13, 59, 224, DateTimeKind.Unspecified).AddTicks(7272), "Aspernatur in deleniti iusto culpa repellendus exercitationem.", new DateTime(2021, 12, 22, 20, 43, 27, 590, DateTimeKind.Unspecified).AddTicks(9838), "Quaerat error.", 31, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 29, 2, 4, 30, 326, DateTimeKind.Unspecified).AddTicks(9369), "Optio nostrum saepe enim ducimus.", new DateTime(2021, 11, 12, 22, 44, 38, 166, DateTimeKind.Unspecified).AddTicks(9043), "Quasi autem.", 2, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 20, 19, 20, 32, 398, DateTimeKind.Unspecified).AddTicks(2885), "Expedita ipsa aut autem at.", new DateTime(2022, 7, 25, 4, 7, 7, 713, DateTimeKind.Unspecified).AddTicks(2768), "Ab accusantium.", 28, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 28, 3, 26, 12, 311, DateTimeKind.Unspecified).AddTicks(6612), "Eius qui laboriosam.", new DateTime(2021, 4, 8, 16, 59, 24, 231, DateTimeKind.Unspecified).AddTicks(5754), "Eius nulla.", 43, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 20, 19, 37, 35, 460, DateTimeKind.Unspecified).AddTicks(8202), "Non itaque nulla id vel neque ut et.", new DateTime(2021, 10, 20, 11, 24, 20, 876, DateTimeKind.Unspecified).AddTicks(7223), "Voluptatem laboriosam.", 37, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 20, 11, 10, 19, 900, DateTimeKind.Unspecified).AddTicks(7033), "Magni magni eligendi.", new DateTime(2021, 5, 22, 10, 2, 24, 202, DateTimeKind.Unspecified).AddTicks(6948), "Dolores voluptatem.", 22, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 22, 9, 0, 53, 454, DateTimeKind.Unspecified).AddTicks(7980), "Velit quo quia.", new DateTime(2022, 10, 22, 14, 19, 52, 783, DateTimeKind.Unspecified).AddTicks(7102), "Eos perferendis.", 27, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 21, 45, 59, 258, DateTimeKind.Unspecified).AddTicks(1083), "Occaecati amet voluptas dolores commodi sed.", new DateTime(2021, 9, 28, 3, 32, 55, 172, DateTimeKind.Unspecified).AddTicks(5827), "Aliquam deleniti.", 33, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 21, 27, 18, 566, DateTimeKind.Unspecified).AddTicks(9692), "Quia quos sequi quis cumque.", new DateTime(2021, 9, 12, 16, 46, 7, 571, DateTimeKind.Unspecified).AddTicks(7763), "Earum eum.", 17, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 5, 19, 50, 13, 978, DateTimeKind.Unspecified).AddTicks(3025), "Tempora natus est ea quae et eos minima deserunt aliquam.", new DateTime(2022, 9, 27, 4, 20, 46, 786, DateTimeKind.Unspecified).AddTicks(6186), "Voluptatem tempore.", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 30, 9, 3, 51, 614, DateTimeKind.Unspecified).AddTicks(9785), "Unde ipsum incidunt ducimus doloribus dolor.", new DateTime(2021, 3, 17, 7, 22, 1, 909, DateTimeKind.Unspecified).AddTicks(4444), "Vel libero.", 37, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 27, 5, 29, 17, 912, DateTimeKind.Unspecified).AddTicks(3616), "Voluptatum consequuntur fuga voluptatem reprehenderit delectus eos.", new DateTime(2021, 9, 4, 12, 14, 54, 700, DateTimeKind.Unspecified).AddTicks(2413), "Explicabo excepturi.", 36, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 13, 8, 45, 582, DateTimeKind.Unspecified).AddTicks(4451), "Error incidunt id.", new DateTime(2022, 2, 26, 2, 53, 7, 986, DateTimeKind.Unspecified).AddTicks(4035), "Repellendus asperiores.", 21, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 11, 2, 48, 43, 189, DateTimeKind.Unspecified).AddTicks(428), "Quibusdam et atque velit ad voluptatem hic soluta nisi et.", new DateTime(2022, 9, 5, 4, 49, 36, 35, DateTimeKind.Unspecified).AddTicks(5315), "Vel harum.", 34, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 25, 14, 28, 18, 462, DateTimeKind.Unspecified).AddTicks(1391), "Quaerat molestias placeat quos alias vel libero odit recusandae.", new DateTime(2021, 3, 11, 8, 25, 53, 691, DateTimeKind.Unspecified).AddTicks(777), "Saepe nesciunt.", 46, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 1, 15, 53, 55, 700, DateTimeKind.Unspecified).AddTicks(2763), "Fugiat voluptatem est alias culpa incidunt suscipit provident rerum repellendus.", new DateTime(2021, 1, 7, 22, 47, 20, 264, DateTimeKind.Unspecified).AddTicks(5483), "Nobis dolore.", 10, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 26, 19, 47, 48, 865, DateTimeKind.Unspecified).AddTicks(286), "Qui impedit facilis atque distinctio porro.", new DateTime(2021, 2, 24, 0, 58, 8, 321, DateTimeKind.Unspecified).AddTicks(8912), "Repudiandae vel.", 23, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 13, 4, 36, 49, 782, DateTimeKind.Unspecified).AddTicks(6324), "Necessitatibus non totam suscipit quo accusamus suscipit voluptatem explicabo.", new DateTime(2022, 7, 5, 13, 52, 25, 401, DateTimeKind.Unspecified).AddTicks(2771), "Dolore incidunt.", 12, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 1, 11, 28, 50, 706, DateTimeKind.Unspecified).AddTicks(2685), "Quidem hic sed dolor.", new DateTime(2022, 3, 18, 9, 8, 44, 262, DateTimeKind.Unspecified).AddTicks(1115), "Officiis ea.", 48, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 3, 11, 54, 12, 541, DateTimeKind.Unspecified).AddTicks(3580), "Et et perspiciatis et rerum eos suscipit.", new DateTime(2022, 6, 4, 22, 11, 13, 889, DateTimeKind.Unspecified).AddTicks(162), "Perspiciatis dolorum.", 45, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 9, 13, 34, 24, 284, DateTimeKind.Unspecified).AddTicks(4609), "Cumque at autem veniam ex animi fugiat.", new DateTime(2022, 5, 9, 23, 21, 25, 401, DateTimeKind.Unspecified).AddTicks(6790), "Et maxime.", 33, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 4, 26, 5, 15, 55, 286, DateTimeKind.Unspecified).AddTicks(1008), "Harum sed est quia velit dolorum.", new DateTime(2022, 4, 23, 4, 54, 40, 825, DateTimeKind.Unspecified).AddTicks(2948), "Porro quaerat.", 49 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 7, 4, 19, 42, 944, DateTimeKind.Unspecified).AddTicks(4551), "Repellat et ipsam et.", new DateTime(2022, 10, 4, 15, 29, 22, 602, DateTimeKind.Unspecified).AddTicks(2600), "Ut magnam.", 7, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 7, 22, 46, 35, 971, DateTimeKind.Unspecified).AddTicks(2269), "Consequatur voluptate adipisci.", new DateTime(2021, 3, 23, 9, 44, 28, 917, DateTimeKind.Unspecified).AddTicks(3760), "Unde qui.", 38, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 8, 6, 6, 43, 51, 9, DateTimeKind.Unspecified).AddTicks(2797), "Eos molestias mollitia consequatur cum sit.", new DateTime(2021, 3, 28, 15, 43, 36, 842, DateTimeKind.Unspecified).AddTicks(4831), "Ipsum et.", 46 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 10, 7, 27, 54, 279, DateTimeKind.Unspecified).AddTicks(3422), "Debitis suscipit praesentium.", new DateTime(2021, 8, 30, 5, 13, 24, 715, DateTimeKind.Unspecified).AddTicks(3817), "Id dolores.", 26, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 4, 0, 29, 44, 983, DateTimeKind.Unspecified).AddTicks(1276), "Magnam fuga cum ipsum autem amet minima ut quasi vero.", new DateTime(2022, 8, 28, 6, 4, 40, 354, DateTimeKind.Unspecified).AddTicks(3879), "Commodi enim.", 18, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 20, 0, 44, 21, 73, DateTimeKind.Unspecified).AddTicks(5621), "Vel corrupti est ut omnis.", new DateTime(2021, 10, 29, 22, 30, 51, 131, DateTimeKind.Unspecified).AddTicks(4563), "A quia.", 37, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 30, 13, 58, 58, 182, DateTimeKind.Unspecified).AddTicks(1541), "Necessitatibus et sint cumque a numquam.", new DateTime(2022, 10, 31, 7, 52, 35, 925, DateTimeKind.Unspecified).AddTicks(1853), "Atque non.", 14, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 3, 15, 10, 59, 0, 79, DateTimeKind.Unspecified).AddTicks(3333), "Quae et qui perspiciatis placeat sit repellat.", new DateTime(2021, 9, 20, 13, 31, 40, 308, DateTimeKind.Unspecified).AddTicks(4780), "Doloremque doloribus.", 27 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 15, 2, 41, 15, 815, DateTimeKind.Unspecified).AddTicks(2973), "Id repudiandae ea consequuntur neque magnam porro.", new DateTime(2022, 10, 6, 10, 51, 26, 357, DateTimeKind.Unspecified).AddTicks(4161), "Vitae sit.", 8, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 24, 13, 11, 31, 993, DateTimeKind.Unspecified).AddTicks(8665), "Fugit sapiente amet enim qui.", new DateTime(2022, 9, 12, 14, 33, 20, 339, DateTimeKind.Unspecified).AddTicks(5142), "Doloremque aut.", 47, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 8, 25, 14, 44, 58, 745, DateTimeKind.Unspecified).AddTicks(8577), "Totam voluptatum ullam ut ipsam nesciunt quisquam suscipit.", new DateTime(2022, 12, 26, 19, 6, 57, 746, DateTimeKind.Unspecified).AddTicks(8890), "Et sapiente.", 7 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 27, 13, 3, 2, 121, DateTimeKind.Unspecified).AddTicks(4805), "Aperiam voluptate illum voluptatem sed.", new DateTime(2021, 9, 23, 14, 42, 56, 315, DateTimeKind.Unspecified).AddTicks(1661), "Expedita recusandae.", 28, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 22, 10, 11, 18, 356, DateTimeKind.Unspecified).AddTicks(635), "Omnis dolore ut et magni et quis optio voluptas.", new DateTime(2021, 5, 25, 19, 43, 44, 500, DateTimeKind.Unspecified).AddTicks(9268), "Dignissimos enim.", 32, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 22, 1, 14, 52, 406, DateTimeKind.Unspecified).AddTicks(2989), "Voluptatem voluptas voluptatem id molestiae quo nisi rerum.", new DateTime(2021, 5, 16, 9, 58, 33, 257, DateTimeKind.Unspecified).AddTicks(3051), "Velit et.", 29, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 27, 1, 26, 20, 745, DateTimeKind.Unspecified).AddTicks(3845), "Iusto vel temporibus voluptates.", new DateTime(2022, 2, 20, 4, 0, 48, 39, DateTimeKind.Unspecified).AddTicks(1322), "Minima recusandae.", 3, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 14, 57, 51, 635, DateTimeKind.Unspecified).AddTicks(353), "Sed facilis nam consectetur porro aut eligendi explicabo.", new DateTime(2022, 11, 21, 19, 40, 37, 746, DateTimeKind.Unspecified).AddTicks(4009), "Ut facere.", 24, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 18, 22, 12, 35, 272, DateTimeKind.Unspecified).AddTicks(5022), "Et ducimus earum eveniet quis eaque.", new DateTime(2022, 8, 6, 15, 38, 14, 757, DateTimeKind.Unspecified).AddTicks(6706), "Explicabo ut.", 44, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 26, 12, 27, 9, 296, DateTimeKind.Unspecified).AddTicks(9844), "Quia autem ut quae corporis voluptates.", new DateTime(2022, 4, 1, 3, 0, 38, 220, DateTimeKind.Unspecified).AddTicks(9694), "Accusamus omnis.", 47, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 0, 53, 23, 381, DateTimeKind.Unspecified).AddTicks(9013), "Sunt reiciendis neque facere et eveniet sed voluptates.", new DateTime(2021, 5, 14, 8, 43, 3, 657, DateTimeKind.Unspecified).AddTicks(5218), "Nam recusandae.", 5, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 10, 15, 7, 38, 301, DateTimeKind.Unspecified).AddTicks(4359), "Assumenda consequuntur id itaque et ea sint voluptatem qui.", new DateTime(2022, 7, 15, 5, 38, 27, 506, DateTimeKind.Unspecified).AddTicks(9136), "Quae velit.", 17, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 1, 12, 5, 53, 664, DateTimeKind.Unspecified).AddTicks(6148), "Dolorum distinctio est minima.", new DateTime(2021, 10, 4, 23, 37, 52, 412, DateTimeKind.Unspecified).AddTicks(3379), "Molestias odio.", 2, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 7, 16, 50, 43, 421, DateTimeKind.Unspecified).AddTicks(8543), "Vitae consectetur magni eveniet provident totam eum aliquid est est.", new DateTime(2022, 9, 28, 16, 29, 57, 157, DateTimeKind.Unspecified).AddTicks(6324), "Omnis explicabo.", 27, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 2, 13, 31, 53, 798, DateTimeKind.Unspecified).AddTicks(1094), "Et sapiente et.", new DateTime(2021, 12, 10, 8, 1, 28, 295, DateTimeKind.Unspecified).AddTicks(8232), "Eveniet consequatur.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 21, 8, 26, 57, 65, DateTimeKind.Unspecified).AddTicks(6916), "Et aliquam quod culpa voluptatibus distinctio doloremque.", new DateTime(2021, 6, 25, 13, 2, 25, 259, DateTimeKind.Unspecified).AddTicks(5799), "Voluptatibus est.", 4, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 7, 7, 25, 27, 516, DateTimeKind.Unspecified).AddTicks(3292), "Ut quasi maiores eum.", new DateTime(2021, 3, 28, 16, 31, 32, 577, DateTimeKind.Unspecified).AddTicks(9465), "Harum aliquid.", 25, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 25, 22, 32, 2, 972, DateTimeKind.Unspecified).AddTicks(9200), "Voluptatem et veritatis.", new DateTime(2021, 7, 27, 0, 50, 34, 309, DateTimeKind.Unspecified).AddTicks(6093), "Velit aut.", 22, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 15, 17, 1, 172, DateTimeKind.Unspecified).AddTicks(5911), "Quibusdam vel modi a qui cupiditate odio nisi id qui.", new DateTime(2021, 3, 18, 20, 14, 24, 659, DateTimeKind.Unspecified).AddTicks(8156), "Expedita recusandae.", 46, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 20, 12, 56, 29, 945, DateTimeKind.Unspecified).AddTicks(2572), "Ad consequuntur eligendi sed quia enim minima.", new DateTime(2021, 10, 7, 10, 10, 31, 827, DateTimeKind.Unspecified).AddTicks(1851), "Rerum ut.", 26, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 14, 18, 32, 335, DateTimeKind.Unspecified).AddTicks(3708), "Voluptate quo ex sed.", new DateTime(2021, 5, 2, 3, 4, 8, 789, DateTimeKind.Unspecified).AddTicks(9408), "Corrupti vel.", 6, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 4, 1, 40, 11, 312, DateTimeKind.Unspecified).AddTicks(1887), "Temporibus temporibus eum maiores maxime id ad sapiente.", new DateTime(2022, 4, 12, 9, 15, 1, 564, DateTimeKind.Unspecified).AddTicks(3948), "Aut soluta.", 32, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 13, 14, 38, 38, 460, DateTimeKind.Unspecified).AddTicks(5624), "Hic ea veniam aperiam cumque accusantium non.", new DateTime(2021, 9, 10, 22, 55, 39, 562, DateTimeKind.Unspecified).AddTicks(6302), "Eligendi earum.", 37, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 22, 20, 25, 14, 311, DateTimeKind.Unspecified).AddTicks(9279), "Perferendis iusto et unde enim consequatur aut.", new DateTime(2021, 9, 28, 11, 49, 18, 22, DateTimeKind.Unspecified).AddTicks(9051), "Earum nisi.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 21, 43, 39, 434, DateTimeKind.Unspecified).AddTicks(8813), "Aut eos eum veniam.", new DateTime(2022, 7, 29, 10, 13, 13, 375, DateTimeKind.Unspecified).AddTicks(9977), "Est vel.", 3, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 9, 4, 11, 4, 20, DateTimeKind.Unspecified).AddTicks(4909), "Corporis modi dolorum dolor.", new DateTime(2022, 1, 7, 17, 14, 3, 520, DateTimeKind.Unspecified).AddTicks(3072), "Totam sed.", 36, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 24, 3, 42, 55, 747, DateTimeKind.Unspecified).AddTicks(5927), "Debitis a occaecati quis.", new DateTime(2022, 4, 20, 9, 41, 42, 81, DateTimeKind.Unspecified).AddTicks(5544), "Quisquam quidem.", 50, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 11, 21, 5, 938, DateTimeKind.Unspecified).AddTicks(8025), "Excepturi ipsam consequatur repellendus est.", new DateTime(2022, 3, 7, 16, 4, 59, 257, DateTimeKind.Unspecified).AddTicks(924), "Tenetur dicta.", 7, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 23, 23, 53, 23, 134, DateTimeKind.Unspecified).AddTicks(7742), "Sapiente magnam doloribus accusamus non.", new DateTime(2021, 1, 13, 22, 6, 49, 756, DateTimeKind.Unspecified).AddTicks(9284), "Voluptates et.", 27, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 18, 35, 44, 131, DateTimeKind.Unspecified).AddTicks(4111), "Vitae mollitia et expedita porro.", new DateTime(2021, 7, 4, 4, 31, 12, 700, DateTimeKind.Unspecified).AddTicks(1785), "Nobis temporibus.", 14, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 15, 17, 26, 21, 35, DateTimeKind.Unspecified).AddTicks(8194), "Natus ullam ipsum harum quis repellendus sed itaque.", new DateTime(2022, 6, 24, 22, 36, 38, 892, DateTimeKind.Unspecified).AddTicks(4344), "Quam est.", 33, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 8, 19, 27, 56, 324, DateTimeKind.Unspecified).AddTicks(7221), "Voluptas et incidunt cumque iure blanditiis nobis a quam aut.", new DateTime(2022, 2, 23, 17, 25, 56, 353, DateTimeKind.Unspecified).AddTicks(792), "Nemo debitis.", 10, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 26, 0, 52, 14, 629, DateTimeKind.Unspecified).AddTicks(1892), "Aut repellat vel aut fuga quis quae sapiente rerum tempora.", new DateTime(2021, 3, 1, 15, 50, 15, 51, DateTimeKind.Unspecified).AddTicks(6944), "Et temporibus.", 42, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 2, 6, 59, 935, DateTimeKind.Unspecified).AddTicks(2000), "Omnis ut quam in sunt eius explicabo qui maxime laudantium.", new DateTime(2022, 10, 30, 4, 45, 26, 129, DateTimeKind.Unspecified).AddTicks(2779), "Quia ducimus.", 20, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 10, 16, 48, 18, 451, DateTimeKind.Unspecified).AddTicks(1067), "A qui qui temporibus numquam quam in aliquam.", new DateTime(2021, 6, 10, 11, 23, 38, 816, DateTimeKind.Unspecified).AddTicks(1426), "Doloribus quia.", 23, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 30, 9, 2, 42, 100, DateTimeKind.Unspecified).AddTicks(7083), "Ex facere saepe omnis non.", new DateTime(2022, 6, 13, 9, 55, 18, 337, DateTimeKind.Unspecified).AddTicks(8377), "Quo enim.", 19, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 6, 48, 45, 552, DateTimeKind.Unspecified).AddTicks(1807), "Suscipit rerum repellendus reiciendis quam placeat sed quos.", new DateTime(2022, 12, 3, 18, 1, 8, 606, DateTimeKind.Unspecified).AddTicks(6471), "Quidem aut.", 23, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 3, 18, 45, 55, 4, DateTimeKind.Unspecified).AddTicks(5467), "Commodi quo optio blanditiis odit itaque.", new DateTime(2021, 4, 12, 4, 15, 40, 14, DateTimeKind.Unspecified).AddTicks(1120), "Inventore delectus.", 1, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 3, 39, 0, 168, DateTimeKind.Unspecified).AddTicks(7483), "Et quam sequi fuga ut deleniti repellendus explicabo occaecati nihil.", new DateTime(2022, 7, 4, 4, 37, 52, 22, DateTimeKind.Unspecified).AddTicks(4534), "Neque qui.", 16, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 9, 6, 43, 8, 809, DateTimeKind.Unspecified).AddTicks(3734), "Quod maiores dolorem a vel.", new DateTime(2021, 1, 28, 21, 17, 6, 76, DateTimeKind.Unspecified).AddTicks(9019), "Dolores maxime.", 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 6, 3, 25, 530, DateTimeKind.Unspecified).AddTicks(2888), "Tempore sed illum asperiores ad maxime doloribus in.", new DateTime(2021, 8, 19, 13, 41, 11, 725, DateTimeKind.Unspecified).AddTicks(6090), "Et accusamus.", 42, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 14, 0, 4, 51, 900, DateTimeKind.Unspecified).AddTicks(1566), "Et voluptatem omnis similique ipsa.", new DateTime(2022, 2, 9, 16, 44, 10, 490, DateTimeKind.Unspecified).AddTicks(8458), "Quisquam unde.", 50, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 9, 44, 40, 42, DateTimeKind.Unspecified).AddTicks(9676), "Iusto totam ea hic nisi sed labore rem.", new DateTime(2022, 1, 5, 19, 15, 54, 141, DateTimeKind.Unspecified).AddTicks(2597), "Earum ipsum.", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 9, 19, 20, 872, DateTimeKind.Unspecified).AddTicks(3038), "Ratione perspiciatis ducimus.", new DateTime(2022, 9, 28, 1, 27, 17, 270, DateTimeKind.Unspecified).AddTicks(1117), "Amet impedit.", 39, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 9, 23, 0, 7, 47, 262, DateTimeKind.Unspecified).AddTicks(3290), "Repudiandae officia tempore incidunt ullam fugiat odit dolor voluptas.", new DateTime(2021, 9, 26, 10, 12, 34, 718, DateTimeKind.Unspecified).AddTicks(6766), "Inventore eius.", 16 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 26, 11, 26, 24, 865, DateTimeKind.Unspecified).AddTicks(5886), "In beatae asperiores quaerat molestiae adipisci fugit aspernatur.", new DateTime(2021, 7, 13, 5, 7, 18, 858, DateTimeKind.Unspecified).AddTicks(4788), "Alias illum.", 44, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 5, 3, 11, 32, 19, 422, DateTimeKind.Unspecified).AddTicks(2642), "Leannon LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 10, 18, 14, 48, 22, 887, DateTimeKind.Unspecified).AddTicks(4342), "Smitham Inc" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 8, 6, 12, 17, 25, 933, DateTimeKind.Unspecified).AddTicks(8438), "Koss, Wisoky and Rutherford" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 11, 20, 5, 2, 41, 954, DateTimeKind.Unspecified).AddTicks(5946), "Stroman Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 10, 31, 5, 57, 4, 441, DateTimeKind.Unspecified).AddTicks(1266), "Weber - Schultz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 3, 15, 19, 11, 42, 237, DateTimeKind.Unspecified).AddTicks(5832), "Corkery Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 5, 20, 18, 30, 48, 871, DateTimeKind.Unspecified).AddTicks(6808), "Armstrong LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 9, 3, 3, 13, 1, 451, DateTimeKind.Unspecified).AddTicks(3583), "Hahn - Beer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 12, 29, 17, 53, 2, 596, DateTimeKind.Unspecified).AddTicks(3948), "Hills LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 2, 9, 9, 47, 41, 154, DateTimeKind.Unspecified).AddTicks(5239), "Hane Group" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 10, 18, 7, 18, 50, 667, DateTimeKind.Unspecified).AddTicks(6423), "Ervin4@yahoo.com", "Stephania", "Bahringer", new DateTime(2011, 1, 24, 23, 45, 58, 667, DateTimeKind.Unspecified).AddTicks(1457), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 2, 21, 43, 5, 132, DateTimeKind.Unspecified).AddTicks(4318), "Ralph_Olson95@gmail.com", "Sally", "Raynor", new DateTime(2011, 8, 30, 17, 34, 3, 18, DateTimeKind.Unspecified).AddTicks(3640), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 25, 5, 59, 51, 787, DateTimeKind.Unspecified).AddTicks(1412), "Graciela.Beier47@gmail.com", "Ransom", "Padberg", new DateTime(2016, 8, 26, 13, 57, 59, 386, DateTimeKind.Unspecified).AddTicks(1189), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 4, 12, 6, 16, 29, 143, DateTimeKind.Unspecified).AddTicks(620), "Anastacio_Hamill@hotmail.com", "Wellington", "Bogisich", new DateTime(2012, 3, 27, 8, 57, 24, 930, DateTimeKind.Unspecified).AddTicks(1593), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1994, 12, 14, 6, 17, 58, 365, DateTimeKind.Unspecified).AddTicks(2773), "Florine89@yahoo.com", "Maurine", "Barrows", new DateTime(2010, 1, 6, 19, 44, 49, 89, DateTimeKind.Unspecified).AddTicks(5215) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 6, 9, 18, 27, 28, 718, DateTimeKind.Unspecified).AddTicks(1394), "Lauren85@gmail.com", "Amira", "Schaefer", new DateTime(2013, 10, 10, 3, 11, 5, 740, DateTimeKind.Unspecified).AddTicks(5662), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 12, 14, 8, 40, 1, 956, DateTimeKind.Unspecified).AddTicks(2731), "Shana45@yahoo.com", "Ardella", "Hand", new DateTime(2017, 5, 12, 21, 31, 19, 388, DateTimeKind.Unspecified).AddTicks(7434), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 3, 5, 21, 6, 33, 405, DateTimeKind.Unspecified).AddTicks(72), "Haskell7@hotmail.com", "Jacques", "Hayes", new DateTime(2013, 6, 29, 8, 56, 36, 171, DateTimeKind.Unspecified).AddTicks(2537), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 6, 7, 54, 1, 138, DateTimeKind.Unspecified).AddTicks(4795), "Abbigail.Daugherty74@hotmail.com", "Ibrahim", "Strosin", new DateTime(2011, 5, 24, 20, 10, 26, 798, DateTimeKind.Unspecified).AddTicks(1095), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 7, 11, 21, 6, 205, DateTimeKind.Unspecified).AddTicks(725), "Kailee_Gleason@yahoo.com", "Danial", "Schneider", new DateTime(2013, 5, 5, 6, 41, 11, 293, DateTimeKind.Unspecified).AddTicks(2128), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 8, 20, 2, 49, 11, 413, DateTimeKind.Unspecified).AddTicks(6636), "Coby.Marquardt42@gmail.com", "Hershel", "Rippin", new DateTime(2017, 3, 19, 19, 46, 21, 626, DateTimeKind.Unspecified).AddTicks(5756) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 8, 23, 6, 2, 26, 862, DateTimeKind.Unspecified).AddTicks(7212), "Luis_Reichert33@gmail.com", "Emmanuelle", "Crooks", new DateTime(2012, 8, 4, 5, 45, 35, 370, DateTimeKind.Unspecified).AddTicks(6776), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 9, 28, 21, 6, 58, 984, DateTimeKind.Unspecified).AddTicks(5040), "Jaycee.OReilly23@hotmail.com", "Efren", "Johnson", new DateTime(2016, 6, 3, 6, 36, 53, 719, DateTimeKind.Unspecified).AddTicks(5286), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 18, 9, 11, 43, 669, DateTimeKind.Unspecified).AddTicks(2251), "Fae.Dooley37@yahoo.com", "Rodolfo", "Emard", new DateTime(2014, 1, 26, 16, 32, 7, 568, DateTimeKind.Unspecified).AddTicks(932), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 6, 5, 1, 39, 16, 994, DateTimeKind.Unspecified).AddTicks(5738), "Roderick.Feeney20@hotmail.com", "Armando", "Smitham", new DateTime(2010, 8, 15, 21, 28, 54, 193, DateTimeKind.Unspecified).AddTicks(8735), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 12, 8, 44, 2, 999, DateTimeKind.Unspecified).AddTicks(756), "Golda58@yahoo.com", "Thad", "Dooley", new DateTime(2013, 8, 17, 17, 52, 2, 993, DateTimeKind.Unspecified).AddTicks(445), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 29, 19, 44, 20, 75, DateTimeKind.Unspecified).AddTicks(6860), "Jaleel_Gusikowski@gmail.com", "Colt", "Schroeder", new DateTime(2016, 1, 21, 10, 13, 41, 441, DateTimeKind.Unspecified).AddTicks(1868), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 30, 11, 27, 12, 988, DateTimeKind.Unspecified).AddTicks(6197), "Foster62@gmail.com", "Ahmad", "Windler", new DateTime(2013, 11, 16, 20, 7, 27, 402, DateTimeKind.Unspecified).AddTicks(8679), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 3, 1, 20, 25, 47, 637, DateTimeKind.Unspecified).AddTicks(6488), "Eveline_Schaefer67@gmail.com", "Alverta", "Reilly", new DateTime(2014, 3, 22, 19, 23, 26, 732, DateTimeKind.Unspecified).AddTicks(7922), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 9, 9, 21, 5, 739, DateTimeKind.Unspecified).AddTicks(3674), "Okey0@hotmail.com", "Georgiana", "Waelchi", new DateTime(2017, 6, 1, 14, 25, 22, 6, DateTimeKind.Unspecified).AddTicks(5495), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 11, 6, 22, 55, 36, 852, DateTimeKind.Unspecified).AddTicks(4446), "Arno_Bayer18@hotmail.com", "Antonina", "Kirlin", new DateTime(2016, 8, 21, 12, 52, 53, 852, DateTimeKind.Unspecified).AddTicks(6795), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 5, 1, 2, 28, 7, 574, DateTimeKind.Unspecified).AddTicks(281), "Elijah93@gmail.com", "Lloyd", "Gulgowski", new DateTime(2017, 1, 14, 13, 11, 39, 187, DateTimeKind.Unspecified).AddTicks(3178), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 16, 1, 25, 35, 702, DateTimeKind.Unspecified).AddTicks(2831), "Bryana_Fay@yahoo.com", "Cecelia", "Hoppe", new DateTime(2012, 7, 1, 11, 52, 14, 790, DateTimeKind.Unspecified).AddTicks(2539), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 28, 6, 21, 51, 707, DateTimeKind.Unspecified).AddTicks(8762), "Anika.Stoltenberg22@gmail.com", "Brooklyn", "Stark", new DateTime(2014, 2, 3, 22, 59, 53, 145, DateTimeKind.Unspecified).AddTicks(9020), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 1, 1, 6, 22, 5, 207, DateTimeKind.Unspecified).AddTicks(501), "Alessia_Mohr@yahoo.com", "Donnie", "Thiel", new DateTime(2016, 11, 18, 19, 47, 0, 871, DateTimeKind.Unspecified).AddTicks(5137), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 10, 22, 19, 21, 31, 499, DateTimeKind.Unspecified).AddTicks(7454), "Christy.Zboncak94@yahoo.com", "Judd", "Stracke", new DateTime(2014, 5, 27, 3, 39, 0, 692, DateTimeKind.Unspecified).AddTicks(6514) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1994, 6, 29, 17, 4, 58, 531, DateTimeKind.Unspecified).AddTicks(9718), "Haylee.Marks@yahoo.com", "Tianna", "Kohler", new DateTime(2017, 2, 5, 18, 31, 54, 539, DateTimeKind.Unspecified).AddTicks(9198) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 10, 1, 22, 8, 55, 607, DateTimeKind.Unspecified).AddTicks(4201), "Joshuah28@gmail.com", "Cristian", "Williamson", new DateTime(2013, 6, 28, 20, 12, 18, 952, DateTimeKind.Unspecified).AddTicks(4627), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 5, 16, 17, 2, 153, DateTimeKind.Unspecified).AddTicks(3424), "Jose78@hotmail.com", "Curtis", "Balistreri", new DateTime(2014, 5, 2, 22, 57, 33, 808, DateTimeKind.Unspecified).AddTicks(764), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 12, 17, 14, 16, 5, 595, DateTimeKind.Unspecified).AddTicks(7651), "Kathlyn.Hodkiewicz@gmail.com", "Eldred", "Abshire", new DateTime(2016, 3, 25, 21, 35, 49, 803, DateTimeKind.Unspecified).AddTicks(9217), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 23, 20, 16, 26, 990, DateTimeKind.Unspecified).AddTicks(1279), "Keely49@hotmail.com", "Anastasia", "Ratke", new DateTime(2010, 6, 4, 14, 13, 8, 52, DateTimeKind.Unspecified).AddTicks(7251), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 12, 3, 2, 43, 2, 729, DateTimeKind.Unspecified).AddTicks(903), "Jevon_Cummings88@hotmail.com", "Adalberto", "Bergstrom", new DateTime(2015, 2, 28, 12, 23, 54, 704, DateTimeKind.Unspecified).AddTicks(3541), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 24, 17, 4, 57, 291, DateTimeKind.Unspecified).AddTicks(2008), "Lisandro10@yahoo.com", "Golda", "Oberbrunner", new DateTime(2013, 2, 25, 11, 47, 39, 1, DateTimeKind.Unspecified).AddTicks(3039), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 11, 20, 6, 21, 32, 38, DateTimeKind.Unspecified).AddTicks(7807), "Maribel71@yahoo.com", "Imani", "Kutch", new DateTime(2014, 3, 4, 16, 34, 49, 645, DateTimeKind.Unspecified).AddTicks(8960), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 6, 1, 22, 37, 39, 438, DateTimeKind.Unspecified).AddTicks(7744), "Xzavier_Skiles@gmail.com", "Eliseo", "Auer", new DateTime(2012, 6, 27, 22, 41, 34, 273, DateTimeKind.Unspecified).AddTicks(2854), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 3, 5, 0, 21, 29, 215, DateTimeKind.Unspecified).AddTicks(913), "Kieran83@gmail.com", "Maxie", "McClure", new DateTime(2015, 10, 5, 11, 52, 39, 793, DateTimeKind.Unspecified).AddTicks(736), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 12, 16, 7, 20, 16, 896, DateTimeKind.Unspecified).AddTicks(5354), "Wilmer12@yahoo.com", "Noah", "Treutel", new DateTime(2010, 5, 16, 3, 26, 9, 474, DateTimeKind.Unspecified).AddTicks(5574), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 13, 8, 19, 50, 464, DateTimeKind.Unspecified).AddTicks(4664), "Paolo91@yahoo.com", "Bridget", "Lueilwitz", new DateTime(2016, 12, 25, 8, 37, 0, 596, DateTimeKind.Unspecified).AddTicks(3211), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 10, 9, 37, 2, 755, DateTimeKind.Unspecified).AddTicks(7444), "Hannah.Terry@hotmail.com", "Mossie", "Bradtke", new DateTime(2016, 4, 11, 0, 0, 58, 295, DateTimeKind.Unspecified).AddTicks(7156), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 6, 28, 2, 12, 47, 242, DateTimeKind.Unspecified).AddTicks(2882), "Aileen_Bruen19@hotmail.com", "Tre", "Grady", new DateTime(2014, 1, 7, 14, 2, 8, 758, DateTimeKind.Unspecified).AddTicks(527), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 4, 12, 16, 19, 41, 856, DateTimeKind.Unspecified).AddTicks(3561), "Pierre_Towne4@yahoo.com", "Dejah", "Cronin", new DateTime(2012, 11, 10, 1, 35, 14, 474, DateTimeKind.Unspecified).AddTicks(3442), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 19, 1, 27, 35, 640, DateTimeKind.Unspecified).AddTicks(4941), "Joan.Bosco71@gmail.com", "Ara", "Stanton", new DateTime(2015, 1, 28, 18, 35, 57, 398, DateTimeKind.Unspecified).AddTicks(545), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 4, 6, 15, 13, 364, DateTimeKind.Unspecified).AddTicks(1669), "Melba36@hotmail.com", "Alex", "Stanton", new DateTime(2016, 8, 13, 23, 14, 18, 862, DateTimeKind.Unspecified).AddTicks(8251), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 25, 6, 10, 5, 220, DateTimeKind.Unspecified).AddTicks(9215), "Tyra99@gmail.com", "Jewel", "Schumm", new DateTime(2016, 11, 19, 23, 13, 13, 417, DateTimeKind.Unspecified).AddTicks(5141), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 11, 12, 22, 39, 948, DateTimeKind.Unspecified).AddTicks(1571), "Christine_Miller83@gmail.com", "Heath", "Jacobi", new DateTime(2015, 8, 12, 1, 9, 34, 842, DateTimeKind.Unspecified).AddTicks(6907), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 11, 25, 21, 54, 27, 286, DateTimeKind.Unspecified).AddTicks(7010), "Esta99@gmail.com", "Kenny", "Rodriguez", new DateTime(2010, 11, 27, 21, 50, 11, 516, DateTimeKind.Unspecified).AddTicks(2695), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 2, 21, 1, 57, 47, 367, DateTimeKind.Unspecified).AddTicks(5266), "Reynold_Marks@hotmail.com", "Seamus", "Walsh", new DateTime(2012, 8, 8, 4, 41, 7, 521, DateTimeKind.Unspecified).AddTicks(4092), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 30, 23, 22, 10, 661, DateTimeKind.Unspecified).AddTicks(1915), "Ansel.Ruecker38@hotmail.com", "Bailey", "Ruecker", new DateTime(2017, 11, 19, 10, 32, 39, 668, DateTimeKind.Unspecified).AddTicks(5690), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 3, 0, 46, 11, 638, DateTimeKind.Unspecified).AddTicks(5831), "Lorenz_Smith@yahoo.com", "Loren", "Dooley", new DateTime(2014, 10, 22, 5, 39, 12, 79, DateTimeKind.Unspecified).AddTicks(674), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 3, 20, 40, 5, 369, DateTimeKind.Unspecified).AddTicks(4846), "Geovany_Rice19@gmail.com", "Alysson", "Bruen", new DateTime(2016, 6, 11, 4, 5, 51, 398, DateTimeKind.Unspecified).AddTicks(7668), 7 });

            migrationBuilder.AddForeignKey(
                name: "FK_Task_Users_PerformerId",
                table: "Task",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
