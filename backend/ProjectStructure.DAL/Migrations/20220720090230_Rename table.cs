﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectStructure.DAL.Migrations
{
    public partial class Renametable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Task");

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PerformerUserId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerUserId",
                        column: x => x.PerformerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 22, new DateTime(2019, 8, 24, 7, 40, 26, 806, DateTimeKind.Unspecified).AddTicks(6001), new DateTime(2021, 12, 26, 14, 40, 56, 5, DateTimeKind.Unspecified).AddTicks(5805), "Libero vero nihil animi quidem.", "Gorgeous Metal Shirt", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 21, new DateTime(2019, 6, 26, 5, 22, 38, 486, DateTimeKind.Unspecified).AddTicks(5724), new DateTime(2021, 3, 17, 11, 20, 56, 614, DateTimeKind.Unspecified).AddTicks(8421), "Rem error placeat enim harum nemo blanditiis.", "Ergonomic Cotton Ball", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 47, new DateTime(2019, 7, 17, 16, 5, 17, 410, DateTimeKind.Unspecified).AddTicks(130), new DateTime(2022, 6, 13, 14, 2, 48, 353, DateTimeKind.Unspecified).AddTicks(8837), "Non sint et et vel qui odit repellat.", "Refined Concrete Tuna", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 26, new DateTime(2019, 6, 3, 5, 46, 31, 346, DateTimeKind.Unspecified).AddTicks(1640), new DateTime(2021, 1, 14, 18, 19, 41, 708, DateTimeKind.Unspecified).AddTicks(5474), "Consequatur autem est ratione fugit atque et repudiandae ex.", "Refined Soft Salad" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 24, new DateTime(2019, 9, 4, 20, 14, 15, 879, DateTimeKind.Unspecified).AddTicks(2656), new DateTime(2021, 7, 27, 22, 11, 27, 512, DateTimeKind.Unspecified).AddTicks(6428), "Necessitatibus id deleniti et velit exercitationem aut qui.", "Unbranded Wooden Car" });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 3, 12, 19, 1, 33, 50, DateTimeKind.Unspecified).AddTicks(1273), "Explicabo sunt distinctio eligendi aliquid quia earum qui.", new DateTime(2021, 7, 13, 0, 40, 25, 38, DateTimeKind.Unspecified).AddTicks(3826), "Optio est.", 34, 5, 2 },
                    { 2, new DateTime(2019, 12, 7, 7, 21, 53, 31, DateTimeKind.Unspecified).AddTicks(5934), "Enim exercitationem ea cum nihil.", new DateTime(2021, 8, 7, 7, 25, 46, 392, DateTimeKind.Unspecified).AddTicks(9158), "Et molestiae.", 38, 1, 3 },
                    { 3, new DateTime(2019, 4, 10, 16, 11, 3, 232, DateTimeKind.Unspecified).AddTicks(3790), "Non eum reprehenderit.", new DateTime(2021, 7, 30, 13, 54, 43, 207, DateTimeKind.Unspecified).AddTicks(2278), "Odit laboriosam.", 40, 3, 2 },
                    { 4, new DateTime(2019, 7, 31, 13, 6, 45, 87, DateTimeKind.Unspecified).AddTicks(3148), "Veniam consequuntur enim ipsam facere.", new DateTime(2021, 1, 27, 12, 30, 11, 557, DateTimeKind.Unspecified).AddTicks(3522), "Voluptas aspernatur.", 44, 2, 1 },
                    { 5, new DateTime(2019, 6, 17, 19, 14, 9, 294, DateTimeKind.Unspecified).AddTicks(3349), "Quia architecto fugit tenetur voluptas rerum qui.", new DateTime(2022, 9, 16, 4, 42, 48, 170, DateTimeKind.Unspecified).AddTicks(3378), "Accusantium optio.", 21, 4, 2 },
                    { 6, new DateTime(2019, 1, 4, 18, 0, 14, 29, DateTimeKind.Unspecified).AddTicks(2423), "Est molestiae nihil ut.", new DateTime(2022, 12, 22, 17, 51, 46, 948, DateTimeKind.Unspecified).AddTicks(9786), "Dolore praesentium.", 6, 1, 1 },
                    { 7, new DateTime(2019, 2, 22, 1, 11, 26, 481, DateTimeKind.Unspecified).AddTicks(4108), "Enim reiciendis fugit et ratione officia.", new DateTime(2022, 2, 17, 8, 33, 36, 585, DateTimeKind.Unspecified).AddTicks(8595), "Distinctio ipsum.", 8, 5, 1 },
                    { 8, new DateTime(2019, 7, 20, 17, 51, 33, 560, DateTimeKind.Unspecified).AddTicks(6735), "Et fugit voluptatem reprehenderit asperiores.", new DateTime(2021, 2, 24, 5, 56, 47, 885, DateTimeKind.Unspecified).AddTicks(2817), "Laudantium illum.", 7, 5, 3 },
                    { 9, new DateTime(2019, 12, 17, 10, 14, 4, 623, DateTimeKind.Unspecified).AddTicks(8263), "Quaerat qui doloribus optio minima dolorem qui ex consequuntur et.", new DateTime(2022, 3, 1, 6, 10, 9, 621, DateTimeKind.Unspecified).AddTicks(4122), "Sint quos.", 10, 1, 3 },
                    { 10, new DateTime(2019, 5, 16, 4, 59, 25, 517, DateTimeKind.Unspecified).AddTicks(4232), "Ut recusandae eos ullam qui veniam quas qui delectus inventore.", new DateTime(2022, 2, 21, 20, 58, 46, 944, DateTimeKind.Unspecified).AddTicks(3086), "Laboriosam quas.", 6, 4, 3 },
                    { 11, new DateTime(2019, 2, 24, 17, 56, 16, 966, DateTimeKind.Unspecified).AddTicks(671), "Laudantium corporis ad quis sed qui odio eos dolore sed.", new DateTime(2022, 11, 10, 5, 11, 14, 13, DateTimeKind.Unspecified).AddTicks(787), "Officia et.", 8, 3, 0 },
                    { 12, new DateTime(2019, 4, 11, 11, 10, 40, 511, DateTimeKind.Unspecified).AddTicks(2865), "Dolorem in consequuntur et saepe voluptatem quas officiis velit.", new DateTime(2021, 9, 28, 13, 23, 23, 56, DateTimeKind.Unspecified).AddTicks(2092), "Ipsam maxime.", 7, 1, 2 },
                    { 13, new DateTime(2019, 1, 9, 23, 46, 36, 909, DateTimeKind.Unspecified).AddTicks(9986), "Omnis esse nobis sed vitae et mollitia esse molestias rem.", new DateTime(2021, 3, 18, 15, 14, 29, 630, DateTimeKind.Unspecified).AddTicks(5264), "Dolores quisquam.", 3, 3, 1 },
                    { 14, new DateTime(2019, 5, 18, 8, 57, 49, 581, DateTimeKind.Unspecified).AddTicks(7468), "Omnis commodi voluptatem aliquam ut dolorem unde sunt repellendus.", new DateTime(2022, 5, 9, 19, 22, 58, 599, DateTimeKind.Unspecified).AddTicks(9140), "Voluptates aperiam.", 44, 5, 0 },
                    { 15, new DateTime(2019, 4, 26, 20, 31, 36, 783, DateTimeKind.Unspecified).AddTicks(1035), "Accusamus est aut ad pariatur quis labore in.", new DateTime(2021, 9, 24, 23, 28, 39, 260, DateTimeKind.Unspecified).AddTicks(1609), "Tempora veritatis.", 36, 4, 1 },
                    { 16, new DateTime(2019, 1, 3, 12, 46, 35, 110, DateTimeKind.Unspecified).AddTicks(6396), "Facere quia est non.", new DateTime(2022, 10, 9, 6, 28, 57, 222, DateTimeKind.Unspecified).AddTicks(1906), "Et est.", 8, 5, 1 },
                    { 17, new DateTime(2019, 12, 8, 11, 45, 53, 154, DateTimeKind.Unspecified).AddTicks(8264), "Eaque laudantium et quaerat possimus.", new DateTime(2021, 11, 7, 4, 0, 19, 162, DateTimeKind.Unspecified).AddTicks(79), "Eligendi eum.", 24, 5, 0 },
                    { 18, new DateTime(2019, 3, 13, 13, 17, 47, 923, DateTimeKind.Unspecified).AddTicks(8020), "Natus possimus quibusdam voluptas ullam voluptas quidem tempore laboriosam.", new DateTime(2021, 1, 12, 9, 8, 13, 583, DateTimeKind.Unspecified).AddTicks(8751), "Eaque tenetur.", 38, 4, 3 },
                    { 19, new DateTime(2019, 7, 21, 18, 22, 16, 998, DateTimeKind.Unspecified).AddTicks(6200), "Commodi tempore ducimus qui voluptatibus.", new DateTime(2021, 11, 4, 15, 39, 43, 19, DateTimeKind.Unspecified).AddTicks(3429), "Deleniti dicta.", 25, 1, 1 },
                    { 20, new DateTime(2019, 11, 24, 17, 1, 14, 974, DateTimeKind.Unspecified).AddTicks(8162), "Voluptatem dolor ex labore et eum ex non et.", new DateTime(2022, 9, 8, 22, 46, 24, 100, DateTimeKind.Unspecified).AddTicks(188), "Ea cumque.", 1, 1, 1 },
                    { 21, new DateTime(2019, 10, 21, 18, 28, 34, 204, DateTimeKind.Unspecified).AddTicks(8179), "Ea fugit voluptatibus odio consequatur dolor beatae.", new DateTime(2022, 11, 6, 11, 34, 19, 188, DateTimeKind.Unspecified).AddTicks(7603), "Quos sint.", 17, 2, 2 },
                    { 22, new DateTime(2019, 7, 10, 3, 37, 9, 836, DateTimeKind.Unspecified).AddTicks(8709), "Voluptatem sint et maxime itaque ea nulla qui quos.", new DateTime(2021, 4, 27, 13, 7, 48, 1, DateTimeKind.Unspecified).AddTicks(3328), "Saepe neque.", 3, 5, 0 },
                    { 23, new DateTime(2019, 10, 16, 1, 47, 17, 813, DateTimeKind.Unspecified).AddTicks(4853), "Dolore est velit similique necessitatibus quia esse temporibus.", new DateTime(2021, 3, 25, 8, 0, 34, 250, DateTimeKind.Unspecified).AddTicks(421), "Facilis architecto.", 20, 1, 0 },
                    { 24, new DateTime(2019, 9, 14, 12, 52, 22, 217, DateTimeKind.Unspecified).AddTicks(4527), "Non fuga dolore sunt autem eum nulla quia.", new DateTime(2022, 7, 10, 12, 15, 53, 809, DateTimeKind.Unspecified).AddTicks(6631), "Quasi velit.", 23, 3, 2 },
                    { 25, new DateTime(2019, 12, 13, 12, 5, 6, 305, DateTimeKind.Unspecified).AddTicks(5718), "Consequatur commodi corrupti dolore cumque veniam et.", new DateTime(2021, 6, 15, 8, 5, 34, 527, DateTimeKind.Unspecified).AddTicks(2521), "Qui quo.", 40, 5, 2 },
                    { 26, new DateTime(2019, 5, 18, 11, 9, 38, 584, DateTimeKind.Unspecified).AddTicks(8941), "Repellat quasi numquam non animi sed provident consequatur delectus nostrum.", new DateTime(2022, 7, 5, 15, 31, 23, 755, DateTimeKind.Unspecified).AddTicks(3378), "Id minima.", 49, 4, 3 },
                    { 27, new DateTime(2019, 8, 4, 21, 20, 8, 981, DateTimeKind.Unspecified).AddTicks(564), "Inventore voluptas dolore.", new DateTime(2022, 3, 20, 13, 44, 33, 861, DateTimeKind.Unspecified).AddTicks(4799), "Dolore vitae.", 46, 1, 1 },
                    { 28, new DateTime(2019, 2, 22, 22, 40, 47, 772, DateTimeKind.Unspecified).AddTicks(2259), "Consequatur voluptatibus aut optio magnam voluptas minus.", new DateTime(2022, 6, 24, 18, 16, 10, 671, DateTimeKind.Unspecified).AddTicks(3082), "Maxime commodi.", 32, 5, 1 },
                    { 29, new DateTime(2019, 3, 4, 1, 8, 6, 554, DateTimeKind.Unspecified).AddTicks(6611), "Soluta sapiente sapiente aut.", new DateTime(2021, 7, 22, 21, 11, 28, 48, DateTimeKind.Unspecified).AddTicks(9819), "Quo corrupti.", 36, 3, 0 },
                    { 30, new DateTime(2019, 2, 6, 14, 42, 37, 212, DateTimeKind.Unspecified).AddTicks(8567), "Quae nemo quae.", new DateTime(2022, 2, 5, 8, 7, 54, 829, DateTimeKind.Unspecified).AddTicks(5644), "Nostrum numquam.", 15, 4, 3 },
                    { 31, new DateTime(2019, 11, 5, 7, 27, 51, 176, DateTimeKind.Unspecified).AddTicks(6966), "Impedit ut voluptas pariatur tempora.", new DateTime(2021, 9, 11, 8, 19, 33, 310, DateTimeKind.Unspecified).AddTicks(7693), "Quae est.", 33, 1, 1 },
                    { 32, new DateTime(2019, 4, 13, 13, 3, 31, 339, DateTimeKind.Unspecified).AddTicks(9199), "Omnis est fugiat explicabo dolor at voluptas illo iste.", new DateTime(2021, 5, 6, 11, 39, 2, 394, DateTimeKind.Unspecified).AddTicks(7497), "Assumenda inventore.", 46, 4, 2 },
                    { 33, new DateTime(2019, 5, 12, 16, 25, 51, 470, DateTimeKind.Unspecified).AddTicks(265), "Laborum labore voluptatem commodi.", new DateTime(2021, 7, 20, 2, 36, 12, 296, DateTimeKind.Unspecified).AddTicks(1004), "Ipsam maxime.", 23, 1, 3 },
                    { 34, new DateTime(2019, 2, 15, 9, 25, 7, 492, DateTimeKind.Unspecified).AddTicks(6211), "Similique aperiam est omnis quis.", new DateTime(2022, 5, 17, 13, 16, 12, 894, DateTimeKind.Unspecified).AddTicks(8977), "Pariatur quaerat.", 6, 2, 1 },
                    { 35, new DateTime(2019, 8, 7, 9, 55, 12, 818, DateTimeKind.Unspecified).AddTicks(9736), "In reiciendis aliquid tenetur.", new DateTime(2022, 12, 23, 6, 21, 0, 93, DateTimeKind.Unspecified).AddTicks(265), "Sit excepturi.", 21, 5, 1 },
                    { 36, new DateTime(2019, 8, 9, 23, 43, 56, 455, DateTimeKind.Unspecified).AddTicks(1036), "At aliquam id placeat quas.", new DateTime(2021, 2, 5, 17, 53, 12, 858, DateTimeKind.Unspecified).AddTicks(4587), "Omnis dolorem.", 6, 2, 3 },
                    { 37, new DateTime(2019, 12, 31, 4, 32, 1, 982, DateTimeKind.Unspecified).AddTicks(1587), "Labore culpa quasi sit labore nemo.", new DateTime(2021, 2, 2, 11, 17, 5, 290, DateTimeKind.Unspecified).AddTicks(9167), "Minus exercitationem.", 45, 3, 0 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 38, new DateTime(2019, 3, 3, 7, 33, 13, 964, DateTimeKind.Unspecified).AddTicks(5737), "Consequatur eum quia exercitationem.", new DateTime(2022, 1, 15, 2, 50, 30, 5, DateTimeKind.Unspecified).AddTicks(3870), "Dignissimos quis.", 30, 1, 1 },
                    { 39, new DateTime(2019, 2, 1, 4, 43, 22, 832, DateTimeKind.Unspecified).AddTicks(7079), "Enim reprehenderit quae est eum dolore minima nihil.", new DateTime(2021, 12, 27, 22, 28, 49, 164, DateTimeKind.Unspecified).AddTicks(633), "Incidunt perferendis.", 33, 1, 1 },
                    { 40, new DateTime(2019, 11, 18, 21, 36, 25, 941, DateTimeKind.Unspecified).AddTicks(605), "Error cumque animi tempora.", new DateTime(2021, 7, 9, 20, 39, 53, 449, DateTimeKind.Unspecified).AddTicks(1826), "Repellat quo.", 43, 3, 1 },
                    { 41, new DateTime(2019, 7, 9, 9, 7, 22, 67, DateTimeKind.Unspecified).AddTicks(6459), "Earum itaque quo error tempore saepe porro sed deserunt.", new DateTime(2021, 10, 31, 1, 18, 20, 935, DateTimeKind.Unspecified).AddTicks(4143), "Consequatur et.", 3, 4, 1 },
                    { 42, new DateTime(2019, 2, 27, 6, 14, 58, 239, DateTimeKind.Unspecified).AddTicks(817), "Natus itaque cum.", new DateTime(2021, 7, 30, 16, 41, 57, 711, DateTimeKind.Unspecified).AddTicks(1012), "Sunt minus.", 21, 3, 2 },
                    { 43, new DateTime(2019, 5, 4, 13, 6, 49, 17, DateTimeKind.Unspecified).AddTicks(5587), "Nam dolores laboriosam.", new DateTime(2021, 9, 2, 2, 24, 46, 518, DateTimeKind.Unspecified).AddTicks(7690), "Voluptate at.", 14, 4, 3 },
                    { 44, new DateTime(2019, 5, 5, 18, 14, 21, 143, DateTimeKind.Unspecified).AddTicks(7963), "Sed non porro veniam.", new DateTime(2022, 12, 15, 5, 23, 42, 967, DateTimeKind.Unspecified).AddTicks(3909), "Recusandae eius.", 16, 5, 0 },
                    { 45, new DateTime(2019, 9, 23, 20, 34, 41, 981, DateTimeKind.Unspecified).AddTicks(4542), "Ipsa dolorem qui omnis consequatur dolorem qui.", new DateTime(2021, 1, 10, 11, 17, 11, 291, DateTimeKind.Unspecified).AddTicks(9016), "Ex earum.", 34, 4, 0 },
                    { 46, new DateTime(2019, 4, 21, 0, 27, 10, 642, DateTimeKind.Unspecified).AddTicks(4589), "Ad est officiis.", new DateTime(2021, 3, 9, 13, 22, 15, 825, DateTimeKind.Unspecified).AddTicks(7105), "Nostrum consequatur.", 20, 3, 1 },
                    { 47, new DateTime(2019, 7, 20, 19, 51, 11, 949, DateTimeKind.Unspecified).AddTicks(7632), "Ratione repellendus tenetur voluptatem dolorem reprehenderit voluptas reiciendis eos perspiciatis.", new DateTime(2022, 4, 30, 22, 18, 44, 806, DateTimeKind.Unspecified).AddTicks(678), "Eum molestiae.", 14, 5, 0 },
                    { 48, new DateTime(2019, 4, 1, 1, 9, 58, 690, DateTimeKind.Unspecified).AddTicks(765), "Alias aspernatur porro quia dolores repellendus id sed accusamus nostrum.", new DateTime(2022, 1, 21, 12, 52, 31, 496, DateTimeKind.Unspecified).AddTicks(800), "Accusantium dolor.", 18, 1, 2 },
                    { 49, new DateTime(2019, 1, 5, 21, 17, 50, 433, DateTimeKind.Unspecified).AddTicks(2199), "Voluptas dolor vel reiciendis iure autem molestiae libero id placeat.", new DateTime(2022, 9, 12, 2, 51, 29, 656, DateTimeKind.Unspecified).AddTicks(8126), "Ut ut.", 8, 2, 3 },
                    { 50, new DateTime(2019, 9, 29, 16, 1, 41, 417, DateTimeKind.Unspecified).AddTicks(9186), "Veniam ipsum pariatur optio quia possimus.", new DateTime(2021, 9, 8, 8, 23, 0, 379, DateTimeKind.Unspecified).AddTicks(6299), "Temporibus eligendi.", 11, 1, 3 },
                    { 51, new DateTime(2019, 3, 30, 22, 33, 48, 855, DateTimeKind.Unspecified).AddTicks(1650), "Sit saepe quod quia repellendus ipsam repudiandae corporis minus.", new DateTime(2021, 10, 8, 0, 42, 0, 746, DateTimeKind.Unspecified).AddTicks(2802), "Id eius.", 27, 2, 2 },
                    { 52, new DateTime(2019, 8, 19, 6, 13, 29, 465, DateTimeKind.Unspecified).AddTicks(384), "Modi quo unde ipsum in quo et porro ea quis.", new DateTime(2021, 6, 4, 10, 48, 3, 324, DateTimeKind.Unspecified).AddTicks(5994), "Perspiciatis et.", 15, 3, 2 },
                    { 53, new DateTime(2019, 5, 3, 22, 10, 33, 130, DateTimeKind.Unspecified).AddTicks(4751), "Iure quia magnam amet voluptas.", new DateTime(2021, 2, 15, 10, 42, 0, 396, DateTimeKind.Unspecified).AddTicks(4543), "Qui architecto.", 15, 5, 3 },
                    { 54, new DateTime(2019, 7, 29, 0, 59, 35, 257, DateTimeKind.Unspecified).AddTicks(1854), "Sunt aut facere.", new DateTime(2021, 6, 15, 16, 57, 38, 427, DateTimeKind.Unspecified).AddTicks(6602), "Ducimus assumenda.", 7, 3, 1 },
                    { 55, new DateTime(2019, 8, 31, 12, 0, 11, 899, DateTimeKind.Unspecified).AddTicks(7995), "Et quae quibusdam molestiae est ex debitis occaecati commodi.", new DateTime(2021, 5, 1, 13, 0, 47, 854, DateTimeKind.Unspecified).AddTicks(2904), "Nihil minima.", 25, 3, 3 },
                    { 56, new DateTime(2019, 6, 29, 11, 44, 10, 62, DateTimeKind.Unspecified).AddTicks(4962), "Qui dolorem at suscipit qui ad occaecati.", new DateTime(2022, 4, 22, 12, 55, 49, 844, DateTimeKind.Unspecified).AddTicks(6693), "Debitis id.", 24, 5, 3 },
                    { 57, new DateTime(2019, 7, 19, 13, 34, 40, 423, DateTimeKind.Unspecified).AddTicks(2983), "Voluptatibus fugit autem sit aliquid explicabo neque quia incidunt eos.", new DateTime(2022, 3, 24, 11, 23, 29, 212, DateTimeKind.Unspecified).AddTicks(6735), "Quaerat quo.", 23, 1, 3 },
                    { 58, new DateTime(2019, 5, 28, 3, 41, 29, 529, DateTimeKind.Unspecified).AddTicks(1487), "Aut et repellat voluptatibus.", new DateTime(2022, 1, 2, 9, 58, 53, 546, DateTimeKind.Unspecified).AddTicks(2221), "Quod totam.", 27, 3, 0 },
                    { 59, new DateTime(2019, 2, 9, 10, 47, 59, 407, DateTimeKind.Unspecified).AddTicks(595), "Neque qui est necessitatibus in sed corporis ratione.", new DateTime(2022, 7, 16, 11, 56, 46, 281, DateTimeKind.Unspecified).AddTicks(3121), "Excepturi quod.", 34, 1, 3 },
                    { 60, new DateTime(2019, 7, 5, 17, 26, 4, 801, DateTimeKind.Unspecified).AddTicks(2698), "Occaecati vero voluptas id eligendi soluta iure eum aut.", new DateTime(2021, 3, 10, 14, 0, 19, 424, DateTimeKind.Unspecified).AddTicks(5666), "Officia nostrum.", 21, 3, 2 },
                    { 61, new DateTime(2019, 2, 3, 0, 10, 55, 957, DateTimeKind.Unspecified).AddTicks(5037), "Esse qui laudantium illum dolorem porro corrupti.", new DateTime(2021, 12, 26, 23, 9, 13, 918, DateTimeKind.Unspecified).AddTicks(7563), "Enim sequi.", 16, 3, 2 },
                    { 62, new DateTime(2019, 3, 5, 22, 58, 0, 886, DateTimeKind.Unspecified).AddTicks(2531), "Atque inventore unde non non sint sed aut reprehenderit.", new DateTime(2022, 7, 21, 20, 9, 38, 961, DateTimeKind.Unspecified).AddTicks(2357), "Cupiditate ipsam.", 6, 5, 0 },
                    { 63, new DateTime(2019, 3, 30, 10, 17, 44, 126, DateTimeKind.Unspecified).AddTicks(5782), "Itaque voluptas excepturi ullam adipisci sunt rerum aliquam temporibus.", new DateTime(2022, 1, 20, 19, 17, 8, 650, DateTimeKind.Unspecified).AddTicks(8903), "Qui adipisci.", 46, 4, 0 },
                    { 64, new DateTime(2019, 8, 6, 0, 49, 17, 995, DateTimeKind.Unspecified).AddTicks(3249), "Ad reprehenderit modi ullam.", new DateTime(2022, 3, 14, 17, 5, 41, 443, DateTimeKind.Unspecified).AddTicks(2480), "Fuga optio.", 6, 1, 3 },
                    { 65, new DateTime(2019, 3, 27, 19, 46, 21, 631, DateTimeKind.Unspecified).AddTicks(8619), "Deserunt tempore et vel ullam qui.", new DateTime(2021, 12, 14, 20, 25, 35, 492, DateTimeKind.Unspecified).AddTicks(9), "Sit minima.", 14, 2, 3 },
                    { 66, new DateTime(2019, 5, 26, 6, 38, 53, 687, DateTimeKind.Unspecified).AddTicks(6443), "Aut et maxime perspiciatis.", new DateTime(2022, 1, 10, 8, 55, 11, 932, DateTimeKind.Unspecified).AddTicks(62), "Voluptatem numquam.", 9, 5, 3 },
                    { 67, new DateTime(2019, 11, 4, 17, 32, 52, 144, DateTimeKind.Unspecified).AddTicks(155), "Ipsam vel sed ipsum omnis ea sequi minus perspiciatis.", new DateTime(2022, 6, 23, 17, 58, 7, 993, DateTimeKind.Unspecified).AddTicks(9936), "Maiores eos.", 28, 5, 2 },
                    { 68, new DateTime(2019, 9, 16, 20, 50, 15, 348, DateTimeKind.Unspecified).AddTicks(3983), "Enim eius fuga eos animi eos voluptas quasi.", new DateTime(2021, 8, 9, 16, 33, 9, 213, DateTimeKind.Unspecified).AddTicks(4283), "Animi aperiam.", 12, 4, 3 },
                    { 69, new DateTime(2019, 3, 14, 2, 23, 36, 787, DateTimeKind.Unspecified).AddTicks(5477), "Totam est unde omnis dolore.", new DateTime(2022, 5, 12, 17, 6, 58, 884, DateTimeKind.Unspecified).AddTicks(327), "Asperiores voluptatibus.", 6, 5, 0 },
                    { 70, new DateTime(2019, 3, 9, 11, 3, 42, 474, DateTimeKind.Unspecified).AddTicks(6191), "Exercitationem et voluptas.", new DateTime(2021, 11, 26, 22, 44, 22, 536, DateTimeKind.Unspecified).AddTicks(4615), "Alias sit.", 20, 1, 1 },
                    { 71, new DateTime(2019, 12, 17, 1, 34, 56, 875, DateTimeKind.Unspecified).AddTicks(3741), "Voluptatem alias sit corporis explicabo ex incidunt voluptatem.", new DateTime(2021, 10, 21, 23, 58, 25, 753, DateTimeKind.Unspecified).AddTicks(972), "Laboriosam facere.", 8, 3, 2 },
                    { 72, new DateTime(2019, 11, 21, 11, 45, 51, 500, DateTimeKind.Unspecified).AddTicks(5643), "Minus quis vitae minima expedita rem molestias.", new DateTime(2021, 7, 27, 3, 13, 58, 646, DateTimeKind.Unspecified).AddTicks(3936), "Itaque in.", 28, 2, 1 },
                    { 73, new DateTime(2019, 6, 7, 3, 50, 35, 918, DateTimeKind.Unspecified).AddTicks(2695), "Ducimus et assumenda dolore beatae expedita fuga et.", new DateTime(2021, 4, 21, 12, 0, 59, 662, DateTimeKind.Unspecified).AddTicks(8086), "Et eos.", 13, 1, 1 },
                    { 74, new DateTime(2019, 5, 26, 17, 30, 51, 163, DateTimeKind.Unspecified).AddTicks(7073), "Reprehenderit quis eveniet placeat a laborum.", new DateTime(2022, 11, 1, 17, 50, 9, 392, DateTimeKind.Unspecified).AddTicks(2666), "Non non.", 49, 4, 1 },
                    { 75, new DateTime(2019, 1, 12, 7, 7, 42, 941, DateTimeKind.Unspecified).AddTicks(8688), "Quas quibusdam fuga sapiente sequi officiis.", new DateTime(2021, 9, 22, 16, 22, 33, 477, DateTimeKind.Unspecified).AddTicks(8117), "Ullam magni.", 18, 4, 2 },
                    { 76, new DateTime(2019, 5, 15, 15, 7, 48, 863, DateTimeKind.Unspecified).AddTicks(1723), "Deleniti quia quia deserunt repellat vero quis consequatur voluptas.", new DateTime(2021, 9, 18, 14, 18, 52, 938, DateTimeKind.Unspecified).AddTicks(1378), "Suscipit et.", 29, 5, 0 },
                    { 77, new DateTime(2019, 2, 26, 17, 3, 40, 775, DateTimeKind.Unspecified).AddTicks(9853), "Maiores veritatis placeat ut consequatur.", new DateTime(2022, 9, 21, 23, 7, 54, 609, DateTimeKind.Unspecified).AddTicks(8472), "Dignissimos itaque.", 4, 5, 0 },
                    { 78, new DateTime(2019, 3, 16, 6, 33, 13, 181, DateTimeKind.Unspecified).AddTicks(8157), "Ullam ut ex quos nam harum officia.", new DateTime(2021, 6, 22, 17, 48, 51, 856, DateTimeKind.Unspecified).AddTicks(8068), "Voluptatem molestiae.", 37, 3, 3 },
                    { 79, new DateTime(2019, 7, 6, 6, 44, 9, 473, DateTimeKind.Unspecified).AddTicks(977), "Dolorem non velit harum.", new DateTime(2022, 4, 6, 0, 2, 17, 494, DateTimeKind.Unspecified).AddTicks(8487), "Provident sed.", 8, 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 80, new DateTime(2019, 11, 3, 1, 37, 17, 100, DateTimeKind.Unspecified).AddTicks(5181), "Qui laudantium velit consequuntur.", new DateTime(2022, 3, 23, 2, 17, 52, 248, DateTimeKind.Unspecified).AddTicks(6255), "Et quia.", 17, 3, 1 },
                    { 81, new DateTime(2019, 5, 26, 9, 2, 37, 611, DateTimeKind.Unspecified).AddTicks(9145), "Quo quisquam amet inventore temporibus minus voluptas reiciendis repellat et.", new DateTime(2022, 11, 24, 18, 7, 38, 931, DateTimeKind.Unspecified).AddTicks(2281), "Nulla delectus.", 24, 5, 1 },
                    { 82, new DateTime(2019, 1, 23, 8, 37, 32, 473, DateTimeKind.Unspecified).AddTicks(1491), "Eum quidem et.", new DateTime(2021, 11, 6, 2, 21, 26, 194, DateTimeKind.Unspecified).AddTicks(2800), "Eligendi fugiat.", 4, 4, 1 },
                    { 83, new DateTime(2019, 8, 29, 2, 49, 17, 456, DateTimeKind.Unspecified).AddTicks(3955), "Nam sunt vel perferendis consectetur voluptates ut sunt.", new DateTime(2021, 3, 25, 20, 22, 21, 814, DateTimeKind.Unspecified).AddTicks(3618), "Dolorem earum.", 41, 4, 0 },
                    { 84, new DateTime(2019, 1, 5, 12, 36, 35, 307, DateTimeKind.Unspecified).AddTicks(2944), "Quia autem doloremque.", new DateTime(2022, 4, 18, 15, 17, 46, 91, DateTimeKind.Unspecified).AddTicks(7599), "Rerum asperiores.", 30, 5, 1 },
                    { 85, new DateTime(2019, 8, 26, 3, 23, 14, 685, DateTimeKind.Unspecified).AddTicks(4086), "Aut deleniti velit et occaecati culpa voluptate.", new DateTime(2022, 7, 16, 1, 3, 59, 202, DateTimeKind.Unspecified).AddTicks(2646), "Sed necessitatibus.", 29, 4, 1 },
                    { 86, new DateTime(2019, 7, 7, 17, 6, 25, 354, DateTimeKind.Unspecified).AddTicks(4752), "Aperiam officiis dolor laboriosam error maxime.", new DateTime(2022, 2, 9, 11, 15, 0, 946, DateTimeKind.Unspecified).AddTicks(2737), "Iusto ad.", 13, 5, 0 },
                    { 87, new DateTime(2019, 2, 1, 20, 58, 47, 468, DateTimeKind.Unspecified).AddTicks(73), "Doloribus qui ut aut iste.", new DateTime(2022, 12, 31, 18, 20, 11, 806, DateTimeKind.Unspecified).AddTicks(9085), "Et dolor.", 20, 4, 1 },
                    { 88, new DateTime(2019, 5, 19, 4, 50, 17, 808, DateTimeKind.Unspecified).AddTicks(6032), "Autem eos quia quisquam sint at consectetur quia tenetur.", new DateTime(2021, 12, 15, 3, 30, 11, 484, DateTimeKind.Unspecified).AddTicks(9717), "Similique sit.", 1, 4, 3 },
                    { 89, new DateTime(2019, 6, 10, 9, 50, 6, 961, DateTimeKind.Unspecified).AddTicks(1841), "Laborum architecto aut delectus quidem sit omnis quia.", new DateTime(2022, 12, 23, 20, 46, 1, 553, DateTimeKind.Unspecified).AddTicks(2154), "Odit doloremque.", 34, 5, 0 },
                    { 90, new DateTime(2019, 12, 7, 21, 45, 35, 765, DateTimeKind.Unspecified).AddTicks(3972), "Eligendi illum asperiores veniam assumenda.", new DateTime(2021, 9, 9, 16, 26, 14, 746, DateTimeKind.Unspecified).AddTicks(5001), "Corrupti modi.", 46, 2, 1 },
                    { 91, new DateTime(2019, 2, 22, 22, 41, 37, 367, DateTimeKind.Unspecified).AddTicks(3896), "Et dolorum dolor voluptatem debitis ullam maxime ipsam.", new DateTime(2022, 1, 5, 22, 50, 54, 926, DateTimeKind.Unspecified).AddTicks(8867), "Atque magni.", 21, 5, 1 },
                    { 92, new DateTime(2019, 6, 23, 20, 34, 11, 572, DateTimeKind.Unspecified).AddTicks(9210), "Blanditiis totam et delectus consectetur voluptatum repudiandae.", new DateTime(2022, 5, 19, 1, 53, 18, 97, DateTimeKind.Unspecified).AddTicks(5676), "Cumque quas.", 35, 4, 2 },
                    { 93, new DateTime(2019, 6, 15, 7, 35, 17, 792, DateTimeKind.Unspecified).AddTicks(2326), "Voluptatem et possimus voluptas earum ad voluptas.", new DateTime(2022, 4, 20, 13, 10, 28, 229, DateTimeKind.Unspecified).AddTicks(3035), "Ullam aut.", 42, 2, 1 },
                    { 94, new DateTime(2019, 3, 27, 1, 1, 49, 360, DateTimeKind.Unspecified).AddTicks(3585), "Nulla rerum repudiandae.", new DateTime(2021, 4, 14, 5, 10, 11, 456, DateTimeKind.Unspecified).AddTicks(2441), "Saepe blanditiis.", 25, 2, 1 },
                    { 95, new DateTime(2019, 8, 12, 12, 41, 52, 477, DateTimeKind.Unspecified).AddTicks(4967), "Molestiae qui eos nam praesentium.", new DateTime(2022, 5, 24, 4, 9, 47, 614, DateTimeKind.Unspecified).AddTicks(3730), "Vero quo.", 1, 4, 0 },
                    { 96, new DateTime(2019, 5, 23, 21, 27, 47, 713, DateTimeKind.Unspecified).AddTicks(9790), "Amet dolorem porro esse similique qui.", new DateTime(2022, 7, 25, 15, 48, 30, 955, DateTimeKind.Unspecified).AddTicks(1682), "Commodi deleniti.", 50, 3, 1 },
                    { 97, new DateTime(2019, 9, 24, 12, 46, 6, 576, DateTimeKind.Unspecified).AddTicks(3289), "Non sed sit quae necessitatibus dolorem.", new DateTime(2021, 2, 20, 12, 39, 28, 868, DateTimeKind.Unspecified).AddTicks(9579), "Sit omnis.", 10, 5, 3 },
                    { 98, new DateTime(2019, 7, 29, 1, 48, 53, 262, DateTimeKind.Unspecified).AddTicks(5731), "Sit eveniet voluptas unde est enim.", new DateTime(2022, 2, 17, 9, 15, 17, 380, DateTimeKind.Unspecified).AddTicks(8787), "Nihil qui.", 10, 3, 3 },
                    { 99, new DateTime(2019, 11, 1, 15, 44, 40, 323, DateTimeKind.Unspecified).AddTicks(674), "Totam quas qui qui.", new DateTime(2022, 6, 12, 15, 33, 49, 700, DateTimeKind.Unspecified).AddTicks(9518), "Quis non.", 40, 3, 2 },
                    { 100, new DateTime(2019, 6, 10, 4, 57, 28, 590, DateTimeKind.Unspecified).AddTicks(4354), "Et quibusdam placeat tempore illum cum.", new DateTime(2021, 4, 3, 16, 40, 53, 232, DateTimeKind.Unspecified).AddTicks(9231), "Perspiciatis tenetur.", 48, 2, 2 },
                    { 101, new DateTime(2019, 9, 30, 16, 25, 11, 998, DateTimeKind.Unspecified).AddTicks(4172), "Laudantium dolorem fugiat perspiciatis quasi est deleniti est veritatis velit.", new DateTime(2021, 5, 8, 17, 15, 17, 941, DateTimeKind.Unspecified).AddTicks(1356), "Laboriosam doloribus.", 13, 1, 3 },
                    { 102, new DateTime(2019, 3, 30, 12, 16, 59, 163, DateTimeKind.Unspecified).AddTicks(3192), "Provident sit quas dolorum vitae nesciunt ea.", new DateTime(2022, 11, 6, 9, 51, 4, 697, DateTimeKind.Unspecified).AddTicks(8276), "Minima itaque.", 27, 1, 3 },
                    { 103, new DateTime(2019, 9, 8, 4, 20, 15, 954, DateTimeKind.Unspecified).AddTicks(1211), "Omnis qui consequatur porro quas dolorem iste repudiandae nostrum.", new DateTime(2022, 12, 17, 15, 21, 48, 327, DateTimeKind.Unspecified).AddTicks(7943), "Voluptatem earum.", 9, 3, 2 },
                    { 104, new DateTime(2019, 11, 6, 2, 43, 6, 144, DateTimeKind.Unspecified).AddTicks(152), "Temporibus itaque ut est laboriosam.", new DateTime(2022, 10, 3, 15, 34, 44, 669, DateTimeKind.Unspecified).AddTicks(2460), "Sint aut.", 42, 1, 3 },
                    { 105, new DateTime(2019, 4, 13, 19, 18, 59, 648, DateTimeKind.Unspecified).AddTicks(6126), "Officiis excepturi numquam quaerat ipsam.", new DateTime(2022, 11, 22, 13, 25, 57, 379, DateTimeKind.Unspecified).AddTicks(4188), "At odit.", 46, 5, 1 },
                    { 106, new DateTime(2019, 6, 26, 23, 26, 6, 334, DateTimeKind.Unspecified).AddTicks(8504), "Et est sed aliquid ut culpa molestias.", new DateTime(2022, 10, 12, 14, 42, 17, 598, DateTimeKind.Unspecified).AddTicks(6132), "Et et.", 27, 3, 1 },
                    { 107, new DateTime(2019, 10, 22, 15, 23, 6, 13, DateTimeKind.Unspecified).AddTicks(3931), "Explicabo molestiae assumenda sint laborum reiciendis odio beatae.", new DateTime(2021, 12, 9, 19, 15, 49, 955, DateTimeKind.Unspecified).AddTicks(3371), "Nihil et.", 19, 2, 0 },
                    { 108, new DateTime(2019, 6, 25, 18, 57, 52, 693, DateTimeKind.Unspecified).AddTicks(6469), "Magnam repudiandae fugit aliquid.", new DateTime(2021, 2, 1, 1, 38, 18, 358, DateTimeKind.Unspecified).AddTicks(1571), "Illum qui.", 33, 4, 2 },
                    { 109, new DateTime(2019, 6, 15, 7, 1, 36, 174, DateTimeKind.Unspecified).AddTicks(8933), "Cum sed aut pariatur sed repellat et atque iusto.", new DateTime(2021, 12, 24, 13, 49, 24, 596, DateTimeKind.Unspecified).AddTicks(6539), "Totam quia.", 16, 3, 3 },
                    { 110, new DateTime(2019, 4, 23, 23, 16, 26, 903, DateTimeKind.Unspecified).AddTicks(2917), "Asperiores sit reprehenderit quos architecto est vero molestiae eos.", new DateTime(2021, 10, 23, 23, 1, 24, 705, DateTimeKind.Unspecified).AddTicks(4616), "In veritatis.", 29, 1, 2 },
                    { 111, new DateTime(2019, 11, 4, 20, 30, 26, 786, DateTimeKind.Unspecified).AddTicks(740), "Molestiae ut ea nisi.", new DateTime(2021, 2, 22, 3, 40, 33, 592, DateTimeKind.Unspecified).AddTicks(9058), "Beatae voluptatem.", 47, 3, 1 },
                    { 112, new DateTime(2019, 2, 2, 14, 7, 28, 299, DateTimeKind.Unspecified).AddTicks(4690), "Aliquid hic quis optio odio sed et aut fugiat.", new DateTime(2021, 9, 14, 21, 57, 23, 721, DateTimeKind.Unspecified).AddTicks(9478), "Ea perferendis.", 4, 2, 1 },
                    { 113, new DateTime(2019, 2, 4, 11, 24, 42, 281, DateTimeKind.Unspecified).AddTicks(3904), "Consectetur libero omnis enim tempore qui aliquid ab.", new DateTime(2021, 2, 13, 5, 31, 19, 973, DateTimeKind.Unspecified).AddTicks(1195), "Asperiores ullam.", 30, 3, 3 },
                    { 114, new DateTime(2019, 10, 25, 21, 56, 23, 645, DateTimeKind.Unspecified).AddTicks(8077), "Neque et tempora molestiae rerum aut occaecati sunt.", new DateTime(2021, 12, 23, 4, 17, 35, 249, DateTimeKind.Unspecified).AddTicks(6725), "Adipisci atque.", 34, 1, 0 },
                    { 115, new DateTime(2019, 7, 9, 13, 41, 5, 949, DateTimeKind.Unspecified).AddTicks(9742), "Molestias fugit omnis alias.", new DateTime(2021, 12, 6, 13, 8, 36, 538, DateTimeKind.Unspecified).AddTicks(8537), "Quia rerum.", 26, 1, 2 },
                    { 116, new DateTime(2019, 5, 6, 19, 59, 13, 194, DateTimeKind.Unspecified).AddTicks(9546), "Ut quia consequatur et hic optio rerum.", new DateTime(2021, 10, 3, 11, 58, 10, 204, DateTimeKind.Unspecified).AddTicks(7872), "Laborum iusto.", 48, 5, 0 },
                    { 117, new DateTime(2019, 3, 20, 9, 39, 22, 523, DateTimeKind.Unspecified).AddTicks(210), "Ut vel dolorem reprehenderit porro ipsa dolorem eligendi.", new DateTime(2021, 7, 30, 1, 50, 29, 308, DateTimeKind.Unspecified).AddTicks(7077), "Eos aspernatur.", 38, 1, 3 },
                    { 118, new DateTime(2019, 1, 30, 4, 15, 42, 297, DateTimeKind.Unspecified).AddTicks(5305), "Quo dolor sed ex.", new DateTime(2022, 12, 31, 14, 10, 44, 145, DateTimeKind.Unspecified).AddTicks(8606), "Velit enim.", 41, 1, 0 },
                    { 119, new DateTime(2019, 5, 14, 23, 51, 10, 540, DateTimeKind.Unspecified).AddTicks(9736), "Aut reprehenderit consectetur eligendi aliquam.", new DateTime(2022, 11, 13, 15, 13, 52, 221, DateTimeKind.Unspecified).AddTicks(7915), "Itaque est.", 36, 2, 2 },
                    { 120, new DateTime(2019, 5, 20, 3, 59, 16, 623, DateTimeKind.Unspecified).AddTicks(3206), "Amet odit architecto alias doloremque voluptates officia et saepe et.", new DateTime(2021, 10, 22, 2, 4, 36, 490, DateTimeKind.Unspecified).AddTicks(617), "Molestiae quia.", 23, 5, 1 },
                    { 121, new DateTime(2019, 10, 23, 14, 22, 1, 117, DateTimeKind.Unspecified).AddTicks(993), "Omnis harum cumque.", new DateTime(2022, 3, 11, 18, 53, 58, 612, DateTimeKind.Unspecified).AddTicks(8034), "Itaque at.", 23, 5, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 122, new DateTime(2019, 2, 11, 19, 23, 15, 522, DateTimeKind.Unspecified).AddTicks(9805), "Ut sit sunt et ab.", new DateTime(2022, 8, 31, 15, 6, 3, 518, DateTimeKind.Unspecified).AddTicks(2344), "Sit aut.", 49, 1, 3 },
                    { 123, new DateTime(2019, 8, 31, 3, 17, 7, 992, DateTimeKind.Unspecified).AddTicks(3079), "Exercitationem molestiae odio eum sequi.", new DateTime(2022, 11, 12, 5, 0, 32, 741, DateTimeKind.Unspecified).AddTicks(6573), "Eius dolorum.", 36, 4, 2 },
                    { 124, new DateTime(2019, 4, 29, 2, 40, 12, 332, DateTimeKind.Unspecified).AddTicks(939), "Tenetur saepe placeat voluptatem debitis facilis.", new DateTime(2022, 9, 24, 5, 56, 49, 559, DateTimeKind.Unspecified).AddTicks(5613), "Deserunt quod.", 24, 1, 1 },
                    { 125, new DateTime(2019, 3, 15, 17, 17, 58, 645, DateTimeKind.Unspecified).AddTicks(8600), "Sed enim dolores veniam quis vitae porro sapiente.", new DateTime(2021, 5, 5, 2, 39, 56, 853, DateTimeKind.Unspecified).AddTicks(5516), "Possimus et.", 42, 1, 0 },
                    { 126, new DateTime(2019, 12, 14, 10, 45, 35, 237, DateTimeKind.Unspecified).AddTicks(2156), "Quas rerum voluptas dolor delectus vitae atque.", new DateTime(2022, 9, 29, 14, 55, 57, 346, DateTimeKind.Unspecified).AddTicks(9710), "Eius facilis.", 41, 5, 2 },
                    { 127, new DateTime(2019, 9, 14, 1, 10, 49, 74, DateTimeKind.Unspecified).AddTicks(5083), "Necessitatibus quam hic aut sit aut odit.", new DateTime(2021, 12, 25, 15, 5, 45, 991, DateTimeKind.Unspecified).AddTicks(4316), "Blanditiis omnis.", 10, 3, 1 },
                    { 128, new DateTime(2019, 2, 6, 3, 29, 38, 2, DateTimeKind.Unspecified).AddTicks(6188), "Voluptas reiciendis autem illum qui ut dicta sunt illum nihil.", new DateTime(2021, 7, 23, 15, 36, 39, 553, DateTimeKind.Unspecified).AddTicks(198), "Temporibus qui.", 25, 1, 3 },
                    { 129, new DateTime(2019, 6, 4, 0, 7, 37, 109, DateTimeKind.Unspecified).AddTicks(1352), "Expedita molestiae placeat.", new DateTime(2022, 10, 18, 9, 29, 5, 883, DateTimeKind.Unspecified).AddTicks(9260), "Eum voluptatem.", 39, 2, 1 },
                    { 130, new DateTime(2019, 9, 12, 21, 59, 39, 35, DateTimeKind.Unspecified).AddTicks(8189), "Sint vel possimus quis possimus ipsum.", new DateTime(2022, 7, 11, 3, 40, 45, 649, DateTimeKind.Unspecified).AddTicks(5998), "Saepe est.", 44, 2, 0 },
                    { 131, new DateTime(2019, 4, 25, 17, 0, 18, 997, DateTimeKind.Unspecified).AddTicks(3435), "Sit mollitia est.", new DateTime(2021, 12, 18, 15, 49, 32, 41, DateTimeKind.Unspecified).AddTicks(3680), "Sit modi.", 19, 1, 3 },
                    { 132, new DateTime(2019, 12, 9, 11, 31, 22, 359, DateTimeKind.Unspecified).AddTicks(2054), "Quo qui voluptatem ex adipisci.", new DateTime(2021, 3, 21, 17, 18, 35, 887, DateTimeKind.Unspecified).AddTicks(7149), "In laudantium.", 44, 4, 3 },
                    { 133, new DateTime(2019, 5, 30, 15, 12, 54, 724, DateTimeKind.Unspecified).AddTicks(3687), "Culpa nulla earum sapiente vel dolores est earum veniam quas.", new DateTime(2021, 10, 15, 13, 21, 30, 298, DateTimeKind.Unspecified).AddTicks(2111), "Omnis ut.", 12, 3, 2 },
                    { 134, new DateTime(2019, 7, 7, 16, 19, 12, 819, DateTimeKind.Unspecified).AddTicks(6195), "Labore recusandae aut.", new DateTime(2021, 8, 10, 4, 28, 58, 53, DateTimeKind.Unspecified).AddTicks(7342), "Quo non.", 48, 2, 2 },
                    { 135, new DateTime(2019, 7, 24, 6, 56, 53, 576, DateTimeKind.Unspecified).AddTicks(4532), "Quia non id voluptatem totam.", new DateTime(2022, 11, 19, 19, 44, 42, 713, DateTimeKind.Unspecified).AddTicks(1719), "Accusamus suscipit.", 19, 4, 0 },
                    { 136, new DateTime(2019, 10, 31, 14, 31, 34, 87, DateTimeKind.Unspecified).AddTicks(9988), "Nobis asperiores enim deserunt commodi nulla et voluptatem harum.", new DateTime(2021, 8, 3, 14, 50, 29, 179, DateTimeKind.Unspecified).AddTicks(665), "Veritatis eos.", 50, 3, 0 },
                    { 137, new DateTime(2019, 8, 2, 11, 18, 14, 932, DateTimeKind.Unspecified).AddTicks(4102), "Quaerat delectus ab est.", new DateTime(2022, 12, 28, 1, 0, 40, 501, DateTimeKind.Unspecified).AddTicks(4496), "Est harum.", 10, 3, 3 },
                    { 138, new DateTime(2019, 5, 4, 11, 7, 9, 330, DateTimeKind.Unspecified).AddTicks(713), "A id amet.", new DateTime(2021, 10, 28, 4, 44, 49, 173, DateTimeKind.Unspecified).AddTicks(9508), "Temporibus ut.", 2, 3, 1 },
                    { 139, new DateTime(2019, 1, 10, 23, 3, 58, 273, DateTimeKind.Unspecified).AddTicks(9753), "Similique unde vero itaque sit.", new DateTime(2022, 6, 6, 4, 28, 40, 71, DateTimeKind.Unspecified).AddTicks(8942), "Vel eligendi.", 19, 3, 2 },
                    { 140, new DateTime(2019, 9, 25, 7, 34, 22, 622, DateTimeKind.Unspecified).AddTicks(5501), "Numquam at soluta.", new DateTime(2022, 7, 11, 4, 37, 24, 99, DateTimeKind.Unspecified).AddTicks(1809), "Dolores ut.", 15, 4, 3 },
                    { 141, new DateTime(2019, 7, 13, 20, 16, 4, 191, DateTimeKind.Unspecified).AddTicks(40), "Aut illum ipsum et repellat nesciunt autem in harum sit.", new DateTime(2022, 1, 24, 7, 30, 7, 41, DateTimeKind.Unspecified).AddTicks(5765), "Quia laborum.", 13, 4, 3 },
                    { 142, new DateTime(2019, 11, 20, 23, 1, 16, 562, DateTimeKind.Unspecified).AddTicks(8943), "Excepturi laudantium placeat occaecati eveniet ex sint dolore temporibus est.", new DateTime(2021, 4, 29, 0, 0, 0, 918, DateTimeKind.Unspecified).AddTicks(1823), "Qui quia.", 20, 5, 3 },
                    { 143, new DateTime(2019, 2, 24, 9, 7, 30, 626, DateTimeKind.Unspecified).AddTicks(5878), "Cupiditate culpa placeat dignissimos magni dolorem repudiandae nihil sit.", new DateTime(2022, 9, 23, 0, 10, 11, 690, DateTimeKind.Unspecified).AddTicks(3943), "Non corporis.", 9, 4, 3 },
                    { 144, new DateTime(2019, 3, 25, 12, 50, 38, 38, DateTimeKind.Unspecified).AddTicks(1091), "Corporis et nesciunt qui rem.", new DateTime(2022, 3, 15, 13, 6, 29, 913, DateTimeKind.Unspecified).AddTicks(98), "Iste sunt.", 23, 1, 2 },
                    { 145, new DateTime(2019, 2, 20, 22, 27, 43, 188, DateTimeKind.Unspecified).AddTicks(8961), "A sapiente velit.", new DateTime(2022, 4, 6, 8, 21, 52, 522, DateTimeKind.Unspecified).AddTicks(9694), "Facilis alias.", 13, 4, 3 },
                    { 146, new DateTime(2019, 7, 10, 10, 59, 38, 583, DateTimeKind.Unspecified).AddTicks(7146), "Et sed soluta.", new DateTime(2022, 5, 1, 20, 14, 21, 143, DateTimeKind.Unspecified).AddTicks(3053), "Ipsa aut.", 22, 3, 0 },
                    { 147, new DateTime(2019, 3, 29, 2, 18, 42, 825, DateTimeKind.Unspecified).AddTicks(1930), "Omnis aut id in voluptates rem ex tempora sit soluta.", new DateTime(2021, 9, 11, 12, 0, 17, 318, DateTimeKind.Unspecified).AddTicks(8696), "Omnis ea.", 39, 1, 3 },
                    { 148, new DateTime(2019, 2, 26, 10, 33, 25, 29, DateTimeKind.Unspecified).AddTicks(830), "A nobis quia voluptates.", new DateTime(2021, 6, 28, 21, 40, 53, 756, DateTimeKind.Unspecified).AddTicks(5337), "Dolorem ipsa.", 16, 3, 1 },
                    { 149, new DateTime(2019, 7, 25, 13, 34, 21, 915, DateTimeKind.Unspecified).AddTicks(3230), "Assumenda suscipit enim.", new DateTime(2022, 3, 19, 16, 12, 44, 854, DateTimeKind.Unspecified).AddTicks(2532), "Ratione consectetur.", 2, 2, 1 },
                    { 150, new DateTime(2019, 11, 27, 6, 15, 59, 196, DateTimeKind.Unspecified).AddTicks(1627), "Est quis quo.", new DateTime(2021, 9, 29, 3, 45, 11, 726, DateTimeKind.Unspecified).AddTicks(6158), "Quae corrupti.", 31, 5, 2 },
                    { 151, new DateTime(2019, 12, 24, 20, 36, 25, 239, DateTimeKind.Unspecified).AddTicks(1883), "Voluptates non amet consequuntur quod et tempora et nisi.", new DateTime(2022, 11, 25, 3, 23, 44, 577, DateTimeKind.Unspecified).AddTicks(9994), "Beatae et.", 15, 2, 2 },
                    { 152, new DateTime(2019, 8, 15, 2, 8, 31, 851, DateTimeKind.Unspecified).AddTicks(2351), "Sed qui exercitationem suscipit quibusdam et optio eveniet quasi.", new DateTime(2021, 2, 20, 22, 41, 21, 923, DateTimeKind.Unspecified).AddTicks(826), "Accusamus assumenda.", 36, 1, 3 },
                    { 153, new DateTime(2019, 6, 5, 19, 57, 0, 202, DateTimeKind.Unspecified).AddTicks(4651), "Soluta sunt aut quidem in tempora deserunt.", new DateTime(2021, 8, 27, 18, 30, 58, 287, DateTimeKind.Unspecified).AddTicks(2021), "Ut et.", 39, 5, 3 },
                    { 154, new DateTime(2019, 1, 7, 23, 43, 22, 259, DateTimeKind.Unspecified).AddTicks(1363), "Eligendi accusantium ex tenetur iusto numquam necessitatibus.", new DateTime(2021, 7, 6, 10, 45, 3, 436, DateTimeKind.Unspecified).AddTicks(637), "Est quidem.", 48, 4, 1 },
                    { 155, new DateTime(2019, 10, 30, 16, 49, 58, 323, DateTimeKind.Unspecified).AddTicks(9487), "Dolores sunt debitis reprehenderit quam provident non possimus.", new DateTime(2022, 8, 25, 1, 46, 21, 708, DateTimeKind.Unspecified).AddTicks(8794), "Numquam ut.", 20, 4, 3 },
                    { 156, new DateTime(2019, 3, 27, 0, 42, 12, 106, DateTimeKind.Unspecified).AddTicks(6103), "Velit architecto repudiandae.", new DateTime(2021, 5, 13, 11, 11, 4, 271, DateTimeKind.Unspecified).AddTicks(554), "Iure recusandae.", 20, 4, 1 },
                    { 157, new DateTime(2019, 3, 1, 23, 4, 4, 682, DateTimeKind.Unspecified).AddTicks(1869), "Et dolores vel.", new DateTime(2021, 4, 28, 16, 2, 48, 989, DateTimeKind.Unspecified).AddTicks(5152), "Aut consectetur.", 41, 4, 0 },
                    { 158, new DateTime(2019, 11, 20, 5, 8, 15, 698, DateTimeKind.Unspecified).AddTicks(3006), "Qui molestiae ipsum voluptatem mollitia voluptatem suscipit labore voluptates.", new DateTime(2021, 6, 22, 11, 25, 54, 571, DateTimeKind.Unspecified).AddTicks(8780), "Reiciendis corporis.", 40, 3, 1 },
                    { 159, new DateTime(2019, 8, 6, 12, 38, 57, 830, DateTimeKind.Unspecified).AddTicks(9740), "Dolores tempore est qui qui illo vel tempora.", new DateTime(2022, 9, 22, 0, 10, 33, 964, DateTimeKind.Unspecified).AddTicks(6240), "Est vitae.", 34, 4, 1 },
                    { 160, new DateTime(2019, 11, 6, 11, 15, 52, 240, DateTimeKind.Unspecified).AddTicks(165), "Dolorem aut et ut molestias incidunt numquam et.", new DateTime(2022, 4, 15, 19, 39, 51, 676, DateTimeKind.Unspecified).AddTicks(9015), "Sapiente et.", 7, 4, 1 },
                    { 161, new DateTime(2019, 10, 3, 9, 56, 33, 734, DateTimeKind.Unspecified).AddTicks(610), "Facere culpa voluptatem et non odio quis iure.", new DateTime(2021, 11, 30, 7, 30, 13, 578, DateTimeKind.Unspecified).AddTicks(8506), "Distinctio ipsa.", 33, 5, 2 },
                    { 162, new DateTime(2019, 9, 13, 20, 22, 37, 639, DateTimeKind.Unspecified).AddTicks(318), "Sequi quo voluptates suscipit nobis qui quaerat maiores velit.", new DateTime(2021, 8, 15, 7, 3, 13, 512, DateTimeKind.Unspecified).AddTicks(4707), "Enim voluptas.", 43, 3, 3 },
                    { 163, new DateTime(2019, 9, 23, 5, 32, 7, 754, DateTimeKind.Unspecified).AddTicks(7072), "Consectetur repellat veritatis aut ab eaque enim eum.", new DateTime(2022, 4, 26, 8, 10, 4, 496, DateTimeKind.Unspecified).AddTicks(5115), "Deleniti nulla.", 3, 4, 2 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 164, new DateTime(2019, 9, 1, 22, 13, 21, 343, DateTimeKind.Unspecified).AddTicks(1890), "Suscipit fuga ipsum qui cumque laborum quia similique.", new DateTime(2021, 5, 1, 20, 42, 30, 975, DateTimeKind.Unspecified).AddTicks(5868), "Id veritatis.", 10, 4, 0 },
                    { 165, new DateTime(2019, 1, 30, 10, 56, 22, 133, DateTimeKind.Unspecified).AddTicks(3388), "Velit facilis similique neque vel sint blanditiis odit asperiores.", new DateTime(2021, 2, 20, 7, 42, 25, 467, DateTimeKind.Unspecified).AddTicks(8430), "Quam est.", 45, 4, 0 },
                    { 166, new DateTime(2019, 5, 27, 6, 1, 6, 868, DateTimeKind.Unspecified).AddTicks(4428), "Eligendi aperiam quibusdam illum provident hic et.", new DateTime(2022, 9, 7, 0, 25, 40, 92, DateTimeKind.Unspecified).AddTicks(3169), "Necessitatibus atque.", 23, 1, 3 },
                    { 167, new DateTime(2019, 9, 5, 2, 29, 5, 302, DateTimeKind.Unspecified).AddTicks(5283), "Aut fugit consequatur quasi suscipit tenetur beatae doloribus.", new DateTime(2022, 7, 25, 9, 6, 44, 887, DateTimeKind.Unspecified).AddTicks(1485), "Sed quam.", 33, 5, 3 },
                    { 168, new DateTime(2019, 12, 17, 10, 44, 5, 12, DateTimeKind.Unspecified).AddTicks(524), "Id officia id est nihil doloremque maiores cum eaque.", new DateTime(2021, 11, 19, 20, 26, 8, 443, DateTimeKind.Unspecified).AddTicks(2200), "Maxime numquam.", 35, 1, 0 },
                    { 169, new DateTime(2019, 1, 7, 0, 32, 59, 113, DateTimeKind.Unspecified).AddTicks(829), "Voluptatibus dolores ad dolorem voluptatum suscipit quo tempora tempore quidem.", new DateTime(2022, 9, 4, 8, 29, 17, 86, DateTimeKind.Unspecified).AddTicks(6630), "Esse dolore.", 24, 5, 0 },
                    { 170, new DateTime(2019, 12, 17, 5, 14, 1, 876, DateTimeKind.Unspecified).AddTicks(2936), "Saepe est corporis ut qui animi.", new DateTime(2022, 7, 12, 22, 25, 18, 632, DateTimeKind.Unspecified).AddTicks(1778), "Voluptatum ut.", 33, 2, 0 },
                    { 171, new DateTime(2019, 4, 10, 18, 30, 37, 156, DateTimeKind.Unspecified).AddTicks(9398), "Quis et maxime et libero cum.", new DateTime(2022, 12, 30, 7, 6, 19, 215, DateTimeKind.Unspecified).AddTicks(5254), "Doloremque quos.", 38, 1, 1 },
                    { 172, new DateTime(2019, 1, 2, 7, 43, 47, 758, DateTimeKind.Unspecified).AddTicks(2275), "Doloremque sit ea iusto quidem labore voluptatem explicabo commodi doloremque.", new DateTime(2021, 2, 12, 4, 14, 53, 256, DateTimeKind.Unspecified).AddTicks(3620), "Nostrum qui.", 41, 4, 0 },
                    { 173, new DateTime(2019, 1, 13, 14, 18, 48, 382, DateTimeKind.Unspecified).AddTicks(651), "Eligendi inventore veritatis.", new DateTime(2022, 9, 2, 6, 52, 34, 552, DateTimeKind.Unspecified).AddTicks(1916), "Fuga est.", 29, 3, 0 },
                    { 174, new DateTime(2019, 5, 25, 5, 12, 45, 413, DateTimeKind.Unspecified).AddTicks(701), "Nobis praesentium doloribus fuga et sed et debitis.", new DateTime(2022, 6, 3, 6, 51, 32, 883, DateTimeKind.Unspecified).AddTicks(3240), "Ad provident.", 41, 1, 0 },
                    { 175, new DateTime(2019, 1, 3, 14, 33, 2, 186, DateTimeKind.Unspecified).AddTicks(2574), "Commodi excepturi error cum.", new DateTime(2021, 9, 18, 14, 9, 17, 113, DateTimeKind.Unspecified).AddTicks(2139), "Nihil praesentium.", 14, 5, 1 },
                    { 176, new DateTime(2019, 4, 23, 5, 44, 38, 726, DateTimeKind.Unspecified).AddTicks(9469), "Consequatur ullam adipisci.", new DateTime(2021, 3, 29, 10, 15, 37, 356, DateTimeKind.Unspecified).AddTicks(3696), "Qui omnis.", 26, 3, 2 },
                    { 177, new DateTime(2019, 5, 10, 0, 49, 13, 256, DateTimeKind.Unspecified).AddTicks(1256), "Corporis consequatur earum quo quo.", new DateTime(2021, 4, 16, 0, 50, 49, 366, DateTimeKind.Unspecified).AddTicks(4949), "Consequuntur suscipit.", 27, 2, 0 },
                    { 178, new DateTime(2019, 7, 30, 19, 19, 31, 304, DateTimeKind.Unspecified).AddTicks(9310), "Molestiae et odio.", new DateTime(2022, 8, 11, 1, 34, 2, 358, DateTimeKind.Unspecified).AddTicks(7787), "Consequatur reprehenderit.", 25, 5, 0 },
                    { 179, new DateTime(2019, 4, 14, 17, 38, 29, 385, DateTimeKind.Unspecified).AddTicks(7778), "Possimus voluptas cumque omnis sunt.", new DateTime(2021, 5, 13, 13, 4, 7, 842, DateTimeKind.Unspecified).AddTicks(1857), "Amet adipisci.", 4, 3, 0 },
                    { 180, new DateTime(2019, 4, 21, 10, 24, 6, 512, DateTimeKind.Unspecified).AddTicks(2243), "Qui id facilis voluptatem id deserunt voluptatum sequi autem ea.", new DateTime(2021, 2, 26, 18, 16, 25, 276, DateTimeKind.Unspecified).AddTicks(8732), "Laborum eum.", 50, 2, 1 },
                    { 181, new DateTime(2019, 4, 22, 3, 13, 36, 944, DateTimeKind.Unspecified).AddTicks(4878), "Id aut eligendi dicta.", new DateTime(2021, 8, 20, 5, 11, 50, 737, DateTimeKind.Unspecified).AddTicks(3712), "Esse doloremque.", 47, 4, 0 },
                    { 182, new DateTime(2019, 5, 22, 3, 30, 10, 242, DateTimeKind.Unspecified).AddTicks(8284), "Nostrum aut architecto vero quo voluptatem dolores non.", new DateTime(2022, 12, 21, 16, 23, 49, 498, DateTimeKind.Unspecified).AddTicks(1541), "Repellendus architecto.", 1, 4, 3 },
                    { 183, new DateTime(2019, 8, 5, 2, 40, 40, 250, DateTimeKind.Unspecified).AddTicks(4104), "Est blanditiis quisquam fugiat optio consequatur recusandae quisquam.", new DateTime(2022, 10, 12, 5, 13, 8, 901, DateTimeKind.Unspecified).AddTicks(2196), "Sunt deleniti.", 8, 1, 1 },
                    { 184, new DateTime(2019, 5, 31, 23, 18, 31, 989, DateTimeKind.Unspecified).AddTicks(5335), "Et repudiandae architecto consectetur.", new DateTime(2022, 11, 27, 23, 42, 53, 868, DateTimeKind.Unspecified).AddTicks(4554), "Eaque mollitia.", 22, 5, 0 },
                    { 185, new DateTime(2019, 10, 24, 19, 11, 51, 464, DateTimeKind.Unspecified).AddTicks(8327), "Omnis magnam nesciunt ut facilis.", new DateTime(2021, 10, 25, 6, 29, 55, 309, DateTimeKind.Unspecified).AddTicks(3994), "Assumenda quo.", 48, 3, 1 },
                    { 186, new DateTime(2019, 7, 8, 1, 15, 30, 144, DateTimeKind.Unspecified).AddTicks(4930), "Amet et sed ab voluptatem.", new DateTime(2021, 4, 13, 10, 59, 33, 474, DateTimeKind.Unspecified).AddTicks(2236), "Mollitia totam.", 25, 3, 0 },
                    { 187, new DateTime(2019, 2, 4, 17, 35, 19, 473, DateTimeKind.Unspecified).AddTicks(9581), "Voluptas vel dignissimos modi recusandae labore ad quis.", new DateTime(2021, 5, 7, 14, 50, 11, 702, DateTimeKind.Unspecified).AddTicks(4061), "Sed possimus.", 31, 2, 2 },
                    { 188, new DateTime(2019, 8, 11, 1, 58, 36, 271, DateTimeKind.Unspecified).AddTicks(4389), "Sunt atque sit perferendis omnis consequatur eligendi quos nesciunt.", new DateTime(2021, 9, 6, 5, 17, 11, 373, DateTimeKind.Unspecified).AddTicks(2585), "Dolor eaque.", 40, 4, 1 },
                    { 189, new DateTime(2019, 1, 21, 2, 18, 28, 51, DateTimeKind.Unspecified).AddTicks(3932), "Aperiam tenetur dolores.", new DateTime(2022, 8, 9, 8, 8, 4, 907, DateTimeKind.Unspecified).AddTicks(4847), "Reiciendis eum.", 17, 1, 1 },
                    { 190, new DateTime(2019, 12, 8, 16, 3, 28, 556, DateTimeKind.Unspecified).AddTicks(5946), "Esse vero harum et molestiae minus excepturi quisquam saepe.", new DateTime(2021, 1, 1, 4, 45, 59, 250, DateTimeKind.Unspecified).AddTicks(6494), "In impedit.", 36, 4, 2 },
                    { 191, new DateTime(2019, 5, 27, 8, 7, 22, 851, DateTimeKind.Unspecified).AddTicks(4893), "Ducimus veniam neque ab eligendi cupiditate doloremque nam officiis numquam.", new DateTime(2022, 10, 22, 9, 25, 13, 51, DateTimeKind.Unspecified).AddTicks(1059), "Assumenda dolore.", 20, 5, 1 },
                    { 192, new DateTime(2019, 4, 17, 11, 55, 18, 964, DateTimeKind.Unspecified).AddTicks(6066), "Ut omnis iusto magnam.", new DateTime(2021, 6, 7, 9, 30, 51, 635, DateTimeKind.Unspecified).AddTicks(6729), "Illum delectus.", 34, 2, 3 },
                    { 193, new DateTime(2019, 2, 10, 9, 11, 23, 738, DateTimeKind.Unspecified).AddTicks(7817), "Quaerat facilis inventore facilis quia incidunt sapiente natus asperiores.", new DateTime(2021, 8, 23, 5, 26, 9, 449, DateTimeKind.Unspecified).AddTicks(4245), "Dolores et.", 27, 4, 3 },
                    { 194, new DateTime(2019, 11, 2, 4, 59, 52, 224, DateTimeKind.Unspecified).AddTicks(5342), "Voluptatem nam veritatis unde.", new DateTime(2021, 11, 7, 22, 50, 35, 184, DateTimeKind.Unspecified).AddTicks(6910), "Repellendus officia.", 8, 5, 1 },
                    { 195, new DateTime(2019, 12, 22, 3, 6, 27, 48, DateTimeKind.Unspecified).AddTicks(9950), "Sint et quae quod quam ut illo rerum.", new DateTime(2022, 4, 16, 17, 33, 36, 386, DateTimeKind.Unspecified).AddTicks(4903), "Explicabo ut.", 34, 3, 1 },
                    { 196, new DateTime(2019, 6, 20, 1, 38, 1, 735, DateTimeKind.Unspecified).AddTicks(3658), "Qui neque non dolorum numquam.", new DateTime(2022, 1, 20, 8, 15, 15, 969, DateTimeKind.Unspecified).AddTicks(4201), "Est repudiandae.", 39, 2, 1 },
                    { 197, new DateTime(2019, 1, 15, 7, 36, 24, 146, DateTimeKind.Unspecified).AddTicks(6457), "Modi et asperiores perspiciatis qui ea et distinctio rem.", new DateTime(2021, 10, 5, 7, 46, 8, 696, DateTimeKind.Unspecified).AddTicks(6937), "Facere sint.", 38, 1, 3 },
                    { 198, new DateTime(2019, 4, 10, 12, 8, 21, 103, DateTimeKind.Unspecified).AddTicks(3625), "Aut ut est officiis et et possimus dolorum.", new DateTime(2021, 1, 1, 1, 55, 31, 323, DateTimeKind.Unspecified).AddTicks(4267), "Exercitationem magni.", 25, 3, 3 },
                    { 199, new DateTime(2019, 4, 6, 16, 41, 53, 66, DateTimeKind.Unspecified).AddTicks(4040), "Sunt quo maxime eligendi recusandae sapiente culpa porro odio sed.", new DateTime(2022, 10, 22, 18, 16, 38, 315, DateTimeKind.Unspecified).AddTicks(443), "Eos incidunt.", 18, 4, 2 },
                    { 200, new DateTime(2019, 8, 15, 14, 15, 32, 95, DateTimeKind.Unspecified).AddTicks(1721), "Repudiandae et deserunt sit.", new DateTime(2021, 2, 23, 19, 7, 51, 557, DateTimeKind.Unspecified).AddTicks(334), "Sapiente occaecati.", 47, 1, 0 }
                });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 2, 18, 6, 29, 46, 277, DateTimeKind.Unspecified).AddTicks(939), "Ebert, Cassin and Stark" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 1, 1, 8, 14, 2, 811, DateTimeKind.Unspecified).AddTicks(1614), "DuBuque - O'Conner" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 11, 27, 0, 59, 13, 886, DateTimeKind.Unspecified).AddTicks(3865), "Kuhlman, Corwin and Thiel" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 11, 22, 4, 49, 8, 849, DateTimeKind.Unspecified).AddTicks(9907), "Kuhic LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 6, 11, 18, 39, 3, 266, DateTimeKind.Unspecified).AddTicks(5736), "Howell Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 7, 17, 11, 7, 24, 36, DateTimeKind.Unspecified).AddTicks(8578), "Crist, Sauer and Gutkowski" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 12, 26, 23, 1, 50, 142, DateTimeKind.Unspecified).AddTicks(3570), "Parisian - Watsica" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 10, 12, 14, 28, 18, 903, DateTimeKind.Unspecified).AddTicks(9380), "Cremin - Toy" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 3, 20, 8, 43, 3, 218, DateTimeKind.Unspecified).AddTicks(9163), "Haag, O'Hara and Bailey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 7, 16, 10, 0, 16, 384, DateTimeKind.Unspecified).AddTicks(4479), "Buckridge, Paucek and Conroy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 9, 2, 18, 34, 3, 208, DateTimeKind.Unspecified).AddTicks(2303), "Krystina.Welch90@hotmail.com", "Jeramie", "Roob", new DateTime(2015, 4, 27, 5, 9, 48, 617, DateTimeKind.Unspecified).AddTicks(7028), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1998, 10, 17, 9, 34, 2, 560, DateTimeKind.Unspecified).AddTicks(2427), "Jaydon.Koss@gmail.com", "Hermina", "Hayes", new DateTime(2012, 6, 17, 18, 19, 26, 427, DateTimeKind.Unspecified).AddTicks(2614) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 4, 10, 22, 19, 0, 802, DateTimeKind.Unspecified).AddTicks(2266), "Blake_Luettgen@gmail.com", "Tracy", "Krajcik", new DateTime(2013, 9, 24, 4, 29, 15, 671, DateTimeKind.Unspecified).AddTicks(1119), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 25, 12, 13, 48, 502, DateTimeKind.Unspecified).AddTicks(6304), "Chester8@gmail.com", "Elvis", "Lehner", new DateTime(2012, 1, 13, 10, 11, 32, 233, DateTimeKind.Unspecified).AddTicks(3317), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 8, 25, 22, 54, 34, 783, DateTimeKind.Unspecified).AddTicks(7546), "Hope2@yahoo.com", "Jean", "Gleichner", new DateTime(2013, 9, 26, 8, 3, 29, 246, DateTimeKind.Unspecified).AddTicks(9224), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 1, 3, 43, 45, 620, DateTimeKind.Unspecified).AddTicks(1500), "Breana_Stamm@yahoo.com", "Vena", "DuBuque", new DateTime(2016, 2, 10, 22, 35, 16, 587, DateTimeKind.Unspecified).AddTicks(6599), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 21, 3, 14, 3, 760, DateTimeKind.Unspecified).AddTicks(2544), "Dillan13@hotmail.com", "Ryley", "Hilll", new DateTime(2017, 12, 22, 21, 0, 54, 217, DateTimeKind.Unspecified).AddTicks(9769), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 26, 2, 26, 56, 492, DateTimeKind.Unspecified).AddTicks(8425), "Lulu_Kessler54@hotmail.com", "Tyrique", "Fahey", new DateTime(2011, 10, 18, 5, 40, 39, 373, DateTimeKind.Unspecified).AddTicks(4360), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 4, 22, 7, 30, 55, 256, DateTimeKind.Unspecified).AddTicks(2598), "Kara.Robel19@gmail.com", "Ardith", "MacGyver", new DateTime(2015, 12, 19, 22, 35, 35, 852, DateTimeKind.Unspecified).AddTicks(3426), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 24, 23, 7, 0, 559, DateTimeKind.Unspecified).AddTicks(5159), "Kenyon.Wehner@gmail.com", "Laverna", "Kreiger", new DateTime(2015, 8, 17, 20, 19, 25, 45, DateTimeKind.Unspecified).AddTicks(7366), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 29, 17, 24, 58, 255, DateTimeKind.Unspecified).AddTicks(9382), "Chesley38@yahoo.com", "Jennings", "Kemmer", new DateTime(2011, 12, 29, 12, 58, 19, 249, DateTimeKind.Unspecified).AddTicks(4981), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 11, 7, 18, 41, 54, 136, DateTimeKind.Unspecified).AddTicks(4832), "Conner.Kovacek@yahoo.com", "Marisol", "Mayer", new DateTime(2010, 10, 30, 20, 18, 40, 649, DateTimeKind.Unspecified).AddTicks(8745), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 1, 7, 20, 12, 25, 933, DateTimeKind.Unspecified).AddTicks(9407), "Santino2@gmail.com", "Jorge", "Mohr", new DateTime(2015, 10, 31, 21, 38, 25, 843, DateTimeKind.Unspecified).AddTicks(2539), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 29, 14, 24, 34, 856, DateTimeKind.Unspecified).AddTicks(668), "Keely_Lakin84@hotmail.com", "Candace", "Kuphal", new DateTime(2014, 11, 8, 9, 18, 38, 242, DateTimeKind.Unspecified).AddTicks(9969), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 4, 25, 22, 22, 33, 810, DateTimeKind.Unspecified).AddTicks(6486), "Royce_Breitenberg@gmail.com", "Alessia", "Quitzon", new DateTime(2012, 5, 26, 2, 40, 1, 900, DateTimeKind.Unspecified).AddTicks(2196), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 3, 20, 2, 55, 4, 444, DateTimeKind.Unspecified).AddTicks(8199), "Horace46@hotmail.com", "Clint", "Cremin", new DateTime(2010, 4, 6, 3, 50, 30, 374, DateTimeKind.Unspecified).AddTicks(2011), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 14, 17, 19, 5, 5, DateTimeKind.Unspecified).AddTicks(2579), "Oscar.Toy@hotmail.com", "Ollie", "Tremblay", new DateTime(2015, 11, 24, 21, 15, 32, 258, DateTimeKind.Unspecified).AddTicks(6473), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 5, 21, 7, 55, 57, 167, DateTimeKind.Unspecified).AddTicks(1162), "Kyleigh.Torphy24@yahoo.com", "Damon", "Terry", new DateTime(2012, 7, 13, 22, 3, 29, 990, DateTimeKind.Unspecified).AddTicks(1153), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 11, 6, 4, 37, 44, 959, DateTimeKind.Unspecified).AddTicks(2921), "Vivian88@yahoo.com", "Mara", "Braun", new DateTime(2012, 6, 14, 13, 22, 1, 498, DateTimeKind.Unspecified).AddTicks(9228), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 4, 27, 15, 9, 13, 825, DateTimeKind.Unspecified).AddTicks(8970), "Brandyn60@yahoo.com", "Kelley", "Hudson", new DateTime(2017, 9, 11, 7, 26, 10, 327, DateTimeKind.Unspecified).AddTicks(5115), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 12, 13, 3, 28, 34, 165, DateTimeKind.Unspecified).AddTicks(2013), "Emmitt.Bartoletti@gmail.com", "Alvera", "Fadel", new DateTime(2012, 10, 1, 1, 19, 0, 738, DateTimeKind.Unspecified).AddTicks(3152), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 9, 16, 5, 38, 0, 963, DateTimeKind.Unspecified).AddTicks(6402), "Julio.Bogan32@gmail.com", "Ulices", "Conroy", new DateTime(2017, 2, 26, 16, 12, 45, 474, DateTimeKind.Unspecified).AddTicks(6763), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 14, 6, 58, 47, 483, DateTimeKind.Unspecified).AddTicks(8115), "Kaleigh15@gmail.com", "Floy", "Pouros", new DateTime(2014, 12, 20, 9, 55, 15, 144, DateTimeKind.Unspecified).AddTicks(5218), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 5, 24, 21, 34, 37, 888, DateTimeKind.Unspecified).AddTicks(7446), "Garnett4@hotmail.com", "Marge", "Blick", new DateTime(2016, 4, 30, 2, 38, 33, 174, DateTimeKind.Unspecified).AddTicks(651), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 5, 3, 15, 15, 46, 169, DateTimeKind.Unspecified).AddTicks(4981), "Letitia_Littel@hotmail.com", "Domenica", "Pagac", new DateTime(2011, 8, 16, 3, 3, 5, 648, DateTimeKind.Unspecified).AddTicks(8659), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 4, 7, 20, 46, 51, 965, DateTimeKind.Unspecified).AddTicks(1495), "Clifton.Bode@yahoo.com", "Brody", "Little", new DateTime(2010, 7, 9, 7, 28, 46, 981, DateTimeKind.Unspecified).AddTicks(6152), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 12, 31, 0, 13, 2, 917, DateTimeKind.Unspecified).AddTicks(9655), "Evie_Schmitt@gmail.com", "Carolyn", "Hagenes", new DateTime(2016, 6, 3, 9, 23, 53, 234, DateTimeKind.Unspecified).AddTicks(1644), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 25, 3, 22, 45, 399, DateTimeKind.Unspecified).AddTicks(1671), "Marianna35@hotmail.com", "Bridgette", "Herman", new DateTime(2010, 6, 24, 10, 48, 21, 806, DateTimeKind.Unspecified).AddTicks(6455), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 6, 14, 7, 43, 9, 601, DateTimeKind.Unspecified).AddTicks(2707), "Princess_Mayer68@hotmail.com", "Tristin", "Zboncak", new DateTime(2015, 3, 9, 16, 3, 22, 326, DateTimeKind.Unspecified).AddTicks(8190), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 4, 14, 20, 43, 33, 755, DateTimeKind.Unspecified).AddTicks(935), "Dimitri_Douglas@hotmail.com", "Una", "Homenick", new DateTime(2010, 12, 26, 1, 5, 46, 850, DateTimeKind.Unspecified).AddTicks(2864), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 5, 17, 8, 23, 6, 526, DateTimeKind.Unspecified).AddTicks(9174), "Sigmund_Cartwright2@gmail.com", "Retta", "Ortiz", new DateTime(2013, 3, 21, 11, 13, 29, 614, DateTimeKind.Unspecified).AddTicks(341), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 26, 1, 24, 44, 951, DateTimeKind.Unspecified).AddTicks(2797), "Viva38@yahoo.com", "Jonas", "Larkin", new DateTime(2015, 6, 19, 2, 26, 11, 643, DateTimeKind.Unspecified).AddTicks(1177), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 9, 26, 10, 30, 23, 750, DateTimeKind.Unspecified).AddTicks(1082), "Brenna.Lesch12@yahoo.com", "Gideon", "Huel", new DateTime(2010, 3, 10, 23, 24, 31, 919, DateTimeKind.Unspecified).AddTicks(4297), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 2, 17, 23, 9, 9, 485, DateTimeKind.Unspecified).AddTicks(4540), "Leonie65@gmail.com", "Kirstin", "Bayer", new DateTime(2011, 8, 8, 10, 0, 50, 373, DateTimeKind.Unspecified).AddTicks(3439), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 10, 13, 17, 0, 0, 127, DateTimeKind.Unspecified).AddTicks(456), "Pearlie.Ward2@yahoo.com", "Reymundo", "Murphy", new DateTime(2011, 2, 6, 10, 45, 38, 555, DateTimeKind.Unspecified).AddTicks(5634), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 26, 3, 34, 32, 188, DateTimeKind.Unspecified).AddTicks(2356), "Roxanne.Schuster@yahoo.com", "Jovani", "Douglas", new DateTime(2010, 1, 1, 14, 6, 0, 234, DateTimeKind.Unspecified).AddTicks(3872), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1996, 9, 5, 13, 18, 35, 749, DateTimeKind.Unspecified).AddTicks(1254), "Armando37@yahoo.com", "Monty", "Boyle", new DateTime(2011, 3, 25, 13, 3, 26, 317, DateTimeKind.Unspecified).AddTicks(4235) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 6, 21, 20, 24, 55, 129, DateTimeKind.Unspecified).AddTicks(9034), "Macie.Gibson@hotmail.com", "Betsy", "Gerlach", new DateTime(2011, 4, 18, 12, 9, 21, 794, DateTimeKind.Unspecified).AddTicks(8531), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 12, 14, 23, 12, 29, 841, DateTimeKind.Unspecified).AddTicks(8987), "Carson_Rolfson@yahoo.com", "Amelie", "Beatty", new DateTime(2010, 2, 7, 16, 1, 44, 541, DateTimeKind.Unspecified).AddTicks(1564) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 3, 5, 2, 51, 1, 552, DateTimeKind.Unspecified).AddTicks(1935), "Willard.Abbott@yahoo.com", "Minnie", "Lockman", new DateTime(2017, 1, 29, 23, 55, 50, 598, DateTimeKind.Unspecified).AddTicks(404), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 9, 9, 5, 14, 45, 205, DateTimeKind.Unspecified).AddTicks(7825), "Karelle.McClure70@yahoo.com", "Holly", "Ruecker", new DateTime(2011, 7, 27, 11, 7, 26, 120, DateTimeKind.Unspecified).AddTicks(4281), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 10, 13, 0, 42, 18, 735, DateTimeKind.Unspecified).AddTicks(6442), "Rusty8@hotmail.com", "Frieda", "Stokes", new DateTime(2015, 2, 15, 4, 8, 7, 692, DateTimeKind.Unspecified).AddTicks(2074), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 4, 28, 23, 14, 34, 974, DateTimeKind.Unspecified).AddTicks(355), "Rodolfo_Swaniawski19@gmail.com", "Albin", "Paucek", new DateTime(2013, 8, 12, 20, 28, 13, 101, DateTimeKind.Unspecified).AddTicks(1362), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 4, 29, 12, 56, 5, 200, DateTimeKind.Unspecified).AddTicks(5422), "Veronica49@gmail.com", "Fae", "Spinka", new DateTime(2015, 6, 20, 18, 5, 36, 926, DateTimeKind.Unspecified).AddTicks(5772), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 23, 22, 6, 19, 325, DateTimeKind.Unspecified).AddTicks(5906), "Fermin44@gmail.com", "Keshawn", "Hyatt", new DateTime(2014, 2, 9, 8, 30, 31, 897, DateTimeKind.Unspecified).AddTicks(9783), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1996, 10, 14, 21, 7, 52, 801, DateTimeKind.Unspecified).AddTicks(9487), "Marcelle.Fay@hotmail.com", "Lon", "Carroll", new DateTime(2017, 3, 28, 6, 47, 19, 282, DateTimeKind.Unspecified).AddTicks(7268) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 9, 26, 3, 53, 36, 619, DateTimeKind.Unspecified).AddTicks(8679), "Olin.Krajcik95@yahoo.com", "Alison", "Schoen", new DateTime(2010, 6, 4, 5, 26, 25, 449, DateTimeKind.Unspecified).AddTicks(9773), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 2, 28, 15, 15, 8, 692, DateTimeKind.Unspecified).AddTicks(8808), "Sister_Langosh@hotmail.com", "Golden", "Stanton", new DateTime(2016, 5, 21, 20, 31, 17, 188, DateTimeKind.Unspecified).AddTicks(2898), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 9, 4, 4, 50, 40, 205, DateTimeKind.Unspecified).AddTicks(3944), "Thelma21@yahoo.com", "Amely", "Rath", new DateTime(2012, 6, 29, 3, 21, 15, 130, DateTimeKind.Unspecified).AddTicks(7605), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 2, 0, 18, 12, 801, DateTimeKind.Unspecified).AddTicks(6787), "Javon6@yahoo.com", "Kayley", "Daniel", new DateTime(2010, 9, 22, 3, 13, 16, 576, DateTimeKind.Unspecified).AddTicks(8678), 5 });

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerUserId",
                table: "Tasks",
                column: "PerformerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.CreateTable(
                name: "Task",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PerformerUserId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    State = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Task", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Task_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Users_PerformerUserId",
                        column: x => x.PerformerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 29, new DateTime(2019, 6, 22, 10, 51, 20, 127, DateTimeKind.Unspecified).AddTicks(6528), new DateTime(2021, 12, 7, 10, 23, 53, 39, DateTimeKind.Unspecified).AddTicks(5962), "Ipsam ut recusandae aspernatur aut molestiae et aliquid.", "Handcrafted Steel Ball", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 8, 4, 23, 56, 37, 923, DateTimeKind.Unspecified).AddTicks(3377), new DateTime(2022, 4, 30, 11, 2, 4, 391, DateTimeKind.Unspecified).AddTicks(7048), "Enim labore rerum corporis qui provident qui libero.", "Sleek Metal Table", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 5, 22, 23, 35, 12, 12, DateTimeKind.Unspecified).AddTicks(6204), new DateTime(2022, 8, 28, 10, 52, 2, 901, DateTimeKind.Unspecified).AddTicks(8003), "Id itaque adipisci et impedit.", "Small Concrete Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 28, new DateTime(2019, 10, 9, 21, 30, 39, 404, DateTimeKind.Unspecified).AddTicks(2694), new DateTime(2021, 3, 27, 6, 9, 20, 637, DateTimeKind.Unspecified).AddTicks(55), "Inventore rem saepe est provident temporibus veritatis reiciendis est aut.", "Practical Rubber Chair" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 2, new DateTime(2019, 9, 5, 23, 18, 7, 86, DateTimeKind.Unspecified).AddTicks(2477), new DateTime(2021, 2, 11, 2, 54, 17, 464, DateTimeKind.Unspecified).AddTicks(4205), "Quo labore sit recusandae praesentium et quisquam quo vel.", "Intelligent Plastic Shirt" });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 3, 31, 0, 1, 12, 276, DateTimeKind.Unspecified).AddTicks(3994), "Fugit officiis inventore optio sit odio.", new DateTime(2022, 4, 21, 10, 12, 29, 78, DateTimeKind.Unspecified).AddTicks(9841), "Alias et.", 13, 3, 1 },
                    { 2, new DateTime(2019, 5, 16, 2, 56, 47, 830, DateTimeKind.Unspecified).AddTicks(1683), "Temporibus vel cupiditate.", new DateTime(2021, 7, 11, 14, 59, 42, 254, DateTimeKind.Unspecified).AddTicks(6125), "Quis fuga.", 13, 2, 2 },
                    { 3, new DateTime(2019, 4, 7, 8, 39, 44, 906, DateTimeKind.Unspecified).AddTicks(9335), "Quia dolorem quia enim minus placeat ad perferendis nulla at.", new DateTime(2021, 11, 20, 5, 9, 24, 979, DateTimeKind.Unspecified).AddTicks(9901), "Aut velit.", 12, 5, 1 },
                    { 4, new DateTime(2019, 1, 1, 8, 4, 20, 681, DateTimeKind.Unspecified).AddTicks(1189), "Ut pariatur explicabo enim eveniet praesentium quas et.", new DateTime(2021, 6, 22, 0, 4, 49, 544, DateTimeKind.Unspecified).AddTicks(1857), "Qui molestiae.", 40, 3, 0 },
                    { 5, new DateTime(2019, 12, 12, 12, 38, 23, 730, DateTimeKind.Unspecified).AddTicks(2462), "Eveniet perferendis unde id eum.", new DateTime(2021, 12, 18, 22, 0, 3, 563, DateTimeKind.Unspecified).AddTicks(5209), "Voluptatibus officiis.", 10, 5, 1 },
                    { 6, new DateTime(2019, 4, 3, 12, 19, 55, 624, DateTimeKind.Unspecified).AddTicks(840), "Dolorem est beatae vel iste voluptatem beatae quos placeat.", new DateTime(2022, 12, 24, 7, 5, 10, 253, DateTimeKind.Unspecified).AddTicks(207), "Mollitia laudantium.", 42, 2, 2 },
                    { 7, new DateTime(2019, 8, 7, 1, 13, 28, 225, DateTimeKind.Unspecified).AddTicks(5728), "Velit assumenda nobis expedita porro omnis.", new DateTime(2021, 1, 4, 11, 46, 53, 360, DateTimeKind.Unspecified).AddTicks(7640), "Aut sunt.", 17, 5, 1 },
                    { 8, new DateTime(2019, 7, 21, 3, 22, 21, 90, DateTimeKind.Unspecified).AddTicks(6162), "Odit enim nisi ea nobis perspiciatis aut.", new DateTime(2022, 10, 7, 1, 30, 25, 332, DateTimeKind.Unspecified).AddTicks(4856), "Aspernatur excepturi.", 28, 3, 3 },
                    { 9, new DateTime(2019, 4, 9, 23, 22, 54, 71, DateTimeKind.Unspecified).AddTicks(6335), "Et sed voluptas ea possimus sit sint.", new DateTime(2021, 3, 13, 18, 50, 34, 725, DateTimeKind.Unspecified).AddTicks(6318), "Nemo voluptatem.", 42, 1, 1 },
                    { 10, new DateTime(2019, 11, 7, 4, 0, 17, 828, DateTimeKind.Unspecified).AddTicks(8332), "Dolor excepturi nemo saepe ut at vel quia.", new DateTime(2021, 3, 5, 13, 34, 22, 80, DateTimeKind.Unspecified).AddTicks(6411), "Quo vitae.", 30, 3, 0 },
                    { 11, new DateTime(2019, 8, 14, 23, 22, 55, 494, DateTimeKind.Unspecified).AddTicks(939), "Aut est est et.", new DateTime(2021, 5, 19, 8, 53, 12, 563, DateTimeKind.Unspecified).AddTicks(9831), "Eum eum.", 11, 3, 0 },
                    { 12, new DateTime(2019, 3, 27, 13, 28, 50, 813, DateTimeKind.Unspecified).AddTicks(4162), "Hic tenetur et eius non sint omnis culpa non.", new DateTime(2022, 2, 10, 7, 37, 35, 557, DateTimeKind.Unspecified).AddTicks(1825), "Sit sed.", 44, 3, 2 },
                    { 13, new DateTime(2019, 7, 11, 23, 6, 21, 741, DateTimeKind.Unspecified).AddTicks(5575), "Sit laborum esse maxime voluptatem necessitatibus voluptatibus autem.", new DateTime(2021, 1, 17, 19, 6, 18, 85, DateTimeKind.Unspecified).AddTicks(3369), "Pariatur enim.", 11, 2, 1 },
                    { 14, new DateTime(2019, 6, 26, 14, 18, 50, 13, DateTimeKind.Unspecified).AddTicks(4433), "Quo non tempora ipsam ad nesciunt quis eum.", new DateTime(2022, 2, 14, 10, 47, 33, 31, DateTimeKind.Unspecified).AddTicks(8146), "Ut consequuntur.", 47, 5, 3 },
                    { 15, new DateTime(2019, 2, 3, 16, 53, 41, 604, DateTimeKind.Unspecified).AddTicks(5152), "Sed repellat a veniam.", new DateTime(2022, 4, 16, 20, 11, 44, 332, DateTimeKind.Unspecified).AddTicks(667), "Sapiente iure.", 9, 1, 3 },
                    { 16, new DateTime(2019, 9, 11, 6, 37, 15, 173, DateTimeKind.Unspecified).AddTicks(1157), "Consequatur ab saepe molestias quidem odio enim.", new DateTime(2021, 6, 8, 8, 25, 41, 613, DateTimeKind.Unspecified).AddTicks(1130), "Explicabo quo.", 11, 3, 2 },
                    { 17, new DateTime(2019, 9, 16, 7, 59, 57, 971, DateTimeKind.Unspecified).AddTicks(4854), "Eum quia laborum labore sed dolorum molestiae accusantium.", new DateTime(2022, 11, 16, 9, 51, 20, 503, DateTimeKind.Unspecified).AddTicks(5263), "Et eligendi.", 33, 2, 0 },
                    { 18, new DateTime(2019, 6, 6, 13, 12, 7, 309, DateTimeKind.Unspecified).AddTicks(9045), "Nam laudantium earum nulla dolor ducimus assumenda.", new DateTime(2021, 10, 17, 7, 6, 49, 868, DateTimeKind.Unspecified).AddTicks(2476), "Dicta nostrum.", 5, 4, 0 },
                    { 19, new DateTime(2019, 9, 18, 21, 59, 12, 682, DateTimeKind.Unspecified).AddTicks(3809), "Dignissimos quia culpa.", new DateTime(2021, 1, 3, 2, 45, 35, 592, DateTimeKind.Unspecified).AddTicks(6744), "Voluptatem modi.", 35, 1, 1 },
                    { 20, new DateTime(2019, 8, 23, 17, 21, 42, 930, DateTimeKind.Unspecified).AddTicks(5473), "Ut voluptatem omnis qui ipsum.", new DateTime(2022, 10, 22, 3, 26, 25, 43, DateTimeKind.Unspecified).AddTicks(3922), "Cupiditate sapiente.", 32, 1, 1 },
                    { 21, new DateTime(2019, 2, 6, 7, 33, 51, 954, DateTimeKind.Unspecified).AddTicks(5866), "Architecto exercitationem repellat laudantium molestias tempore.", new DateTime(2022, 3, 20, 11, 55, 57, 325, DateTimeKind.Unspecified).AddTicks(2509), "Neque sit.", 49, 4, 0 },
                    { 22, new DateTime(2019, 1, 18, 12, 18, 32, 826, DateTimeKind.Unspecified).AddTicks(3868), "Qui omnis enim amet eos doloremque perferendis aut.", new DateTime(2022, 7, 8, 7, 26, 56, 601, DateTimeKind.Unspecified).AddTicks(3251), "Quasi et.", 7, 4, 2 },
                    { 23, new DateTime(2019, 8, 25, 9, 52, 25, 725, DateTimeKind.Unspecified).AddTicks(9620), "Ducimus aut nobis.", new DateTime(2021, 1, 7, 0, 51, 5, 643, DateTimeKind.Unspecified).AddTicks(4629), "Sed amet.", 19, 4, 1 },
                    { 24, new DateTime(2019, 9, 22, 1, 37, 24, 971, DateTimeKind.Unspecified).AddTicks(5487), "Recusandae dolor beatae voluptas aliquid magni qui officiis.", new DateTime(2021, 8, 2, 7, 11, 2, 966, DateTimeKind.Unspecified).AddTicks(5369), "Fugiat praesentium.", 48, 4, 1 },
                    { 25, new DateTime(2019, 10, 15, 11, 25, 35, 747, DateTimeKind.Unspecified).AddTicks(2273), "Ut nihil laborum magni et aspernatur consequatur et.", new DateTime(2021, 1, 8, 1, 59, 34, 317, DateTimeKind.Unspecified).AddTicks(4208), "Impedit delectus.", 27, 1, 1 },
                    { 26, new DateTime(2019, 6, 4, 20, 31, 45, 400, DateTimeKind.Unspecified).AddTicks(4074), "Accusantium qui ut pariatur voluptatem repellat omnis.", new DateTime(2021, 7, 15, 23, 35, 15, 959, DateTimeKind.Unspecified).AddTicks(6486), "Iste ipsa.", 9, 3, 3 },
                    { 27, new DateTime(2019, 3, 13, 13, 31, 36, 32, DateTimeKind.Unspecified).AddTicks(6111), "Magni et nobis molestiae suscipit laboriosam tempore eveniet quod.", new DateTime(2021, 2, 15, 8, 16, 53, 725, DateTimeKind.Unspecified).AddTicks(6102), "Aut et.", 16, 2, 0 },
                    { 28, new DateTime(2019, 1, 24, 18, 43, 41, 791, DateTimeKind.Unspecified).AddTicks(1879), "Sint enim assumenda sunt libero.", new DateTime(2021, 5, 16, 5, 41, 35, 860, DateTimeKind.Unspecified).AddTicks(3826), "Blanditiis vero.", 36, 5, 0 },
                    { 29, new DateTime(2019, 3, 3, 21, 23, 6, 734, DateTimeKind.Unspecified).AddTicks(3328), "Hic nulla ut facilis libero accusamus quia est.", new DateTime(2022, 2, 18, 14, 3, 23, 144, DateTimeKind.Unspecified).AddTicks(8619), "Accusamus perspiciatis.", 38, 2, 1 },
                    { 30, new DateTime(2019, 9, 17, 18, 5, 58, 562, DateTimeKind.Unspecified).AddTicks(4658), "Cum qui nihil laboriosam facere et dolorem porro.", new DateTime(2021, 9, 14, 15, 27, 20, 425, DateTimeKind.Unspecified).AddTicks(7705), "Veniam quo.", 2, 5, 2 },
                    { 31, new DateTime(2019, 2, 22, 7, 32, 27, 908, DateTimeKind.Unspecified).AddTicks(3798), "Eaque enim officiis ad nihil cum ut.", new DateTime(2022, 12, 2, 13, 39, 7, 886, DateTimeKind.Unspecified).AddTicks(1963), "Et temporibus.", 27, 5, 0 },
                    { 32, new DateTime(2019, 2, 21, 17, 26, 5, 661, DateTimeKind.Unspecified).AddTicks(1234), "Modi ut rerum velit.", new DateTime(2021, 6, 8, 18, 14, 40, 777, DateTimeKind.Unspecified).AddTicks(894), "Rerum distinctio.", 38, 4, 3 },
                    { 33, new DateTime(2019, 5, 25, 18, 19, 59, 203, DateTimeKind.Unspecified).AddTicks(9169), "Sed dolorem corrupti.", new DateTime(2021, 5, 18, 22, 37, 48, 931, DateTimeKind.Unspecified).AddTicks(8707), "Enim blanditiis.", 2, 5, 0 },
                    { 34, new DateTime(2019, 1, 23, 20, 34, 48, 359, DateTimeKind.Unspecified).AddTicks(52), "Voluptas vel possimus facilis.", new DateTime(2022, 4, 7, 23, 41, 25, 238, DateTimeKind.Unspecified).AddTicks(1337), "Quia debitis.", 33, 1, 0 },
                    { 35, new DateTime(2019, 6, 14, 7, 45, 7, 166, DateTimeKind.Unspecified).AddTicks(6988), "Illo et et atque ea velit vel.", new DateTime(2022, 3, 9, 0, 56, 49, 796, DateTimeKind.Unspecified).AddTicks(2976), "Vel omnis.", 27, 5, 2 },
                    { 36, new DateTime(2019, 7, 26, 12, 35, 56, 920, DateTimeKind.Unspecified).AddTicks(7118), "Quae qui molestias sit veniam ad nemo.", new DateTime(2022, 4, 11, 4, 1, 18, 806, DateTimeKind.Unspecified).AddTicks(8363), "Qui ut.", 2, 2, 0 },
                    { 37, new DateTime(2019, 5, 29, 16, 26, 14, 494, DateTimeKind.Unspecified).AddTicks(5818), "Vitae similique totam impedit.", new DateTime(2022, 9, 25, 9, 59, 36, 530, DateTimeKind.Unspecified).AddTicks(5063), "Tenetur eligendi.", 47, 5, 0 }
                });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 38, new DateTime(2019, 2, 24, 15, 44, 44, 401, DateTimeKind.Unspecified).AddTicks(1775), "Vel voluptas vitae maxime veniam perspiciatis aut a voluptates esse.", new DateTime(2021, 8, 23, 18, 49, 8, 476, DateTimeKind.Unspecified).AddTicks(4254), "Nemo molestias.", 45, 2, 0 },
                    { 39, new DateTime(2019, 8, 31, 20, 2, 57, 672, DateTimeKind.Unspecified).AddTicks(7497), "Distinctio nobis numquam nostrum.", new DateTime(2022, 4, 16, 5, 44, 48, 382, DateTimeKind.Unspecified).AddTicks(2360), "Omnis veniam.", 9, 3, 1 },
                    { 40, new DateTime(2019, 4, 15, 9, 46, 55, 316, DateTimeKind.Unspecified).AddTicks(495), "Qui cumque inventore est veritatis praesentium unde sit.", new DateTime(2022, 9, 26, 5, 53, 18, 940, DateTimeKind.Unspecified).AddTicks(4322), "Incidunt id.", 19, 5, 3 },
                    { 41, new DateTime(2019, 4, 4, 15, 3, 4, 390, DateTimeKind.Unspecified).AddTicks(6213), "Qui assumenda blanditiis omnis nesciunt eum deleniti molestiae eos ut.", new DateTime(2022, 9, 10, 10, 4, 10, 924, DateTimeKind.Unspecified).AddTicks(1921), "Illum rerum.", 18, 2, 1 },
                    { 42, new DateTime(2019, 5, 11, 18, 34, 28, 335, DateTimeKind.Unspecified).AddTicks(2207), "Eveniet distinctio dolorem dicta odit magnam accusamus nulla.", new DateTime(2022, 3, 3, 14, 6, 10, 498, DateTimeKind.Unspecified).AddTicks(8939), "Dolor omnis.", 32, 3, 2 },
                    { 43, new DateTime(2019, 9, 13, 2, 4, 12, 495, DateTimeKind.Unspecified).AddTicks(5188), "Officia necessitatibus repellat delectus et.", new DateTime(2022, 2, 18, 7, 54, 33, 186, DateTimeKind.Unspecified).AddTicks(4010), "Perspiciatis ratione.", 37, 3, 2 },
                    { 44, new DateTime(2019, 5, 6, 13, 18, 38, 135, DateTimeKind.Unspecified).AddTicks(6482), "Doloribus reiciendis veniam sint fugit sit assumenda.", new DateTime(2022, 2, 20, 12, 52, 40, 445, DateTimeKind.Unspecified).AddTicks(3179), "Nemo accusamus.", 17, 2, 1 },
                    { 45, new DateTime(2019, 12, 18, 17, 53, 51, 88, DateTimeKind.Unspecified).AddTicks(2316), "Sapiente dolore sequi aut.", new DateTime(2022, 6, 24, 14, 36, 4, 521, DateTimeKind.Unspecified).AddTicks(2606), "In labore.", 13, 5, 0 },
                    { 46, new DateTime(2019, 12, 26, 20, 57, 58, 357, DateTimeKind.Unspecified).AddTicks(9864), "Sed consectetur nisi reprehenderit sapiente voluptatem.", new DateTime(2021, 4, 21, 14, 27, 44, 680, DateTimeKind.Unspecified).AddTicks(4286), "Ut facere.", 43, 1, 1 },
                    { 47, new DateTime(2019, 7, 14, 17, 13, 37, 203, DateTimeKind.Unspecified).AddTicks(4083), "Sit debitis occaecati voluptatibus expedita autem et ullam voluptatem at.", new DateTime(2021, 11, 22, 6, 57, 42, 376, DateTimeKind.Unspecified).AddTicks(5697), "Ipsam incidunt.", 36, 4, 3 },
                    { 48, new DateTime(2019, 9, 7, 4, 21, 25, 513, DateTimeKind.Unspecified).AddTicks(1849), "Ipsum sapiente dignissimos illo ea molestiae asperiores facere quo ratione.", new DateTime(2021, 8, 13, 20, 22, 34, 30, DateTimeKind.Unspecified).AddTicks(8414), "Deleniti distinctio.", 3, 2, 0 },
                    { 49, new DateTime(2019, 6, 14, 22, 11, 54, 918, DateTimeKind.Unspecified).AddTicks(1613), "Labore tempore laudantium.", new DateTime(2021, 4, 8, 16, 47, 40, 158, DateTimeKind.Unspecified).AddTicks(5380), "Atque quae.", 12, 3, 2 },
                    { 50, new DateTime(2019, 10, 15, 22, 1, 11, 585, DateTimeKind.Unspecified).AddTicks(9624), "Qui hic delectus repellendus et deleniti ullam velit dicta omnis.", new DateTime(2021, 12, 6, 13, 4, 41, 736, DateTimeKind.Unspecified).AddTicks(903), "Tempora quia.", 1, 3, 3 },
                    { 51, new DateTime(2019, 5, 23, 9, 52, 46, 581, DateTimeKind.Unspecified).AddTicks(8432), "Unde qui et possimus debitis dolorem alias enim facilis.", new DateTime(2022, 4, 12, 18, 49, 32, 666, DateTimeKind.Unspecified).AddTicks(5180), "Et recusandae.", 12, 4, 3 },
                    { 52, new DateTime(2019, 1, 16, 3, 27, 8, 177, DateTimeKind.Unspecified).AddTicks(7142), "Nihil sit illum quo asperiores molestiae quam nesciunt illum quo.", new DateTime(2021, 5, 22, 1, 16, 27, 32, DateTimeKind.Unspecified).AddTicks(1189), "Assumenda voluptas.", 9, 1, 1 },
                    { 53, new DateTime(2019, 3, 19, 23, 49, 15, 566, DateTimeKind.Unspecified).AddTicks(7988), "Saepe ipsa tenetur assumenda aliquam adipisci fugit.", new DateTime(2022, 7, 7, 23, 17, 14, 659, DateTimeKind.Unspecified).AddTicks(8621), "Dolores non.", 25, 1, 1 },
                    { 54, new DateTime(2019, 5, 25, 15, 12, 15, 271, DateTimeKind.Unspecified).AddTicks(8281), "Itaque dolorem molestias fugiat et odit nesciunt cumque architecto.", new DateTime(2021, 2, 26, 8, 54, 32, 487, DateTimeKind.Unspecified).AddTicks(8119), "Aliquid ut.", 24, 2, 1 },
                    { 55, new DateTime(2019, 1, 25, 3, 40, 46, 587, DateTimeKind.Unspecified).AddTicks(9050), "Aut dolores velit sunt excepturi.", new DateTime(2021, 5, 6, 11, 14, 4, 53, DateTimeKind.Unspecified).AddTicks(856), "Quia aliquid.", 16, 4, 0 },
                    { 56, new DateTime(2019, 11, 13, 16, 35, 50, 33, DateTimeKind.Unspecified).AddTicks(1344), "Ea doloremque consequatur tenetur adipisci labore porro non libero deserunt.", new DateTime(2022, 8, 4, 17, 2, 10, 698, DateTimeKind.Unspecified).AddTicks(5872), "Quo ducimus.", 27, 4, 1 },
                    { 57, new DateTime(2019, 9, 14, 21, 50, 15, 991, DateTimeKind.Unspecified).AddTicks(735), "Consequatur porro enim voluptas delectus qui aut maxime delectus ducimus.", new DateTime(2021, 9, 8, 17, 29, 7, 805, DateTimeKind.Unspecified).AddTicks(7237), "Aperiam et.", 12, 4, 3 },
                    { 58, new DateTime(2019, 8, 3, 6, 41, 54, 437, DateTimeKind.Unspecified).AddTicks(332), "Soluta atque perferendis labore quo dolores sunt ducimus.", new DateTime(2022, 7, 17, 7, 0, 58, 776, DateTimeKind.Unspecified).AddTicks(7021), "Et iusto.", 10, 3, 2 },
                    { 59, new DateTime(2019, 10, 2, 16, 57, 29, 286, DateTimeKind.Unspecified).AddTicks(3066), "Atque quidem itaque illo vitae corrupti sit sit cupiditate perferendis.", new DateTime(2021, 2, 16, 17, 34, 44, 440, DateTimeKind.Unspecified).AddTicks(3101), "Dolores optio.", 40, 3, 2 },
                    { 60, new DateTime(2019, 12, 11, 5, 44, 51, 405, DateTimeKind.Unspecified).AddTicks(2344), "In dicta error qui ipsam enim quia odio.", new DateTime(2022, 11, 25, 22, 6, 40, 815, DateTimeKind.Unspecified).AddTicks(1685), "Quos incidunt.", 7, 1, 1 },
                    { 61, new DateTime(2019, 9, 21, 13, 5, 16, 758, DateTimeKind.Unspecified).AddTicks(2979), "Rerum quia nesciunt ipsum deleniti eum dolores.", new DateTime(2022, 11, 21, 11, 36, 12, 662, DateTimeKind.Unspecified).AddTicks(139), "Alias reiciendis.", 16, 5, 2 },
                    { 62, new DateTime(2019, 7, 20, 20, 45, 35, 533, DateTimeKind.Unspecified).AddTicks(9807), "Tempora dolores officiis velit doloribus aut id aperiam quia.", new DateTime(2021, 11, 30, 19, 26, 23, 58, DateTimeKind.Unspecified).AddTicks(2530), "Voluptas sint.", 39, 4, 1 },
                    { 63, new DateTime(2019, 12, 23, 21, 3, 44, 530, DateTimeKind.Unspecified).AddTicks(5761), "Maiores tenetur et ipsa modi voluptatum.", new DateTime(2021, 3, 25, 20, 10, 4, 495, DateTimeKind.Unspecified).AddTicks(59), "Impedit quidem.", 38, 3, 3 },
                    { 64, new DateTime(2019, 2, 4, 6, 53, 1, 250, DateTimeKind.Unspecified).AddTicks(5270), "In autem nostrum eligendi nostrum.", new DateTime(2022, 11, 10, 21, 3, 28, 704, DateTimeKind.Unspecified).AddTicks(9718), "Dolor consequatur.", 10, 3, 2 },
                    { 65, new DateTime(2019, 2, 27, 18, 20, 42, 335, DateTimeKind.Unspecified).AddTicks(9084), "Commodi amet exercitationem iure assumenda nesciunt atque.", new DateTime(2022, 4, 23, 12, 12, 9, 572, DateTimeKind.Unspecified).AddTicks(5523), "Velit vel.", 39, 5, 3 },
                    { 66, new DateTime(2019, 5, 28, 11, 8, 58, 202, DateTimeKind.Unspecified).AddTicks(2799), "Est provident vel voluptas voluptatem rerum in.", new DateTime(2022, 7, 28, 2, 18, 11, 81, DateTimeKind.Unspecified).AddTicks(4538), "Eveniet quia.", 15, 4, 2 },
                    { 67, new DateTime(2019, 2, 13, 21, 23, 9, 759, DateTimeKind.Unspecified).AddTicks(3438), "Facere est voluptatibus corrupti et quas autem eum similique excepturi.", new DateTime(2022, 2, 24, 12, 8, 35, 367, DateTimeKind.Unspecified).AddTicks(8547), "Et aut.", 48, 1, 0 },
                    { 68, new DateTime(2019, 9, 10, 19, 59, 59, 17, DateTimeKind.Unspecified).AddTicks(8450), "Veniam officiis molestias ut vitae nemo labore quos vel maxime.", new DateTime(2022, 6, 16, 22, 40, 26, 490, DateTimeKind.Unspecified).AddTicks(1900), "Minima dolor.", 42, 2, 3 },
                    { 69, new DateTime(2019, 2, 15, 3, 25, 35, 69, DateTimeKind.Unspecified).AddTicks(2287), "In quidem odit nulla veniam perspiciatis magni provident ducimus.", new DateTime(2022, 7, 1, 6, 32, 35, 717, DateTimeKind.Unspecified).AddTicks(3411), "Omnis velit.", 11, 5, 0 },
                    { 70, new DateTime(2019, 12, 1, 17, 1, 12, 24, DateTimeKind.Unspecified).AddTicks(8249), "Totam autem et commodi cupiditate.", new DateTime(2021, 7, 20, 12, 30, 44, 115, DateTimeKind.Unspecified).AddTicks(6225), "Est nulla.", 16, 3, 2 },
                    { 71, new DateTime(2019, 12, 27, 16, 46, 29, 117, DateTimeKind.Unspecified).AddTicks(2950), "Asperiores architecto quis ullam et.", new DateTime(2022, 11, 20, 15, 49, 41, 399, DateTimeKind.Unspecified).AddTicks(5259), "Vitae aliquam.", 26, 5, 3 },
                    { 72, new DateTime(2019, 9, 18, 3, 54, 44, 860, DateTimeKind.Unspecified).AddTicks(6109), "Aut explicabo omnis quia inventore reiciendis qui eum debitis qui.", new DateTime(2022, 8, 28, 14, 10, 34, 46, DateTimeKind.Unspecified).AddTicks(4182), "Aut nam.", 27, 5, 1 },
                    { 73, new DateTime(2019, 3, 25, 21, 59, 1, 460, DateTimeKind.Unspecified).AddTicks(6299), "Ut vel ut dolores.", new DateTime(2022, 8, 1, 3, 12, 10, 57, DateTimeKind.Unspecified).AddTicks(3628), "Est dolore.", 16, 4, 0 },
                    { 74, new DateTime(2019, 5, 23, 6, 47, 45, 737, DateTimeKind.Unspecified).AddTicks(5977), "Voluptatibus in explicabo.", new DateTime(2022, 3, 14, 8, 33, 50, 45, DateTimeKind.Unspecified).AddTicks(6766), "Vel dolorem.", 47, 2, 1 },
                    { 75, new DateTime(2019, 7, 7, 2, 22, 33, 600, DateTimeKind.Unspecified).AddTicks(8987), "Eligendi et aut officia vel.", new DateTime(2021, 4, 3, 21, 37, 11, 172, DateTimeKind.Unspecified).AddTicks(9207), "Culpa laborum.", 44, 2, 1 },
                    { 76, new DateTime(2019, 10, 30, 6, 54, 54, 937, DateTimeKind.Unspecified).AddTicks(4179), "Beatae amet quia et et commodi accusamus.", new DateTime(2021, 9, 18, 2, 47, 11, 814, DateTimeKind.Unspecified).AddTicks(8012), "Autem impedit.", 28, 1, 2 },
                    { 77, new DateTime(2019, 9, 3, 5, 27, 4, 209, DateTimeKind.Unspecified).AddTicks(5273), "Quam a repudiandae consequatur est sequi delectus itaque.", new DateTime(2021, 12, 2, 17, 4, 40, 775, DateTimeKind.Unspecified).AddTicks(6496), "Iste magni.", 27, 5, 2 },
                    { 78, new DateTime(2019, 1, 31, 3, 46, 19, 292, DateTimeKind.Unspecified).AddTicks(6569), "Voluptate repellat alias.", new DateTime(2022, 2, 12, 13, 25, 19, 473, DateTimeKind.Unspecified).AddTicks(8462), "Unde molestias.", 48, 3, 1 },
                    { 79, new DateTime(2019, 10, 24, 18, 46, 28, 714, DateTimeKind.Unspecified).AddTicks(7955), "Laborum dignissimos enim quo molestiae et voluptatem accusantium aspernatur possimus.", new DateTime(2021, 12, 30, 23, 14, 59, 807, DateTimeKind.Unspecified).AddTicks(1355), "Eligendi praesentium.", 27, 4, 3 }
                });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 80, new DateTime(2019, 10, 27, 22, 46, 47, 392, DateTimeKind.Unspecified).AddTicks(2343), "Est ratione impedit quibusdam.", new DateTime(2021, 2, 5, 22, 8, 7, 770, DateTimeKind.Unspecified).AddTicks(8207), "Tempore hic.", 6, 5, 2 },
                    { 81, new DateTime(2019, 1, 31, 10, 48, 42, 447, DateTimeKind.Unspecified).AddTicks(7377), "Voluptatem ut autem voluptate ut iste perferendis omnis eaque.", new DateTime(2022, 3, 7, 22, 56, 6, 63, DateTimeKind.Unspecified).AddTicks(7734), "Rem architecto.", 48, 3, 0 },
                    { 82, new DateTime(2019, 4, 18, 8, 4, 56, 881, DateTimeKind.Unspecified).AddTicks(2610), "Velit laborum distinctio laudantium deleniti quos natus odit quisquam voluptatem.", new DateTime(2021, 1, 2, 9, 35, 49, 933, DateTimeKind.Unspecified).AddTicks(5161), "In iusto.", 3, 5, 0 },
                    { 83, new DateTime(2019, 2, 3, 5, 30, 39, 706, DateTimeKind.Unspecified).AddTicks(9477), "Est ratione et minima beatae neque.", new DateTime(2021, 4, 22, 8, 44, 21, 511, DateTimeKind.Unspecified).AddTicks(7063), "Vel quasi.", 21, 5, 2 },
                    { 84, new DateTime(2019, 6, 25, 10, 31, 57, 390, DateTimeKind.Unspecified).AddTicks(8308), "Excepturi et ipsum at.", new DateTime(2022, 5, 25, 10, 23, 55, 444, DateTimeKind.Unspecified).AddTicks(6004), "Quia sint.", 25, 5, 1 },
                    { 85, new DateTime(2019, 8, 18, 9, 46, 42, 572, DateTimeKind.Unspecified).AddTicks(589), "Nulla maxime in sed exercitationem.", new DateTime(2022, 2, 22, 3, 8, 3, 554, DateTimeKind.Unspecified).AddTicks(6276), "Ut odit.", 24, 5, 1 },
                    { 86, new DateTime(2019, 7, 5, 15, 26, 34, 202, DateTimeKind.Unspecified).AddTicks(6551), "Non voluptatem quae.", new DateTime(2021, 11, 2, 21, 55, 42, 584, DateTimeKind.Unspecified).AddTicks(155), "A labore.", 8, 3, 1 },
                    { 87, new DateTime(2019, 4, 13, 21, 21, 13, 800, DateTimeKind.Unspecified).AddTicks(9431), "Sit nisi et quo.", new DateTime(2022, 11, 30, 15, 36, 21, 577, DateTimeKind.Unspecified).AddTicks(6822), "Repellat et.", 2, 5, 1 },
                    { 88, new DateTime(2019, 1, 23, 20, 50, 26, 770, DateTimeKind.Unspecified).AddTicks(762), "Earum rerum qui nam consequatur et fugiat commodi.", new DateTime(2021, 9, 12, 2, 13, 9, 470, DateTimeKind.Unspecified).AddTicks(1084), "Reprehenderit est.", 38, 3, 1 },
                    { 89, new DateTime(2019, 11, 14, 23, 55, 20, 858, DateTimeKind.Unspecified).AddTicks(6013), "Asperiores explicabo labore nulla autem vel molestiae a.", new DateTime(2022, 2, 23, 11, 26, 27, 771, DateTimeKind.Unspecified).AddTicks(5457), "Sunt doloremque.", 5, 5, 0 },
                    { 90, new DateTime(2019, 7, 30, 1, 13, 50, 121, DateTimeKind.Unspecified).AddTicks(7270), "Qui quod maxime quia at quaerat dolorem ut et.", new DateTime(2022, 5, 12, 9, 31, 22, 348, DateTimeKind.Unspecified).AddTicks(1287), "Perspiciatis eaque.", 25, 2, 2 },
                    { 91, new DateTime(2019, 5, 9, 21, 33, 43, 616, DateTimeKind.Unspecified).AddTicks(5370), "Voluptas ut cumque.", new DateTime(2022, 11, 7, 11, 25, 45, 356, DateTimeKind.Unspecified).AddTicks(2559), "Tempora doloremque.", 33, 3, 0 },
                    { 92, new DateTime(2019, 7, 19, 13, 50, 14, 860, DateTimeKind.Unspecified).AddTicks(9964), "Eveniet ea nihil voluptatem sit et.", new DateTime(2021, 1, 28, 11, 34, 51, 284, DateTimeKind.Unspecified).AddTicks(5650), "Natus ea.", 34, 5, 1 },
                    { 93, new DateTime(2019, 4, 3, 12, 31, 13, 401, DateTimeKind.Unspecified).AddTicks(9986), "Similique iusto ratione voluptate nesciunt.", new DateTime(2021, 10, 24, 6, 51, 36, 169, DateTimeKind.Unspecified).AddTicks(8546), "Aperiam sint.", 48, 3, 3 },
                    { 94, new DateTime(2019, 8, 22, 0, 53, 28, 763, DateTimeKind.Unspecified).AddTicks(172), "Veritatis autem aut nulla voluptas veniam et commodi quam quo.", new DateTime(2021, 8, 29, 17, 44, 35, 659, DateTimeKind.Unspecified).AddTicks(4184), "Libero voluptates.", 16, 3, 1 },
                    { 95, new DateTime(2019, 3, 10, 7, 41, 42, 507, DateTimeKind.Unspecified).AddTicks(8405), "Optio temporibus dolores animi.", new DateTime(2021, 6, 11, 9, 38, 20, 918, DateTimeKind.Unspecified).AddTicks(4433), "Facilis dolore.", 21, 5, 3 },
                    { 96, new DateTime(2019, 2, 6, 4, 25, 13, 411, DateTimeKind.Unspecified).AddTicks(2134), "Eveniet maiores et eos aut eos voluptatem ducimus omnis ut.", new DateTime(2021, 10, 26, 3, 28, 29, 105, DateTimeKind.Unspecified).AddTicks(8926), "Non veritatis.", 20, 1, 1 },
                    { 97, new DateTime(2019, 2, 16, 13, 58, 50, 941, DateTimeKind.Unspecified).AddTicks(7608), "Inventore iste quis.", new DateTime(2022, 8, 12, 8, 45, 30, 935, DateTimeKind.Unspecified).AddTicks(7903), "Similique eos.", 50, 1, 3 },
                    { 98, new DateTime(2019, 4, 17, 0, 20, 42, 469, DateTimeKind.Unspecified).AddTicks(1806), "Voluptatem officia dolores eos ut sit totam.", new DateTime(2022, 6, 11, 20, 15, 18, 304, DateTimeKind.Unspecified).AddTicks(2160), "Tempora quia.", 7, 2, 1 },
                    { 99, new DateTime(2019, 7, 5, 16, 47, 41, 617, DateTimeKind.Unspecified).AddTicks(7080), "Molestias ad voluptates ut est quasi nesciunt quia et sapiente.", new DateTime(2021, 3, 28, 0, 20, 44, 958, DateTimeKind.Unspecified).AddTicks(3666), "Velit necessitatibus.", 22, 4, 2 },
                    { 100, new DateTime(2019, 7, 6, 18, 17, 53, 949, DateTimeKind.Unspecified).AddTicks(4882), "Velit voluptatum incidunt et molestiae sed qui placeat explicabo reprehenderit.", new DateTime(2022, 4, 18, 19, 11, 45, 973, DateTimeKind.Unspecified).AddTicks(649), "Ipsam enim.", 46, 5, 0 },
                    { 101, new DateTime(2019, 4, 27, 20, 56, 25, 904, DateTimeKind.Unspecified).AddTicks(6367), "Non ex iste laboriosam consequatur labore.", new DateTime(2021, 9, 20, 23, 23, 32, 123, DateTimeKind.Unspecified).AddTicks(5750), "Id consequuntur.", 33, 2, 2 },
                    { 102, new DateTime(2019, 10, 22, 6, 46, 45, 872, DateTimeKind.Unspecified).AddTicks(8441), "Commodi nulla quidem dolorem sint explicabo aliquam ducimus.", new DateTime(2021, 9, 9, 7, 16, 7, 318, DateTimeKind.Unspecified).AddTicks(7153), "Eum pariatur.", 18, 1, 3 },
                    { 103, new DateTime(2019, 8, 4, 3, 34, 25, 779, DateTimeKind.Unspecified).AddTicks(2261), "Et qui sint vero et.", new DateTime(2022, 9, 12, 10, 0, 7, 530, DateTimeKind.Unspecified).AddTicks(2826), "Nisi ipsam.", 17, 1, 2 },
                    { 104, new DateTime(2019, 4, 11, 12, 56, 18, 329, DateTimeKind.Unspecified).AddTicks(1586), "Iste sed ipsam aliquam aut nisi eum aut quia consectetur.", new DateTime(2022, 2, 20, 3, 40, 8, 48, DateTimeKind.Unspecified).AddTicks(311), "Soluta molestiae.", 28, 2, 2 },
                    { 105, new DateTime(2019, 6, 26, 1, 35, 25, 205, DateTimeKind.Unspecified).AddTicks(7966), "Facilis odit ut in in voluptatem tempora.", new DateTime(2021, 8, 23, 10, 7, 32, 649, DateTimeKind.Unspecified).AddTicks(9351), "Ratione voluptates.", 6, 1, 2 },
                    { 106, new DateTime(2019, 3, 22, 5, 50, 11, 870, DateTimeKind.Unspecified).AddTicks(404), "Sit exercitationem reiciendis quis illum.", new DateTime(2022, 2, 5, 10, 2, 46, 351, DateTimeKind.Unspecified).AddTicks(7352), "Cum corporis.", 37, 4, 1 },
                    { 107, new DateTime(2019, 4, 29, 0, 35, 35, 159, DateTimeKind.Unspecified).AddTicks(5811), "Nisi unde in minus modi dolores repudiandae.", new DateTime(2022, 5, 22, 16, 2, 6, 213, DateTimeKind.Unspecified).AddTicks(3542), "Ut sed.", 48, 5, 1 },
                    { 108, new DateTime(2019, 12, 21, 10, 52, 57, 896, DateTimeKind.Unspecified).AddTicks(2778), "Eum nesciunt et ut.", new DateTime(2022, 6, 8, 11, 33, 44, 385, DateTimeKind.Unspecified).AddTicks(7453), "Necessitatibus dolorem.", 12, 4, 3 },
                    { 109, new DateTime(2019, 10, 25, 7, 23, 4, 790, DateTimeKind.Unspecified).AddTicks(7335), "Aut quae autem.", new DateTime(2022, 10, 27, 18, 8, 23, 475, DateTimeKind.Unspecified).AddTicks(870), "Laudantium autem.", 38, 5, 2 },
                    { 110, new DateTime(2019, 4, 1, 3, 52, 13, 996, DateTimeKind.Unspecified).AddTicks(7007), "Sapiente molestiae voluptatem qui consectetur ab dolore qui ducimus sed.", new DateTime(2022, 11, 4, 2, 2, 2, 74, DateTimeKind.Unspecified).AddTicks(7670), "Magni delectus.", 2, 1, 0 },
                    { 111, new DateTime(2019, 3, 19, 23, 6, 8, 885, DateTimeKind.Unspecified).AddTicks(2503), "Cumque dolor nihil aperiam aut aperiam omnis sequi expedita mollitia.", new DateTime(2022, 4, 17, 9, 33, 3, 785, DateTimeKind.Unspecified).AddTicks(1621), "Illum dolorem.", 29, 2, 0 },
                    { 112, new DateTime(2019, 3, 2, 16, 25, 32, 653, DateTimeKind.Unspecified).AddTicks(1522), "Nam repellat provident.", new DateTime(2022, 5, 30, 4, 40, 52, 762, DateTimeKind.Unspecified).AddTicks(4776), "In perspiciatis.", 21, 3, 0 },
                    { 113, new DateTime(2019, 8, 15, 12, 59, 14, 200, DateTimeKind.Unspecified).AddTicks(8433), "Voluptatem reprehenderit consequuntur ut tempora dolorem numquam quia provident aspernatur.", new DateTime(2022, 10, 28, 5, 17, 14, 847, DateTimeKind.Unspecified).AddTicks(2767), "Debitis ab.", 44, 3, 1 },
                    { 114, new DateTime(2019, 8, 23, 17, 59, 17, 673, DateTimeKind.Unspecified).AddTicks(4471), "Animi velit sint reprehenderit dolores ad quaerat iusto fugit libero.", new DateTime(2021, 6, 24, 11, 15, 33, 940, DateTimeKind.Unspecified).AddTicks(8862), "Quibusdam ea.", 45, 4, 3 },
                    { 115, new DateTime(2019, 8, 18, 4, 26, 40, 384, DateTimeKind.Unspecified).AddTicks(4518), "Est est sint nostrum aut itaque rerum cumque odio.", new DateTime(2021, 9, 19, 13, 3, 54, 988, DateTimeKind.Unspecified).AddTicks(1644), "Commodi vero.", 1, 3, 3 },
                    { 116, new DateTime(2019, 10, 16, 7, 42, 17, 895, DateTimeKind.Unspecified).AddTicks(9706), "Nulla voluptas rerum pariatur autem quo qui praesentium sit.", new DateTime(2021, 10, 14, 16, 33, 21, 210, DateTimeKind.Unspecified).AddTicks(9711), "Quis dolores.", 36, 5, 2 },
                    { 117, new DateTime(2019, 10, 7, 10, 9, 47, 503, DateTimeKind.Unspecified).AddTicks(2195), "Quidem voluptas nihil distinctio suscipit neque expedita beatae consequatur.", new DateTime(2022, 5, 15, 18, 40, 24, 117, DateTimeKind.Unspecified).AddTicks(167), "Rerum labore.", 43, 3, 1 },
                    { 118, new DateTime(2019, 3, 24, 4, 55, 6, 520, DateTimeKind.Unspecified).AddTicks(6305), "Aperiam quam ducimus.", new DateTime(2021, 2, 14, 1, 39, 22, 295, DateTimeKind.Unspecified).AddTicks(447), "Animi veniam.", 29, 2, 2 },
                    { 119, new DateTime(2019, 7, 12, 20, 39, 39, 363, DateTimeKind.Unspecified).AddTicks(4705), "Maxime ea qui quasi molestiae perspiciatis mollitia quo alias magni.", new DateTime(2021, 3, 12, 21, 38, 51, 819, DateTimeKind.Unspecified).AddTicks(1687), "Possimus recusandae.", 43, 4, 0 },
                    { 120, new DateTime(2019, 6, 14, 10, 42, 30, 477, DateTimeKind.Unspecified).AddTicks(2414), "Et doloremque aperiam iusto velit voluptatum.", new DateTime(2021, 6, 29, 17, 31, 58, 282, DateTimeKind.Unspecified).AddTicks(9131), "Et velit.", 37, 3, 3 },
                    { 121, new DateTime(2019, 8, 31, 12, 44, 51, 805, DateTimeKind.Unspecified).AddTicks(6460), "Accusantium eveniet corrupti id autem quam beatae recusandae qui vel.", new DateTime(2021, 11, 4, 5, 17, 50, 678, DateTimeKind.Unspecified).AddTicks(6385), "Iusto a.", 47, 1, 0 }
                });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 122, new DateTime(2019, 4, 3, 4, 31, 26, 126, DateTimeKind.Unspecified).AddTicks(2489), "Eos corrupti ea.", new DateTime(2021, 7, 12, 17, 2, 46, 156, DateTimeKind.Unspecified).AddTicks(3803), "Et quis.", 32, 1, 2 },
                    { 123, new DateTime(2019, 7, 4, 17, 33, 49, 198, DateTimeKind.Unspecified).AddTicks(3919), "Et necessitatibus itaque qui est corrupti illo.", new DateTime(2022, 11, 26, 11, 32, 29, 409, DateTimeKind.Unspecified).AddTicks(6245), "Nemo facere.", 35, 5, 3 },
                    { 124, new DateTime(2019, 6, 19, 15, 31, 32, 713, DateTimeKind.Unspecified).AddTicks(46), "Facilis nihil laboriosam sapiente iusto magni officiis labore ea fugiat.", new DateTime(2022, 9, 25, 10, 46, 29, 358, DateTimeKind.Unspecified).AddTicks(1554), "Atque magnam.", 36, 3, 0 },
                    { 125, new DateTime(2019, 7, 12, 18, 28, 47, 358, DateTimeKind.Unspecified).AddTicks(5855), "Quam iste dolore enim veniam.", new DateTime(2021, 9, 1, 11, 1, 26, 412, DateTimeKind.Unspecified).AddTicks(9775), "Est omnis.", 6, 2, 0 },
                    { 126, new DateTime(2019, 6, 17, 17, 18, 33, 934, DateTimeKind.Unspecified).AddTicks(6327), "Est quidem a et illo mollitia aut et.", new DateTime(2022, 8, 26, 5, 13, 35, 733, DateTimeKind.Unspecified).AddTicks(1900), "Est doloribus.", 23, 2, 2 },
                    { 127, new DateTime(2019, 5, 30, 20, 25, 22, 876, DateTimeKind.Unspecified).AddTicks(1891), "Ut necessitatibus distinctio.", new DateTime(2022, 2, 27, 5, 7, 32, 599, DateTimeKind.Unspecified).AddTicks(8816), "Omnis iusto.", 36, 3, 2 },
                    { 128, new DateTime(2019, 4, 21, 19, 39, 33, 469, DateTimeKind.Unspecified).AddTicks(8503), "Velit est autem recusandae mollitia illum sed explicabo et rerum.", new DateTime(2021, 6, 24, 23, 41, 38, 790, DateTimeKind.Unspecified).AddTicks(7902), "Ut consequatur.", 6, 2, 3 },
                    { 129, new DateTime(2019, 11, 26, 3, 54, 32, 413, DateTimeKind.Unspecified).AddTicks(8684), "Sapiente in minima.", new DateTime(2021, 11, 4, 18, 35, 29, 532, DateTimeKind.Unspecified).AddTicks(6375), "Enim et.", 8, 5, 2 },
                    { 130, new DateTime(2019, 4, 22, 8, 21, 2, 814, DateTimeKind.Unspecified).AddTicks(4164), "Ipsum incidunt laudantium et exercitationem reprehenderit unde quasi aut architecto.", new DateTime(2021, 1, 15, 6, 5, 39, 478, DateTimeKind.Unspecified).AddTicks(7800), "Numquam et.", 8, 3, 0 },
                    { 131, new DateTime(2019, 5, 13, 0, 15, 31, 489, DateTimeKind.Unspecified).AddTicks(5531), "Cupiditate explicabo dolor debitis modi quos eum quia.", new DateTime(2021, 11, 16, 0, 25, 37, 481, DateTimeKind.Unspecified).AddTicks(7918), "Repudiandae nulla.", 11, 4, 3 },
                    { 132, new DateTime(2019, 11, 27, 2, 26, 22, 981, DateTimeKind.Unspecified).AddTicks(4130), "Nobis nam corporis debitis.", new DateTime(2021, 2, 3, 3, 27, 10, 91, DateTimeKind.Unspecified).AddTicks(1827), "Eius est.", 14, 3, 2 },
                    { 133, new DateTime(2019, 3, 17, 21, 47, 28, 582, DateTimeKind.Unspecified).AddTicks(9976), "Ex autem quibusdam qui vitae qui voluptas.", new DateTime(2022, 7, 20, 22, 29, 44, 127, DateTimeKind.Unspecified).AddTicks(5467), "Qui aut.", 31, 4, 1 },
                    { 134, new DateTime(2019, 8, 8, 13, 26, 10, 441, DateTimeKind.Unspecified).AddTicks(7094), "Id quidem asperiores vel.", new DateTime(2022, 11, 15, 23, 13, 40, 983, DateTimeKind.Unspecified).AddTicks(8053), "Consequatur nihil.", 14, 1, 0 },
                    { 135, new DateTime(2019, 8, 11, 5, 10, 24, 348, DateTimeKind.Unspecified).AddTicks(6514), "Qui deserunt sit sunt eos aut aut.", new DateTime(2021, 12, 25, 23, 30, 26, 105, DateTimeKind.Unspecified).AddTicks(3990), "Non repudiandae.", 24, 2, 3 },
                    { 136, new DateTime(2019, 4, 13, 14, 39, 5, 666, DateTimeKind.Unspecified).AddTicks(4270), "Praesentium rem dolores qui eius blanditiis inventore ex.", new DateTime(2021, 1, 26, 1, 35, 21, 168, DateTimeKind.Unspecified).AddTicks(6143), "Qui eveniet.", 27, 4, 3 },
                    { 137, new DateTime(2019, 2, 21, 7, 12, 44, 946, DateTimeKind.Unspecified).AddTicks(5409), "Error dolorum eum ex neque voluptatem culpa nam fuga ipsam.", new DateTime(2022, 12, 8, 3, 54, 33, 893, DateTimeKind.Unspecified).AddTicks(2420), "Ut temporibus.", 19, 1, 2 },
                    { 138, new DateTime(2019, 3, 7, 5, 6, 49, 647, DateTimeKind.Unspecified).AddTicks(2096), "Tempore mollitia quo sit dolorem accusantium placeat error et.", new DateTime(2022, 10, 22, 19, 37, 41, 236, DateTimeKind.Unspecified).AddTicks(6309), "Qui et.", 25, 3, 2 },
                    { 139, new DateTime(2019, 7, 26, 21, 25, 43, 48, DateTimeKind.Unspecified).AddTicks(6004), "Porro pariatur quasi quis minima et sapiente omnis.", new DateTime(2021, 9, 8, 17, 51, 34, 663, DateTimeKind.Unspecified).AddTicks(6239), "Et natus.", 41, 2, 0 },
                    { 140, new DateTime(2019, 9, 23, 20, 44, 7, 202, DateTimeKind.Unspecified).AddTicks(1361), "Necessitatibus nihil quis unde reiciendis dolorem praesentium eaque similique.", new DateTime(2022, 8, 20, 3, 25, 15, 56, DateTimeKind.Unspecified).AddTicks(6358), "Optio velit.", 13, 5, 0 },
                    { 141, new DateTime(2019, 5, 19, 3, 50, 6, 167, DateTimeKind.Unspecified).AddTicks(4593), "Facilis dolores iusto sit non enim.", new DateTime(2021, 11, 29, 12, 11, 9, 863, DateTimeKind.Unspecified).AddTicks(8957), "Facilis eius.", 40, 3, 2 },
                    { 142, new DateTime(2019, 1, 17, 13, 31, 23, 324, DateTimeKind.Unspecified).AddTicks(7330), "Neque aperiam vel est corrupti nihil.", new DateTime(2022, 4, 10, 3, 55, 44, 526, DateTimeKind.Unspecified).AddTicks(3756), "Voluptatem error.", 44, 4, 2 },
                    { 143, new DateTime(2019, 3, 19, 7, 19, 48, 810, DateTimeKind.Unspecified).AddTicks(310), "Architecto molestias natus quo soluta officia.", new DateTime(2021, 10, 23, 0, 57, 6, 25, DateTimeKind.Unspecified).AddTicks(2801), "Pariatur animi.", 40, 5, 0 },
                    { 144, new DateTime(2019, 3, 11, 21, 27, 45, 840, DateTimeKind.Unspecified).AddTicks(2000), "Ducimus vitae saepe molestias vel rerum minus nesciunt placeat.", new DateTime(2022, 3, 11, 5, 45, 9, 517, DateTimeKind.Unspecified).AddTicks(4587), "Perferendis illum.", 31, 2, 3 },
                    { 145, new DateTime(2019, 7, 29, 2, 58, 22, 351, DateTimeKind.Unspecified).AddTicks(2700), "Ullam molestias et et nihil aut quaerat quam molestias.", new DateTime(2022, 11, 17, 5, 57, 39, 65, DateTimeKind.Unspecified).AddTicks(5174), "Architecto numquam.", 6, 3, 2 },
                    { 146, new DateTime(2019, 11, 7, 13, 59, 38, 580, DateTimeKind.Unspecified).AddTicks(8584), "Veritatis officia laudantium velit doloribus sit.", new DateTime(2022, 8, 7, 18, 3, 56, 11, DateTimeKind.Unspecified).AddTicks(5930), "Delectus quo.", 6, 5, 0 },
                    { 147, new DateTime(2019, 6, 19, 16, 33, 55, 682, DateTimeKind.Unspecified).AddTicks(7265), "Ipsam excepturi aut eum incidunt reiciendis dolores explicabo.", new DateTime(2022, 4, 26, 3, 57, 43, 633, DateTimeKind.Unspecified).AddTicks(8179), "Quia corporis.", 18, 1, 2 },
                    { 148, new DateTime(2019, 7, 6, 14, 58, 25, 361, DateTimeKind.Unspecified).AddTicks(7082), "Quia ut aperiam ipsam quia similique.", new DateTime(2022, 3, 7, 4, 58, 17, 143, DateTimeKind.Unspecified).AddTicks(4472), "Fuga est.", 22, 1, 3 },
                    { 149, new DateTime(2019, 8, 21, 10, 16, 17, 799, DateTimeKind.Unspecified).AddTicks(3542), "Non iusto autem qui.", new DateTime(2021, 8, 4, 0, 52, 33, 868, DateTimeKind.Unspecified).AddTicks(5372), "Mollitia ut.", 50, 5, 3 },
                    { 150, new DateTime(2019, 9, 13, 13, 55, 39, 134, DateTimeKind.Unspecified).AddTicks(14), "Dolorem qui excepturi earum.", new DateTime(2021, 1, 19, 2, 29, 57, 489, DateTimeKind.Unspecified).AddTicks(8010), "Adipisci atque.", 28, 3, 0 },
                    { 151, new DateTime(2019, 7, 20, 5, 19, 0, 154, DateTimeKind.Unspecified).AddTicks(7184), "Eos qui culpa.", new DateTime(2022, 1, 24, 22, 54, 14, 643, DateTimeKind.Unspecified).AddTicks(4550), "Sunt sed.", 17, 1, 0 },
                    { 152, new DateTime(2019, 8, 8, 9, 24, 2, 3, DateTimeKind.Unspecified).AddTicks(506), "Eos non ut et iste recusandae delectus.", new DateTime(2022, 3, 1, 1, 25, 11, 930, DateTimeKind.Unspecified).AddTicks(366), "Occaecati ut.", 19, 5, 0 },
                    { 153, new DateTime(2019, 8, 18, 23, 30, 31, 360, DateTimeKind.Unspecified).AddTicks(6603), "Possimus voluptatem quod voluptatem.", new DateTime(2022, 6, 16, 14, 42, 18, 802, DateTimeKind.Unspecified).AddTicks(8832), "Dicta dolorem.", 1, 4, 3 },
                    { 154, new DateTime(2019, 9, 22, 3, 38, 42, 57, DateTimeKind.Unspecified).AddTicks(3177), "Vero velit corrupti labore sunt et.", new DateTime(2021, 4, 2, 10, 10, 10, 919, DateTimeKind.Unspecified).AddTicks(683), "Et et.", 49, 1, 2 },
                    { 155, new DateTime(2019, 3, 18, 8, 23, 49, 770, DateTimeKind.Unspecified).AddTicks(2166), "Ex voluptate omnis fugit maiores labore quam et.", new DateTime(2021, 12, 30, 22, 52, 59, 831, DateTimeKind.Unspecified).AddTicks(6431), "Dolores sint.", 29, 1, 1 },
                    { 156, new DateTime(2019, 2, 16, 10, 57, 46, 701, DateTimeKind.Unspecified).AddTicks(1587), "Officiis ut modi et assumenda dolores voluptate.", new DateTime(2021, 7, 5, 21, 34, 56, 867, DateTimeKind.Unspecified).AddTicks(8765), "Et temporibus.", 30, 1, 3 },
                    { 157, new DateTime(2019, 10, 4, 21, 39, 22, 628, DateTimeKind.Unspecified).AddTicks(345), "Quis ducimus aut consequatur ipsa velit dolor adipisci adipisci reprehenderit.", new DateTime(2021, 7, 26, 19, 2, 19, 146, DateTimeKind.Unspecified).AddTicks(5082), "At tempore.", 16, 4, 1 },
                    { 158, new DateTime(2019, 12, 8, 0, 6, 7, 301, DateTimeKind.Unspecified).AddTicks(6864), "Harum voluptatum perspiciatis consequatur amet nesciunt aut et est.", new DateTime(2021, 12, 17, 19, 59, 24, 568, DateTimeKind.Unspecified).AddTicks(5425), "Possimus qui.", 28, 4, 1 },
                    { 159, new DateTime(2019, 10, 28, 18, 2, 50, 423, DateTimeKind.Unspecified).AddTicks(5780), "Quam sit eveniet velit aut et cum.", new DateTime(2022, 9, 28, 1, 10, 59, 195, DateTimeKind.Unspecified).AddTicks(5720), "Similique vel.", 30, 1, 2 },
                    { 160, new DateTime(2019, 12, 24, 3, 31, 22, 916, DateTimeKind.Unspecified).AddTicks(1701), "Est vel quis rem velit exercitationem id unde.", new DateTime(2022, 7, 8, 23, 20, 29, 81, DateTimeKind.Unspecified).AddTicks(1897), "Est repellat.", 4, 1, 0 },
                    { 161, new DateTime(2019, 6, 27, 4, 7, 55, 634, DateTimeKind.Unspecified).AddTicks(1278), "Nobis fugit illo non omnis ut doloremque illo.", new DateTime(2022, 4, 15, 3, 21, 13, 589, DateTimeKind.Unspecified).AddTicks(7965), "Quaerat repudiandae.", 36, 3, 0 },
                    { 162, new DateTime(2019, 7, 13, 14, 39, 46, 602, DateTimeKind.Unspecified).AddTicks(4558), "Omnis et qui saepe.", new DateTime(2022, 5, 8, 3, 9, 54, 829, DateTimeKind.Unspecified).AddTicks(1250), "Harum repellat.", 40, 1, 3 },
                    { 163, new DateTime(2019, 1, 3, 11, 5, 11, 831, DateTimeKind.Unspecified).AddTicks(1500), "Deserunt dolores ab expedita eum veritatis repellat.", new DateTime(2022, 7, 1, 0, 16, 16, 219, DateTimeKind.Unspecified).AddTicks(5389), "Minima et.", 16, 5, 2 }
                });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerUserId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 164, new DateTime(2019, 11, 19, 23, 43, 55, 440, DateTimeKind.Unspecified).AddTicks(8110), "Alias sunt sed.", new DateTime(2022, 3, 1, 6, 59, 18, 457, DateTimeKind.Unspecified).AddTicks(3468), "Quia aspernatur.", 34, 4, 1 },
                    { 165, new DateTime(2019, 8, 25, 2, 46, 54, 612, DateTimeKind.Unspecified).AddTicks(8038), "Placeat dolore magni voluptatem.", new DateTime(2021, 8, 15, 11, 44, 12, 584, DateTimeKind.Unspecified).AddTicks(8966), "Id molestiae.", 38, 5, 2 },
                    { 166, new DateTime(2019, 8, 14, 1, 59, 26, 63, DateTimeKind.Unspecified).AddTicks(4835), "Ratione est in accusamus sed impedit illo.", new DateTime(2022, 1, 19, 15, 58, 52, 18, DateTimeKind.Unspecified).AddTicks(1340), "Ut aspernatur.", 29, 2, 1 },
                    { 167, new DateTime(2019, 6, 7, 11, 22, 19, 847, DateTimeKind.Unspecified).AddTicks(4419), "Nihil nihil non.", new DateTime(2021, 9, 5, 14, 42, 15, 946, DateTimeKind.Unspecified).AddTicks(6277), "Architecto vel.", 30, 3, 1 },
                    { 168, new DateTime(2019, 6, 22, 9, 11, 16, 57, DateTimeKind.Unspecified).AddTicks(3059), "Suscipit voluptas voluptatem deleniti.", new DateTime(2022, 3, 15, 19, 25, 4, 976, DateTimeKind.Unspecified).AddTicks(1674), "Delectus velit.", 3, 2, 3 },
                    { 169, new DateTime(2019, 9, 28, 20, 12, 9, 981, DateTimeKind.Unspecified).AddTicks(7872), "A commodi et consequatur iusto perferendis nobis et.", new DateTime(2021, 9, 9, 4, 50, 47, 681, DateTimeKind.Unspecified).AddTicks(2821), "Odio consequatur.", 19, 5, 2 },
                    { 170, new DateTime(2019, 4, 3, 18, 55, 23, 579, DateTimeKind.Unspecified).AddTicks(1163), "Excepturi ex vero et eos labore et nobis adipisci soluta.", new DateTime(2021, 5, 7, 8, 47, 14, 343, DateTimeKind.Unspecified).AddTicks(7345), "Aut nostrum.", 50, 5, 2 },
                    { 171, new DateTime(2019, 9, 28, 7, 29, 30, 544, DateTimeKind.Unspecified).AddTicks(8585), "Dolor laboriosam illo aliquid.", new DateTime(2021, 10, 5, 11, 13, 52, 831, DateTimeKind.Unspecified).AddTicks(2242), "Odit aut.", 19, 2, 2 },
                    { 172, new DateTime(2019, 4, 8, 21, 58, 36, 441, DateTimeKind.Unspecified).AddTicks(2689), "Cum doloribus eligendi quia itaque id sed.", new DateTime(2022, 12, 22, 9, 45, 20, 157, DateTimeKind.Unspecified).AddTicks(3228), "Accusantium corrupti.", 11, 2, 1 },
                    { 173, new DateTime(2019, 9, 21, 19, 19, 9, 266, DateTimeKind.Unspecified).AddTicks(1044), "Quidem modi sunt itaque architecto corrupti harum tempora et.", new DateTime(2021, 7, 18, 21, 50, 50, 64, DateTimeKind.Unspecified).AddTicks(1261), "Est maiores.", 30, 4, 0 },
                    { 174, new DateTime(2019, 12, 2, 20, 49, 31, 388, DateTimeKind.Unspecified).AddTicks(1639), "Deserunt soluta libero eligendi omnis et voluptatem aspernatur.", new DateTime(2022, 5, 5, 11, 26, 53, 490, DateTimeKind.Unspecified).AddTicks(6861), "Nesciunt voluptatibus.", 25, 4, 3 },
                    { 175, new DateTime(2019, 6, 10, 17, 7, 19, 306, DateTimeKind.Unspecified).AddTicks(2417), "Aspernatur distinctio dolore et est adipisci ut libero deleniti quisquam.", new DateTime(2022, 6, 30, 2, 45, 18, 864, DateTimeKind.Unspecified).AddTicks(4170), "Iste nihil.", 42, 4, 0 },
                    { 176, new DateTime(2019, 8, 19, 20, 34, 37, 179, DateTimeKind.Unspecified).AddTicks(8564), "Non omnis harum provident ea nisi.", new DateTime(2022, 3, 8, 1, 13, 32, 847, DateTimeKind.Unspecified).AddTicks(3876), "Consequatur in.", 45, 3, 3 },
                    { 177, new DateTime(2019, 5, 20, 10, 3, 22, 969, DateTimeKind.Unspecified).AddTicks(3814), "Voluptatum vel rem molestiae nostrum officia magnam itaque mollitia.", new DateTime(2021, 7, 8, 7, 9, 55, 169, DateTimeKind.Unspecified).AddTicks(7558), "Deleniti molestiae.", 30, 4, 0 },
                    { 178, new DateTime(2019, 6, 26, 5, 39, 38, 414, DateTimeKind.Unspecified).AddTicks(3387), "Quia sed aut aperiam et mollitia ea non.", new DateTime(2021, 10, 26, 10, 8, 10, 877, DateTimeKind.Unspecified).AddTicks(2608), "Animi fugit.", 1, 5, 2 },
                    { 179, new DateTime(2019, 4, 1, 1, 40, 1, 593, DateTimeKind.Unspecified).AddTicks(504), "Cumque officiis ut sapiente et quibusdam sint.", new DateTime(2021, 9, 20, 11, 24, 42, 862, DateTimeKind.Unspecified).AddTicks(137), "Eum amet.", 22, 1, 0 },
                    { 180, new DateTime(2019, 10, 9, 8, 10, 15, 130, DateTimeKind.Unspecified).AddTicks(8826), "Sint et impedit.", new DateTime(2022, 3, 7, 4, 37, 1, 418, DateTimeKind.Unspecified).AddTicks(9127), "Sit et.", 28, 5, 3 },
                    { 181, new DateTime(2019, 6, 25, 3, 51, 33, 853, DateTimeKind.Unspecified).AddTicks(7869), "Sit pariatur deserunt voluptatum mollitia facere architecto sed.", new DateTime(2022, 9, 30, 15, 9, 22, 822, DateTimeKind.Unspecified).AddTicks(6780), "Ea laborum.", 19, 1, 1 },
                    { 182, new DateTime(2019, 7, 6, 14, 6, 29, 396, DateTimeKind.Unspecified).AddTicks(804), "Laborum officiis deleniti minima quod.", new DateTime(2021, 4, 13, 16, 45, 42, 87, DateTimeKind.Unspecified).AddTicks(7051), "Ipsa sed.", 38, 2, 3 },
                    { 183, new DateTime(2019, 10, 11, 2, 50, 54, 527, DateTimeKind.Unspecified).AddTicks(3071), "Tempore dolore ullam eveniet optio distinctio harum.", new DateTime(2021, 4, 8, 8, 46, 11, 879, DateTimeKind.Unspecified).AddTicks(819), "Asperiores consequatur.", 17, 3, 2 },
                    { 184, new DateTime(2019, 6, 30, 21, 44, 10, 222, DateTimeKind.Unspecified).AddTicks(2018), "Expedita ipsa expedita dolor rerum voluptas in quis quaerat.", new DateTime(2022, 5, 8, 13, 51, 53, 860, DateTimeKind.Unspecified).AddTicks(3865), "Aut et.", 40, 3, 1 },
                    { 185, new DateTime(2019, 12, 10, 18, 15, 29, 467, DateTimeKind.Unspecified).AddTicks(3315), "Alias velit minima quasi eveniet facilis.", new DateTime(2022, 5, 30, 6, 31, 26, 714, DateTimeKind.Unspecified).AddTicks(2715), "Aut nam.", 48, 4, 1 },
                    { 186, new DateTime(2019, 7, 18, 7, 20, 56, 226, DateTimeKind.Unspecified).AddTicks(8000), "Exercitationem earum et inventore.", new DateTime(2021, 3, 24, 10, 49, 55, 902, DateTimeKind.Unspecified).AddTicks(6616), "Ea aperiam.", 32, 2, 1 },
                    { 187, new DateTime(2019, 1, 17, 9, 46, 38, 979, DateTimeKind.Unspecified).AddTicks(4713), "Recusandae numquam autem enim.", new DateTime(2021, 3, 7, 4, 11, 2, 608, DateTimeKind.Unspecified).AddTicks(9676), "Rem est.", 8, 2, 2 },
                    { 188, new DateTime(2019, 6, 29, 23, 54, 53, 9, DateTimeKind.Unspecified).AddTicks(2239), "Facilis molestiae rerum inventore at dolorem.", new DateTime(2022, 2, 27, 2, 31, 39, 418, DateTimeKind.Unspecified).AddTicks(4222), "Modi et.", 34, 3, 2 },
                    { 189, new DateTime(2019, 5, 9, 5, 39, 8, 450, DateTimeKind.Unspecified).AddTicks(2509), "Inventore magni qui quae nam sit quia fugit dolor excepturi.", new DateTime(2022, 6, 16, 13, 27, 17, 531, DateTimeKind.Unspecified).AddTicks(7593), "Et saepe.", 1, 1, 3 },
                    { 190, new DateTime(2019, 9, 18, 21, 10, 54, 561, DateTimeKind.Unspecified).AddTicks(4481), "Rerum adipisci incidunt veritatis accusantium occaecati cum cum voluptatem id.", new DateTime(2021, 3, 26, 16, 46, 50, 621, DateTimeKind.Unspecified).AddTicks(4688), "Tenetur quidem.", 19, 4, 1 },
                    { 191, new DateTime(2019, 1, 7, 0, 37, 38, 981, DateTimeKind.Unspecified).AddTicks(3289), "Delectus iusto totam ipsam temporibus.", new DateTime(2022, 6, 25, 11, 9, 58, 751, DateTimeKind.Unspecified).AddTicks(3714), "Consequatur tempora.", 46, 2, 1 },
                    { 192, new DateTime(2019, 6, 20, 2, 55, 2, 206, DateTimeKind.Unspecified).AddTicks(5243), "Iste repellendus rerum voluptate autem eveniet doloremque rerum magni ad.", new DateTime(2021, 9, 18, 1, 30, 6, 32, DateTimeKind.Unspecified).AddTicks(6235), "Odit laudantium.", 42, 1, 3 },
                    { 193, new DateTime(2019, 10, 29, 12, 17, 53, 547, DateTimeKind.Unspecified).AddTicks(5761), "Similique qui quam.", new DateTime(2022, 1, 17, 9, 1, 11, 974, DateTimeKind.Unspecified).AddTicks(1148), "Hic odio.", 16, 2, 3 },
                    { 194, new DateTime(2019, 6, 17, 11, 11, 5, 231, DateTimeKind.Unspecified).AddTicks(8973), "Recusandae eius dignissimos qui sunt repudiandae aperiam animi magni.", new DateTime(2022, 9, 20, 10, 43, 22, 407, DateTimeKind.Unspecified).AddTicks(822), "Quo minus.", 30, 1, 3 },
                    { 195, new DateTime(2019, 5, 13, 7, 42, 40, 830, DateTimeKind.Unspecified).AddTicks(2318), "Nisi est dolor in cumque suscipit et ea.", new DateTime(2021, 6, 25, 7, 51, 0, 104, DateTimeKind.Unspecified).AddTicks(6413), "Quae velit.", 41, 5, 2 },
                    { 196, new DateTime(2019, 8, 7, 7, 23, 56, 579, DateTimeKind.Unspecified).AddTicks(2586), "Sit animi modi vel sequi.", new DateTime(2022, 9, 18, 13, 32, 36, 284, DateTimeKind.Unspecified).AddTicks(4359), "Repudiandae est.", 14, 3, 0 },
                    { 197, new DateTime(2019, 12, 28, 5, 53, 8, 502, DateTimeKind.Unspecified).AddTicks(3064), "Amet rerum qui iste aut.", new DateTime(2022, 10, 29, 12, 25, 35, 949, DateTimeKind.Unspecified).AddTicks(7152), "Molestiae ut.", 12, 1, 3 },
                    { 198, new DateTime(2019, 12, 6, 7, 55, 30, 33, DateTimeKind.Unspecified).AddTicks(583), "In doloremque quibusdam.", new DateTime(2022, 10, 13, 1, 52, 26, 631, DateTimeKind.Unspecified).AddTicks(6521), "Accusantium voluptatum.", 12, 5, 0 },
                    { 199, new DateTime(2019, 3, 31, 8, 17, 43, 761, DateTimeKind.Unspecified).AddTicks(9363), "Repudiandae aperiam labore.", new DateTime(2021, 4, 16, 14, 54, 55, 628, DateTimeKind.Unspecified).AddTicks(7826), "Culpa quia.", 31, 1, 0 },
                    { 200, new DateTime(2019, 6, 19, 0, 9, 6, 934, DateTimeKind.Unspecified).AddTicks(2138), "Repellat dignissimos provident ut rerum rem dolore magnam eum.", new DateTime(2022, 6, 22, 10, 57, 48, 756, DateTimeKind.Unspecified).AddTicks(1551), "Omnis error.", 30, 2, 2 }
                });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 10, 12, 7, 39, 41, 192, DateTimeKind.Unspecified).AddTicks(252), "Schmidt, Schroeder and Yost" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 12, 6, 14, 7, 24, 323, DateTimeKind.Unspecified).AddTicks(9937), "Lakin LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 7, 16, 0, 31, 52, 238, DateTimeKind.Unspecified).AddTicks(5472), "Beahan LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 5, 24, 18, 22, 34, 521, DateTimeKind.Unspecified).AddTicks(2542), "Collins, Blick and Thompson" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2013, 5, 30, 17, 39, 1, 438, DateTimeKind.Unspecified).AddTicks(9948), "Wilderman, Spencer and Bailey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 12, 25, 23, 10, 44, 143, DateTimeKind.Unspecified).AddTicks(3560), "Wisozk - Crist" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 9, 27, 20, 0, 49, 4, DateTimeKind.Unspecified).AddTicks(6959), "Hyatt Inc" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 11, 6, 11, 45, 35, 702, DateTimeKind.Unspecified).AddTicks(7188), "Wolff, Dietrich and Feest" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 8, 10, 22, 38, 11, 620, DateTimeKind.Unspecified).AddTicks(3341), "Gerhold LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 7, 11, 17, 16, 42, 925, DateTimeKind.Unspecified).AddTicks(5716), "Bode, Ratke and Keeling" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 6, 1, 0, 1, 4, 959, DateTimeKind.Unspecified).AddTicks(6055), "Audreanne_Wintheiser@hotmail.com", "Jayden", "Trantow", new DateTime(2015, 12, 30, 16, 32, 19, 280, DateTimeKind.Unspecified).AddTicks(1484), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1993, 8, 3, 20, 15, 26, 706, DateTimeKind.Unspecified).AddTicks(1494), "Nellie_Veum@yahoo.com", "Watson", "Cronin", new DateTime(2017, 2, 16, 22, 16, 41, 20, DateTimeKind.Unspecified).AddTicks(4600) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 21, 11, 8, 30, 266, DateTimeKind.Unspecified).AddTicks(5389), "Cullen_Hand54@hotmail.com", "Napoleon", "Schaden", new DateTime(2015, 1, 17, 8, 59, 34, 193, DateTimeKind.Unspecified).AddTicks(3706), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 7, 21, 3, 9, 37, 796, DateTimeKind.Unspecified).AddTicks(1867), "Nicholaus_Paucek82@hotmail.com", "Darrin", "Jaskolski", new DateTime(2015, 6, 25, 19, 36, 0, 201, DateTimeKind.Unspecified).AddTicks(6079), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 21, 23, 59, 56, 24, DateTimeKind.Unspecified).AddTicks(6701), "Grayce.Koepp@gmail.com", "Chanelle", "Huel", new DateTime(2011, 2, 24, 16, 52, 42, 552, DateTimeKind.Unspecified).AddTicks(1052), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 6, 2, 3, 20, 6, 29, DateTimeKind.Unspecified).AddTicks(1206), "Dagmar.Trantow@yahoo.com", "Norbert", "Murray", new DateTime(2010, 12, 5, 0, 45, 15, 575, DateTimeKind.Unspecified).AddTicks(4941), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 1, 10, 11, 57, 38, 2, DateTimeKind.Unspecified).AddTicks(6734), "Erin_Boyle93@hotmail.com", "Herman", "Roob", new DateTime(2016, 12, 5, 21, 44, 45, 954, DateTimeKind.Unspecified).AddTicks(7710), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 15, 18, 8, 5, 289, DateTimeKind.Unspecified).AddTicks(6692), "Gilbert.Kunze94@gmail.com", "Olen", "Wisoky", new DateTime(2010, 6, 8, 21, 21, 18, 399, DateTimeKind.Unspecified).AddTicks(9814), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 9, 18, 14, 54, 36, 795, DateTimeKind.Unspecified).AddTicks(9632), "Sadye.Yost@hotmail.com", "Deon", "Adams", new DateTime(2014, 7, 19, 19, 34, 0, 712, DateTimeKind.Unspecified).AddTicks(6996), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 8, 18, 2, 46, 2, 627, DateTimeKind.Unspecified).AddTicks(4904), "Wilton.Dibbert@hotmail.com", "Genevieve", "Dooley", new DateTime(2011, 3, 25, 19, 45, 3, 548, DateTimeKind.Unspecified).AddTicks(4988), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 6, 3, 30, 30, 395, DateTimeKind.Unspecified).AddTicks(7937), "Sallie.Kemmer94@hotmail.com", "Brycen", "Weissnat", new DateTime(2010, 9, 3, 16, 29, 36, 216, DateTimeKind.Unspecified).AddTicks(2097), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 27, 6, 37, 1, 631, DateTimeKind.Unspecified).AddTicks(4836), "Wendell85@hotmail.com", "Jon", "Ruecker", new DateTime(2016, 5, 28, 20, 30, 22, 526, DateTimeKind.Unspecified).AddTicks(7003), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 30, 18, 42, 18, 241, DateTimeKind.Unspecified).AddTicks(5962), "Dulce82@yahoo.com", "Elda", "Gerlach", new DateTime(2014, 8, 20, 7, 4, 26, 741, DateTimeKind.Unspecified).AddTicks(1752), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 7, 15, 1, 44, 388, DateTimeKind.Unspecified).AddTicks(1517), "Cornelius_Spinka54@gmail.com", "Maximillia", "Watsica", new DateTime(2017, 12, 9, 12, 45, 54, 982, DateTimeKind.Unspecified).AddTicks(9860), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 29, 20, 58, 52, 805, DateTimeKind.Unspecified).AddTicks(6368), "Aaron_Littel69@hotmail.com", "Lelia", "Wisoky", new DateTime(2010, 3, 26, 7, 27, 39, 700, DateTimeKind.Unspecified).AddTicks(7544), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 1, 28, 13, 18, 41, 836, DateTimeKind.Unspecified).AddTicks(8146), "Imelda37@gmail.com", "Genoveva", "Hamill", new DateTime(2016, 11, 30, 9, 22, 54, 618, DateTimeKind.Unspecified).AddTicks(9415), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 12, 12, 14, 55, 49, 69, DateTimeKind.Unspecified).AddTicks(8159), "Anastacio.Ziemann@yahoo.com", "Josefa", "Kuphal", new DateTime(2010, 3, 14, 16, 8, 32, 543, DateTimeKind.Unspecified).AddTicks(1494), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 11, 25, 3, 3, 34, 407, DateTimeKind.Unspecified).AddTicks(7610), "Emmie.Collins@hotmail.com", "Bruce", "Kohler", new DateTime(2013, 5, 26, 0, 17, 14, 953, DateTimeKind.Unspecified).AddTicks(3384), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 5, 8, 39, 12, 557, DateTimeKind.Unspecified).AddTicks(6003), "Nathanial.Kuhlman@gmail.com", "Juston", "Roberts", new DateTime(2013, 3, 18, 10, 40, 0, 988, DateTimeKind.Unspecified).AddTicks(5022), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 26, 22, 4, 52, 323, DateTimeKind.Unspecified).AddTicks(7570), "Bailey19@hotmail.com", "Barrett", "Stroman", new DateTime(2017, 12, 31, 17, 56, 56, 145, DateTimeKind.Unspecified).AddTicks(2776), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 3, 22, 17, 4, 15, 152, DateTimeKind.Unspecified).AddTicks(952), "Jadon.Fadel@yahoo.com", "Jacinthe", "Schiller", new DateTime(2015, 1, 7, 8, 14, 11, 588, DateTimeKind.Unspecified).AddTicks(6813), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 5, 19, 13, 20, 20, 332, DateTimeKind.Unspecified).AddTicks(8106), "Valerie.Bergstrom@hotmail.com", "Tillman", "Koelpin", new DateTime(2012, 4, 21, 17, 55, 15, 427, DateTimeKind.Unspecified).AddTicks(3254), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 3, 6, 23, 6, 27, 10, DateTimeKind.Unspecified).AddTicks(5616), "Allene_Sanford@hotmail.com", "Emmalee", "Treutel", new DateTime(2012, 7, 8, 10, 5, 44, 312, DateTimeKind.Unspecified).AddTicks(8590), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 4, 20, 22, 22, 48, 324, DateTimeKind.Unspecified).AddTicks(4412), "Jeremy_Wiza5@yahoo.com", "Ara", "Labadie", new DateTime(2012, 12, 12, 2, 45, 3, 89, DateTimeKind.Unspecified).AddTicks(2343), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 15, 9, 22, 14, 28, DateTimeKind.Unspecified).AddTicks(7740), "Eladio66@yahoo.com", "Daphne", "Hyatt", new DateTime(2012, 3, 2, 0, 51, 13, 56, DateTimeKind.Unspecified).AddTicks(5750), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 12, 17, 44, 8, 208, DateTimeKind.Unspecified).AddTicks(4290), "Katelynn14@gmail.com", "Eugenia", "Ledner", new DateTime(2014, 6, 24, 7, 7, 35, 75, DateTimeKind.Unspecified).AddTicks(3511), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 31, 10, 55, 29, 604, DateTimeKind.Unspecified).AddTicks(7688), "Camren.Emmerich91@gmail.com", "Itzel", "Sanford", new DateTime(2011, 11, 1, 21, 40, 36, 794, DateTimeKind.Unspecified).AddTicks(8899), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 10, 5, 28, 41, 528, DateTimeKind.Unspecified).AddTicks(8497), "Trinity.Monahan76@yahoo.com", "Clemens", "Upton", new DateTime(2014, 11, 10, 20, 32, 58, 273, DateTimeKind.Unspecified).AddTicks(2900), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 12, 12, 12, 54, 29, 421, DateTimeKind.Unspecified).AddTicks(2329), "Onie_Boehm41@hotmail.com", "Luz", "Beer", new DateTime(2016, 5, 14, 0, 28, 19, 666, DateTimeKind.Unspecified).AddTicks(4911), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 1, 9, 16, 58, 38, 433, DateTimeKind.Unspecified).AddTicks(7961), "Terrill.Keebler@hotmail.com", "Madisyn", "Stark", new DateTime(2012, 1, 2, 15, 27, 26, 30, DateTimeKind.Unspecified).AddTicks(3389), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 10, 16, 16, 37, 47, 439, DateTimeKind.Unspecified).AddTicks(9826), "Abigayle_Huel@hotmail.com", "Natalia", "Lakin", new DateTime(2012, 9, 11, 23, 45, 24, 281, DateTimeKind.Unspecified).AddTicks(7926), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 5, 10, 22, 28, 5, 572, DateTimeKind.Unspecified).AddTicks(4091), "Carlee_Streich@gmail.com", "Lora", "Rice", new DateTime(2014, 4, 9, 9, 54, 56, 521, DateTimeKind.Unspecified).AddTicks(3624), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 24, 2, 8, 7, 872, DateTimeKind.Unspecified).AddTicks(2964), "Omari.DAmore@hotmail.com", "Leta", "Howe", new DateTime(2012, 1, 7, 20, 3, 1, 756, DateTimeKind.Unspecified).AddTicks(7552), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 12, 8, 22, 39, 27, 499, DateTimeKind.Unspecified).AddTicks(918), "Hester.OReilly@hotmail.com", "Claude", "Tromp", new DateTime(2011, 9, 6, 2, 34, 22, 795, DateTimeKind.Unspecified).AddTicks(474), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 2, 2, 48, 0, 28, DateTimeKind.Unspecified).AddTicks(9237), "Evelyn40@hotmail.com", "Lester", "Muller", new DateTime(2013, 5, 5, 11, 39, 8, 277, DateTimeKind.Unspecified).AddTicks(187), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 3, 3, 7, 42, 50, 372, DateTimeKind.Unspecified).AddTicks(8955), "Fausto10@yahoo.com", "Oma", "Quitzon", new DateTime(2010, 4, 23, 14, 39, 55, 871, DateTimeKind.Unspecified).AddTicks(6162), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1999, 7, 21, 3, 59, 17, 735, DateTimeKind.Unspecified).AddTicks(5208), "Jaiden11@hotmail.com", "Kennedy", "Ferry", new DateTime(2012, 8, 6, 22, 2, 22, 474, DateTimeKind.Unspecified).AddTicks(8679) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 10, 3, 14, 55, 203, DateTimeKind.Unspecified).AddTicks(5717), "Raleigh.Bode@yahoo.com", "Emmalee", "Stracke", new DateTime(2015, 2, 2, 18, 48, 33, 177, DateTimeKind.Unspecified).AddTicks(5350), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 5, 15, 2, 19, 16, 724, DateTimeKind.Unspecified).AddTicks(4108), "Mario_OConnell@hotmail.com", "Daphney", "Gibson", new DateTime(2011, 10, 19, 9, 22, 52, 622, DateTimeKind.Unspecified).AddTicks(7511) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 6, 14, 23, 17, 14, 446, DateTimeKind.Unspecified).AddTicks(2595), "Alan_Dibbert45@hotmail.com", "Carter", "Anderson", new DateTime(2014, 1, 9, 20, 13, 39, 290, DateTimeKind.Unspecified).AddTicks(802), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 23, 1, 12, 16, 406, DateTimeKind.Unspecified).AddTicks(4954), "Hailee.Romaguera9@hotmail.com", "Mariela", "West", new DateTime(2017, 4, 12, 10, 14, 2, 895, DateTimeKind.Unspecified).AddTicks(3291), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 1, 25, 9, 40, 9, 436, DateTimeKind.Unspecified).AddTicks(4478), "Kim_Larkin@yahoo.com", "Kylee", "VonRueden", new DateTime(2015, 12, 16, 19, 39, 50, 235, DateTimeKind.Unspecified).AddTicks(7815), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 10, 14, 14, 6, 768, DateTimeKind.Unspecified).AddTicks(5128), "Sheridan.Pouros@hotmail.com", "Rusty", "Simonis", new DateTime(2010, 6, 22, 15, 21, 40, 999, DateTimeKind.Unspecified).AddTicks(7421), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 4, 14, 18, 51, 900, DateTimeKind.Unspecified).AddTicks(8430), "Blair_Dietrich@hotmail.com", "Gwendolyn", "Huel", new DateTime(2016, 9, 13, 14, 23, 14, 543, DateTimeKind.Unspecified).AddTicks(3635), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 5, 17, 49, 51, 695, DateTimeKind.Unspecified).AddTicks(3982), "Alana_Johnson@hotmail.com", "Pink", "Gutkowski", new DateTime(2016, 4, 30, 12, 59, 15, 65, DateTimeKind.Unspecified).AddTicks(5554), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1995, 12, 4, 17, 43, 2, 251, DateTimeKind.Unspecified).AddTicks(8498), "Clovis58@gmail.com", "Elyssa", "Frami", new DateTime(2016, 1, 8, 14, 15, 27, 415, DateTimeKind.Unspecified).AddTicks(3730) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 29, 2, 19, 24, 32, DateTimeKind.Unspecified).AddTicks(3483), "Adaline.Prohaska73@gmail.com", "Markus", "Mitchell", new DateTime(2010, 7, 25, 4, 0, 52, 503, DateTimeKind.Unspecified).AddTicks(1025), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 15, 14, 14, 15, 2, DateTimeKind.Unspecified).AddTicks(1329), "Parker.Hessel@gmail.com", "Ruby", "Botsford", new DateTime(2011, 5, 23, 8, 9, 38, 557, DateTimeKind.Unspecified).AddTicks(1173), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 21, 22, 19, 58, 201, DateTimeKind.Unspecified).AddTicks(190), "Troy.Gerlach@yahoo.com", "Rickey", "Wolf", new DateTime(2014, 7, 28, 11, 43, 0, 399, DateTimeKind.Unspecified).AddTicks(2138), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 7, 17, 35, 26, 446, DateTimeKind.Unspecified).AddTicks(5008), "Deondre.Dietrich87@yahoo.com", "Madonna", "Hane", new DateTime(2017, 11, 29, 16, 8, 3, 828, DateTimeKind.Unspecified).AddTicks(6722), 9 });

            migrationBuilder.CreateIndex(
                name: "IX_Task_PerformerUserId",
                table: "Task",
                column: "PerformerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_ProjectId",
                table: "Task",
                column: "ProjectId");
        }
    }
}
