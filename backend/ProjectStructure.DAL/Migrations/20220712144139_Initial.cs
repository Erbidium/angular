﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectStructure.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectTasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PerformerId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectTasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectTasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2012, 10, 12, 0, 26, 50, 882, DateTimeKind.Unspecified).AddTicks(77), "Ledner, Stokes and Frami" },
                    { 2, new DateTime(2015, 5, 29, 2, 48, 23, 564, DateTimeKind.Unspecified).AddTicks(5970), "Anderson Inc" },
                    { 3, new DateTime(2017, 4, 17, 15, 27, 52, 475, DateTimeKind.Unspecified).AddTicks(1368), "Kerluke - Orn" },
                    { 4, new DateTime(2011, 10, 28, 10, 57, 0, 737, DateTimeKind.Unspecified).AddTicks(9794), "O'Reilly - Kihn" },
                    { 5, new DateTime(2014, 1, 22, 21, 20, 29, 837, DateTimeKind.Unspecified).AddTicks(7481), "Berge LLC" },
                    { 6, new DateTime(2015, 5, 26, 3, 29, 18, 759, DateTimeKind.Unspecified).AddTicks(6229), "Rau LLC" },
                    { 7, new DateTime(2012, 8, 23, 13, 49, 45, 69, DateTimeKind.Unspecified).AddTicks(1381), "Murphy LLC" },
                    { 8, new DateTime(2014, 6, 22, 10, 49, 16, 535, DateTimeKind.Unspecified).AddTicks(8759), "Aufderhar - Weissnat" },
                    { 9, new DateTime(2014, 5, 29, 3, 59, 18, 824, DateTimeKind.Unspecified).AddTicks(9648), "Mosciski, Muller and Legros" },
                    { 10, new DateTime(2012, 9, 18, 22, 55, 24, 134, DateTimeKind.Unspecified).AddTicks(2856), "Pfannerstill Group" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1994, 5, 22, 22, 6, 2, 956, DateTimeKind.Unspecified).AddTicks(1959), "Monserrate12@gmail.com", "Marlene", "Flatley", new DateTime(2014, 4, 17, 13, 31, 3, 346, DateTimeKind.Unspecified).AddTicks(9587), 1 },
                    { 2, new DateTime(1996, 12, 9, 11, 20, 44, 518, DateTimeKind.Unspecified).AddTicks(6665), "Ima.Schoen1@gmail.com", "Barrett", "Roob", new DateTime(2013, 12, 16, 21, 41, 44, 272, DateTimeKind.Unspecified).AddTicks(595), 10 },
                    { 3, new DateTime(1998, 5, 24, 7, 3, 20, 457, DateTimeKind.Unspecified).AddTicks(5434), "Cassandra4@hotmail.com", "Orpha", "Daugherty", new DateTime(2015, 5, 21, 11, 31, 43, 35, DateTimeKind.Unspecified).AddTicks(6305), 10 },
                    { 4, new DateTime(1990, 7, 4, 13, 44, 54, 869, DateTimeKind.Unspecified).AddTicks(4525), "Willie_Moen37@yahoo.com", "Zion", "Lockman", new DateTime(2015, 9, 14, 12, 48, 2, 833, DateTimeKind.Unspecified).AddTicks(7873), 9 },
                    { 5, new DateTime(1995, 5, 1, 18, 50, 54, 835, DateTimeKind.Unspecified).AddTicks(9764), "Aubrey.Jakubowski@hotmail.com", "Burnice", "Price", new DateTime(2014, 10, 7, 8, 44, 7, 763, DateTimeKind.Unspecified).AddTicks(1298), 3 },
                    { 6, new DateTime(1994, 8, 19, 7, 41, 19, 933, DateTimeKind.Unspecified).AddTicks(2400), "Boyd.Haag@hotmail.com", "Jamison", "Leannon", new DateTime(2017, 9, 21, 13, 38, 8, 485, DateTimeKind.Unspecified).AddTicks(270), 5 },
                    { 7, new DateTime(1996, 6, 4, 17, 33, 27, 511, DateTimeKind.Unspecified).AddTicks(7396), "Shania66@hotmail.com", "Brannon", "Champlin", new DateTime(2014, 2, 26, 0, 10, 20, 441, DateTimeKind.Unspecified).AddTicks(5900), 1 },
                    { 8, new DateTime(1997, 11, 18, 10, 26, 25, 58, DateTimeKind.Unspecified).AddTicks(4293), "Elissa_Kshlerin@yahoo.com", "Garnet", "Yost", new DateTime(2015, 10, 30, 13, 16, 50, 656, DateTimeKind.Unspecified).AddTicks(8729), 7 },
                    { 9, new DateTime(1998, 8, 29, 0, 25, 6, 946, DateTimeKind.Unspecified).AddTicks(7884), "Palma_Goyette@yahoo.com", "Tod", "Mosciski", new DateTime(2016, 5, 12, 18, 28, 12, 977, DateTimeKind.Unspecified).AddTicks(3636), 10 },
                    { 10, new DateTime(1996, 2, 2, 18, 46, 19, 397, DateTimeKind.Unspecified).AddTicks(4196), "Katlyn61@yahoo.com", "Xavier", "Cronin", new DateTime(2013, 7, 11, 0, 35, 38, 519, DateTimeKind.Unspecified).AddTicks(8184), 8 },
                    { 11, new DateTime(1993, 6, 27, 14, 20, 16, 845, DateTimeKind.Unspecified).AddTicks(7314), "Darlene47@yahoo.com", "Kiara", "Reichert", new DateTime(2014, 7, 27, 8, 8, 56, 477, DateTimeKind.Unspecified).AddTicks(7980), 9 },
                    { 12, new DateTime(1993, 11, 20, 10, 23, 42, 605, DateTimeKind.Unspecified).AddTicks(584), "Kobe_Larson@hotmail.com", "Abelardo", "Fahey", new DateTime(2015, 4, 14, 14, 6, 27, 12, DateTimeKind.Unspecified).AddTicks(338), 3 },
                    { 13, new DateTime(1996, 11, 24, 15, 36, 13, 172, DateTimeKind.Unspecified).AddTicks(6458), "Daphnee25@hotmail.com", "Vinnie", "Renner", new DateTime(2017, 12, 13, 2, 37, 29, 767, DateTimeKind.Unspecified).AddTicks(9930), 9 },
                    { 14, new DateTime(1992, 10, 7, 10, 12, 46, 263, DateTimeKind.Unspecified).AddTicks(7433), "Kendrick.Jones75@yahoo.com", "Alexis", "Wilderman", new DateTime(2012, 8, 19, 20, 55, 25, 618, DateTimeKind.Unspecified).AddTicks(6710), 5 },
                    { 15, new DateTime(1997, 5, 9, 0, 47, 15, 584, DateTimeKind.Unspecified).AddTicks(4212), "Joshua.Jenkins59@hotmail.com", "Mason", "Schaden", new DateTime(2013, 4, 22, 22, 17, 55, 463, DateTimeKind.Unspecified).AddTicks(289), 2 },
                    { 16, new DateTime(1993, 12, 18, 3, 24, 21, 902, DateTimeKind.Unspecified).AddTicks(5005), "Remington_Hamill75@hotmail.com", "Dahlia", "Wiegand", new DateTime(2013, 9, 15, 19, 36, 50, 524, DateTimeKind.Unspecified).AddTicks(6608), 7 },
                    { 17, new DateTime(1999, 3, 14, 17, 36, 10, 471, DateTimeKind.Unspecified).AddTicks(5096), "Gideon_Braun20@gmail.com", "Jodie", "Russel", new DateTime(2014, 1, 3, 9, 45, 22, 653, DateTimeKind.Unspecified).AddTicks(7608), 3 },
                    { 18, new DateTime(1995, 5, 23, 19, 2, 22, 45, DateTimeKind.Unspecified).AddTicks(8935), "Mckenzie_Nitzsche56@gmail.com", "Xander", "Olson", new DateTime(2015, 8, 11, 20, 15, 5, 51, DateTimeKind.Unspecified).AddTicks(6578), 2 },
                    { 19, new DateTime(1992, 8, 1, 12, 24, 50, 135, DateTimeKind.Unspecified).AddTicks(2159), "Leo_Olson95@gmail.com", "Florida", "Lesch", new DateTime(2010, 10, 3, 19, 59, 40, 726, DateTimeKind.Unspecified).AddTicks(2525), 2 },
                    { 20, new DateTime(1994, 1, 28, 19, 7, 12, 113, DateTimeKind.Unspecified).AddTicks(4558), "Christelle.VonRueden36@gmail.com", "Sigmund", "Pagac", new DateTime(2012, 10, 25, 23, 30, 54, 242, DateTimeKind.Unspecified).AddTicks(6607), 8 },
                    { 21, new DateTime(1994, 1, 26, 22, 52, 59, 114, DateTimeKind.Unspecified).AddTicks(7983), "Adonis.Littel@gmail.com", "Kayley", "Larkin", new DateTime(2014, 5, 18, 17, 31, 55, 734, DateTimeKind.Unspecified).AddTicks(3688), 5 },
                    { 22, new DateTime(1993, 6, 30, 0, 17, 41, 430, DateTimeKind.Unspecified).AddTicks(7145), "Alexandria70@gmail.com", "Manuel", "Gibson", new DateTime(2013, 4, 6, 20, 2, 24, 137, DateTimeKind.Unspecified).AddTicks(423), 10 },
                    { 23, new DateTime(1999, 8, 8, 8, 40, 17, 946, DateTimeKind.Unspecified).AddTicks(9761), "Cheyenne.Fisher@hotmail.com", "Taya", "Gutmann", new DateTime(2015, 7, 29, 13, 44, 49, 685, DateTimeKind.Unspecified).AddTicks(4828), 1 },
                    { 24, new DateTime(1998, 11, 17, 9, 58, 58, 521, DateTimeKind.Unspecified).AddTicks(2810), "Lance.Konopelski35@hotmail.com", "Russell", "Kessler", new DateTime(2016, 3, 31, 0, 59, 23, 153, DateTimeKind.Unspecified).AddTicks(2280), 4 },
                    { 25, new DateTime(1991, 10, 3, 17, 0, 25, 230, DateTimeKind.Unspecified).AddTicks(7787), "Geovanny_Baumbach57@hotmail.com", "Rylan", "Walker", new DateTime(2011, 7, 29, 0, 45, 17, 893, DateTimeKind.Unspecified).AddTicks(220), 1 },
                    { 26, new DateTime(1996, 5, 15, 0, 55, 41, 827, DateTimeKind.Unspecified).AddTicks(349), "Wilford_Rutherford89@gmail.com", "Francisca", "Greenfelder", new DateTime(2011, 5, 18, 15, 46, 14, 456, DateTimeKind.Unspecified).AddTicks(3786), 4 },
                    { 27, new DateTime(1997, 8, 17, 16, 8, 20, 594, DateTimeKind.Unspecified).AddTicks(2780), "Anika.Simonis@hotmail.com", "Leonardo", "Rodriguez", new DateTime(2010, 10, 31, 11, 22, 34, 406, DateTimeKind.Unspecified).AddTicks(6844), 1 },
                    { 28, new DateTime(1996, 9, 19, 19, 24, 35, 909, DateTimeKind.Unspecified).AddTicks(9552), "Lenore.Klein@hotmail.com", "D'angelo", "Kilback", new DateTime(2010, 8, 9, 3, 8, 46, 63, DateTimeKind.Unspecified).AddTicks(2618), 5 },
                    { 29, new DateTime(1994, 7, 3, 11, 9, 8, 977, DateTimeKind.Unspecified).AddTicks(9052), "Ressie_Bauch@hotmail.com", "Melvin", "Walsh", new DateTime(2016, 8, 29, 0, 33, 34, 957, DateTimeKind.Unspecified).AddTicks(4816), 10 },
                    { 30, new DateTime(1990, 1, 9, 19, 46, 45, 155, DateTimeKind.Unspecified).AddTicks(2434), "Reta_Bailey81@gmail.com", "Adrain", "O'Keefe", new DateTime(2011, 2, 21, 7, 6, 56, 100, DateTimeKind.Unspecified).AddTicks(9541), 9 },
                    { 31, new DateTime(1996, 4, 11, 2, 21, 2, 383, DateTimeKind.Unspecified).AddTicks(3232), "Eugenia72@gmail.com", "Celestino", "Bashirian", new DateTime(2013, 7, 20, 11, 47, 59, 79, DateTimeKind.Unspecified).AddTicks(7420), 4 },
                    { 32, new DateTime(1995, 2, 13, 12, 24, 56, 142, DateTimeKind.Unspecified).AddTicks(2516), "Thea_Welch13@hotmail.com", "Al", "Reichert", new DateTime(2014, 2, 6, 16, 21, 45, 374, DateTimeKind.Unspecified).AddTicks(8673), 7 },
                    { 33, new DateTime(1990, 1, 17, 19, 7, 30, 954, DateTimeKind.Unspecified).AddTicks(1119), "Donavon.Denesik38@gmail.com", "Reilly", "Flatley", new DateTime(2012, 7, 7, 8, 6, 26, 725, DateTimeKind.Unspecified).AddTicks(3767), 3 },
                    { 34, new DateTime(1991, 2, 11, 21, 23, 20, 729, DateTimeKind.Unspecified).AddTicks(1530), "Mertie_Reilly@gmail.com", "Jimmie", "Lind", new DateTime(2015, 4, 6, 8, 6, 0, 517, DateTimeKind.Unspecified).AddTicks(697), 10 },
                    { 35, new DateTime(1991, 9, 19, 20, 8, 13, 853, DateTimeKind.Unspecified).AddTicks(1778), "Kole.Leuschke@gmail.com", "Fermin", "Yost", new DateTime(2014, 2, 2, 2, 21, 6, 374, DateTimeKind.Unspecified).AddTicks(6985), 3 },
                    { 36, new DateTime(1990, 11, 23, 3, 11, 37, 919, DateTimeKind.Unspecified).AddTicks(910), "Deangelo.Langworth@gmail.com", "Javier", "McGlynn", new DateTime(2017, 3, 12, 22, 14, 30, 800, DateTimeKind.Unspecified).AddTicks(2778), 8 },
                    { 37, new DateTime(1997, 8, 9, 14, 18, 13, 815, DateTimeKind.Unspecified).AddTicks(2166), "Claud66@hotmail.com", "Kylie", "Kunde", new DateTime(2012, 6, 19, 19, 7, 33, 384, DateTimeKind.Unspecified).AddTicks(2991), 2 },
                    { 38, new DateTime(1991, 10, 1, 11, 16, 42, 918, DateTimeKind.Unspecified).AddTicks(2327), "Euna45@gmail.com", "Amani", "Daniel", new DateTime(2016, 9, 25, 11, 21, 31, 40, DateTimeKind.Unspecified).AddTicks(6506), 2 },
                    { 39, new DateTime(1995, 5, 12, 14, 19, 28, 174, DateTimeKind.Unspecified).AddTicks(6622), "Sherman_Mosciski@hotmail.com", "Quincy", "Bergnaum", new DateTime(2017, 10, 17, 16, 29, 9, 69, DateTimeKind.Unspecified).AddTicks(886), 7 },
                    { 40, new DateTime(1990, 1, 23, 20, 8, 59, 142, DateTimeKind.Unspecified).AddTicks(3319), "Hermina19@gmail.com", "Brett", "Weimann", new DateTime(2011, 3, 12, 2, 2, 43, 308, DateTimeKind.Unspecified).AddTicks(7857), 1 },
                    { 41, new DateTime(1996, 2, 2, 0, 15, 43, 148, DateTimeKind.Unspecified).AddTicks(5763), "Ezra55@gmail.com", "Meaghan", "Goldner", new DateTime(2017, 6, 15, 13, 1, 50, 613, DateTimeKind.Unspecified).AddTicks(3000), 8 },
                    { 42, new DateTime(1990, 1, 25, 11, 35, 8, 296, DateTimeKind.Unspecified).AddTicks(1297), "Cristobal78@yahoo.com", "Will", "Rau", new DateTime(2010, 5, 2, 17, 18, 51, 686, DateTimeKind.Unspecified).AddTicks(2364), 5 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 43, new DateTime(1999, 12, 21, 20, 51, 26, 426, DateTimeKind.Unspecified).AddTicks(2484), "Garrison35@gmail.com", "Maia", "Glover", new DateTime(2016, 1, 28, 9, 11, 27, 413, DateTimeKind.Unspecified).AddTicks(8414), 3 },
                    { 44, new DateTime(1999, 7, 17, 20, 14, 42, 126, DateTimeKind.Unspecified).AddTicks(8129), "Shayna42@hotmail.com", "Gavin", "Yundt", new DateTime(2011, 4, 6, 13, 23, 30, 599, DateTimeKind.Unspecified).AddTicks(6714), 2 },
                    { 45, new DateTime(1996, 7, 3, 3, 5, 22, 630, DateTimeKind.Unspecified).AddTicks(1086), "Kelvin_Abbott@gmail.com", "Michaela", "Klein", new DateTime(2013, 10, 22, 13, 2, 52, 310, DateTimeKind.Unspecified).AddTicks(4157), 7 },
                    { 46, new DateTime(1999, 9, 9, 21, 50, 28, 166, DateTimeKind.Unspecified).AddTicks(3124), "George_Wyman63@yahoo.com", "Abel", "Weissnat", new DateTime(2013, 7, 25, 18, 56, 15, 877, DateTimeKind.Unspecified).AddTicks(6186), 8 },
                    { 47, new DateTime(1990, 7, 17, 10, 12, 50, 266, DateTimeKind.Unspecified).AddTicks(142), "Samson.Veum@gmail.com", "Raul", "Grady", new DateTime(2012, 9, 11, 22, 57, 57, 252, DateTimeKind.Unspecified).AddTicks(5300), 3 },
                    { 48, new DateTime(1991, 5, 31, 2, 14, 32, 914, DateTimeKind.Unspecified).AddTicks(9140), "Kirk_Labadie@hotmail.com", "Kenton", "Beahan", new DateTime(2013, 10, 8, 8, 58, 48, 116, DateTimeKind.Unspecified).AddTicks(299), 5 },
                    { 49, new DateTime(1995, 2, 6, 11, 16, 1, 332, DateTimeKind.Unspecified).AddTicks(7111), "Twila48@yahoo.com", "Carmelo", "Larson", new DateTime(2015, 5, 11, 7, 38, 23, 135, DateTimeKind.Unspecified).AddTicks(8811), 8 },
                    { 50, new DateTime(1996, 4, 9, 3, 11, 4, 390, DateTimeKind.Unspecified).AddTicks(4345), "Alize_Little@hotmail.com", "Joesph", "Bernhard", new DateTime(2014, 12, 25, 6, 30, 59, 777, DateTimeKind.Unspecified).AddTicks(6646), 10 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 50, new DateTime(2019, 6, 6, 16, 39, 0, 832, DateTimeKind.Unspecified).AddTicks(702), new DateTime(2022, 6, 26, 19, 25, 42, 329, DateTimeKind.Unspecified).AddTicks(8134), "Doloribus sint ad eum magni ut dolor et rerum vel.", "Incredible Rubber Tuna", 10 },
                    { 2, 22, new DateTime(2019, 7, 4, 22, 30, 44, 302, DateTimeKind.Unspecified).AddTicks(4779), new DateTime(2022, 2, 22, 4, 15, 54, 568, DateTimeKind.Unspecified).AddTicks(9798), "Repudiandae nam qui cupiditate quos consequuntur.", "Rustic Concrete Soap", 6 },
                    { 3, 21, new DateTime(2019, 4, 26, 13, 39, 14, 444, DateTimeKind.Unspecified).AddTicks(7672), new DateTime(2021, 5, 20, 2, 45, 26, 781, DateTimeKind.Unspecified).AddTicks(7452), "Ea totam a voluptas suscipit provident ducimus rerum.", "Gorgeous Soft Ball", 4 },
                    { 4, 12, new DateTime(2019, 2, 11, 21, 16, 37, 948, DateTimeKind.Unspecified).AddTicks(8452), new DateTime(2021, 10, 27, 2, 33, 47, 831, DateTimeKind.Unspecified).AddTicks(7956), "Facere vitae veritatis eligendi rerum illo et aut laborum.", "Tasty Rubber Chair", 10 },
                    { 5, 11, new DateTime(2019, 11, 7, 6, 32, 19, 106, DateTimeKind.Unspecified).AddTicks(2886), new DateTime(2022, 2, 18, 16, 25, 33, 615, DateTimeKind.Unspecified).AddTicks(8690), "Provident id sunt et nihil accusantium dolore nam id.", "Generic Soft Pants", 1 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 3, 20, 8, 48, 25, 749, DateTimeKind.Unspecified).AddTicks(1569), "Saepe quis officia et est impedit laborum.", new DateTime(2021, 7, 10, 18, 27, 8, 131, DateTimeKind.Unspecified).AddTicks(9511), "Consequatur pariatur.", 45, 5, 3 },
                    { 2, new DateTime(2019, 6, 17, 3, 41, 11, 860, DateTimeKind.Unspecified).AddTicks(7051), "Voluptate in quis.", new DateTime(2022, 4, 24, 5, 32, 33, 576, DateTimeKind.Unspecified).AddTicks(3686), "Aut autem.", 32, 5, 2 },
                    { 3, new DateTime(2019, 10, 12, 23, 22, 20, 432, DateTimeKind.Unspecified).AddTicks(922), "Necessitatibus vel accusamus dolor magnam cum in occaecati.", new DateTime(2021, 11, 5, 13, 2, 21, 896, DateTimeKind.Unspecified).AddTicks(9006), "Excepturi quam.", 28, 5, 0 },
                    { 4, new DateTime(2019, 10, 22, 23, 18, 36, 839, DateTimeKind.Unspecified).AddTicks(4020), "Harum libero voluptas voluptatum.", new DateTime(2022, 5, 21, 3, 37, 13, 271, DateTimeKind.Unspecified).AddTicks(5855), "Doloremque in.", 44, 1, 3 },
                    { 5, new DateTime(2019, 4, 7, 9, 11, 10, 253, DateTimeKind.Unspecified).AddTicks(2607), "Quos odit autem commodi alias quia quae porro.", new DateTime(2021, 1, 2, 18, 26, 4, 416, DateTimeKind.Unspecified).AddTicks(3951), "Sed nulla.", 35, 3, 2 },
                    { 6, new DateTime(2019, 5, 30, 3, 14, 7, 301, DateTimeKind.Unspecified).AddTicks(1793), "Officiis nostrum amet ut placeat.", new DateTime(2022, 6, 1, 1, 48, 23, 239, DateTimeKind.Unspecified).AddTicks(5480), "Quae mollitia.", 47, 3, 1 },
                    { 7, new DateTime(2019, 6, 30, 20, 14, 34, 393, DateTimeKind.Unspecified).AddTicks(542), "Recusandae eveniet sit quibusdam ea recusandae eum porro est.", new DateTime(2021, 6, 8, 0, 2, 1, 20, DateTimeKind.Unspecified).AddTicks(7106), "Animi rerum.", 39, 4, 0 },
                    { 8, new DateTime(2019, 9, 9, 2, 50, 56, 497, DateTimeKind.Unspecified).AddTicks(6000), "Magnam occaecati qui.", new DateTime(2021, 11, 12, 8, 43, 25, 64, DateTimeKind.Unspecified).AddTicks(7348), "Veritatis at.", 34, 3, 2 },
                    { 9, new DateTime(2019, 6, 22, 18, 13, 9, 66, DateTimeKind.Unspecified).AddTicks(3467), "Ducimus iure iste minima.", new DateTime(2022, 6, 29, 16, 30, 52, 991, DateTimeKind.Unspecified).AddTicks(9010), "Aut quas.", 23, 5, 2 },
                    { 10, new DateTime(2019, 5, 24, 21, 32, 7, 674, DateTimeKind.Unspecified).AddTicks(6405), "Non ut distinctio velit possimus ratione repellat.", new DateTime(2022, 4, 24, 5, 11, 51, 41, DateTimeKind.Unspecified).AddTicks(520), "Sunt error.", 41, 3, 1 },
                    { 11, new DateTime(2019, 11, 9, 0, 53, 45, 16, DateTimeKind.Unspecified).AddTicks(4759), "Et dolor tenetur molestiae.", new DateTime(2022, 1, 29, 0, 11, 33, 41, DateTimeKind.Unspecified).AddTicks(4985), "Sunt alias.", 3, 5, 0 },
                    { 12, new DateTime(2019, 5, 31, 18, 53, 44, 285, DateTimeKind.Unspecified).AddTicks(6129), "Eum aliquid est voluptas sit.", new DateTime(2021, 3, 26, 21, 15, 54, 503, DateTimeKind.Unspecified).AddTicks(5201), "Rerum possimus.", 2, 3, 3 },
                    { 13, new DateTime(2019, 10, 22, 13, 33, 52, 412, DateTimeKind.Unspecified).AddTicks(7211), "Sed ullam explicabo sed eum dolores.", new DateTime(2022, 8, 11, 16, 26, 51, 367, DateTimeKind.Unspecified).AddTicks(4561), "Est est.", 34, 2, 2 },
                    { 14, new DateTime(2019, 7, 17, 10, 31, 19, 5, DateTimeKind.Unspecified).AddTicks(1982), "Ullam neque aut quae at.", new DateTime(2022, 3, 6, 0, 49, 23, 792, DateTimeKind.Unspecified).AddTicks(7538), "Reiciendis aliquam.", 1, 5, 0 },
                    { 15, new DateTime(2019, 8, 2, 5, 20, 44, 359, DateTimeKind.Unspecified).AddTicks(564), "Voluptatibus dolor ab expedita est blanditiis qui.", new DateTime(2021, 1, 6, 13, 10, 7, 124, DateTimeKind.Unspecified).AddTicks(3415), "Voluptas omnis.", 42, 3, 1 },
                    { 16, new DateTime(2019, 2, 6, 3, 4, 39, 779, DateTimeKind.Unspecified).AddTicks(5347), "Laboriosam pariatur dolore hic accusamus.", new DateTime(2021, 1, 8, 20, 51, 41, 526, DateTimeKind.Unspecified).AddTicks(1776), "Amet rem.", 16, 2, 0 },
                    { 17, new DateTime(2019, 4, 21, 23, 29, 46, 19, DateTimeKind.Unspecified).AddTicks(4326), "Dolores aut excepturi dolores.", new DateTime(2021, 3, 30, 6, 58, 6, 990, DateTimeKind.Unspecified).AddTicks(6510), "Ipsum consequatur.", 48, 3, 1 },
                    { 18, new DateTime(2019, 12, 9, 20, 4, 54, 742, DateTimeKind.Unspecified).AddTicks(8250), "Vero voluptatibus excepturi illum perferendis.", new DateTime(2022, 4, 30, 4, 56, 35, 364, DateTimeKind.Unspecified).AddTicks(6982), "Qui voluptas.", 48, 5, 0 },
                    { 19, new DateTime(2019, 11, 13, 12, 48, 34, 904, DateTimeKind.Unspecified).AddTicks(7869), "Dolorem aut repellendus ut ut consequatur est aperiam iste.", new DateTime(2021, 7, 6, 11, 50, 27, 382, DateTimeKind.Unspecified).AddTicks(5904), "Odit veniam.", 18, 4, 0 },
                    { 20, new DateTime(2019, 2, 7, 21, 30, 15, 686, DateTimeKind.Unspecified).AddTicks(7101), "Voluptas ipsum ullam blanditiis id repellendus ipsa quas dolores.", new DateTime(2022, 9, 3, 17, 52, 2, 804, DateTimeKind.Unspecified).AddTicks(8283), "Ratione fuga.", 42, 3, 0 },
                    { 21, new DateTime(2019, 5, 4, 3, 20, 5, 21, DateTimeKind.Unspecified).AddTicks(1340), "Voluptatum suscipit aperiam adipisci dolores tempore quo suscipit ut ut.", new DateTime(2022, 1, 26, 7, 10, 56, 776, DateTimeKind.Unspecified).AddTicks(3680), "Libero nam.", 5, 5, 3 },
                    { 22, new DateTime(2019, 10, 30, 11, 50, 43, 209, DateTimeKind.Unspecified).AddTicks(8682), "Soluta modi accusamus architecto.", new DateTime(2021, 10, 16, 13, 8, 18, 186, DateTimeKind.Unspecified).AddTicks(3854), "Necessitatibus provident.", 37, 1, 1 },
                    { 23, new DateTime(2019, 1, 22, 15, 56, 6, 182, DateTimeKind.Unspecified).AddTicks(8910), "Facere aut aut nisi occaecati ut ipsam occaecati dolore neque.", new DateTime(2022, 8, 20, 23, 28, 24, 199, DateTimeKind.Unspecified).AddTicks(8298), "Et ducimus.", 9, 1, 2 },
                    { 24, new DateTime(2019, 3, 19, 8, 45, 50, 563, DateTimeKind.Unspecified).AddTicks(1148), "Sit voluptatibus et voluptates ratione.", new DateTime(2021, 3, 9, 7, 44, 49, 733, DateTimeKind.Unspecified).AddTicks(416), "Sapiente expedita.", 31, 2, 0 },
                    { 25, new DateTime(2019, 10, 31, 0, 49, 6, 986, DateTimeKind.Unspecified).AddTicks(2181), "Accusamus necessitatibus consectetur dolores optio ea.", new DateTime(2021, 2, 11, 9, 48, 1, 603, DateTimeKind.Unspecified).AddTicks(5132), "Est enim.", 12, 3, 1 },
                    { 26, new DateTime(2019, 2, 17, 0, 15, 43, 981, DateTimeKind.Unspecified).AddTicks(7285), "Impedit reprehenderit possimus.", new DateTime(2022, 3, 4, 14, 10, 49, 474, DateTimeKind.Unspecified).AddTicks(2303), "Ipsum eum.", 2, 1, 0 },
                    { 27, new DateTime(2019, 11, 24, 14, 27, 38, 599, DateTimeKind.Unspecified).AddTicks(9856), "Tenetur occaecati et nemo officia odit quas.", new DateTime(2021, 8, 3, 12, 2, 16, 385, DateTimeKind.Unspecified).AddTicks(9413), "Aperiam commodi.", 35, 3, 2 },
                    { 28, new DateTime(2019, 10, 2, 17, 51, 30, 480, DateTimeKind.Unspecified).AddTicks(8561), "Ut non cupiditate sed temporibus aspernatur.", new DateTime(2022, 12, 17, 1, 36, 29, 807, DateTimeKind.Unspecified).AddTicks(6461), "Facere autem.", 24, 2, 1 },
                    { 29, new DateTime(2019, 3, 1, 8, 52, 56, 722, DateTimeKind.Unspecified).AddTicks(3673), "Incidunt neque qui est distinctio omnis.", new DateTime(2021, 9, 8, 21, 28, 40, 900, DateTimeKind.Unspecified).AddTicks(8619), "Recusandae ratione.", 42, 4, 3 },
                    { 30, new DateTime(2019, 12, 20, 18, 19, 29, 59, DateTimeKind.Unspecified).AddTicks(353), "Aut perspiciatis optio sint commodi doloribus.", new DateTime(2021, 7, 8, 7, 13, 22, 630, DateTimeKind.Unspecified).AddTicks(998), "Eos nobis.", 18, 2, 3 },
                    { 31, new DateTime(2019, 3, 17, 18, 30, 57, 258, DateTimeKind.Unspecified).AddTicks(4363), "Non ad repellendus accusantium ab doloribus animi et.", new DateTime(2021, 5, 17, 3, 54, 41, 477, DateTimeKind.Unspecified).AddTicks(7415), "Nesciunt impedit.", 13, 5, 0 },
                    { 32, new DateTime(2019, 2, 10, 10, 48, 10, 680, DateTimeKind.Unspecified).AddTicks(4289), "Deserunt ducimus officiis quas architecto iste voluptas non.", new DateTime(2021, 3, 10, 18, 55, 15, 388, DateTimeKind.Unspecified).AddTicks(5678), "Ducimus et.", 47, 1, 0 },
                    { 33, new DateTime(2019, 11, 28, 11, 33, 29, 505, DateTimeKind.Unspecified).AddTicks(8046), "Non culpa corporis et ex sint nobis aut quidem.", new DateTime(2021, 6, 4, 0, 36, 5, 551, DateTimeKind.Unspecified).AddTicks(629), "Dignissimos at.", 4, 3, 3 },
                    { 34, new DateTime(2019, 12, 2, 9, 23, 41, 225, DateTimeKind.Unspecified).AddTicks(5817), "Suscipit provident expedita explicabo modi.", new DateTime(2021, 10, 20, 18, 10, 48, 991, DateTimeKind.Unspecified).AddTicks(3066), "Doloribus dolorem.", 35, 4, 3 },
                    { 35, new DateTime(2019, 11, 4, 10, 32, 57, 569, DateTimeKind.Unspecified).AddTicks(9239), "Vel delectus numquam dolor.", new DateTime(2021, 9, 4, 23, 13, 22, 49, DateTimeKind.Unspecified).AddTicks(755), "Ipsa odit.", 15, 3, 0 },
                    { 36, new DateTime(2019, 7, 28, 14, 25, 19, 899, DateTimeKind.Unspecified).AddTicks(2847), "Quisquam rem deleniti animi est quibusdam accusamus ut placeat natus.", new DateTime(2022, 3, 3, 13, 54, 53, 938, DateTimeKind.Unspecified).AddTicks(4806), "In eos.", 11, 4, 1 },
                    { 37, new DateTime(2019, 8, 20, 11, 27, 56, 574, DateTimeKind.Unspecified).AddTicks(1248), "Et ab ut ut sit sed quas voluptatem iusto.", new DateTime(2021, 2, 27, 23, 14, 44, 14, DateTimeKind.Unspecified).AddTicks(7750), "Ex omnis.", 18, 5, 1 },
                    { 38, new DateTime(2019, 10, 8, 18, 29, 22, 303, DateTimeKind.Unspecified).AddTicks(3803), "Dolorum qui suscipit laboriosam voluptas.", new DateTime(2021, 4, 29, 20, 55, 28, 274, DateTimeKind.Unspecified).AddTicks(5618), "Ut ut.", 40, 4, 3 },
                    { 39, new DateTime(2019, 10, 12, 1, 44, 15, 435, DateTimeKind.Unspecified).AddTicks(8691), "Et iste quo saepe magni.", new DateTime(2022, 6, 25, 6, 15, 4, 323, DateTimeKind.Unspecified).AddTicks(1044), "Illo sit.", 24, 2, 1 },
                    { 40, new DateTime(2019, 9, 29, 1, 49, 18, 474, DateTimeKind.Unspecified).AddTicks(310), "Eligendi dignissimos praesentium officiis reprehenderit molestiae ea laudantium facere.", new DateTime(2022, 8, 6, 14, 48, 39, 714, DateTimeKind.Unspecified).AddTicks(2479), "Veritatis fuga.", 41, 4, 1 },
                    { 41, new DateTime(2019, 6, 1, 16, 8, 15, 893, DateTimeKind.Unspecified).AddTicks(9630), "Delectus autem unde qui eos sed animi earum.", new DateTime(2022, 4, 29, 8, 3, 31, 303, DateTimeKind.Unspecified).AddTicks(4852), "Nostrum et.", 36, 5, 3 },
                    { 42, new DateTime(2019, 3, 27, 4, 32, 40, 475, DateTimeKind.Unspecified).AddTicks(8896), "Accusantium fugiat molestias eum rerum quia animi voluptatum.", new DateTime(2021, 9, 20, 16, 37, 13, 683, DateTimeKind.Unspecified).AddTicks(4894), "Reiciendis labore.", 40, 1, 2 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 43, new DateTime(2019, 7, 26, 4, 8, 14, 938, DateTimeKind.Unspecified).AddTicks(5606), "Quisquam voluptas est sequi saepe quia ut laudantium necessitatibus aut.", new DateTime(2022, 12, 10, 4, 8, 0, 969, DateTimeKind.Unspecified).AddTicks(7795), "Deserunt sunt.", 47, 3, 0 },
                    { 44, new DateTime(2019, 9, 3, 22, 31, 51, 440, DateTimeKind.Unspecified).AddTicks(2100), "Cum dolorum quasi architecto quod quasi eum sequi eaque.", new DateTime(2021, 7, 27, 12, 4, 59, 277, DateTimeKind.Unspecified).AddTicks(9520), "Sed velit.", 36, 3, 0 },
                    { 45, new DateTime(2019, 5, 13, 10, 41, 47, 610, DateTimeKind.Unspecified).AddTicks(3324), "Qui dolorum assumenda et voluptas ea in quae.", new DateTime(2022, 4, 22, 2, 17, 41, 429, DateTimeKind.Unspecified).AddTicks(1205), "Cupiditate iusto.", 42, 2, 2 },
                    { 46, new DateTime(2019, 1, 27, 21, 38, 44, 783, DateTimeKind.Unspecified).AddTicks(507), "Sit sunt quo architecto voluptatem rerum alias placeat.", new DateTime(2021, 8, 21, 1, 9, 48, 522, DateTimeKind.Unspecified).AddTicks(2101), "Dolorem sed.", 40, 4, 2 },
                    { 47, new DateTime(2019, 1, 26, 8, 31, 4, 388, DateTimeKind.Unspecified).AddTicks(3101), "Quia mollitia animi non reprehenderit praesentium aut fugiat sunt quod.", new DateTime(2021, 8, 14, 10, 47, 15, 320, DateTimeKind.Unspecified).AddTicks(7450), "Commodi et.", 43, 3, 3 },
                    { 48, new DateTime(2019, 12, 8, 15, 34, 11, 722, DateTimeKind.Unspecified).AddTicks(4319), "Et illum corporis dolorem qui maiores amet.", new DateTime(2021, 10, 9, 2, 32, 42, 370, DateTimeKind.Unspecified).AddTicks(3684), "Sunt temporibus.", 33, 5, 0 },
                    { 49, new DateTime(2019, 2, 23, 20, 32, 28, 607, DateTimeKind.Unspecified).AddTicks(5031), "Sapiente quos optio iste deserunt quia nemo ipsam veritatis.", new DateTime(2021, 4, 1, 11, 47, 47, 511, DateTimeKind.Unspecified).AddTicks(2324), "Sunt quia.", 4, 2, 3 },
                    { 50, new DateTime(2019, 1, 31, 15, 57, 22, 97, DateTimeKind.Unspecified).AddTicks(8511), "Aut temporibus ab sunt rerum eos consequatur.", new DateTime(2021, 10, 26, 11, 8, 51, 448, DateTimeKind.Unspecified).AddTicks(8780), "Necessitatibus nihil.", 17, 5, 1 },
                    { 51, new DateTime(2019, 3, 13, 10, 5, 39, 352, DateTimeKind.Unspecified).AddTicks(399), "Expedita eum quasi.", new DateTime(2022, 7, 12, 18, 2, 21, 887, DateTimeKind.Unspecified).AddTicks(8621), "Sequi labore.", 21, 4, 1 },
                    { 52, new DateTime(2019, 4, 7, 3, 58, 19, 526, DateTimeKind.Unspecified).AddTicks(9114), "Architecto incidunt ex.", new DateTime(2021, 2, 4, 18, 40, 0, 36, DateTimeKind.Unspecified).AddTicks(4832), "Accusantium id.", 33, 4, 1 },
                    { 53, new DateTime(2019, 11, 3, 22, 56, 53, 327, DateTimeKind.Unspecified).AddTicks(72), "Ullam et aspernatur adipisci.", new DateTime(2021, 11, 21, 6, 14, 23, 845, DateTimeKind.Unspecified).AddTicks(1904), "Vero earum.", 34, 4, 0 },
                    { 54, new DateTime(2019, 7, 17, 12, 31, 11, 768, DateTimeKind.Unspecified).AddTicks(1145), "Aperiam minus illum sed qui.", new DateTime(2022, 3, 30, 22, 3, 21, 454, DateTimeKind.Unspecified).AddTicks(4736), "Sed est.", 10, 4, 2 },
                    { 55, new DateTime(2019, 3, 6, 0, 17, 49, 585, DateTimeKind.Unspecified).AddTicks(8620), "Vitae nihil qui cupiditate est itaque est ut eaque.", new DateTime(2021, 10, 21, 8, 47, 42, 758, DateTimeKind.Unspecified).AddTicks(3652), "Aut laboriosam.", 19, 5, 3 },
                    { 56, new DateTime(2019, 3, 29, 4, 20, 12, 236, DateTimeKind.Unspecified).AddTicks(9154), "Quia molestiae enim neque ut.", new DateTime(2021, 12, 18, 14, 49, 55, 99, DateTimeKind.Unspecified).AddTicks(3815), "Et veritatis.", 31, 3, 2 },
                    { 57, new DateTime(2019, 6, 20, 3, 34, 14, 225, DateTimeKind.Unspecified).AddTicks(7337), "Non sit unde dolores.", new DateTime(2021, 3, 16, 13, 21, 26, 414, DateTimeKind.Unspecified).AddTicks(5000), "Et nihil.", 27, 3, 2 },
                    { 58, new DateTime(2019, 8, 10, 13, 56, 27, 711, DateTimeKind.Unspecified).AddTicks(4136), "Voluptatum accusamus cupiditate est doloribus autem et nihil.", new DateTime(2021, 6, 20, 10, 13, 38, 124, DateTimeKind.Unspecified).AddTicks(3066), "Sapiente esse.", 36, 4, 0 },
                    { 59, new DateTime(2019, 2, 6, 1, 26, 1, 672, DateTimeKind.Unspecified).AddTicks(5842), "Delectus officia est aut et velit nulla et aut.", new DateTime(2021, 7, 5, 18, 22, 48, 109, DateTimeKind.Unspecified).AddTicks(8352), "Dolores voluptatem.", 15, 5, 2 },
                    { 60, new DateTime(2019, 6, 14, 6, 55, 5, 164, DateTimeKind.Unspecified).AddTicks(7635), "Et quam velit.", new DateTime(2022, 3, 27, 1, 1, 37, 177, DateTimeKind.Unspecified).AddTicks(7286), "Ducimus cumque.", 10, 5, 3 },
                    { 61, new DateTime(2019, 2, 11, 22, 14, 56, 646, DateTimeKind.Unspecified).AddTicks(9240), "Consequatur ea sit consequatur nam nisi fugiat.", new DateTime(2022, 10, 4, 2, 44, 52, 390, DateTimeKind.Unspecified).AddTicks(8086), "Rerum aliquam.", 30, 2, 0 },
                    { 62, new DateTime(2019, 2, 27, 13, 19, 55, 120, DateTimeKind.Unspecified).AddTicks(8571), "Non unde totam hic provident.", new DateTime(2021, 8, 15, 23, 17, 53, 461, DateTimeKind.Unspecified).AddTicks(5851), "Dolores id.", 13, 2, 2 },
                    { 63, new DateTime(2019, 4, 10, 13, 52, 33, 110, DateTimeKind.Unspecified).AddTicks(8303), "Tempora explicabo nostrum eum ut asperiores necessitatibus.", new DateTime(2022, 2, 10, 22, 59, 17, 679, DateTimeKind.Unspecified).AddTicks(9328), "Qui aperiam.", 8, 5, 3 },
                    { 64, new DateTime(2019, 10, 26, 23, 53, 30, 427, DateTimeKind.Unspecified).AddTicks(7562), "Aut officiis enim.", new DateTime(2021, 12, 4, 14, 23, 50, 673, DateTimeKind.Unspecified).AddTicks(8481), "Dolorum ullam.", 3, 4, 2 },
                    { 65, new DateTime(2019, 9, 17, 17, 32, 5, 818, DateTimeKind.Unspecified).AddTicks(7591), "Suscipit reprehenderit sapiente rerum beatae doloribus vitae.", new DateTime(2021, 6, 29, 13, 39, 54, 515, DateTimeKind.Unspecified).AddTicks(5970), "Atque est.", 22, 3, 3 },
                    { 66, new DateTime(2019, 7, 18, 23, 11, 44, 507, DateTimeKind.Unspecified).AddTicks(4542), "Id nihil iusto in libero.", new DateTime(2022, 3, 20, 22, 32, 3, 625, DateTimeKind.Unspecified).AddTicks(4833), "Architecto et.", 21, 1, 1 },
                    { 67, new DateTime(2019, 12, 20, 17, 38, 21, 187, DateTimeKind.Unspecified).AddTicks(2423), "Eos quis dolore impedit distinctio adipisci.", new DateTime(2022, 1, 25, 7, 59, 43, 275, DateTimeKind.Unspecified).AddTicks(370), "Enim odio.", 47, 5, 3 },
                    { 68, new DateTime(2019, 11, 28, 19, 2, 24, 775, DateTimeKind.Unspecified).AddTicks(3225), "Autem aliquam ad accusamus eos.", new DateTime(2022, 2, 3, 3, 2, 23, 513, DateTimeKind.Unspecified).AddTicks(5680), "Laborum suscipit.", 22, 5, 0 },
                    { 69, new DateTime(2019, 11, 17, 16, 33, 39, 62, DateTimeKind.Unspecified).AddTicks(1759), "Adipisci consequatur facere laudantium.", new DateTime(2021, 7, 27, 3, 6, 12, 707, DateTimeKind.Unspecified).AddTicks(447), "Et vitae.", 43, 2, 2 },
                    { 70, new DateTime(2019, 8, 1, 6, 55, 3, 81, DateTimeKind.Unspecified).AddTicks(2713), "Repudiandae est perspiciatis at aut.", new DateTime(2022, 1, 17, 22, 30, 32, 807, DateTimeKind.Unspecified).AddTicks(575), "Nesciunt culpa.", 13, 2, 0 },
                    { 71, new DateTime(2019, 4, 1, 1, 16, 45, 34, DateTimeKind.Unspecified).AddTicks(278), "Qui omnis cum deserunt est voluptatem et.", new DateTime(2021, 12, 30, 15, 49, 43, 716, DateTimeKind.Unspecified).AddTicks(4770), "Suscipit non.", 26, 2, 1 },
                    { 72, new DateTime(2019, 5, 6, 18, 27, 22, 328, DateTimeKind.Unspecified).AddTicks(7371), "Et voluptatibus in cupiditate vel adipisci similique beatae qui fuga.", new DateTime(2021, 4, 7, 0, 27, 26, 809, DateTimeKind.Unspecified).AddTicks(8892), "Odit eum.", 44, 3, 0 },
                    { 73, new DateTime(2019, 3, 15, 19, 46, 53, 859, DateTimeKind.Unspecified).AddTicks(2805), "Nihil optio aperiam sed eligendi autem consequatur quae facilis repudiandae.", new DateTime(2022, 7, 18, 5, 0, 7, 309, DateTimeKind.Unspecified).AddTicks(6960), "Pariatur quia.", 4, 4, 1 },
                    { 74, new DateTime(2019, 1, 8, 16, 50, 19, 258, DateTimeKind.Unspecified).AddTicks(5528), "Voluptates aspernatur molestiae non.", new DateTime(2021, 5, 1, 2, 48, 14, 119, DateTimeKind.Unspecified).AddTicks(8102), "Aut sit.", 29, 4, 2 },
                    { 75, new DateTime(2019, 11, 29, 21, 43, 55, 859, DateTimeKind.Unspecified).AddTicks(2506), "Earum aliquam similique molestias veniam.", new DateTime(2021, 6, 23, 4, 46, 45, 756, DateTimeKind.Unspecified).AddTicks(30), "Dolorem blanditiis.", 7, 5, 3 },
                    { 76, new DateTime(2019, 6, 11, 20, 32, 20, 789, DateTimeKind.Unspecified).AddTicks(2634), "Exercitationem praesentium dolores assumenda aut repellendus doloribus quam qui.", new DateTime(2022, 2, 3, 15, 20, 19, 281, DateTimeKind.Unspecified).AddTicks(2810), "Velit veniam.", 37, 1, 2 },
                    { 77, new DateTime(2019, 3, 11, 4, 8, 17, 585, DateTimeKind.Unspecified).AddTicks(9770), "Et quae delectus.", new DateTime(2021, 10, 28, 15, 19, 49, 969, DateTimeKind.Unspecified).AddTicks(5345), "Maiores ad.", 38, 3, 1 },
                    { 78, new DateTime(2019, 3, 11, 22, 30, 53, 669, DateTimeKind.Unspecified).AddTicks(5151), "Possimus distinctio quis eos.", new DateTime(2022, 11, 21, 1, 41, 42, 380, DateTimeKind.Unspecified).AddTicks(6900), "Laborum architecto.", 25, 3, 3 },
                    { 79, new DateTime(2019, 12, 9, 16, 30, 1, 539, DateTimeKind.Unspecified).AddTicks(2176), "In sequi eligendi commodi recusandae.", new DateTime(2021, 8, 26, 15, 47, 57, 87, DateTimeKind.Unspecified).AddTicks(3540), "Ipsum vel.", 30, 1, 1 },
                    { 80, new DateTime(2019, 9, 25, 13, 20, 10, 971, DateTimeKind.Unspecified).AddTicks(8690), "Sapiente totam fugit rerum exercitationem maxime veniam exercitationem ut.", new DateTime(2021, 9, 23, 4, 16, 34, 582, DateTimeKind.Unspecified).AddTicks(2018), "Maiores a.", 30, 3, 1 },
                    { 81, new DateTime(2019, 8, 16, 1, 58, 5, 975, DateTimeKind.Unspecified).AddTicks(2529), "Tempore quo aut dolor.", new DateTime(2021, 10, 29, 18, 22, 20, 406, DateTimeKind.Unspecified).AddTicks(216), "Iste et.", 8, 5, 3 },
                    { 82, new DateTime(2019, 3, 6, 13, 57, 56, 37, DateTimeKind.Unspecified).AddTicks(4258), "Et qui totam fuga ut facilis corporis quaerat sequi saepe.", new DateTime(2022, 5, 25, 2, 29, 55, 554, DateTimeKind.Unspecified).AddTicks(8821), "Quisquam voluptatibus.", 3, 5, 2 },
                    { 83, new DateTime(2019, 11, 23, 16, 24, 41, 655, DateTimeKind.Unspecified).AddTicks(7272), "Illo numquam dolorum fugit et aut omnis.", new DateTime(2021, 2, 26, 9, 54, 31, 976, DateTimeKind.Unspecified).AddTicks(3305), "Aut ipsa.", 42, 4, 1 },
                    { 84, new DateTime(2019, 11, 2, 1, 35, 49, 726, DateTimeKind.Unspecified).AddTicks(215), "Quisquam omnis sint at reprehenderit reprehenderit sint suscipit.", new DateTime(2021, 2, 7, 1, 50, 22, 524, DateTimeKind.Unspecified).AddTicks(4756), "Voluptas deleniti.", 44, 3, 0 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 85, new DateTime(2019, 12, 25, 21, 11, 37, 393, DateTimeKind.Unspecified).AddTicks(6049), "Voluptatem adipisci illo repellat dolore.", new DateTime(2021, 11, 3, 0, 8, 45, 638, DateTimeKind.Unspecified).AddTicks(3419), "Unde inventore.", 31, 3, 0 },
                    { 86, new DateTime(2019, 9, 8, 17, 21, 56, 411, DateTimeKind.Unspecified).AddTicks(6588), "Quo ea similique quia.", new DateTime(2022, 9, 29, 18, 23, 7, 66, DateTimeKind.Unspecified).AddTicks(2938), "Eius mollitia.", 19, 2, 2 },
                    { 87, new DateTime(2019, 7, 13, 13, 39, 15, 240, DateTimeKind.Unspecified).AddTicks(2144), "Qui pariatur sed ducimus fuga sunt officiis.", new DateTime(2022, 5, 16, 2, 15, 53, 128, DateTimeKind.Unspecified).AddTicks(6527), "Et rerum.", 12, 4, 3 },
                    { 88, new DateTime(2019, 5, 13, 4, 14, 47, 872, DateTimeKind.Unspecified).AddTicks(2852), "Necessitatibus id atque.", new DateTime(2021, 5, 17, 15, 21, 20, 441, DateTimeKind.Unspecified).AddTicks(7649), "Quis quia.", 18, 5, 3 },
                    { 89, new DateTime(2019, 9, 10, 11, 13, 0, 819, DateTimeKind.Unspecified).AddTicks(7953), "Sit delectus exercitationem est incidunt tenetur.", new DateTime(2022, 4, 24, 8, 26, 20, 357, DateTimeKind.Unspecified).AddTicks(4723), "Repellat accusantium.", 47, 5, 0 },
                    { 90, new DateTime(2019, 5, 11, 14, 51, 20, 464, DateTimeKind.Unspecified).AddTicks(8482), "Quisquam et architecto sint iusto consequatur pariatur commodi et unde.", new DateTime(2022, 1, 8, 2, 48, 56, 488, DateTimeKind.Unspecified).AddTicks(159), "Fuga impedit.", 37, 4, 3 },
                    { 91, new DateTime(2019, 8, 30, 17, 22, 37, 587, DateTimeKind.Unspecified).AddTicks(5652), "A unde velit porro amet est eum odio nihil dolorem.", new DateTime(2021, 5, 9, 1, 5, 45, 931, DateTimeKind.Unspecified).AddTicks(4464), "Commodi consequatur.", 23, 1, 3 },
                    { 92, new DateTime(2019, 8, 29, 21, 17, 59, 929, DateTimeKind.Unspecified).AddTicks(6790), "Incidunt omnis aliquid quia.", new DateTime(2021, 4, 24, 13, 3, 6, 390, DateTimeKind.Unspecified).AddTicks(1903), "Voluptas aut.", 3, 3, 3 },
                    { 93, new DateTime(2019, 7, 31, 17, 35, 38, 871, DateTimeKind.Unspecified).AddTicks(245), "Sit non voluptatibus accusantium.", new DateTime(2021, 2, 21, 3, 22, 58, 328, DateTimeKind.Unspecified).AddTicks(3525), "Natus quo.", 37, 5, 3 },
                    { 94, new DateTime(2019, 8, 31, 17, 27, 4, 312, DateTimeKind.Unspecified).AddTicks(2872), "Ut quia a magnam aliquam nam neque officiis fugit.", new DateTime(2021, 3, 18, 15, 50, 34, 65, DateTimeKind.Unspecified).AddTicks(6355), "Sit quia.", 8, 2, 1 },
                    { 95, new DateTime(2019, 10, 24, 7, 30, 28, 333, DateTimeKind.Unspecified).AddTicks(2987), "Dolore ducimus ex praesentium.", new DateTime(2022, 8, 9, 2, 51, 23, 239, DateTimeKind.Unspecified).AddTicks(8916), "Eos adipisci.", 8, 5, 1 },
                    { 96, new DateTime(2019, 2, 4, 13, 31, 19, 410, DateTimeKind.Unspecified).AddTicks(6668), "Quis nulla nostrum sapiente ut.", new DateTime(2022, 9, 16, 17, 58, 12, 5, DateTimeKind.Unspecified).AddTicks(2240), "Libero id.", 23, 1, 2 },
                    { 97, new DateTime(2019, 9, 11, 8, 31, 49, 645, DateTimeKind.Unspecified).AddTicks(5377), "Non pariatur voluptatem.", new DateTime(2022, 10, 19, 5, 34, 25, 671, DateTimeKind.Unspecified).AddTicks(4763), "Quis eius.", 33, 3, 0 },
                    { 98, new DateTime(2019, 1, 24, 20, 58, 18, 166, DateTimeKind.Unspecified).AddTicks(1690), "Omnis sed eos magni fugiat.", new DateTime(2021, 2, 17, 12, 48, 50, 562, DateTimeKind.Unspecified).AddTicks(3880), "Maxime officia.", 10, 1, 0 },
                    { 99, new DateTime(2019, 12, 11, 18, 40, 13, 363, DateTimeKind.Unspecified).AddTicks(4572), "Voluptatibus blanditiis explicabo veniam sunt ipsam consequatur exercitationem.", new DateTime(2021, 2, 14, 14, 17, 56, 220, DateTimeKind.Unspecified).AddTicks(9023), "Ipsam quaerat.", 50, 3, 0 },
                    { 100, new DateTime(2019, 2, 13, 20, 45, 33, 636, DateTimeKind.Unspecified).AddTicks(744), "Veritatis nostrum voluptas voluptatibus ratione dolor soluta rem perspiciatis illum.", new DateTime(2022, 2, 18, 23, 42, 32, 256, DateTimeKind.Unspecified).AddTicks(5182), "Possimus rerum.", 12, 5, 2 },
                    { 101, new DateTime(2019, 1, 18, 13, 44, 6, 514, DateTimeKind.Unspecified).AddTicks(7595), "Aut omnis porro autem.", new DateTime(2021, 3, 13, 9, 2, 46, 879, DateTimeKind.Unspecified).AddTicks(4488), "Laboriosam aliquid.", 26, 3, 2 },
                    { 102, new DateTime(2019, 6, 23, 21, 26, 52, 663, DateTimeKind.Unspecified).AddTicks(3384), "Ipsam optio inventore velit dolores qui quisquam incidunt corrupti aperiam.", new DateTime(2022, 6, 6, 4, 23, 10, 712, DateTimeKind.Unspecified).AddTicks(8060), "Aut sit.", 18, 5, 3 },
                    { 103, new DateTime(2019, 1, 31, 20, 53, 16, 357, DateTimeKind.Unspecified).AddTicks(6545), "A et quisquam quibusdam in vel cupiditate.", new DateTime(2021, 12, 29, 11, 27, 24, 873, DateTimeKind.Unspecified).AddTicks(9948), "Soluta quos.", 2, 1, 2 },
                    { 104, new DateTime(2019, 6, 6, 23, 23, 52, 519, DateTimeKind.Unspecified).AddTicks(8725), "Dignissimos consequatur consectetur rem.", new DateTime(2022, 3, 27, 10, 13, 40, 372, DateTimeKind.Unspecified).AddTicks(9638), "Et delectus.", 47, 5, 3 },
                    { 105, new DateTime(2019, 9, 14, 1, 4, 6, 21, DateTimeKind.Unspecified).AddTicks(5386), "Adipisci itaque perferendis odit qui autem odit.", new DateTime(2022, 11, 15, 21, 20, 5, 34, DateTimeKind.Unspecified).AddTicks(7932), "Aliquid ut.", 10, 2, 0 },
                    { 106, new DateTime(2019, 5, 21, 19, 7, 31, 503, DateTimeKind.Unspecified).AddTicks(33), "Similique sunt dolorum.", new DateTime(2021, 9, 27, 11, 33, 50, 544, DateTimeKind.Unspecified).AddTicks(4098), "Molestias eum.", 45, 1, 0 },
                    { 107, new DateTime(2019, 11, 19, 4, 43, 21, 881, DateTimeKind.Unspecified).AddTicks(9221), "Velit laudantium beatae quo et temporibus minus.", new DateTime(2021, 1, 30, 21, 10, 57, 574, DateTimeKind.Unspecified).AddTicks(539), "Est omnis.", 2, 3, 2 },
                    { 108, new DateTime(2019, 10, 30, 6, 13, 57, 486, DateTimeKind.Unspecified).AddTicks(1586), "Architecto et quas iusto vel temporibus laboriosam sit sint.", new DateTime(2022, 6, 19, 13, 35, 10, 136, DateTimeKind.Unspecified).AddTicks(7300), "Voluptas aut.", 42, 4, 0 },
                    { 109, new DateTime(2019, 7, 30, 6, 42, 25, 934, DateTimeKind.Unspecified).AddTicks(5223), "Voluptatum quia laudantium minima explicabo aperiam ratione eos vitae voluptas.", new DateTime(2021, 11, 18, 10, 41, 50, 154, DateTimeKind.Unspecified).AddTicks(8015), "Id quibusdam.", 33, 3, 2 },
                    { 110, new DateTime(2019, 3, 19, 15, 43, 58, 267, DateTimeKind.Unspecified).AddTicks(8631), "Nostrum odit ipsam dolor voluptatum ex cum ratione.", new DateTime(2022, 6, 15, 18, 34, 16, 397, DateTimeKind.Unspecified).AddTicks(2155), "Quis fugiat.", 43, 1, 1 },
                    { 111, new DateTime(2019, 2, 15, 14, 33, 52, 628, DateTimeKind.Unspecified).AddTicks(7196), "Qui doloribus distinctio neque voluptas.", new DateTime(2022, 10, 30, 10, 13, 0, 533, DateTimeKind.Unspecified).AddTicks(3813), "Vel porro.", 26, 2, 1 },
                    { 112, new DateTime(2019, 8, 27, 3, 40, 41, 405, DateTimeKind.Unspecified).AddTicks(5218), "A incidunt qui quas est quisquam.", new DateTime(2022, 7, 1, 11, 31, 36, 145, DateTimeKind.Unspecified).AddTicks(5819), "Qui voluptate.", 36, 1, 2 },
                    { 113, new DateTime(2019, 5, 19, 17, 55, 36, 406, DateTimeKind.Unspecified).AddTicks(3008), "Maxime consequatur atque optio.", new DateTime(2021, 2, 26, 22, 7, 42, 794, DateTimeKind.Unspecified).AddTicks(2677), "Ea quia.", 1, 2, 3 },
                    { 114, new DateTime(2019, 11, 23, 3, 54, 21, 89, DateTimeKind.Unspecified).AddTicks(5079), "Error fugiat repellat eius voluptatem occaecati modi voluptas ut quae.", new DateTime(2021, 7, 15, 18, 0, 21, 275, DateTimeKind.Unspecified).AddTicks(2473), "Iure est.", 48, 3, 2 },
                    { 115, new DateTime(2019, 8, 7, 18, 55, 56, 367, DateTimeKind.Unspecified).AddTicks(841), "Asperiores tempora hic reiciendis perferendis ipsum rerum deserunt.", new DateTime(2022, 1, 25, 5, 9, 29, 188, DateTimeKind.Unspecified).AddTicks(5221), "Repellendus ea.", 12, 4, 2 },
                    { 116, new DateTime(2019, 4, 29, 10, 10, 40, 713, DateTimeKind.Unspecified).AddTicks(4997), "Quo earum itaque amet nemo.", new DateTime(2022, 11, 6, 15, 33, 37, 969, DateTimeKind.Unspecified).AddTicks(4878), "Laboriosam odit.", 44, 1, 3 },
                    { 117, new DateTime(2019, 10, 6, 6, 9, 50, 477, DateTimeKind.Unspecified).AddTicks(4175), "Voluptatem veritatis vitae voluptas modi quo nulla ipsam.", new DateTime(2022, 4, 18, 5, 18, 28, 618, DateTimeKind.Unspecified).AddTicks(1589), "Repudiandae inventore.", 35, 3, 2 },
                    { 118, new DateTime(2019, 6, 21, 15, 1, 12, 988, DateTimeKind.Unspecified).AddTicks(4342), "Aut quaerat quas consectetur necessitatibus velit.", new DateTime(2021, 4, 25, 0, 41, 27, 753, DateTimeKind.Unspecified).AddTicks(7432), "Eveniet aperiam.", 27, 1, 2 },
                    { 119, new DateTime(2019, 10, 26, 7, 4, 25, 315, DateTimeKind.Unspecified).AddTicks(4744), "Quas omnis fuga repellendus id veritatis enim beatae.", new DateTime(2022, 8, 22, 4, 19, 54, 763, DateTimeKind.Unspecified).AddTicks(3790), "Quos odio.", 39, 4, 3 },
                    { 120, new DateTime(2019, 11, 13, 10, 19, 15, 282, DateTimeKind.Unspecified).AddTicks(5143), "Cupiditate ea assumenda et unde amet non tempore sunt.", new DateTime(2022, 1, 13, 5, 19, 7, 645, DateTimeKind.Unspecified).AddTicks(2124), "Rerum ipsum.", 16, 4, 0 },
                    { 121, new DateTime(2019, 11, 15, 23, 47, 15, 615, DateTimeKind.Unspecified).AddTicks(6021), "Blanditiis adipisci molestiae quis omnis laborum totam voluptatibus accusantium.", new DateTime(2021, 12, 19, 9, 27, 9, 751, DateTimeKind.Unspecified).AddTicks(5159), "Veniam quidem.", 9, 4, 3 },
                    { 122, new DateTime(2019, 11, 2, 20, 7, 20, 220, DateTimeKind.Unspecified).AddTicks(5322), "Occaecati sint veniam dolores quae ullam sit eum.", new DateTime(2022, 9, 3, 16, 39, 38, 719, DateTimeKind.Unspecified).AddTicks(6794), "Ad suscipit.", 34, 2, 2 },
                    { 123, new DateTime(2019, 7, 23, 10, 54, 36, 848, DateTimeKind.Unspecified).AddTicks(1148), "Et dignissimos et.", new DateTime(2022, 2, 15, 16, 0, 37, 889, DateTimeKind.Unspecified).AddTicks(7714), "Sit ipsum.", 43, 4, 1 },
                    { 124, new DateTime(2019, 3, 5, 17, 17, 26, 127, DateTimeKind.Unspecified).AddTicks(5123), "Quia saepe at dolorum distinctio dolorem perspiciatis perferendis sed sint.", new DateTime(2022, 7, 31, 0, 46, 6, 756, DateTimeKind.Unspecified).AddTicks(4936), "Ipsam laboriosam.", 35, 4, 2 },
                    { 125, new DateTime(2019, 5, 28, 2, 33, 29, 620, DateTimeKind.Unspecified).AddTicks(1436), "Quaerat qui cupiditate et vel id.", new DateTime(2021, 8, 11, 7, 48, 36, 381, DateTimeKind.Unspecified).AddTicks(2619), "Voluptates repudiandae.", 50, 3, 0 },
                    { 126, new DateTime(2019, 11, 22, 18, 54, 29, 627, DateTimeKind.Unspecified).AddTicks(3391), "Aliquid sint sit non ipsam molestias autem quae ipsa quo.", new DateTime(2022, 6, 30, 15, 52, 52, 590, DateTimeKind.Unspecified).AddTicks(6070), "Magni illo.", 27, 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 127, new DateTime(2019, 8, 3, 4, 59, 1, 505, DateTimeKind.Unspecified).AddTicks(8013), "Sint animi nam vero officiis.", new DateTime(2022, 12, 30, 12, 19, 55, 974, DateTimeKind.Unspecified).AddTicks(4712), "Iure minima.", 24, 1, 3 },
                    { 128, new DateTime(2019, 8, 13, 18, 12, 27, 894, DateTimeKind.Unspecified).AddTicks(9190), "Dolorem sit est cupiditate.", new DateTime(2021, 5, 30, 21, 48, 1, 958, DateTimeKind.Unspecified).AddTicks(8179), "Ut debitis.", 30, 3, 3 },
                    { 129, new DateTime(2019, 2, 6, 21, 0, 43, 700, DateTimeKind.Unspecified).AddTicks(6835), "Et eum quia sit magni nobis laborum quas quis et.", new DateTime(2022, 12, 1, 12, 1, 21, 217, DateTimeKind.Unspecified).AddTicks(8321), "Quasi praesentium.", 33, 1, 0 },
                    { 130, new DateTime(2019, 7, 25, 5, 27, 11, 478, DateTimeKind.Unspecified).AddTicks(827), "Fuga ut ratione perspiciatis sequi et sed dolores accusantium cumque.", new DateTime(2021, 7, 21, 18, 7, 11, 512, DateTimeKind.Unspecified).AddTicks(720), "Velit cumque.", 5, 3, 2 },
                    { 131, new DateTime(2019, 3, 16, 17, 42, 29, 463, DateTimeKind.Unspecified).AddTicks(6135), "Rerum consectetur rerum ea id odio sed.", new DateTime(2022, 3, 4, 17, 43, 53, 207, DateTimeKind.Unspecified).AddTicks(4951), "Corrupti id.", 37, 3, 1 },
                    { 132, new DateTime(2019, 11, 27, 22, 45, 57, 471, DateTimeKind.Unspecified).AddTicks(8968), "Magni dolore eveniet odio distinctio officiis quasi atque adipisci in.", new DateTime(2022, 11, 15, 22, 50, 21, 614, DateTimeKind.Unspecified).AddTicks(6202), "Dolore quae.", 31, 2, 0 },
                    { 133, new DateTime(2019, 1, 30, 19, 21, 39, 156, DateTimeKind.Unspecified).AddTicks(4076), "Sed eum blanditiis.", new DateTime(2021, 7, 14, 0, 11, 43, 560, DateTimeKind.Unspecified).AddTicks(1740), "Consectetur dolore.", 39, 5, 3 },
                    { 134, new DateTime(2019, 8, 18, 8, 7, 43, 515, DateTimeKind.Unspecified).AddTicks(1553), "Consequatur labore possimus enim.", new DateTime(2022, 5, 12, 4, 3, 18, 312, DateTimeKind.Unspecified).AddTicks(5566), "Vitae nihil.", 8, 4, 0 },
                    { 135, new DateTime(2019, 4, 17, 15, 37, 30, 57, DateTimeKind.Unspecified).AddTicks(9906), "Earum animi assumenda.", new DateTime(2021, 3, 30, 12, 9, 48, 242, DateTimeKind.Unspecified).AddTicks(9590), "Id qui.", 19, 3, 3 },
                    { 136, new DateTime(2019, 10, 20, 6, 30, 21, 647, DateTimeKind.Unspecified).AddTicks(2007), "Voluptates vel ut voluptatem maiores eum et numquam quaerat.", new DateTime(2021, 10, 5, 14, 56, 7, 695, DateTimeKind.Unspecified).AddTicks(7857), "Rerum amet.", 30, 1, 3 },
                    { 137, new DateTime(2019, 9, 30, 5, 16, 36, 579, DateTimeKind.Unspecified).AddTicks(6492), "Odit deserunt hic dolor quia doloremque maiores.", new DateTime(2021, 6, 21, 19, 10, 24, 989, DateTimeKind.Unspecified).AddTicks(7014), "Et nam.", 27, 3, 3 },
                    { 138, new DateTime(2019, 5, 2, 16, 47, 7, 731, DateTimeKind.Unspecified).AddTicks(3848), "Itaque ut eum accusantium nostrum temporibus sit.", new DateTime(2022, 4, 20, 7, 36, 49, 179, DateTimeKind.Unspecified).AddTicks(1541), "Nihil temporibus.", 47, 5, 3 },
                    { 139, new DateTime(2019, 12, 22, 7, 40, 14, 649, DateTimeKind.Unspecified).AddTicks(6658), "Sint porro odio eveniet pariatur labore sit.", new DateTime(2021, 10, 10, 0, 33, 13, 138, DateTimeKind.Unspecified).AddTicks(6171), "Et iure.", 19, 4, 0 },
                    { 140, new DateTime(2019, 9, 5, 19, 45, 22, 3, DateTimeKind.Unspecified).AddTicks(3980), "Inventore fuga voluptas earum praesentium maxime repellat.", new DateTime(2022, 7, 29, 12, 49, 15, 682, DateTimeKind.Unspecified).AddTicks(4566), "Excepturi aut.", 29, 1, 3 },
                    { 141, new DateTime(2019, 3, 16, 1, 32, 50, 914, DateTimeKind.Unspecified).AddTicks(3568), "Rem quas impedit beatae quod occaecati impedit.", new DateTime(2022, 6, 14, 10, 31, 12, 997, DateTimeKind.Unspecified).AddTicks(2010), "Debitis et.", 6, 1, 1 },
                    { 142, new DateTime(2019, 3, 21, 0, 38, 20, 636, DateTimeKind.Unspecified).AddTicks(354), "Quibusdam beatae est id et sit laboriosam id.", new DateTime(2021, 7, 12, 12, 30, 46, 275, DateTimeKind.Unspecified).AddTicks(3445), "Nisi consectetur.", 24, 4, 1 },
                    { 143, new DateTime(2019, 6, 22, 5, 50, 35, 736, DateTimeKind.Unspecified).AddTicks(9629), "Non et eius quia sit molestiae nostrum.", new DateTime(2021, 4, 19, 2, 21, 28, 115, DateTimeKind.Unspecified).AddTicks(1032), "Accusantium porro.", 5, 3, 2 },
                    { 144, new DateTime(2019, 9, 18, 6, 21, 39, 855, DateTimeKind.Unspecified).AddTicks(6059), "Ex non pariatur sapiente quas eligendi ut numquam non.", new DateTime(2021, 2, 5, 12, 34, 27, 9, DateTimeKind.Unspecified).AddTicks(8243), "Tempora deserunt.", 15, 5, 3 },
                    { 145, new DateTime(2019, 9, 14, 2, 30, 42, 146, DateTimeKind.Unspecified).AddTicks(2807), "Non sunt corporis beatae.", new DateTime(2021, 8, 24, 17, 14, 18, 797, DateTimeKind.Unspecified).AddTicks(5812), "Expedita et.", 33, 4, 3 },
                    { 146, new DateTime(2019, 3, 6, 0, 55, 47, 37, DateTimeKind.Unspecified).AddTicks(6281), "Voluptates doloribus laborum ut voluptas quibusdam.", new DateTime(2021, 5, 28, 21, 46, 58, 995, DateTimeKind.Unspecified).AddTicks(4041), "Aut suscipit.", 43, 4, 0 },
                    { 147, new DateTime(2019, 6, 6, 9, 56, 23, 71, DateTimeKind.Unspecified).AddTicks(90), "Quam dolorem neque aliquam qui nam velit.", new DateTime(2021, 9, 27, 22, 13, 48, 316, DateTimeKind.Unspecified).AddTicks(8656), "Dicta aperiam.", 22, 3, 0 },
                    { 148, new DateTime(2019, 4, 21, 5, 14, 35, 748, DateTimeKind.Unspecified).AddTicks(9686), "Facilis atque nesciunt non aspernatur.", new DateTime(2022, 3, 12, 4, 26, 0, 272, DateTimeKind.Unspecified).AddTicks(9199), "Et nemo.", 35, 4, 1 },
                    { 149, new DateTime(2019, 3, 12, 7, 20, 8, 103, DateTimeKind.Unspecified).AddTicks(4736), "Hic quis incidunt eum impedit.", new DateTime(2022, 1, 29, 2, 21, 21, 242, DateTimeKind.Unspecified).AddTicks(7964), "Ex amet.", 29, 3, 0 },
                    { 150, new DateTime(2019, 10, 29, 2, 16, 11, 747, DateTimeKind.Unspecified).AddTicks(1922), "Nostrum nihil omnis sint eos.", new DateTime(2021, 10, 1, 1, 6, 17, 302, DateTimeKind.Unspecified).AddTicks(8932), "Numquam aut.", 4, 1, 3 },
                    { 151, new DateTime(2019, 3, 23, 21, 12, 13, 307, DateTimeKind.Unspecified).AddTicks(4497), "Rerum omnis et est neque cupiditate ipsa laborum veritatis aliquam.", new DateTime(2022, 5, 10, 21, 42, 42, 457, DateTimeKind.Unspecified).AddTicks(7837), "Delectus harum.", 13, 2, 3 },
                    { 152, new DateTime(2019, 1, 8, 21, 47, 52, 156, DateTimeKind.Unspecified).AddTicks(52), "Quisquam in dolorum beatae alias id non optio.", new DateTime(2022, 11, 13, 13, 36, 56, 370, DateTimeKind.Unspecified).AddTicks(4448), "Est enim.", 42, 5, 3 },
                    { 153, new DateTime(2019, 10, 29, 3, 2, 50, 118, DateTimeKind.Unspecified).AddTicks(1142), "Nisi fugiat saepe repudiandae quas et sit eum optio.", new DateTime(2022, 1, 3, 5, 3, 40, 480, DateTimeKind.Unspecified).AddTicks(7765), "Est enim.", 45, 2, 0 },
                    { 154, new DateTime(2019, 5, 7, 7, 37, 22, 990, DateTimeKind.Unspecified).AddTicks(8610), "Non sed quam beatae et.", new DateTime(2021, 1, 1, 8, 2, 9, 114, DateTimeKind.Unspecified).AddTicks(3126), "Molestiae nesciunt.", 34, 2, 1 },
                    { 155, new DateTime(2019, 5, 13, 19, 37, 5, 500, DateTimeKind.Unspecified).AddTicks(8919), "Excepturi veniam laboriosam.", new DateTime(2022, 12, 6, 3, 13, 38, 754, DateTimeKind.Unspecified).AddTicks(3759), "Eum aut.", 45, 5, 2 },
                    { 156, new DateTime(2019, 10, 7, 7, 47, 26, 810, DateTimeKind.Unspecified).AddTicks(2335), "Et molestias fugiat mollitia facere ex id ipsa earum.", new DateTime(2021, 12, 20, 12, 16, 0, 867, DateTimeKind.Unspecified).AddTicks(9273), "Molestias quasi.", 4, 4, 0 },
                    { 157, new DateTime(2019, 6, 17, 15, 13, 33, 405, DateTimeKind.Unspecified).AddTicks(5424), "Rerum incidunt sed ut tempore.", new DateTime(2021, 3, 11, 0, 21, 3, 561, DateTimeKind.Unspecified).AddTicks(1619), "Voluptatum voluptatem.", 5, 5, 3 },
                    { 158, new DateTime(2019, 8, 17, 19, 59, 26, 620, DateTimeKind.Unspecified).AddTicks(8612), "Expedita iure eos ullam quod corporis enim dolor dolor sunt.", new DateTime(2021, 8, 16, 15, 19, 45, 15, DateTimeKind.Unspecified).AddTicks(1072), "Et fuga.", 43, 2, 3 },
                    { 159, new DateTime(2019, 9, 16, 8, 49, 29, 986, DateTimeKind.Unspecified).AddTicks(4517), "Impedit quidem provident accusantium molestiae est.", new DateTime(2021, 5, 2, 4, 10, 41, 151, DateTimeKind.Unspecified).AddTicks(6456), "Dolor cupiditate.", 40, 2, 3 },
                    { 160, new DateTime(2019, 6, 4, 1, 13, 39, 870, DateTimeKind.Unspecified).AddTicks(1521), "Ut dolores et debitis aspernatur ea.", new DateTime(2022, 1, 21, 6, 3, 4, 527, DateTimeKind.Unspecified).AddTicks(4080), "Nisi qui.", 2, 3, 1 },
                    { 161, new DateTime(2019, 8, 29, 18, 17, 0, 775, DateTimeKind.Unspecified).AddTicks(3177), "Id sint et repellendus enim natus dolore dolor ducimus ullam.", new DateTime(2022, 12, 7, 11, 28, 30, 21, DateTimeKind.Unspecified).AddTicks(6142), "Libero et.", 16, 5, 0 },
                    { 162, new DateTime(2019, 5, 22, 5, 7, 2, 916, DateTimeKind.Unspecified).AddTicks(785), "Ipsa soluta quam consequatur ab et veritatis sunt.", new DateTime(2022, 11, 15, 4, 58, 13, 443, DateTimeKind.Unspecified).AddTicks(1441), "Est voluptas.", 45, 1, 2 },
                    { 163, new DateTime(2019, 9, 26, 2, 31, 9, 649, DateTimeKind.Unspecified).AddTicks(4219), "Natus id dicta nihil possimus totam earum in ipsam aspernatur.", new DateTime(2022, 2, 26, 21, 16, 36, 146, DateTimeKind.Unspecified).AddTicks(7761), "Est maiores.", 2, 2, 0 },
                    { 164, new DateTime(2019, 7, 29, 15, 22, 8, 617, DateTimeKind.Unspecified).AddTicks(558), "Voluptates rerum sunt vitae.", new DateTime(2022, 1, 25, 7, 26, 27, 993, DateTimeKind.Unspecified).AddTicks(6587), "Est minima.", 23, 3, 2 },
                    { 165, new DateTime(2019, 8, 27, 15, 39, 2, 484, DateTimeKind.Unspecified).AddTicks(3350), "Distinctio illum in dicta placeat consectetur.", new DateTime(2022, 1, 12, 17, 38, 1, 762, DateTimeKind.Unspecified).AddTicks(3103), "Quia et.", 30, 5, 3 },
                    { 166, new DateTime(2019, 5, 20, 9, 58, 19, 646, DateTimeKind.Unspecified).AddTicks(1828), "Assumenda in ea voluptates quae pariatur praesentium minima non.", new DateTime(2022, 3, 21, 8, 59, 52, 955, DateTimeKind.Unspecified).AddTicks(418), "Architecto aut.", 26, 3, 1 },
                    { 167, new DateTime(2019, 1, 13, 12, 17, 12, 153, DateTimeKind.Unspecified).AddTicks(4250), "Itaque omnis iusto.", new DateTime(2022, 2, 9, 4, 27, 57, 785, DateTimeKind.Unspecified).AddTicks(8346), "Rem similique.", 12, 1, 2 },
                    { 168, new DateTime(2019, 4, 2, 3, 42, 10, 998, DateTimeKind.Unspecified).AddTicks(4697), "Saepe voluptas earum ab.", new DateTime(2022, 1, 17, 22, 54, 32, 666, DateTimeKind.Unspecified).AddTicks(8834), "Sequi quibusdam.", 32, 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 169, new DateTime(2019, 7, 27, 9, 29, 57, 263, DateTimeKind.Unspecified).AddTicks(1473), "Molestiae quis molestiae impedit.", new DateTime(2022, 6, 13, 18, 2, 40, 757, DateTimeKind.Unspecified).AddTicks(6103), "Unde sit.", 2, 4, 1 },
                    { 170, new DateTime(2019, 8, 31, 7, 28, 6, 909, DateTimeKind.Unspecified).AddTicks(2619), "Deleniti qui et voluptas debitis in eligendi.", new DateTime(2021, 9, 26, 21, 16, 51, 826, DateTimeKind.Unspecified).AddTicks(6505), "Inventore sunt.", 39, 2, 1 },
                    { 171, new DateTime(2019, 4, 12, 11, 50, 21, 297, DateTimeKind.Unspecified).AddTicks(5811), "Eligendi ducimus ut dolorem est voluptatem architecto explicabo.", new DateTime(2022, 8, 28, 13, 11, 56, 181, DateTimeKind.Unspecified).AddTicks(2553), "Blanditiis eos.", 5, 4, 2 },
                    { 172, new DateTime(2019, 9, 18, 2, 32, 13, 983, DateTimeKind.Unspecified).AddTicks(7926), "Labore rerum omnis.", new DateTime(2022, 12, 15, 15, 6, 3, 448, DateTimeKind.Unspecified).AddTicks(8427), "Iusto est.", 16, 4, 0 },
                    { 173, new DateTime(2019, 12, 2, 13, 2, 51, 731, DateTimeKind.Unspecified).AddTicks(5471), "Esse eum numquam temporibus architecto sapiente laborum sit laboriosam.", new DateTime(2022, 4, 2, 6, 22, 8, 287, DateTimeKind.Unspecified).AddTicks(7366), "Laudantium reiciendis.", 7, 3, 2 },
                    { 174, new DateTime(2019, 10, 7, 13, 36, 51, 355, DateTimeKind.Unspecified).AddTicks(1677), "Voluptas numquam ipsa ullam earum architecto saepe eligendi tempore ut.", new DateTime(2022, 1, 26, 9, 37, 42, 654, DateTimeKind.Unspecified).AddTicks(6759), "Esse quod.", 50, 2, 2 },
                    { 175, new DateTime(2019, 9, 24, 6, 6, 26, 678, DateTimeKind.Unspecified).AddTicks(6413), "Vero recusandae rerum qui tempore.", new DateTime(2022, 3, 14, 3, 35, 57, 492, DateTimeKind.Unspecified).AddTicks(7718), "Sit voluptatem.", 16, 5, 0 },
                    { 176, new DateTime(2019, 12, 26, 22, 12, 31, 331, DateTimeKind.Unspecified).AddTicks(1492), "Fuga asperiores a et molestiae reiciendis fuga maxime est vitae.", new DateTime(2022, 1, 30, 21, 14, 16, 66, DateTimeKind.Unspecified).AddTicks(8216), "Repellat dolorem.", 27, 2, 1 },
                    { 177, new DateTime(2019, 3, 31, 20, 14, 8, 795, DateTimeKind.Unspecified).AddTicks(2186), "Esse inventore et quis perferendis eius enim.", new DateTime(2022, 3, 17, 19, 16, 58, 644, DateTimeKind.Unspecified).AddTicks(7238), "Neque asperiores.", 33, 3, 3 },
                    { 178, new DateTime(2019, 3, 2, 3, 17, 27, 264, DateTimeKind.Unspecified).AddTicks(1943), "Maxime adipisci sint ratione doloribus accusamus commodi quas quos.", new DateTime(2021, 8, 15, 4, 30, 11, 496, DateTimeKind.Unspecified).AddTicks(9559), "Quam labore.", 6, 5, 1 },
                    { 179, new DateTime(2019, 4, 16, 13, 46, 55, 821, DateTimeKind.Unspecified).AddTicks(5562), "Aut voluptatem voluptas esse qui.", new DateTime(2022, 1, 25, 16, 53, 48, 844, DateTimeKind.Unspecified).AddTicks(2212), "Repellat cum.", 6, 1, 3 },
                    { 180, new DateTime(2019, 12, 24, 10, 30, 2, 926, DateTimeKind.Unspecified).AddTicks(1745), "Saepe voluptatem et beatae est.", new DateTime(2022, 6, 24, 17, 18, 4, 841, DateTimeKind.Unspecified).AddTicks(8319), "Omnis nostrum.", 5, 2, 1 },
                    { 181, new DateTime(2019, 4, 20, 4, 21, 22, 202, DateTimeKind.Unspecified).AddTicks(6834), "Ut praesentium eos sit accusamus ut consequatur voluptatum rerum.", new DateTime(2021, 11, 29, 15, 47, 32, 120, DateTimeKind.Unspecified).AddTicks(5144), "Ullam officia.", 37, 3, 2 },
                    { 182, new DateTime(2019, 10, 24, 19, 53, 20, 586, DateTimeKind.Unspecified).AddTicks(6042), "Non totam porro nostrum est voluptate est.", new DateTime(2021, 7, 16, 9, 10, 59, 191, DateTimeKind.Unspecified).AddTicks(8580), "Dolorem quibusdam.", 4, 5, 3 },
                    { 183, new DateTime(2019, 8, 3, 3, 13, 55, 956, DateTimeKind.Unspecified).AddTicks(2661), "Doloribus quia vel natus fuga totam similique.", new DateTime(2022, 5, 3, 19, 22, 53, 371, DateTimeKind.Unspecified).AddTicks(48), "Ut voluptatum.", 43, 2, 1 },
                    { 184, new DateTime(2019, 3, 11, 17, 13, 12, 495, DateTimeKind.Unspecified).AddTicks(6531), "A enim et.", new DateTime(2022, 12, 4, 5, 15, 13, 679, DateTimeKind.Unspecified).AddTicks(4965), "Iure non.", 28, 1, 1 },
                    { 185, new DateTime(2019, 5, 1, 8, 21, 45, 893, DateTimeKind.Unspecified).AddTicks(3706), "Libero esse aliquid et a dolorem rerum fuga eveniet eligendi.", new DateTime(2022, 5, 4, 6, 8, 30, 110, DateTimeKind.Unspecified).AddTicks(8198), "Non labore.", 50, 5, 1 },
                    { 186, new DateTime(2019, 7, 25, 19, 46, 6, 538, DateTimeKind.Unspecified).AddTicks(6072), "Voluptate vero consequatur.", new DateTime(2022, 3, 26, 19, 12, 17, 964, DateTimeKind.Unspecified).AddTicks(9257), "Placeat qui.", 43, 5, 2 },
                    { 187, new DateTime(2019, 3, 26, 2, 3, 20, 874, DateTimeKind.Unspecified).AddTicks(1401), "Voluptas nam nemo ducimus ab.", new DateTime(2021, 8, 12, 21, 5, 13, 96, DateTimeKind.Unspecified).AddTicks(5468), "Et explicabo.", 16, 3, 2 },
                    { 188, new DateTime(2019, 10, 16, 6, 19, 54, 621, DateTimeKind.Unspecified).AddTicks(7047), "Provident reprehenderit cupiditate dolore voluptatem illum itaque qui qui.", new DateTime(2022, 2, 25, 4, 35, 29, 450, DateTimeKind.Unspecified).AddTicks(7214), "Recusandae odit.", 34, 4, 2 },
                    { 189, new DateTime(2019, 4, 26, 10, 24, 44, 191, DateTimeKind.Unspecified).AddTicks(6748), "Hic quia rerum hic modi iste.", new DateTime(2022, 6, 26, 13, 12, 42, 643, DateTimeKind.Unspecified).AddTicks(8576), "Cum aut.", 33, 1, 1 },
                    { 190, new DateTime(2019, 10, 26, 15, 20, 23, 24, DateTimeKind.Unspecified).AddTicks(7735), "Dolorem aut accusamus omnis culpa vel aut.", new DateTime(2021, 1, 5, 8, 44, 17, 732, DateTimeKind.Unspecified).AddTicks(7128), "Molestiae minus.", 47, 3, 3 },
                    { 191, new DateTime(2019, 5, 11, 4, 42, 37, 301, DateTimeKind.Unspecified).AddTicks(9328), "Ut quas numquam consequuntur velit voluptas.", new DateTime(2022, 7, 19, 3, 44, 46, 813, DateTimeKind.Unspecified).AddTicks(5411), "Voluptatem nam.", 13, 5, 3 },
                    { 192, new DateTime(2019, 5, 20, 4, 0, 51, 245, DateTimeKind.Unspecified).AddTicks(2484), "Vitae tempore eveniet minus dolorem.", new DateTime(2022, 4, 16, 22, 3, 25, 938, DateTimeKind.Unspecified).AddTicks(2096), "Qui distinctio.", 20, 4, 1 },
                    { 193, new DateTime(2019, 11, 18, 2, 7, 34, 789, DateTimeKind.Unspecified).AddTicks(8774), "Et inventore consequuntur maiores qui et sed inventore sint.", new DateTime(2022, 5, 16, 23, 25, 55, 881, DateTimeKind.Unspecified).AddTicks(8433), "Eius unde.", 29, 5, 1 },
                    { 194, new DateTime(2019, 4, 18, 20, 15, 17, 238, DateTimeKind.Unspecified).AddTicks(8453), "Possimus ad eos quia.", new DateTime(2022, 4, 28, 10, 14, 3, 461, DateTimeKind.Unspecified).AddTicks(6245), "Aspernatur repudiandae.", 40, 3, 3 },
                    { 195, new DateTime(2019, 1, 21, 4, 55, 53, 749, DateTimeKind.Unspecified).AddTicks(7685), "Quia sit quia voluptatem asperiores cum adipisci.", new DateTime(2022, 2, 28, 6, 54, 30, 841, DateTimeKind.Unspecified).AddTicks(2520), "Aliquid libero.", 33, 3, 3 },
                    { 196, new DateTime(2019, 1, 19, 9, 50, 4, 778, DateTimeKind.Unspecified).AddTicks(8803), "Ducimus nesciunt sunt ab architecto.", new DateTime(2021, 10, 19, 1, 17, 36, 100, DateTimeKind.Unspecified).AddTicks(6620), "Autem iure.", 14, 4, 1 },
                    { 197, new DateTime(2019, 11, 19, 7, 25, 30, 43, DateTimeKind.Unspecified).AddTicks(5749), "Fugiat deserunt cupiditate et aut iure et consequatur fugit ea.", new DateTime(2021, 3, 23, 8, 24, 33, 89, DateTimeKind.Unspecified).AddTicks(8919), "Earum eaque.", 49, 1, 1 },
                    { 198, new DateTime(2019, 2, 4, 5, 43, 45, 289, DateTimeKind.Unspecified).AddTicks(7866), "Ut unde quisquam est qui facere velit.", new DateTime(2022, 5, 20, 3, 21, 33, 61, DateTimeKind.Unspecified).AddTicks(9802), "Sed quibusdam.", 33, 2, 3 },
                    { 199, new DateTime(2019, 6, 1, 21, 11, 15, 261, DateTimeKind.Unspecified).AddTicks(6315), "Aliquam quibusdam mollitia quas reiciendis omnis ut.", new DateTime(2022, 12, 31, 6, 4, 57, 903, DateTimeKind.Unspecified).AddTicks(6571), "Ducimus est.", 12, 1, 3 },
                    { 200, new DateTime(2019, 2, 2, 22, 21, 45, 171, DateTimeKind.Unspecified).AddTicks(1303), "Nihil quibusdam id fuga.", new DateTime(2022, 2, 22, 3, 3, 12, 941, DateTimeKind.Unspecified).AddTicks(6558), "Fugit ut.", 7, 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTasks_PerformerId",
                table: "ProjectTasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTasks_ProjectId",
                table: "ProjectTasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectTasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
