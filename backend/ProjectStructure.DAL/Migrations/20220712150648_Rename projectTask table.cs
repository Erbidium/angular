﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectStructure.DAL.Migrations
{
    public partial class RenameprojectTasktable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectTasks_Projects_ProjectId",
                table: "ProjectTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectTasks_Users_PerformerId",
                table: "ProjectTasks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjectTasks",
                table: "ProjectTasks");

            migrationBuilder.RenameTable(
                name: "ProjectTasks",
                newName: "Task");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectTasks_ProjectId",
                table: "Task",
                newName: "IX_Task_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectTasks_PerformerId",
                table: "Task",
                newName: "IX_Task_PerformerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Task",
                table: "Task",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 11, new DateTime(2019, 12, 6, 5, 59, 28, 923, DateTimeKind.Unspecified).AddTicks(4974), new DateTime(2022, 10, 10, 19, 9, 12, 812, DateTimeKind.Unspecified).AddTicks(2543), "Non eius labore dolorem assumenda sunt aut qui quae.", "Refined Rubber Computer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 11, 3, 4, 50, 35, 159, DateTimeKind.Unspecified).AddTicks(86), new DateTime(2021, 4, 28, 14, 5, 20, 500, DateTimeKind.Unspecified).AddTicks(3086), "Asperiores dignissimos voluptatem voluptatibus saepe dolor ut omnis asperiores nihil.", "Handmade Frozen Bike", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 43, new DateTime(2019, 8, 2, 6, 41, 44, 926, DateTimeKind.Unspecified).AddTicks(7214), new DateTime(2022, 5, 1, 4, 1, 37, 454, DateTimeKind.Unspecified).AddTicks(7716), "Id vero tenetur et omnis rerum est est aperiam.", "Small Wooden Table", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 8, 28, 19, 49, 26, 128, DateTimeKind.Unspecified).AddTicks(3879), new DateTime(2021, 3, 9, 8, 21, 23, 102, DateTimeKind.Unspecified).AddTicks(1885), "Ut et fugit asperiores enim iure excepturi corrupti dolores.", "Tasty Soft Chair", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 34, new DateTime(2019, 6, 22, 0, 35, 17, 952, DateTimeKind.Unspecified).AddTicks(883), new DateTime(2021, 5, 22, 18, 7, 34, 743, DateTimeKind.Unspecified).AddTicks(9702), "Tenetur et repellat et sit sint eum.", "Practical Soft Chair", 10 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 17, 18, 5, 802, DateTimeKind.Unspecified).AddTicks(5373), "Repudiandae et tenetur autem rerum est.", new DateTime(2022, 4, 16, 10, 11, 12, 377, DateTimeKind.Unspecified).AddTicks(7762), "Atque nobis.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 17, 9, 21, 21, 601, DateTimeKind.Unspecified).AddTicks(600), "Eos modi recusandae quae debitis.", new DateTime(2021, 5, 2, 4, 31, 11, 860, DateTimeKind.Unspecified).AddTicks(6197), "Tenetur nihil.", 18, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 28, 9, 35, 16, 566, DateTimeKind.Unspecified).AddTicks(335), "Qui distinctio ut.", new DateTime(2021, 6, 5, 20, 53, 55, 836, DateTimeKind.Unspecified).AddTicks(7886), "Occaecati rerum.", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 23, 13, 31, 28, 342, DateTimeKind.Unspecified).AddTicks(9920), "Et laudantium optio asperiores fugit.", new DateTime(2021, 7, 7, 21, 53, 46, 805, DateTimeKind.Unspecified).AddTicks(6183), "Laboriosam nihil.", 47, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 7, 23, 40, 828, DateTimeKind.Unspecified).AddTicks(5552), "Laudantium id ea.", new DateTime(2022, 3, 29, 23, 12, 47, 837, DateTimeKind.Unspecified).AddTicks(3318), "Nisi et.", 33, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 16, 48, 23, 480, DateTimeKind.Unspecified).AddTicks(5070), "Voluptatem ratione veritatis fugiat.", new DateTime(2021, 2, 27, 10, 14, 29, 37, DateTimeKind.Unspecified).AddTicks(5186), "Non aut.", 32, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 19, 6, 22, 48, 554, DateTimeKind.Unspecified).AddTicks(4359), "Molestiae provident veniam nam ducimus recusandae eaque.", new DateTime(2021, 8, 9, 21, 16, 42, 660, DateTimeKind.Unspecified).AddTicks(4904), "Fugiat et.", 28, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 18, 1, 2, 16, 820, DateTimeKind.Unspecified).AddTicks(4029), "Aliquid aut beatae cupiditate sed consectetur ab suscipit recusandae.", new DateTime(2021, 8, 7, 7, 32, 38, 801, DateTimeKind.Unspecified).AddTicks(3236), "Quas vero.", 14, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 2, 0, 4, 20, 248, DateTimeKind.Unspecified).AddTicks(5367), "Omnis quasi neque et.", new DateTime(2022, 1, 8, 13, 36, 14, 479, DateTimeKind.Unspecified).AddTicks(2473), "Quisquam explicabo.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 23, 17, 26, 56, 236, DateTimeKind.Unspecified).AddTicks(5325), "Id corrupti corrupti nemo quas aliquid.", new DateTime(2022, 10, 21, 1, 4, 5, 778, DateTimeKind.Unspecified).AddTicks(9279), "Voluptatem asperiores.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 21, 8, 5, 121, DateTimeKind.Unspecified).AddTicks(8192), "Minus id deserunt qui.", new DateTime(2021, 7, 30, 2, 55, 34, 506, DateTimeKind.Unspecified).AddTicks(7751), "A itaque.", 19, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 4, 10, 27, 22, 882, DateTimeKind.Unspecified).AddTicks(1618), "Vel voluptatem voluptate et est aut.", new DateTime(2022, 3, 29, 5, 53, 25, 784, DateTimeKind.Unspecified).AddTicks(3456), "Exercitationem numquam.", 7, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 9, 6, 36, 57, 505, DateTimeKind.Unspecified).AddTicks(4262), "Dolores asperiores et distinctio temporibus ut.", new DateTime(2021, 12, 1, 8, 56, 59, 604, DateTimeKind.Unspecified).AddTicks(340), "Est voluptatem.", 23, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 3, 0, 32, 50, 653, DateTimeKind.Unspecified).AddTicks(2077), "Aliquid maxime vel.", new DateTime(2021, 12, 23, 12, 43, 14, 395, DateTimeKind.Unspecified).AddTicks(6379), "Rem corrupti.", 21, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 18, 21, 35, 54, 508, DateTimeKind.Unspecified).AddTicks(8293), "Aliquid voluptas deleniti quas.", new DateTime(2021, 12, 11, 4, 38, 42, 635, DateTimeKind.Unspecified).AddTicks(4668), "Et omnis.", 44, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 28, 0, 42, 45, 251, DateTimeKind.Unspecified).AddTicks(6655), "Placeat sed recusandae vero sit aliquam nesciunt aut est dolore.", new DateTime(2022, 1, 21, 13, 57, 58, 750, DateTimeKind.Unspecified).AddTicks(9132), "Voluptas ad.", 15, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 7, 23, 26, 33, 169, DateTimeKind.Unspecified).AddTicks(8245), "Itaque repellat illo id impedit quibusdam culpa.", new DateTime(2022, 4, 9, 7, 26, 51, 818, DateTimeKind.Unspecified).AddTicks(4435), "Beatae in.", 42, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 19, 25, 41, 938, DateTimeKind.Unspecified).AddTicks(6087), "Eligendi voluptas beatae fugiat.", new DateTime(2022, 1, 21, 17, 4, 8, 522, DateTimeKind.Unspecified).AddTicks(7993), "Saepe recusandae.", 34, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 23, 2, 23, 46, 933, DateTimeKind.Unspecified).AddTicks(3294), "Quas consequuntur ut quibusdam.", new DateTime(2021, 5, 17, 17, 56, 13, 789, DateTimeKind.Unspecified).AddTicks(2923), "Ut saepe.", 29, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 30, 18, 44, 24, 361, DateTimeKind.Unspecified).AddTicks(5646), "Illum et eos perferendis placeat quaerat quia mollitia esse iure.", new DateTime(2021, 8, 1, 16, 18, 28, 655, DateTimeKind.Unspecified).AddTicks(504), "Illum eaque.", 8, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 25, 18, 19, 12, 949, DateTimeKind.Unspecified).AddTicks(7070), "At et omnis tempora pariatur porro harum rem.", new DateTime(2022, 7, 22, 1, 28, 0, 185, DateTimeKind.Unspecified).AddTicks(561), "Est qui.", 45, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 12, 8, 30, 23, 998, DateTimeKind.Unspecified).AddTicks(6126), "Repellendus commodi dolor omnis nobis inventore ut impedit reprehenderit.", new DateTime(2022, 1, 25, 15, 48, 19, 609, DateTimeKind.Unspecified).AddTicks(3111), "Aut debitis.", 5, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 23, 14, 42, 20, 240, DateTimeKind.Unspecified).AddTicks(404), "Sint voluptate ea ullam delectus voluptatibus rerum expedita praesentium.", new DateTime(2022, 9, 29, 0, 33, 12, 392, DateTimeKind.Unspecified).AddTicks(6114), "Occaecati rerum.", 47, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 10, 1, 18, 323, DateTimeKind.Unspecified).AddTicks(3405), "Quia ratione omnis et alias.", new DateTime(2022, 8, 30, 17, 15, 29, 839, DateTimeKind.Unspecified).AddTicks(3888), "Reiciendis veniam.", 12, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 1, 18, 3, 20, 832, DateTimeKind.Unspecified).AddTicks(4115), "Iusto sint qui et vitae minus rerum quo placeat.", new DateTime(2022, 5, 26, 14, 0, 42, 873, DateTimeKind.Unspecified).AddTicks(3949), "Adipisci modi.", 18, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 4, 9, 4, 2, 548, DateTimeKind.Unspecified).AddTicks(1831), "Aut hic ab et voluptatum.", new DateTime(2022, 5, 7, 15, 29, 40, 138, DateTimeKind.Unspecified).AddTicks(3788), "Nostrum beatae.", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 26, 9, 46, 22, 675, DateTimeKind.Unspecified).AddTicks(3985), "Nihil eum facilis quia reprehenderit dignissimos.", new DateTime(2022, 1, 11, 5, 10, 49, 332, DateTimeKind.Unspecified).AddTicks(1360), "Est beatae.", 21, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 26, 13, 38, 44, 927, DateTimeKind.Unspecified).AddTicks(2390), "In voluptas non incidunt ratione odio deleniti a consequuntur consequatur.", new DateTime(2021, 4, 4, 9, 56, 54, 823, DateTimeKind.Unspecified).AddTicks(8531), "Adipisci reprehenderit.", 36, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 15, 16, 37, 989, DateTimeKind.Unspecified).AddTicks(4471), "Ut dolores necessitatibus error dignissimos voluptates.", new DateTime(2021, 12, 20, 6, 2, 6, 566, DateTimeKind.Unspecified).AddTicks(7669), "Odio qui.", 8, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 15, 13, 49, 16, 35, DateTimeKind.Unspecified).AddTicks(1290), "Minima est reprehenderit molestiae ut.", new DateTime(2021, 9, 25, 15, 34, 38, 723, DateTimeKind.Unspecified).AddTicks(9358), "Et et.", 27, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 14, 26, 13, 832, DateTimeKind.Unspecified).AddTicks(2954), "Pariatur molestias autem rerum exercitationem sit sed voluptates exercitationem.", new DateTime(2022, 11, 12, 16, 35, 11, 615, DateTimeKind.Unspecified).AddTicks(3311), "Non sint.", 23, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 0, 12, 23, 855, DateTimeKind.Unspecified).AddTicks(2796), "Quo commodi et qui impedit voluptate et ex similique.", new DateTime(2022, 7, 18, 2, 24, 59, 687, DateTimeKind.Unspecified).AddTicks(7936), "Labore aliquam.", 37, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 6, 16, 35, 37, 800, DateTimeKind.Unspecified).AddTicks(9239), "Officia id consequuntur ipsam eum maiores vel dolorem deleniti.", new DateTime(2021, 12, 9, 16, 56, 22, 399, DateTimeKind.Unspecified).AddTicks(3573), "Odit aperiam.", 29, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 1, 2, 50, 22, 6, DateTimeKind.Unspecified).AddTicks(203), "A deleniti quia ut quibusdam molestiae fuga.", new DateTime(2022, 3, 17, 2, 59, 53, 845, DateTimeKind.Unspecified).AddTicks(5234), "Voluptatum ratione.", 9, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 23, 55, 18, 761, DateTimeKind.Unspecified).AddTicks(7354), "Quaerat et qui.", new DateTime(2021, 2, 2, 22, 21, 17, 174, DateTimeKind.Unspecified).AddTicks(5124), "Dolore et.", 24, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 5, 13, 43, 204, DateTimeKind.Unspecified).AddTicks(5175), "Ut ut tempora dolores.", new DateTime(2021, 7, 22, 12, 56, 56, 837, DateTimeKind.Unspecified).AddTicks(1845), "Magnam porro.", 34, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 16, 20, 32, 54, 648, DateTimeKind.Unspecified).AddTicks(7147), "Voluptatem ipsa dolorum.", new DateTime(2021, 8, 31, 10, 12, 39, 577, DateTimeKind.Unspecified).AddTicks(8512), "Distinctio harum.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 30, 14, 16, 23, 48, DateTimeKind.Unspecified).AddTicks(9266), "Repellat eius placeat et.", new DateTime(2022, 2, 23, 18, 28, 33, 541, DateTimeKind.Unspecified).AddTicks(9341), "Mollitia sunt.", 33, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 3, 4, 45, 7, 175, DateTimeKind.Unspecified).AddTicks(9324), "Aspernatur voluptatibus totam.", new DateTime(2022, 11, 7, 9, 35, 25, 722, DateTimeKind.Unspecified).AddTicks(8107), "Rem excepturi.", 32, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 8, 13, 26, 57, 241, DateTimeKind.Unspecified).AddTicks(7891), "Quo labore numquam et sapiente nesciunt similique doloribus est voluptas.", new DateTime(2022, 5, 15, 5, 14, 55, 299, DateTimeKind.Unspecified).AddTicks(3228), "Quia velit.", 28, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 18, 5, 23, 55, 327, DateTimeKind.Unspecified).AddTicks(5319), "A sed odio.", new DateTime(2021, 2, 3, 14, 46, 23, 370, DateTimeKind.Unspecified).AddTicks(8115), "Saepe alias.", 30, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 12, 42, 52, 159, DateTimeKind.Unspecified).AddTicks(3763), "Assumenda in quod nisi.", new DateTime(2021, 10, 20, 18, 12, 42, 745, DateTimeKind.Unspecified).AddTicks(1586), "Et aut.", 50, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 5, 43, 3, 564, DateTimeKind.Unspecified).AddTicks(211), "Dolorem sunt consectetur id.", new DateTime(2021, 11, 10, 2, 21, 23, 552, DateTimeKind.Unspecified).AddTicks(8195), "Eos libero.", 14, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 6, 20, 55, 10, 621, DateTimeKind.Unspecified).AddTicks(4376), "Libero eligendi soluta.", new DateTime(2022, 4, 21, 16, 26, 47, 594, DateTimeKind.Unspecified).AddTicks(1547), "Omnis qui.", 11, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 30, 13, 19, 44, 34, DateTimeKind.Unspecified).AddTicks(5454), "Dolorum qui rerum quia eos neque cum.", new DateTime(2022, 4, 22, 19, 31, 52, 906, DateTimeKind.Unspecified).AddTicks(9803), "Doloremque autem.", 26, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 10, 11, 5, 12, 25, 348, DateTimeKind.Unspecified).AddTicks(4362), "Incidunt iste est voluptatibus dicta sit.", new DateTime(2022, 2, 1, 3, 32, 46, 399, DateTimeKind.Unspecified).AddTicks(9259), "Saepe quas.", 19 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 20, 46, 8, 646, DateTimeKind.Unspecified).AddTicks(9415), "Autem ea quam possimus.", new DateTime(2021, 8, 31, 14, 13, 12, 600, DateTimeKind.Unspecified).AddTicks(5285), "Architecto sit.", 33, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 13, 8, 14, 35, 348, DateTimeKind.Unspecified).AddTicks(2598), "Quia similique non et nisi nihil nulla nihil doloribus.", new DateTime(2022, 6, 14, 15, 40, 39, 291, DateTimeKind.Unspecified).AddTicks(3554), "Velit sed.", 34, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 16, 14, 42, 35, 958, DateTimeKind.Unspecified).AddTicks(8630), "Et autem natus porro.", new DateTime(2022, 3, 17, 7, 46, 48, 254, DateTimeKind.Unspecified).AddTicks(2804), "Id iure.", 34, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 29, 9, 45, 19, 476, DateTimeKind.Unspecified).AddTicks(8453), "Iusto maxime harum nemo qui itaque est et excepturi expedita.", new DateTime(2022, 2, 23, 16, 45, 14, 856, DateTimeKind.Unspecified).AddTicks(2289), "In consequatur.", 47, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 17, 13, 37, 46, 233, DateTimeKind.Unspecified).AddTicks(3987), "Ad odio qui ut odit.", new DateTime(2022, 5, 16, 22, 12, 17, 364, DateTimeKind.Unspecified).AddTicks(8062), "Et dolores.", 16, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 20, 10, 43, 40, DateTimeKind.Unspecified).AddTicks(8264), "Consequatur ipsum quod delectus perspiciatis voluptatem reprehenderit facere aut.", new DateTime(2022, 11, 30, 16, 31, 49, 443, DateTimeKind.Unspecified).AddTicks(6451), "Voluptatem ipsum.", 39, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 14, 11, 2, 4, 264, DateTimeKind.Unspecified).AddTicks(2309), "Est iste reprehenderit eius corporis in beatae officiis voluptatibus.", new DateTime(2022, 1, 30, 5, 29, 40, 1, DateTimeKind.Unspecified).AddTicks(7628), "Quod nobis.", 47, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 7, 5, 14, 40, 20, DateTimeKind.Unspecified).AddTicks(9841), "Iste qui ea quasi minus.", new DateTime(2021, 2, 13, 23, 27, 20, 647, DateTimeKind.Unspecified).AddTicks(9194), "Inventore et.", 50, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 4, 27, 11, 17, 11, 47, DateTimeKind.Unspecified).AddTicks(8982), "Porro possimus explicabo in eos aut iusto deleniti ipsam ipsa.", new DateTime(2022, 8, 4, 17, 39, 23, 736, DateTimeKind.Unspecified).AddTicks(5264), "Nesciunt et.", 18 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 3, 12, 50, 47, 383, DateTimeKind.Unspecified).AddTicks(827), "Sed tempora quis id sapiente ducimus nemo consequatur sunt.", new DateTime(2022, 9, 25, 3, 44, 52, 693, DateTimeKind.Unspecified).AddTicks(5536), "Iure ducimus.", 22, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 29, 6, 55, 52, 67, DateTimeKind.Unspecified).AddTicks(9046), "A et ut placeat dolores alias aut.", new DateTime(2021, 12, 8, 15, 19, 19, 359, DateTimeKind.Unspecified).AddTicks(409), "Est possimus.", 21, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 29, 4, 57, 1, 265, DateTimeKind.Unspecified).AddTicks(4896), "Consequatur perferendis sunt eos aut praesentium sunt blanditiis cum.", new DateTime(2021, 5, 29, 7, 28, 24, 781, DateTimeKind.Unspecified).AddTicks(5309), "In modi.", 32, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 4, 45, 20, 264, DateTimeKind.Unspecified).AddTicks(4903), "Asperiores ut eligendi dolore est laborum animi fuga eveniet quo.", new DateTime(2021, 1, 6, 12, 55, 13, 795, DateTimeKind.Unspecified).AddTicks(9685), "Voluptates quia.", 26, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 9, 22, 51, 79, DateTimeKind.Unspecified).AddTicks(2368), "In consequatur saepe.", new DateTime(2022, 11, 15, 12, 0, 3, 321, DateTimeKind.Unspecified).AddTicks(3698), "Accusantium accusantium.", 9, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 22, 18, 13, 21, 542, DateTimeKind.Unspecified).AddTicks(2199), "Maxime ipsum natus et quod aut molestiae.", new DateTime(2021, 7, 12, 21, 7, 38, 333, DateTimeKind.Unspecified).AddTicks(6373), "Non aperiam.", 36, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 17, 20, 55, 51, 368, DateTimeKind.Unspecified).AddTicks(5691), "Repudiandae velit veniam odit voluptatum sed non rerum nemo accusantium.", new DateTime(2021, 6, 12, 0, 49, 43, 779, DateTimeKind.Unspecified).AddTicks(7562), "Sed aut.", 44, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 3, 15, 33, 49, 435, DateTimeKind.Unspecified).AddTicks(5049), "Maxime sint doloremque voluptate explicabo veritatis sunt exercitationem.", new DateTime(2021, 5, 16, 20, 46, 40, 528, DateTimeKind.Unspecified).AddTicks(7226), "Et unde.", 19, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 8, 12, 48, 17, 789, DateTimeKind.Unspecified).AddTicks(6312), "Aliquam sit laudantium dolor.", new DateTime(2022, 4, 17, 5, 44, 14, 92, DateTimeKind.Unspecified).AddTicks(7680), "Totam exercitationem.", 22, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 23, 14, 58, 44, 536, DateTimeKind.Unspecified).AddTicks(9528), "Rerum quas eaque voluptatem eum saepe.", new DateTime(2021, 10, 22, 22, 51, 45, 378, DateTimeKind.Unspecified).AddTicks(6807), "Architecto qui.", 1, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 14, 34, 44, 156, DateTimeKind.Unspecified).AddTicks(4710), "Ducimus deserunt enim sapiente repudiandae.", new DateTime(2021, 11, 7, 0, 43, 20, 288, DateTimeKind.Unspecified).AddTicks(7329), "Provident dolor.", 45, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 15, 20, 48, 27, 784, DateTimeKind.Unspecified).AddTicks(3606), "Molestias et accusamus quas provident sequi consectetur.", new DateTime(2021, 10, 17, 0, 37, 11, 217, DateTimeKind.Unspecified).AddTicks(3337), "Sed est.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 10, 11, 56, 12, 574, DateTimeKind.Unspecified).AddTicks(3783), "Repudiandae assumenda qui quod sint aut cumque.", new DateTime(2022, 2, 19, 5, 37, 29, 559, DateTimeKind.Unspecified).AddTicks(7845), "Doloribus earum.", 17, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 1, 34, 18, 921, DateTimeKind.Unspecified).AddTicks(7332), "Quia et dolor magnam nesciunt dolorem quia doloremque qui.", new DateTime(2021, 10, 6, 16, 25, 32, 496, DateTimeKind.Unspecified).AddTicks(9250), "Fugit tenetur.", 21, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 5, 20, 29, 34, 964, DateTimeKind.Unspecified).AddTicks(6274), "Ut sit molestiae voluptates.", new DateTime(2022, 8, 10, 14, 50, 18, 131, DateTimeKind.Unspecified).AddTicks(4108), "Nesciunt explicabo.", 43, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 25, 5, 20, 31, 10, DateTimeKind.Unspecified).AddTicks(6422), "Quis perspiciatis voluptatem quae voluptatem laudantium sed.", new DateTime(2021, 12, 11, 17, 36, 2, 442, DateTimeKind.Unspecified).AddTicks(3604), "Dolorem aut.", 50, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 5, 15, 1, 649, DateTimeKind.Unspecified).AddTicks(1330), "Sit tenetur cum quia ab voluptas consequatur doloremque quisquam.", new DateTime(2021, 6, 5, 21, 9, 0, 79, DateTimeKind.Unspecified).AddTicks(7571), "Sed excepturi.", 23, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 12, 17, 23, 32, 190, DateTimeKind.Unspecified).AddTicks(9662), "Et expedita accusantium illo reprehenderit qui delectus quod ex vel.", new DateTime(2022, 11, 26, 2, 9, 52, 169, DateTimeKind.Unspecified).AddTicks(3706), "Id neque.", 45, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 19, 15, 5, 258, DateTimeKind.Unspecified).AddTicks(1412), "Reiciendis sit vel impedit quo error aut pariatur earum.", new DateTime(2021, 12, 19, 2, 10, 41, 317, DateTimeKind.Unspecified).AddTicks(7756), "Quia voluptas.", 35, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 21, 3, 43, 56, 771, DateTimeKind.Unspecified).AddTicks(4826), "Repellendus eveniet natus odio et ea repudiandae eum possimus.", new DateTime(2021, 4, 20, 2, 28, 44, 375, DateTimeKind.Unspecified).AddTicks(1443), "Adipisci illo.", 38, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 11, 10, 28, 39, 370, DateTimeKind.Unspecified).AddTicks(2441), "Facere ab veritatis et consequatur ab.", new DateTime(2021, 6, 30, 1, 42, 23, 356, DateTimeKind.Unspecified).AddTicks(4530), "Nostrum est.", 16, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 18, 0, 15, 33, 42, DateTimeKind.Unspecified).AddTicks(4186), "Minus fuga vel saepe.", new DateTime(2022, 7, 29, 23, 14, 31, 873, DateTimeKind.Unspecified).AddTicks(5090), "Magni mollitia.", 31, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 2, 13, 11, 57, 524, DateTimeKind.Unspecified).AddTicks(4928), "Distinctio quam ut nesciunt nisi quia enim quia.", new DateTime(2022, 2, 2, 1, 48, 6, 243, DateTimeKind.Unspecified).AddTicks(6287), "Omnis ipsum.", 8, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 3, 17, 46, 57, 363, DateTimeKind.Unspecified).AddTicks(7577), "Accusantium rem modi nesciunt quia quia quidem ut ut.", new DateTime(2022, 5, 29, 11, 54, 23, 511, DateTimeKind.Unspecified).AddTicks(2494), "Esse quibusdam.", 18, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 9, 46, 10, 809, DateTimeKind.Unspecified).AddTicks(8668), "Non amet deserunt minus culpa tenetur consectetur.", new DateTime(2021, 10, 12, 5, 16, 13, 567, DateTimeKind.Unspecified).AddTicks(5977), "Repudiandae consectetur.", 10, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 9, 2, 15, 147, DateTimeKind.Unspecified).AddTicks(4879), "Illo cumque sunt deserunt incidunt adipisci in.", new DateTime(2022, 5, 29, 8, 3, 28, 291, DateTimeKind.Unspecified).AddTicks(2722), "Quo aut.", 27, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 29, 20, 50, 40, 618, DateTimeKind.Unspecified).AddTicks(3514), "Consequuntur ut voluptatem laborum.", new DateTime(2021, 11, 28, 9, 13, 11, 991, DateTimeKind.Unspecified).AddTicks(4418), "Quo sint.", 24, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 17, 0, 52, 36, 187, DateTimeKind.Unspecified).AddTicks(191), "Recusandae repellendus ut vel itaque.", new DateTime(2021, 2, 16, 15, 34, 6, 807, DateTimeKind.Unspecified).AddTicks(4242), "Sapiente dolorem.", 35, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 14, 4, 37, 29, 433, DateTimeKind.Unspecified).AddTicks(3574), "Voluptas autem cum et sunt impedit.", new DateTime(2021, 3, 19, 21, 42, 26, 798, DateTimeKind.Unspecified).AddTicks(762), "Dolores sunt.", 22, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 19, 8, 45, 19, 538, DateTimeKind.Unspecified).AddTicks(2927), "Rerum vero impedit voluptatibus aperiam architecto.", new DateTime(2022, 7, 6, 22, 47, 54, 737, DateTimeKind.Unspecified).AddTicks(8694), "Enim voluptatem.", 38, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 28, 4, 29, 36, 697, DateTimeKind.Unspecified).AddTicks(8501), "Voluptatibus officiis cupiditate nisi earum voluptate corporis.", new DateTime(2022, 8, 1, 7, 52, 17, 171, DateTimeKind.Unspecified).AddTicks(4494), "Harum quibusdam.", 2, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 23, 31, 2, 699, DateTimeKind.Unspecified).AddTicks(98), "Culpa hic veniam fugit ipsam a.", new DateTime(2021, 1, 30, 8, 2, 55, 103, DateTimeKind.Unspecified).AddTicks(2895), "Non atque.", 34, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 23, 49, 34, 125, DateTimeKind.Unspecified).AddTicks(4036), "Sapiente qui corrupti vitae labore delectus id eius dolorum ea.", new DateTime(2021, 4, 25, 6, 9, 40, 503, DateTimeKind.Unspecified).AddTicks(8704), "Aut saepe.", 32, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 16, 5, 56, 48, 641, DateTimeKind.Unspecified).AddTicks(5702), "Sequi ipsa adipisci.", new DateTime(2021, 3, 6, 6, 35, 51, 775, DateTimeKind.Unspecified).AddTicks(2434), "Similique dicta.", 45, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 16, 15, 20, 13, 402, DateTimeKind.Unspecified).AddTicks(4387), "Libero aliquid minus sequi et perspiciatis omnis fugit voluptas.", new DateTime(2021, 12, 24, 9, 10, 39, 700, DateTimeKind.Unspecified).AddTicks(7574), "Vero at.", 25, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 8, 2, 0, 16, 21, 550, DateTimeKind.Unspecified).AddTicks(7933), "Et eligendi ea veniam eveniet et similique.", new DateTime(2022, 8, 19, 22, 45, 37, 967, DateTimeKind.Unspecified).AddTicks(5290), "Consequatur veritatis.", 29 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 5, 22, 22, 3, 345, DateTimeKind.Unspecified).AddTicks(6271), "Occaecati qui natus ut eos.", new DateTime(2022, 10, 9, 23, 40, 4, 541, DateTimeKind.Unspecified).AddTicks(5206), "Qui enim.", 48, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 1, 15, 19, 4, 141, DateTimeKind.Unspecified).AddTicks(7629), "Amet et corporis.", new DateTime(2021, 3, 26, 3, 45, 9, 450, DateTimeKind.Unspecified).AddTicks(4211), "Ut hic.", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 23, 14, 37, 58, 735, DateTimeKind.Unspecified).AddTicks(691), "Odit ullam nobis est quae facere.", new DateTime(2022, 12, 28, 4, 44, 30, 480, DateTimeKind.Unspecified).AddTicks(6550), "Quas mollitia.", 26, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 14, 55, 32, 693, DateTimeKind.Unspecified).AddTicks(5925), "Hic quaerat fugit culpa labore rerum saepe provident at.", new DateTime(2022, 5, 31, 6, 26, 56, 356, DateTimeKind.Unspecified).AddTicks(2591), "Natus consectetur.", 31, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 3, 7, 3, 52, 806, DateTimeKind.Unspecified).AddTicks(3981), "Odio natus minima asperiores voluptatem et quae rerum saepe.", new DateTime(2021, 5, 3, 13, 46, 52, 248, DateTimeKind.Unspecified).AddTicks(3031), "Voluptas eos.", 1, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 24, 8, 52, 28, 279, DateTimeKind.Unspecified).AddTicks(6330), "Minima numquam consequatur sed qui tempora earum dolorem ea blanditiis.", new DateTime(2022, 9, 3, 13, 26, 24, 139, DateTimeKind.Unspecified).AddTicks(8955), "Omnis non.", 22, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 21, 22, 17, 11, 750, DateTimeKind.Unspecified).AddTicks(812), "Odit voluptatem dignissimos est officia ea id quia natus et.", new DateTime(2022, 2, 19, 15, 42, 32, 247, DateTimeKind.Unspecified).AddTicks(6520), "Ipsam quia.", 13, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 20, 4, 34, 47, 414, DateTimeKind.Unspecified).AddTicks(7784), "Dignissimos aspernatur possimus eius rem excepturi commodi animi debitis.", new DateTime(2022, 5, 17, 15, 6, 15, 763, DateTimeKind.Unspecified).AddTicks(4463), "Voluptatem qui.", 27, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 19, 12, 23, 20, 319, DateTimeKind.Unspecified).AddTicks(1486), "Nobis at sit earum nobis vero necessitatibus ea.", new DateTime(2022, 8, 31, 17, 16, 37, 177, DateTimeKind.Unspecified).AddTicks(3104), "Ab voluptate.", 43, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 16, 19, 35, 14, 682, DateTimeKind.Unspecified).AddTicks(2177), "Aspernatur repudiandae natus voluptatem animi dolorem perferendis id.", new DateTime(2022, 4, 24, 21, 49, 24, 800, DateTimeKind.Unspecified).AddTicks(3607), "Omnis quaerat.", 31, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 14, 15, 0, 1, 827, DateTimeKind.Unspecified).AddTicks(8236), "Quos illo maxime ipsa cupiditate amet aut.", new DateTime(2022, 8, 11, 4, 5, 28, 432, DateTimeKind.Unspecified).AddTicks(4724), "Possimus distinctio.", 25, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 18, 12, 25, 25, 549, DateTimeKind.Unspecified).AddTicks(2394), "Ipsa error quod doloremque sint.", new DateTime(2021, 5, 19, 15, 52, 59, 611, DateTimeKind.Unspecified).AddTicks(5970), "Ea occaecati.", 13, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 11, 11, 57, 4, 359, DateTimeKind.Unspecified).AddTicks(3977), "Corporis laboriosam sint repudiandae soluta commodi nihil impedit modi.", new DateTime(2021, 8, 19, 2, 5, 35, 602, DateTimeKind.Unspecified).AddTicks(1588), "Maxime deserunt.", 40, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 20, 5, 59, 233, DateTimeKind.Unspecified).AddTicks(9923), "Ut autem voluptate eum soluta.", new DateTime(2021, 9, 22, 23, 55, 48, 705, DateTimeKind.Unspecified).AddTicks(5748), "Provident error.", 38, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 15, 57, 25, 854, DateTimeKind.Unspecified).AddTicks(5334), "Sed incidunt eaque.", new DateTime(2021, 9, 26, 0, 30, 15, 734, DateTimeKind.Unspecified).AddTicks(7394), "Qui et.", 37, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 3, 19, 35, 25, 597, DateTimeKind.Unspecified).AddTicks(3273), "Quis corrupti aperiam et qui aut vitae.", new DateTime(2021, 6, 3, 5, 14, 26, 764, DateTimeKind.Unspecified).AddTicks(11), "Quisquam quia.", 10, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 20, 9, 44, 255, DateTimeKind.Unspecified).AddTicks(96), "Autem harum nostrum qui.", new DateTime(2022, 12, 24, 17, 28, 8, 820, DateTimeKind.Unspecified).AddTicks(231), "Est beatae.", 21, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 5, 10, 36, 15, 295, DateTimeKind.Unspecified).AddTicks(566), "Quae eos perspiciatis et quasi consequatur aut reprehenderit omnis.", new DateTime(2021, 3, 13, 6, 33, 45, 458, DateTimeKind.Unspecified).AddTicks(1083), "Soluta animi.", 30, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 19, 52, 38, 956, DateTimeKind.Unspecified).AddTicks(238), "Cupiditate amet cum consectetur.", new DateTime(2022, 10, 8, 0, 8, 51, 629, DateTimeKind.Unspecified).AddTicks(3788), "Aspernatur totam.", 6, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 2, 24, 59, 133, DateTimeKind.Unspecified).AddTicks(1037), "Dolor eum iure sed porro rerum aliquam quo.", new DateTime(2022, 11, 17, 19, 56, 22, 285, DateTimeKind.Unspecified).AddTicks(9396), "Ut suscipit.", 36, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 18, 15, 25, 53, 133, DateTimeKind.Unspecified).AddTicks(491), "Tempore numquam sint.", new DateTime(2021, 4, 28, 7, 38, 34, 806, DateTimeKind.Unspecified).AddTicks(5181), "Maxime totam.", 14, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 16, 58, 44, 973, DateTimeKind.Unspecified).AddTicks(7242), "Voluptates facilis deleniti esse voluptas.", new DateTime(2022, 8, 19, 22, 26, 8, 612, DateTimeKind.Unspecified).AddTicks(556), "Aut id.", 33, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 21, 41, 9, 586, DateTimeKind.Unspecified).AddTicks(148), "Animi aliquid non.", new DateTime(2022, 1, 28, 17, 47, 56, 331, DateTimeKind.Unspecified).AddTicks(561), "Consequuntur dignissimos.", 8, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 5, 16, 18, 13, 122, DateTimeKind.Unspecified).AddTicks(5928), "Error veritatis at temporibus incidunt rerum aut sit officiis.", new DateTime(2021, 11, 30, 20, 13, 36, 408, DateTimeKind.Unspecified).AddTicks(8169), "Eaque eius.", 38, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 1, 14, 1, 20, 847, DateTimeKind.Unspecified).AddTicks(7329), "Dolor molestiae iste temporibus repudiandae est sit natus.", new DateTime(2021, 9, 16, 20, 1, 46, 738, DateTimeKind.Unspecified).AddTicks(4892), "Hic et.", 34, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 12, 41, 58, 430, DateTimeKind.Unspecified).AddTicks(6743), "Voluptate eum in consequatur nostrum molestiae.", new DateTime(2022, 6, 19, 4, 24, 49, 994, DateTimeKind.Unspecified).AddTicks(4785), "Distinctio aspernatur.", 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 34, 56, 810, DateTimeKind.Unspecified).AddTicks(1742), "Qui optio illum perspiciatis nemo veniam at tempore soluta.", new DateTime(2021, 1, 6, 9, 56, 52, 194, DateTimeKind.Unspecified).AddTicks(5302), "Cum tenetur.", 41 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 29, 5, 49, 55, 61, DateTimeKind.Unspecified).AddTicks(8079), "Voluptatem qui deserunt soluta aut aspernatur.", new DateTime(2022, 6, 21, 12, 22, 32, 306, DateTimeKind.Unspecified).AddTicks(1828), "Dolores et.", 24, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 23, 7, 57, 288, DateTimeKind.Unspecified).AddTicks(9237), "Occaecati eius qui.", new DateTime(2022, 10, 6, 21, 36, 27, 938, DateTimeKind.Unspecified).AddTicks(5853), "Quibusdam dolorem.", 39, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 13, 5, 21, 415, DateTimeKind.Unspecified).AddTicks(9844), "Accusamus quia cupiditate officiis optio consectetur fugiat.", new DateTime(2022, 10, 26, 23, 42, 17, 384, DateTimeKind.Unspecified).AddTicks(2536), "Autem reprehenderit.", 41, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 14, 18, 8, 21, 191, DateTimeKind.Unspecified).AddTicks(8701), "Fugit quis soluta nihil quo voluptatem.", new DateTime(2022, 6, 29, 9, 25, 29, 527, DateTimeKind.Unspecified).AddTicks(1907), "Iusto amet.", 10, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 0, 16, 7, 992, DateTimeKind.Unspecified).AddTicks(5022), "Aut quam dolorum et.", new DateTime(2022, 9, 9, 0, 11, 11, 377, DateTimeKind.Unspecified).AddTicks(133), "Hic incidunt.", 45, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 20, 17, 9, 54, 687, DateTimeKind.Unspecified).AddTicks(6782), "Temporibus rerum et consequatur sit numquam odio recusandae molestiae omnis.", new DateTime(2021, 1, 25, 11, 38, 50, 99, DateTimeKind.Unspecified).AddTicks(816), "Eos asperiores.", 31, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 13, 9, 13, 59, 224, DateTimeKind.Unspecified).AddTicks(7272), "Aspernatur in deleniti iusto culpa repellendus exercitationem.", new DateTime(2021, 12, 22, 20, 43, 27, 590, DateTimeKind.Unspecified).AddTicks(9838), "Quaerat error.", 31, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 29, 2, 4, 30, 326, DateTimeKind.Unspecified).AddTicks(9369), "Optio nostrum saepe enim ducimus.", new DateTime(2021, 11, 12, 22, 44, 38, 166, DateTimeKind.Unspecified).AddTicks(9043), "Quasi autem.", 2, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 20, 19, 20, 32, 398, DateTimeKind.Unspecified).AddTicks(2885), "Expedita ipsa aut autem at.", new DateTime(2022, 7, 25, 4, 7, 7, 713, DateTimeKind.Unspecified).AddTicks(2768), "Ab accusantium.", 28, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 28, 3, 26, 12, 311, DateTimeKind.Unspecified).AddTicks(6612), "Eius qui laboriosam.", new DateTime(2021, 4, 8, 16, 59, 24, 231, DateTimeKind.Unspecified).AddTicks(5754), "Eius nulla.", 43, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 19, 37, 35, 460, DateTimeKind.Unspecified).AddTicks(8202), "Non itaque nulla id vel neque ut et.", new DateTime(2021, 10, 20, 11, 24, 20, 876, DateTimeKind.Unspecified).AddTicks(7223), "Voluptatem laboriosam.", 37, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 20, 11, 10, 19, 900, DateTimeKind.Unspecified).AddTicks(7033), "Magni magni eligendi.", new DateTime(2021, 5, 22, 10, 2, 24, 202, DateTimeKind.Unspecified).AddTicks(6948), "Dolores voluptatem.", 22, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 22, 9, 0, 53, 454, DateTimeKind.Unspecified).AddTicks(7980), "Velit quo quia.", new DateTime(2022, 10, 22, 14, 19, 52, 783, DateTimeKind.Unspecified).AddTicks(7102), "Eos perferendis.", 27, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 27, 21, 45, 59, 258, DateTimeKind.Unspecified).AddTicks(1083), "Occaecati amet voluptas dolores commodi sed.", new DateTime(2021, 9, 28, 3, 32, 55, 172, DateTimeKind.Unspecified).AddTicks(5827), "Aliquam deleniti.", 33, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 21, 27, 18, 566, DateTimeKind.Unspecified).AddTicks(9692), "Quia quos sequi quis cumque.", new DateTime(2021, 9, 12, 16, 46, 7, 571, DateTimeKind.Unspecified).AddTicks(7763), "Earum eum.", 17, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 5, 19, 50, 13, 978, DateTimeKind.Unspecified).AddTicks(3025), "Tempora natus est ea quae et eos minima deserunt aliquam.", new DateTime(2022, 9, 27, 4, 20, 46, 786, DateTimeKind.Unspecified).AddTicks(6186), "Voluptatem tempore.", 23, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 30, 9, 3, 51, 614, DateTimeKind.Unspecified).AddTicks(9785), "Unde ipsum incidunt ducimus doloribus dolor.", new DateTime(2021, 3, 17, 7, 22, 1, 909, DateTimeKind.Unspecified).AddTicks(4444), "Vel libero.", 37, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 27, 5, 29, 17, 912, DateTimeKind.Unspecified).AddTicks(3616), "Voluptatum consequuntur fuga voluptatem reprehenderit delectus eos.", new DateTime(2021, 9, 4, 12, 14, 54, 700, DateTimeKind.Unspecified).AddTicks(2413), "Explicabo excepturi.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 13, 8, 45, 582, DateTimeKind.Unspecified).AddTicks(4451), "Error incidunt id.", new DateTime(2022, 2, 26, 2, 53, 7, 986, DateTimeKind.Unspecified).AddTicks(4035), "Repellendus asperiores.", 21, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 11, 2, 48, 43, 189, DateTimeKind.Unspecified).AddTicks(428), "Quibusdam et atque velit ad voluptatem hic soluta nisi et.", new DateTime(2022, 9, 5, 4, 49, 36, 35, DateTimeKind.Unspecified).AddTicks(5315), "Vel harum.", 34, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 25, 14, 28, 18, 462, DateTimeKind.Unspecified).AddTicks(1391), "Quaerat molestias placeat quos alias vel libero odit recusandae.", new DateTime(2021, 3, 11, 8, 25, 53, 691, DateTimeKind.Unspecified).AddTicks(777), "Saepe nesciunt.", 46, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 1, 15, 53, 55, 700, DateTimeKind.Unspecified).AddTicks(2763), "Fugiat voluptatem est alias culpa incidunt suscipit provident rerum repellendus.", new DateTime(2021, 1, 7, 22, 47, 20, 264, DateTimeKind.Unspecified).AddTicks(5483), "Nobis dolore.", 10, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 26, 19, 47, 48, 865, DateTimeKind.Unspecified).AddTicks(286), "Qui impedit facilis atque distinctio porro.", new DateTime(2021, 2, 24, 0, 58, 8, 321, DateTimeKind.Unspecified).AddTicks(8912), "Repudiandae vel.", 23, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 13, 4, 36, 49, 782, DateTimeKind.Unspecified).AddTicks(6324), "Necessitatibus non totam suscipit quo accusamus suscipit voluptatem explicabo.", new DateTime(2022, 7, 5, 13, 52, 25, 401, DateTimeKind.Unspecified).AddTicks(2771), "Dolore incidunt.", 12, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 1, 11, 28, 50, 706, DateTimeKind.Unspecified).AddTicks(2685), "Quidem hic sed dolor.", new DateTime(2022, 3, 18, 9, 8, 44, 262, DateTimeKind.Unspecified).AddTicks(1115), "Officiis ea.", 48, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 3, 11, 54, 12, 541, DateTimeKind.Unspecified).AddTicks(3580), "Et et perspiciatis et rerum eos suscipit.", new DateTime(2022, 6, 4, 22, 11, 13, 889, DateTimeKind.Unspecified).AddTicks(162), "Perspiciatis dolorum.", 45, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 8, 9, 13, 34, 24, 284, DateTimeKind.Unspecified).AddTicks(4609), "Cumque at autem veniam ex animi fugiat.", new DateTime(2022, 5, 9, 23, 21, 25, 401, DateTimeKind.Unspecified).AddTicks(6790), "Et maxime.", 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 5, 15, 55, 286, DateTimeKind.Unspecified).AddTicks(1008), "Harum sed est quia velit dolorum.", new DateTime(2022, 4, 23, 4, 54, 40, 825, DateTimeKind.Unspecified).AddTicks(2948), "Porro quaerat.", 49, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 7, 4, 19, 42, 944, DateTimeKind.Unspecified).AddTicks(4551), "Repellat et ipsam et.", new DateTime(2022, 10, 4, 15, 29, 22, 602, DateTimeKind.Unspecified).AddTicks(2600), "Ut magnam.", 7, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 7, 22, 46, 35, 971, DateTimeKind.Unspecified).AddTicks(2269), "Consequatur voluptate adipisci.", new DateTime(2021, 3, 23, 9, 44, 28, 917, DateTimeKind.Unspecified).AddTicks(3760), "Unde qui.", 38, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 6, 6, 43, 51, 9, DateTimeKind.Unspecified).AddTicks(2797), "Eos molestias mollitia consequatur cum sit.", new DateTime(2021, 3, 28, 15, 43, 36, 842, DateTimeKind.Unspecified).AddTicks(4831), "Ipsum et.", 46, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 10, 7, 27, 54, 279, DateTimeKind.Unspecified).AddTicks(3422), "Debitis suscipit praesentium.", new DateTime(2021, 8, 30, 5, 13, 24, 715, DateTimeKind.Unspecified).AddTicks(3817), "Id dolores.", 26, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 4, 0, 29, 44, 983, DateTimeKind.Unspecified).AddTicks(1276), "Magnam fuga cum ipsum autem amet minima ut quasi vero.", new DateTime(2022, 8, 28, 6, 4, 40, 354, DateTimeKind.Unspecified).AddTicks(3879), "Commodi enim.", 18, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 6, 20, 0, 44, 21, 73, DateTimeKind.Unspecified).AddTicks(5621), "Vel corrupti est ut omnis.", new DateTime(2021, 10, 29, 22, 30, 51, 131, DateTimeKind.Unspecified).AddTicks(4563), "A quia.", 37 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 30, 13, 58, 58, 182, DateTimeKind.Unspecified).AddTicks(1541), "Necessitatibus et sint cumque a numquam.", new DateTime(2022, 10, 31, 7, 52, 35, 925, DateTimeKind.Unspecified).AddTicks(1853), "Atque non.", 14, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 15, 10, 59, 0, 79, DateTimeKind.Unspecified).AddTicks(3333), "Quae et qui perspiciatis placeat sit repellat.", new DateTime(2021, 9, 20, 13, 31, 40, 308, DateTimeKind.Unspecified).AddTicks(4780), "Doloremque doloribus.", 27, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 15, 2, 41, 15, 815, DateTimeKind.Unspecified).AddTicks(2973), "Id repudiandae ea consequuntur neque magnam porro.", new DateTime(2022, 10, 6, 10, 51, 26, 357, DateTimeKind.Unspecified).AddTicks(4161), "Vitae sit.", 8, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 24, 13, 11, 31, 993, DateTimeKind.Unspecified).AddTicks(8665), "Fugit sapiente amet enim qui.", new DateTime(2022, 9, 12, 14, 33, 20, 339, DateTimeKind.Unspecified).AddTicks(5142), "Doloremque aut.", 47, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 25, 14, 44, 58, 745, DateTimeKind.Unspecified).AddTicks(8577), "Totam voluptatum ullam ut ipsam nesciunt quisquam suscipit.", new DateTime(2022, 12, 26, 19, 6, 57, 746, DateTimeKind.Unspecified).AddTicks(8890), "Et sapiente.", 7, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 27, 13, 3, 2, 121, DateTimeKind.Unspecified).AddTicks(4805), "Aperiam voluptate illum voluptatem sed.", new DateTime(2021, 9, 23, 14, 42, 56, 315, DateTimeKind.Unspecified).AddTicks(1661), "Expedita recusandae.", 28, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 22, 10, 11, 18, 356, DateTimeKind.Unspecified).AddTicks(635), "Omnis dolore ut et magni et quis optio voluptas.", new DateTime(2021, 5, 25, 19, 43, 44, 500, DateTimeKind.Unspecified).AddTicks(9268), "Dignissimos enim.", 32, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 22, 1, 14, 52, 406, DateTimeKind.Unspecified).AddTicks(2989), "Voluptatem voluptas voluptatem id molestiae quo nisi rerum.", new DateTime(2021, 5, 16, 9, 58, 33, 257, DateTimeKind.Unspecified).AddTicks(3051), "Velit et.", 29, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 27, 1, 26, 20, 745, DateTimeKind.Unspecified).AddTicks(3845), "Iusto vel temporibus voluptates.", new DateTime(2022, 2, 20, 4, 0, 48, 39, DateTimeKind.Unspecified).AddTicks(1322), "Minima recusandae.", 3, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 14, 57, 51, 635, DateTimeKind.Unspecified).AddTicks(353), "Sed facilis nam consectetur porro aut eligendi explicabo.", new DateTime(2022, 11, 21, 19, 40, 37, 746, DateTimeKind.Unspecified).AddTicks(4009), "Ut facere.", 24, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 18, 22, 12, 35, 272, DateTimeKind.Unspecified).AddTicks(5022), "Et ducimus earum eveniet quis eaque.", new DateTime(2022, 8, 6, 15, 38, 14, 757, DateTimeKind.Unspecified).AddTicks(6706), "Explicabo ut.", 44, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 26, 12, 27, 9, 296, DateTimeKind.Unspecified).AddTicks(9844), "Quia autem ut quae corporis voluptates.", new DateTime(2022, 4, 1, 3, 0, 38, 220, DateTimeKind.Unspecified).AddTicks(9694), "Accusamus omnis.", 47, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 0, 53, 23, 381, DateTimeKind.Unspecified).AddTicks(9013), "Sunt reiciendis neque facere et eveniet sed voluptates.", new DateTime(2021, 5, 14, 8, 43, 3, 657, DateTimeKind.Unspecified).AddTicks(5218), "Nam recusandae.", 5, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 10, 15, 7, 38, 301, DateTimeKind.Unspecified).AddTicks(4359), "Assumenda consequuntur id itaque et ea sint voluptatem qui.", new DateTime(2022, 7, 15, 5, 38, 27, 506, DateTimeKind.Unspecified).AddTicks(9136), "Quae velit.", 17, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 1, 12, 5, 53, 664, DateTimeKind.Unspecified).AddTicks(6148), "Dolorum distinctio est minima.", new DateTime(2021, 10, 4, 23, 37, 52, 412, DateTimeKind.Unspecified).AddTicks(3379), "Molestias odio.", 2, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 7, 16, 50, 43, 421, DateTimeKind.Unspecified).AddTicks(8543), "Vitae consectetur magni eveniet provident totam eum aliquid est est.", new DateTime(2022, 9, 28, 16, 29, 57, 157, DateTimeKind.Unspecified).AddTicks(6324), "Omnis explicabo.", 27, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 2, 13, 31, 53, 798, DateTimeKind.Unspecified).AddTicks(1094), "Et sapiente et.", new DateTime(2021, 12, 10, 8, 1, 28, 295, DateTimeKind.Unspecified).AddTicks(8232), "Eveniet consequatur.", 37, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 21, 8, 26, 57, 65, DateTimeKind.Unspecified).AddTicks(6916), "Et aliquam quod culpa voluptatibus distinctio doloremque.", new DateTime(2021, 6, 25, 13, 2, 25, 259, DateTimeKind.Unspecified).AddTicks(5799), "Voluptatibus est.", 4, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 7, 7, 25, 27, 516, DateTimeKind.Unspecified).AddTicks(3292), "Ut quasi maiores eum.", new DateTime(2021, 3, 28, 16, 31, 32, 577, DateTimeKind.Unspecified).AddTicks(9465), "Harum aliquid.", 25, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 25, 22, 32, 2, 972, DateTimeKind.Unspecified).AddTicks(9200), "Voluptatem et veritatis.", new DateTime(2021, 7, 27, 0, 50, 34, 309, DateTimeKind.Unspecified).AddTicks(6093), "Velit aut.", 22, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 6, 15, 17, 1, 172, DateTimeKind.Unspecified).AddTicks(5911), "Quibusdam vel modi a qui cupiditate odio nisi id qui.", new DateTime(2021, 3, 18, 20, 14, 24, 659, DateTimeKind.Unspecified).AddTicks(8156), "Expedita recusandae.", 46, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 20, 12, 56, 29, 945, DateTimeKind.Unspecified).AddTicks(2572), "Ad consequuntur eligendi sed quia enim minima.", new DateTime(2021, 10, 7, 10, 10, 31, 827, DateTimeKind.Unspecified).AddTicks(1851), "Rerum ut.", 26, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 14, 18, 32, 335, DateTimeKind.Unspecified).AddTicks(3708), "Voluptate quo ex sed.", new DateTime(2021, 5, 2, 3, 4, 8, 789, DateTimeKind.Unspecified).AddTicks(9408), "Corrupti vel.", 6, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 4, 1, 40, 11, 312, DateTimeKind.Unspecified).AddTicks(1887), "Temporibus temporibus eum maiores maxime id ad sapiente.", new DateTime(2022, 4, 12, 9, 15, 1, 564, DateTimeKind.Unspecified).AddTicks(3948), "Aut soluta.", 32, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 13, 14, 38, 38, 460, DateTimeKind.Unspecified).AddTicks(5624), "Hic ea veniam aperiam cumque accusantium non.", new DateTime(2021, 9, 10, 22, 55, 39, 562, DateTimeKind.Unspecified).AddTicks(6302), "Eligendi earum.", 37, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 22, 20, 25, 14, 311, DateTimeKind.Unspecified).AddTicks(9279), "Perferendis iusto et unde enim consequatur aut.", new DateTime(2021, 9, 28, 11, 49, 18, 22, DateTimeKind.Unspecified).AddTicks(9051), "Earum nisi.", 36, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 11, 2, 21, 43, 39, 434, DateTimeKind.Unspecified).AddTicks(8813), "Aut eos eum veniam.", new DateTime(2022, 7, 29, 10, 13, 13, 375, DateTimeKind.Unspecified).AddTicks(9977), "Est vel.", 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 9, 4, 11, 4, 20, DateTimeKind.Unspecified).AddTicks(4909), "Corporis modi dolorum dolor.", new DateTime(2022, 1, 7, 17, 14, 3, 520, DateTimeKind.Unspecified).AddTicks(3072), "Totam sed.", 36, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 3, 42, 55, 747, DateTimeKind.Unspecified).AddTicks(5927), "Debitis a occaecati quis.", new DateTime(2022, 4, 20, 9, 41, 42, 81, DateTimeKind.Unspecified).AddTicks(5544), "Quisquam quidem.", 50, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 28, 11, 21, 5, 938, DateTimeKind.Unspecified).AddTicks(8025), "Excepturi ipsam consequatur repellendus est.", new DateTime(2022, 3, 7, 16, 4, 59, 257, DateTimeKind.Unspecified).AddTicks(924), "Tenetur dicta.", 7, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 23, 23, 53, 23, 134, DateTimeKind.Unspecified).AddTicks(7742), "Sapiente magnam doloribus accusamus non.", new DateTime(2021, 1, 13, 22, 6, 49, 756, DateTimeKind.Unspecified).AddTicks(9284), "Voluptates et.", 27, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 10, 18, 35, 44, 131, DateTimeKind.Unspecified).AddTicks(4111), "Vitae mollitia et expedita porro.", new DateTime(2021, 7, 4, 4, 31, 12, 700, DateTimeKind.Unspecified).AddTicks(1785), "Nobis temporibus.", 14, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 15, 17, 26, 21, 35, DateTimeKind.Unspecified).AddTicks(8194), "Natus ullam ipsum harum quis repellendus sed itaque.", new DateTime(2022, 6, 24, 22, 36, 38, 892, DateTimeKind.Unspecified).AddTicks(4344), "Quam est.", 33, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 8, 19, 27, 56, 324, DateTimeKind.Unspecified).AddTicks(7221), "Voluptas et incidunt cumque iure blanditiis nobis a quam aut.", new DateTime(2022, 2, 23, 17, 25, 56, 353, DateTimeKind.Unspecified).AddTicks(792), "Nemo debitis.", 10, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 26, 0, 52, 14, 629, DateTimeKind.Unspecified).AddTicks(1892), "Aut repellat vel aut fuga quis quae sapiente rerum tempora.", new DateTime(2021, 3, 1, 15, 50, 15, 51, DateTimeKind.Unspecified).AddTicks(6944), "Et temporibus.", 42, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 4, 2, 6, 59, 935, DateTimeKind.Unspecified).AddTicks(2000), "Omnis ut quam in sunt eius explicabo qui maxime laudantium.", new DateTime(2022, 10, 30, 4, 45, 26, 129, DateTimeKind.Unspecified).AddTicks(2779), "Quia ducimus.", 20, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 10, 16, 48, 18, 451, DateTimeKind.Unspecified).AddTicks(1067), "A qui qui temporibus numquam quam in aliquam.", new DateTime(2021, 6, 10, 11, 23, 38, 816, DateTimeKind.Unspecified).AddTicks(1426), "Doloribus quia.", 23, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 30, 9, 2, 42, 100, DateTimeKind.Unspecified).AddTicks(7083), "Ex facere saepe omnis non.", new DateTime(2022, 6, 13, 9, 55, 18, 337, DateTimeKind.Unspecified).AddTicks(8377), "Quo enim.", 19, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 12, 6, 48, 45, 552, DateTimeKind.Unspecified).AddTicks(1807), "Suscipit rerum repellendus reiciendis quam placeat sed quos.", new DateTime(2022, 12, 3, 18, 1, 8, 606, DateTimeKind.Unspecified).AddTicks(6471), "Quidem aut.", 23, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 3, 18, 45, 55, 4, DateTimeKind.Unspecified).AddTicks(5467), "Commodi quo optio blanditiis odit itaque.", new DateTime(2021, 4, 12, 4, 15, 40, 14, DateTimeKind.Unspecified).AddTicks(1120), "Inventore delectus.", 1, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 3, 39, 0, 168, DateTimeKind.Unspecified).AddTicks(7483), "Et quam sequi fuga ut deleniti repellendus explicabo occaecati nihil.", new DateTime(2022, 7, 4, 4, 37, 52, 22, DateTimeKind.Unspecified).AddTicks(4534), "Neque qui.", 16, 0 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 9, 6, 43, 8, 809, DateTimeKind.Unspecified).AddTicks(3734), "Quod maiores dolorem a vel.", new DateTime(2021, 1, 28, 21, 17, 6, 76, DateTimeKind.Unspecified).AddTicks(9019), "Dolores maxime.", 2, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 24, 6, 3, 25, 530, DateTimeKind.Unspecified).AddTicks(2888), "Tempore sed illum asperiores ad maxime doloribus in.", new DateTime(2021, 8, 19, 13, 41, 11, 725, DateTimeKind.Unspecified).AddTicks(6090), "Et accusamus.", 42, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 14, 0, 4, 51, 900, DateTimeKind.Unspecified).AddTicks(1566), "Et voluptatem omnis similique ipsa.", new DateTime(2022, 2, 9, 16, 44, 10, 490, DateTimeKind.Unspecified).AddTicks(8458), "Quisquam unde.", 50, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 9, 44, 40, 42, DateTimeKind.Unspecified).AddTicks(9676), "Iusto totam ea hic nisi sed labore rem.", new DateTime(2022, 1, 5, 19, 15, 54, 141, DateTimeKind.Unspecified).AddTicks(2597), "Earum ipsum.", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 27, 9, 19, 20, 872, DateTimeKind.Unspecified).AddTicks(3038), "Ratione perspiciatis ducimus.", new DateTime(2022, 9, 28, 1, 27, 17, 270, DateTimeKind.Unspecified).AddTicks(1117), "Amet impedit.", 39, 5 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 23, 0, 7, 47, 262, DateTimeKind.Unspecified).AddTicks(3290), "Repudiandae officia tempore incidunt ullam fugiat odit dolor voluptas.", new DateTime(2021, 9, 26, 10, 12, 34, 718, DateTimeKind.Unspecified).AddTicks(6766), "Inventore eius.", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Task",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 26, 11, 26, 24, 865, DateTimeKind.Unspecified).AddTicks(5886), "In beatae asperiores quaerat molestiae adipisci fugit aspernatur.", new DateTime(2021, 7, 13, 5, 7, 18, 858, DateTimeKind.Unspecified).AddTicks(4788), "Alias illum.", 44, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 5, 3, 11, 32, 19, 422, DateTimeKind.Unspecified).AddTicks(2642), "Leannon LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 10, 18, 14, 48, 22, 887, DateTimeKind.Unspecified).AddTicks(4342), "Smitham Inc" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 8, 6, 12, 17, 25, 933, DateTimeKind.Unspecified).AddTicks(8438), "Koss, Wisoky and Rutherford" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2016, 11, 20, 5, 2, 41, 954, DateTimeKind.Unspecified).AddTicks(5946), "Stroman Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 10, 31, 5, 57, 4, 441, DateTimeKind.Unspecified).AddTicks(1266), "Weber - Schultz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 3, 15, 19, 11, 42, 237, DateTimeKind.Unspecified).AddTicks(5832), "Corkery Group" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 5, 20, 18, 30, 48, 871, DateTimeKind.Unspecified).AddTicks(6808), "Armstrong LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 9, 3, 3, 13, 1, 451, DateTimeKind.Unspecified).AddTicks(3583), "Hahn - Beer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 12, 29, 17, 53, 2, 596, DateTimeKind.Unspecified).AddTicks(3948), "Hills LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 2, 9, 9, 47, 41, 154, DateTimeKind.Unspecified).AddTicks(5239), "Hane Group" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 10, 18, 7, 18, 50, 667, DateTimeKind.Unspecified).AddTicks(6423), "Ervin4@yahoo.com", "Stephania", "Bahringer", new DateTime(2011, 1, 24, 23, 45, 58, 667, DateTimeKind.Unspecified).AddTicks(1457), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 2, 21, 43, 5, 132, DateTimeKind.Unspecified).AddTicks(4318), "Ralph_Olson95@gmail.com", "Sally", "Raynor", new DateTime(2011, 8, 30, 17, 34, 3, 18, DateTimeKind.Unspecified).AddTicks(3640), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 25, 5, 59, 51, 787, DateTimeKind.Unspecified).AddTicks(1412), "Graciela.Beier47@gmail.com", "Ransom", "Padberg", new DateTime(2016, 8, 26, 13, 57, 59, 386, DateTimeKind.Unspecified).AddTicks(1189), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 4, 12, 6, 16, 29, 143, DateTimeKind.Unspecified).AddTicks(620), "Anastacio_Hamill@hotmail.com", "Wellington", "Bogisich", new DateTime(2012, 3, 27, 8, 57, 24, 930, DateTimeKind.Unspecified).AddTicks(1593), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 12, 14, 6, 17, 58, 365, DateTimeKind.Unspecified).AddTicks(2773), "Florine89@yahoo.com", "Maurine", "Barrows", new DateTime(2010, 1, 6, 19, 44, 49, 89, DateTimeKind.Unspecified).AddTicks(5215), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 6, 9, 18, 27, 28, 718, DateTimeKind.Unspecified).AddTicks(1394), "Lauren85@gmail.com", "Amira", "Schaefer", new DateTime(2013, 10, 10, 3, 11, 5, 740, DateTimeKind.Unspecified).AddTicks(5662), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1990, 12, 14, 8, 40, 1, 956, DateTimeKind.Unspecified).AddTicks(2731), "Shana45@yahoo.com", "Ardella", "Hand", new DateTime(2017, 5, 12, 21, 31, 19, 388, DateTimeKind.Unspecified).AddTicks(7434) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 3, 5, 21, 6, 33, 405, DateTimeKind.Unspecified).AddTicks(72), "Haskell7@hotmail.com", "Jacques", "Hayes", new DateTime(2013, 6, 29, 8, 56, 36, 171, DateTimeKind.Unspecified).AddTicks(2537), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 6, 7, 54, 1, 138, DateTimeKind.Unspecified).AddTicks(4795), "Abbigail.Daugherty74@hotmail.com", "Ibrahim", "Strosin", new DateTime(2011, 5, 24, 20, 10, 26, 798, DateTimeKind.Unspecified).AddTicks(1095), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 11, 7, 11, 21, 6, 205, DateTimeKind.Unspecified).AddTicks(725), "Kailee_Gleason@yahoo.com", "Danial", "Schneider", new DateTime(2013, 5, 5, 6, 41, 11, 293, DateTimeKind.Unspecified).AddTicks(2128), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 8, 20, 2, 49, 11, 413, DateTimeKind.Unspecified).AddTicks(6636), "Coby.Marquardt42@gmail.com", "Hershel", "Rippin", new DateTime(2017, 3, 19, 19, 46, 21, 626, DateTimeKind.Unspecified).AddTicks(5756), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 8, 23, 6, 2, 26, 862, DateTimeKind.Unspecified).AddTicks(7212), "Luis_Reichert33@gmail.com", "Emmanuelle", "Crooks", new DateTime(2012, 8, 4, 5, 45, 35, 370, DateTimeKind.Unspecified).AddTicks(6776), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 9, 28, 21, 6, 58, 984, DateTimeKind.Unspecified).AddTicks(5040), "Jaycee.OReilly23@hotmail.com", "Efren", "Johnson", new DateTime(2016, 6, 3, 6, 36, 53, 719, DateTimeKind.Unspecified).AddTicks(5286), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 18, 9, 11, 43, 669, DateTimeKind.Unspecified).AddTicks(2251), "Fae.Dooley37@yahoo.com", "Rodolfo", "Emard", new DateTime(2014, 1, 26, 16, 32, 7, 568, DateTimeKind.Unspecified).AddTicks(932), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1998, 6, 5, 1, 39, 16, 994, DateTimeKind.Unspecified).AddTicks(5738), "Roderick.Feeney20@hotmail.com", "Armando", "Smitham", new DateTime(2010, 8, 15, 21, 28, 54, 193, DateTimeKind.Unspecified).AddTicks(8735) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 12, 8, 44, 2, 999, DateTimeKind.Unspecified).AddTicks(756), "Golda58@yahoo.com", "Thad", "Dooley", new DateTime(2013, 8, 17, 17, 52, 2, 993, DateTimeKind.Unspecified).AddTicks(445), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 29, 19, 44, 20, 75, DateTimeKind.Unspecified).AddTicks(6860), "Jaleel_Gusikowski@gmail.com", "Colt", "Schroeder", new DateTime(2016, 1, 21, 10, 13, 41, 441, DateTimeKind.Unspecified).AddTicks(1868), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 30, 11, 27, 12, 988, DateTimeKind.Unspecified).AddTicks(6197), "Foster62@gmail.com", "Ahmad", "Windler", new DateTime(2013, 11, 16, 20, 7, 27, 402, DateTimeKind.Unspecified).AddTicks(8679), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 3, 1, 20, 25, 47, 637, DateTimeKind.Unspecified).AddTicks(6488), "Eveline_Schaefer67@gmail.com", "Alverta", "Reilly", new DateTime(2014, 3, 22, 19, 23, 26, 732, DateTimeKind.Unspecified).AddTicks(7922), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 9, 9, 21, 5, 739, DateTimeKind.Unspecified).AddTicks(3674), "Okey0@hotmail.com", "Georgiana", "Waelchi", new DateTime(2017, 6, 1, 14, 25, 22, 6, DateTimeKind.Unspecified).AddTicks(5495), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 11, 6, 22, 55, 36, 852, DateTimeKind.Unspecified).AddTicks(4446), "Arno_Bayer18@hotmail.com", "Antonina", "Kirlin", new DateTime(2016, 8, 21, 12, 52, 53, 852, DateTimeKind.Unspecified).AddTicks(6795), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 5, 1, 2, 28, 7, 574, DateTimeKind.Unspecified).AddTicks(281), "Elijah93@gmail.com", "Lloyd", "Gulgowski", new DateTime(2017, 1, 14, 13, 11, 39, 187, DateTimeKind.Unspecified).AddTicks(3178), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 9, 16, 1, 25, 35, 702, DateTimeKind.Unspecified).AddTicks(2831), "Bryana_Fay@yahoo.com", "Cecelia", "Hoppe", new DateTime(2012, 7, 1, 11, 52, 14, 790, DateTimeKind.Unspecified).AddTicks(2539), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 28, 6, 21, 51, 707, DateTimeKind.Unspecified).AddTicks(8762), "Anika.Stoltenberg22@gmail.com", "Brooklyn", "Stark", new DateTime(2014, 2, 3, 22, 59, 53, 145, DateTimeKind.Unspecified).AddTicks(9020), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 1, 1, 6, 22, 5, 207, DateTimeKind.Unspecified).AddTicks(501), "Alessia_Mohr@yahoo.com", "Donnie", "Thiel", new DateTime(2016, 11, 18, 19, 47, 0, 871, DateTimeKind.Unspecified).AddTicks(5137), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 22, 19, 21, 31, 499, DateTimeKind.Unspecified).AddTicks(7454), "Christy.Zboncak94@yahoo.com", "Judd", "Stracke", new DateTime(2014, 5, 27, 3, 39, 0, 692, DateTimeKind.Unspecified).AddTicks(6514), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 6, 29, 17, 4, 58, 531, DateTimeKind.Unspecified).AddTicks(9718), "Haylee.Marks@yahoo.com", "Tianna", "Kohler", new DateTime(2017, 2, 5, 18, 31, 54, 539, DateTimeKind.Unspecified).AddTicks(9198), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 10, 1, 22, 8, 55, 607, DateTimeKind.Unspecified).AddTicks(4201), "Joshuah28@gmail.com", "Cristian", "Williamson", new DateTime(2013, 6, 28, 20, 12, 18, 952, DateTimeKind.Unspecified).AddTicks(4627), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 5, 16, 17, 2, 153, DateTimeKind.Unspecified).AddTicks(3424), "Jose78@hotmail.com", "Curtis", "Balistreri", new DateTime(2014, 5, 2, 22, 57, 33, 808, DateTimeKind.Unspecified).AddTicks(764), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 12, 17, 14, 16, 5, 595, DateTimeKind.Unspecified).AddTicks(7651), "Kathlyn.Hodkiewicz@gmail.com", "Eldred", "Abshire", new DateTime(2016, 3, 25, 21, 35, 49, 803, DateTimeKind.Unspecified).AddTicks(9217), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 23, 20, 16, 26, 990, DateTimeKind.Unspecified).AddTicks(1279), "Keely49@hotmail.com", "Anastasia", "Ratke", new DateTime(2010, 6, 4, 14, 13, 8, 52, DateTimeKind.Unspecified).AddTicks(7251), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 12, 3, 2, 43, 2, 729, DateTimeKind.Unspecified).AddTicks(903), "Jevon_Cummings88@hotmail.com", "Adalberto", "Bergstrom", new DateTime(2015, 2, 28, 12, 23, 54, 704, DateTimeKind.Unspecified).AddTicks(3541), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 24, 17, 4, 57, 291, DateTimeKind.Unspecified).AddTicks(2008), "Lisandro10@yahoo.com", "Golda", "Oberbrunner", new DateTime(2013, 2, 25, 11, 47, 39, 1, DateTimeKind.Unspecified).AddTicks(3039), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 11, 20, 6, 21, 32, 38, DateTimeKind.Unspecified).AddTicks(7807), "Maribel71@yahoo.com", "Imani", "Kutch", new DateTime(2014, 3, 4, 16, 34, 49, 645, DateTimeKind.Unspecified).AddTicks(8960), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 6, 1, 22, 37, 39, 438, DateTimeKind.Unspecified).AddTicks(7744), "Xzavier_Skiles@gmail.com", "Eliseo", "Auer", new DateTime(2012, 6, 27, 22, 41, 34, 273, DateTimeKind.Unspecified).AddTicks(2854), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 3, 5, 0, 21, 29, 215, DateTimeKind.Unspecified).AddTicks(913), "Kieran83@gmail.com", "Maxie", "McClure", new DateTime(2015, 10, 5, 11, 52, 39, 793, DateTimeKind.Unspecified).AddTicks(736), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 12, 16, 7, 20, 16, 896, DateTimeKind.Unspecified).AddTicks(5354), "Wilmer12@yahoo.com", "Noah", "Treutel", new DateTime(2010, 5, 16, 3, 26, 9, 474, DateTimeKind.Unspecified).AddTicks(5574), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 13, 8, 19, 50, 464, DateTimeKind.Unspecified).AddTicks(4664), "Paolo91@yahoo.com", "Bridget", "Lueilwitz", new DateTime(2016, 12, 25, 8, 37, 0, 596, DateTimeKind.Unspecified).AddTicks(3211), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 5, 10, 9, 37, 2, 755, DateTimeKind.Unspecified).AddTicks(7444), "Hannah.Terry@hotmail.com", "Mossie", "Bradtke", new DateTime(2016, 4, 11, 0, 0, 58, 295, DateTimeKind.Unspecified).AddTicks(7156), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1998, 6, 28, 2, 12, 47, 242, DateTimeKind.Unspecified).AddTicks(2882), "Aileen_Bruen19@hotmail.com", "Tre", "Grady", new DateTime(2014, 1, 7, 14, 2, 8, 758, DateTimeKind.Unspecified).AddTicks(527) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 4, 12, 16, 19, 41, 856, DateTimeKind.Unspecified).AddTicks(3561), "Pierre_Towne4@yahoo.com", "Dejah", "Cronin", new DateTime(2012, 11, 10, 1, 35, 14, 474, DateTimeKind.Unspecified).AddTicks(3442), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 19, 1, 27, 35, 640, DateTimeKind.Unspecified).AddTicks(4941), "Joan.Bosco71@gmail.com", "Ara", "Stanton", new DateTime(2015, 1, 28, 18, 35, 57, 398, DateTimeKind.Unspecified).AddTicks(545), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 4, 6, 15, 13, 364, DateTimeKind.Unspecified).AddTicks(1669), "Melba36@hotmail.com", "Alex", "Stanton", new DateTime(2016, 8, 13, 23, 14, 18, 862, DateTimeKind.Unspecified).AddTicks(8251), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 25, 6, 10, 5, 220, DateTimeKind.Unspecified).AddTicks(9215), "Tyra99@gmail.com", "Jewel", "Schumm", new DateTime(2016, 11, 19, 23, 13, 13, 417, DateTimeKind.Unspecified).AddTicks(5141), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 11, 12, 22, 39, 948, DateTimeKind.Unspecified).AddTicks(1571), "Christine_Miller83@gmail.com", "Heath", "Jacobi", new DateTime(2015, 8, 12, 1, 9, 34, 842, DateTimeKind.Unspecified).AddTicks(6907), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 11, 25, 21, 54, 27, 286, DateTimeKind.Unspecified).AddTicks(7010), "Esta99@gmail.com", "Kenny", "Rodriguez", new DateTime(2010, 11, 27, 21, 50, 11, 516, DateTimeKind.Unspecified).AddTicks(2695), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1998, 2, 21, 1, 57, 47, 367, DateTimeKind.Unspecified).AddTicks(5266), "Reynold_Marks@hotmail.com", "Seamus", "Walsh", new DateTime(2012, 8, 8, 4, 41, 7, 521, DateTimeKind.Unspecified).AddTicks(4092) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 30, 23, 22, 10, 661, DateTimeKind.Unspecified).AddTicks(1915), "Ansel.Ruecker38@hotmail.com", "Bailey", "Ruecker", new DateTime(2017, 11, 19, 10, 32, 39, 668, DateTimeKind.Unspecified).AddTicks(5690), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 3, 0, 46, 11, 638, DateTimeKind.Unspecified).AddTicks(5831), "Lorenz_Smith@yahoo.com", "Loren", "Dooley", new DateTime(2014, 10, 22, 5, 39, 12, 79, DateTimeKind.Unspecified).AddTicks(674), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 3, 20, 40, 5, 369, DateTimeKind.Unspecified).AddTicks(4846), "Geovany_Rice19@gmail.com", "Alysson", "Bruen", new DateTime(2016, 6, 11, 4, 5, 51, 398, DateTimeKind.Unspecified).AddTicks(7668), 7 });

            migrationBuilder.AddForeignKey(
                name: "FK_Task_Projects_ProjectId",
                table: "Task",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Task_Users_PerformerId",
                table: "Task",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Task_Projects_ProjectId",
                table: "Task");

            migrationBuilder.DropForeignKey(
                name: "FK_Task_Users_PerformerId",
                table: "Task");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Task",
                table: "Task");

            migrationBuilder.RenameTable(
                name: "Task",
                newName: "ProjectTasks");

            migrationBuilder.RenameIndex(
                name: "IX_Task_ProjectId",
                table: "ProjectTasks",
                newName: "IX_ProjectTasks_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Task_PerformerId",
                table: "ProjectTasks",
                newName: "IX_ProjectTasks_PerformerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjectTasks",
                table: "ProjectTasks",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 20, 8, 48, 25, 749, DateTimeKind.Unspecified).AddTicks(1569), "Saepe quis officia et est impedit laborum.", new DateTime(2021, 7, 10, 18, 27, 8, 131, DateTimeKind.Unspecified).AddTicks(9511), "Consequatur pariatur.", 45, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 17, 3, 41, 11, 860, DateTimeKind.Unspecified).AddTicks(7051), "Voluptate in quis.", new DateTime(2022, 4, 24, 5, 32, 33, 576, DateTimeKind.Unspecified).AddTicks(3686), "Aut autem.", 32, 5, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 23, 22, 20, 432, DateTimeKind.Unspecified).AddTicks(922), "Necessitatibus vel accusamus dolor magnam cum in occaecati.", new DateTime(2021, 11, 5, 13, 2, 21, 896, DateTimeKind.Unspecified).AddTicks(9006), "Excepturi quam.", 28, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 22, 23, 18, 36, 839, DateTimeKind.Unspecified).AddTicks(4020), "Harum libero voluptas voluptatum.", new DateTime(2022, 5, 21, 3, 37, 13, 271, DateTimeKind.Unspecified).AddTicks(5855), "Doloremque in.", 44, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 7, 9, 11, 10, 253, DateTimeKind.Unspecified).AddTicks(2607), "Quos odit autem commodi alias quia quae porro.", new DateTime(2021, 1, 2, 18, 26, 4, 416, DateTimeKind.Unspecified).AddTicks(3951), "Sed nulla.", 35, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 30, 3, 14, 7, 301, DateTimeKind.Unspecified).AddTicks(1793), "Officiis nostrum amet ut placeat.", new DateTime(2022, 6, 1, 1, 48, 23, 239, DateTimeKind.Unspecified).AddTicks(5480), "Quae mollitia.", 47, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 30, 20, 14, 34, 393, DateTimeKind.Unspecified).AddTicks(542), "Recusandae eveniet sit quibusdam ea recusandae eum porro est.", new DateTime(2021, 6, 8, 0, 2, 1, 20, DateTimeKind.Unspecified).AddTicks(7106), "Animi rerum.", 39, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 9, 2, 50, 56, 497, DateTimeKind.Unspecified).AddTicks(6000), "Magnam occaecati qui.", new DateTime(2021, 11, 12, 8, 43, 25, 64, DateTimeKind.Unspecified).AddTicks(7348), "Veritatis at.", 34, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 22, 18, 13, 9, 66, DateTimeKind.Unspecified).AddTicks(3467), "Ducimus iure iste minima.", new DateTime(2022, 6, 29, 16, 30, 52, 991, DateTimeKind.Unspecified).AddTicks(9010), "Aut quas.", 5, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 24, 21, 32, 7, 674, DateTimeKind.Unspecified).AddTicks(6405), "Non ut distinctio velit possimus ratione repellat.", new DateTime(2022, 4, 24, 5, 11, 51, 41, DateTimeKind.Unspecified).AddTicks(520), "Sunt error.", 41, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 9, 0, 53, 45, 16, DateTimeKind.Unspecified).AddTicks(4759), "Et dolor tenetur molestiae.", new DateTime(2022, 1, 29, 0, 11, 33, 41, DateTimeKind.Unspecified).AddTicks(4985), "Sunt alias.", 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 31, 18, 53, 44, 285, DateTimeKind.Unspecified).AddTicks(6129), "Eum aliquid est voluptas sit.", new DateTime(2021, 3, 26, 21, 15, 54, 503, DateTimeKind.Unspecified).AddTicks(5201), "Rerum possimus.", 2, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 22, 13, 33, 52, 412, DateTimeKind.Unspecified).AddTicks(7211), "Sed ullam explicabo sed eum dolores.", new DateTime(2022, 8, 11, 16, 26, 51, 367, DateTimeKind.Unspecified).AddTicks(4561), "Est est.", 34, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 17, 10, 31, 19, 5, DateTimeKind.Unspecified).AddTicks(1982), "Ullam neque aut quae at.", new DateTime(2022, 3, 6, 0, 49, 23, 792, DateTimeKind.Unspecified).AddTicks(7538), "Reiciendis aliquam.", 1, 5, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 2, 5, 20, 44, 359, DateTimeKind.Unspecified).AddTicks(564), "Voluptatibus dolor ab expedita est blanditiis qui.", new DateTime(2021, 1, 6, 13, 10, 7, 124, DateTimeKind.Unspecified).AddTicks(3415), "Voluptas omnis.", 42, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 3, 4, 39, 779, DateTimeKind.Unspecified).AddTicks(5347), "Laboriosam pariatur dolore hic accusamus.", new DateTime(2021, 1, 8, 20, 51, 41, 526, DateTimeKind.Unspecified).AddTicks(1776), "Amet rem.", 16, 2, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 23, 29, 46, 19, DateTimeKind.Unspecified).AddTicks(4326), "Dolores aut excepturi dolores.", new DateTime(2021, 3, 30, 6, 58, 6, 990, DateTimeKind.Unspecified).AddTicks(6510), "Ipsum consequatur.", 48, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 9, 20, 4, 54, 742, DateTimeKind.Unspecified).AddTicks(8250), "Vero voluptatibus excepturi illum perferendis.", new DateTime(2022, 4, 30, 4, 56, 35, 364, DateTimeKind.Unspecified).AddTicks(6982), "Qui voluptas.", 48, 5, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 13, 12, 48, 34, 904, DateTimeKind.Unspecified).AddTicks(7869), "Dolorem aut repellendus ut ut consequatur est aperiam iste.", new DateTime(2021, 7, 6, 11, 50, 27, 382, DateTimeKind.Unspecified).AddTicks(5904), "Odit veniam.", 18, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 7, 21, 30, 15, 686, DateTimeKind.Unspecified).AddTicks(7101), "Voluptas ipsum ullam blanditiis id repellendus ipsa quas dolores.", new DateTime(2022, 9, 3, 17, 52, 2, 804, DateTimeKind.Unspecified).AddTicks(8283), "Ratione fuga.", 42, 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 4, 3, 20, 5, 21, DateTimeKind.Unspecified).AddTicks(1340), "Voluptatum suscipit aperiam adipisci dolores tempore quo suscipit ut ut.", new DateTime(2022, 1, 26, 7, 10, 56, 776, DateTimeKind.Unspecified).AddTicks(3680), "Libero nam.", 5, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 30, 11, 50, 43, 209, DateTimeKind.Unspecified).AddTicks(8682), "Soluta modi accusamus architecto.", new DateTime(2021, 10, 16, 13, 8, 18, 186, DateTimeKind.Unspecified).AddTicks(3854), "Necessitatibus provident.", 37, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 22, 15, 56, 6, 182, DateTimeKind.Unspecified).AddTicks(8910), "Facere aut aut nisi occaecati ut ipsam occaecati dolore neque.", new DateTime(2022, 8, 20, 23, 28, 24, 199, DateTimeKind.Unspecified).AddTicks(8298), "Et ducimus.", 9, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 8, 45, 50, 563, DateTimeKind.Unspecified).AddTicks(1148), "Sit voluptatibus et voluptates ratione.", new DateTime(2021, 3, 9, 7, 44, 49, 733, DateTimeKind.Unspecified).AddTicks(416), "Sapiente expedita.", 31, 2, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 31, 0, 49, 6, 986, DateTimeKind.Unspecified).AddTicks(2181), "Accusamus necessitatibus consectetur dolores optio ea.", new DateTime(2021, 2, 11, 9, 48, 1, 603, DateTimeKind.Unspecified).AddTicks(5132), "Est enim.", 12, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 17, 0, 15, 43, 981, DateTimeKind.Unspecified).AddTicks(7285), "Impedit reprehenderit possimus.", new DateTime(2022, 3, 4, 14, 10, 49, 474, DateTimeKind.Unspecified).AddTicks(2303), "Ipsum eum.", 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 24, 14, 27, 38, 599, DateTimeKind.Unspecified).AddTicks(9856), "Tenetur occaecati et nemo officia odit quas.", new DateTime(2021, 8, 3, 12, 2, 16, 385, DateTimeKind.Unspecified).AddTicks(9413), "Aperiam commodi.", 35, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 17, 51, 30, 480, DateTimeKind.Unspecified).AddTicks(8561), "Ut non cupiditate sed temporibus aspernatur.", new DateTime(2022, 12, 17, 1, 36, 29, 807, DateTimeKind.Unspecified).AddTicks(6461), "Facere autem.", 24, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 1, 8, 52, 56, 722, DateTimeKind.Unspecified).AddTicks(3673), "Incidunt neque qui est distinctio omnis.", new DateTime(2021, 9, 8, 21, 28, 40, 900, DateTimeKind.Unspecified).AddTicks(8619), "Recusandae ratione.", 42, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 20, 18, 19, 29, 59, DateTimeKind.Unspecified).AddTicks(353), "Aut perspiciatis optio sint commodi doloribus.", new DateTime(2021, 7, 8, 7, 13, 22, 630, DateTimeKind.Unspecified).AddTicks(998), "Eos nobis.", 18, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 17, 18, 30, 57, 258, DateTimeKind.Unspecified).AddTicks(4363), "Non ad repellendus accusantium ab doloribus animi et.", new DateTime(2021, 5, 17, 3, 54, 41, 477, DateTimeKind.Unspecified).AddTicks(7415), "Nesciunt impedit.", 13, 5, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 10, 10, 48, 10, 680, DateTimeKind.Unspecified).AddTicks(4289), "Deserunt ducimus officiis quas architecto iste voluptas non.", new DateTime(2021, 3, 10, 18, 55, 15, 388, DateTimeKind.Unspecified).AddTicks(5678), "Ducimus et.", 47, 1, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 28, 11, 33, 29, 505, DateTimeKind.Unspecified).AddTicks(8046), "Non culpa corporis et ex sint nobis aut quidem.", new DateTime(2021, 6, 4, 0, 36, 5, 551, DateTimeKind.Unspecified).AddTicks(629), "Dignissimos at.", 4, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 2, 9, 23, 41, 225, DateTimeKind.Unspecified).AddTicks(5817), "Suscipit provident expedita explicabo modi.", new DateTime(2021, 10, 20, 18, 10, 48, 991, DateTimeKind.Unspecified).AddTicks(3066), "Doloribus dolorem.", 35, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 4, 10, 32, 57, 569, DateTimeKind.Unspecified).AddTicks(9239), "Vel delectus numquam dolor.", new DateTime(2021, 9, 4, 23, 13, 22, 49, DateTimeKind.Unspecified).AddTicks(755), "Ipsa odit.", 15, 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 28, 14, 25, 19, 899, DateTimeKind.Unspecified).AddTicks(2847), "Quisquam rem deleniti animi est quibusdam accusamus ut placeat natus.", new DateTime(2022, 3, 3, 13, 54, 53, 938, DateTimeKind.Unspecified).AddTicks(4806), "In eos.", 11, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 20, 11, 27, 56, 574, DateTimeKind.Unspecified).AddTicks(1248), "Et ab ut ut sit sed quas voluptatem iusto.", new DateTime(2021, 2, 27, 23, 14, 44, 14, DateTimeKind.Unspecified).AddTicks(7750), "Ex omnis.", 18, 5, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 8, 18, 29, 22, 303, DateTimeKind.Unspecified).AddTicks(3803), "Dolorum qui suscipit laboriosam voluptas.", new DateTime(2021, 4, 29, 20, 55, 28, 274, DateTimeKind.Unspecified).AddTicks(5618), "Ut ut.", 40, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 12, 1, 44, 15, 435, DateTimeKind.Unspecified).AddTicks(8691), "Et iste quo saepe magni.", new DateTime(2022, 6, 25, 6, 15, 4, 323, DateTimeKind.Unspecified).AddTicks(1044), "Illo sit.", 24, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 29, 1, 49, 18, 474, DateTimeKind.Unspecified).AddTicks(310), "Eligendi dignissimos praesentium officiis reprehenderit molestiae ea laudantium facere.", new DateTime(2022, 8, 6, 14, 48, 39, 714, DateTimeKind.Unspecified).AddTicks(2479), "Veritatis fuga.", 41, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 1, 16, 8, 15, 893, DateTimeKind.Unspecified).AddTicks(9630), "Delectus autem unde qui eos sed animi earum.", new DateTime(2022, 4, 29, 8, 3, 31, 303, DateTimeKind.Unspecified).AddTicks(4852), "Nostrum et.", 36, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 4, 32, 40, 475, DateTimeKind.Unspecified).AddTicks(8896), "Accusantium fugiat molestias eum rerum quia animi voluptatum.", new DateTime(2021, 9, 20, 16, 37, 13, 683, DateTimeKind.Unspecified).AddTicks(4894), "Reiciendis labore.", 40, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 26, 4, 8, 14, 938, DateTimeKind.Unspecified).AddTicks(5606), "Quisquam voluptas est sequi saepe quia ut laudantium necessitatibus aut.", new DateTime(2022, 12, 10, 4, 8, 0, 969, DateTimeKind.Unspecified).AddTicks(7795), "Deserunt sunt.", 47, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 3, 22, 31, 51, 440, DateTimeKind.Unspecified).AddTicks(2100), "Cum dolorum quasi architecto quod quasi eum sequi eaque.", new DateTime(2021, 7, 27, 12, 4, 59, 277, DateTimeKind.Unspecified).AddTicks(9520), "Sed velit.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 41, 47, 610, DateTimeKind.Unspecified).AddTicks(3324), "Qui dolorum assumenda et voluptas ea in quae.", new DateTime(2022, 4, 22, 2, 17, 41, 429, DateTimeKind.Unspecified).AddTicks(1205), "Cupiditate iusto.", 42, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 1, 27, 21, 38, 44, 783, DateTimeKind.Unspecified).AddTicks(507), "Sit sunt quo architecto voluptatem rerum alias placeat.", new DateTime(2021, 8, 21, 1, 9, 48, 522, DateTimeKind.Unspecified).AddTicks(2101), "Dolorem sed.", 40 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 26, 8, 31, 4, 388, DateTimeKind.Unspecified).AddTicks(3101), "Quia mollitia animi non reprehenderit praesentium aut fugiat sunt quod.", new DateTime(2021, 8, 14, 10, 47, 15, 320, DateTimeKind.Unspecified).AddTicks(7450), "Commodi et.", 43, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 8, 15, 34, 11, 722, DateTimeKind.Unspecified).AddTicks(4319), "Et illum corporis dolorem qui maiores amet.", new DateTime(2021, 10, 9, 2, 32, 42, 370, DateTimeKind.Unspecified).AddTicks(3684), "Sunt temporibus.", 33, 5, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 23, 20, 32, 28, 607, DateTimeKind.Unspecified).AddTicks(5031), "Sapiente quos optio iste deserunt quia nemo ipsam veritatis.", new DateTime(2021, 4, 1, 11, 47, 47, 511, DateTimeKind.Unspecified).AddTicks(2324), "Sunt quia.", 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 15, 57, 22, 97, DateTimeKind.Unspecified).AddTicks(8511), "Aut temporibus ab sunt rerum eos consequatur.", new DateTime(2021, 10, 26, 11, 8, 51, 448, DateTimeKind.Unspecified).AddTicks(8780), "Necessitatibus nihil.", 17, 5, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 13, 10, 5, 39, 352, DateTimeKind.Unspecified).AddTicks(399), "Expedita eum quasi.", new DateTime(2022, 7, 12, 18, 2, 21, 887, DateTimeKind.Unspecified).AddTicks(8621), "Sequi labore.", 21, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 7, 3, 58, 19, 526, DateTimeKind.Unspecified).AddTicks(9114), "Architecto incidunt ex.", new DateTime(2021, 2, 4, 18, 40, 0, 36, DateTimeKind.Unspecified).AddTicks(4832), "Accusantium id.", 33, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 3, 22, 56, 53, 327, DateTimeKind.Unspecified).AddTicks(72), "Ullam et aspernatur adipisci.", new DateTime(2021, 11, 21, 6, 14, 23, 845, DateTimeKind.Unspecified).AddTicks(1904), "Vero earum.", 34, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 17, 12, 31, 11, 768, DateTimeKind.Unspecified).AddTicks(1145), "Aperiam minus illum sed qui.", new DateTime(2022, 3, 30, 22, 3, 21, 454, DateTimeKind.Unspecified).AddTicks(4736), "Sed est.", 10, 4 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 3, 6, 0, 17, 49, 585, DateTimeKind.Unspecified).AddTicks(8620), "Vitae nihil qui cupiditate est itaque est ut eaque.", new DateTime(2021, 10, 21, 8, 47, 42, 758, DateTimeKind.Unspecified).AddTicks(3652), "Aut laboriosam.", 19 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 29, 4, 20, 12, 236, DateTimeKind.Unspecified).AddTicks(9154), "Quia molestiae enim neque ut.", new DateTime(2021, 12, 18, 14, 49, 55, 99, DateTimeKind.Unspecified).AddTicks(3815), "Et veritatis.", 31, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 20, 3, 34, 14, 225, DateTimeKind.Unspecified).AddTicks(7337), "Non sit unde dolores.", new DateTime(2021, 3, 16, 13, 21, 26, 414, DateTimeKind.Unspecified).AddTicks(5000), "Et nihil.", 27, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 10, 13, 56, 27, 711, DateTimeKind.Unspecified).AddTicks(4136), "Voluptatum accusamus cupiditate est doloribus autem et nihil.", new DateTime(2021, 6, 20, 10, 13, 38, 124, DateTimeKind.Unspecified).AddTicks(3066), "Sapiente esse.", 36, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 1, 26, 1, 672, DateTimeKind.Unspecified).AddTicks(5842), "Delectus officia est aut et velit nulla et aut.", new DateTime(2021, 7, 5, 18, 22, 48, 109, DateTimeKind.Unspecified).AddTicks(8352), "Dolores voluptatem.", 15, 5, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 14, 6, 55, 5, 164, DateTimeKind.Unspecified).AddTicks(7635), "Et quam velit.", new DateTime(2022, 3, 27, 1, 1, 37, 177, DateTimeKind.Unspecified).AddTicks(7286), "Ducimus cumque.", 10, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 11, 22, 14, 56, 646, DateTimeKind.Unspecified).AddTicks(9240), "Consequatur ea sit consequatur nam nisi fugiat.", new DateTime(2022, 10, 4, 2, 44, 52, 390, DateTimeKind.Unspecified).AddTicks(8086), "Rerum aliquam.", 30, 2, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 27, 13, 19, 55, 120, DateTimeKind.Unspecified).AddTicks(8571), "Non unde totam hic provident.", new DateTime(2021, 8, 15, 23, 17, 53, 461, DateTimeKind.Unspecified).AddTicks(5851), "Dolores id.", 13, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 52, 33, 110, DateTimeKind.Unspecified).AddTicks(8303), "Tempora explicabo nostrum eum ut asperiores necessitatibus.", new DateTime(2022, 2, 10, 22, 59, 17, 679, DateTimeKind.Unspecified).AddTicks(9328), "Qui aperiam.", 8, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 23, 53, 30, 427, DateTimeKind.Unspecified).AddTicks(7562), "Aut officiis enim.", new DateTime(2021, 12, 4, 14, 23, 50, 673, DateTimeKind.Unspecified).AddTicks(8481), "Dolorum ullam.", 3, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 17, 17, 32, 5, 818, DateTimeKind.Unspecified).AddTicks(7591), "Suscipit reprehenderit sapiente rerum beatae doloribus vitae.", new DateTime(2021, 6, 29, 13, 39, 54, 515, DateTimeKind.Unspecified).AddTicks(5970), "Atque est.", 22, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 18, 23, 11, 44, 507, DateTimeKind.Unspecified).AddTicks(4542), "Id nihil iusto in libero.", new DateTime(2022, 3, 20, 22, 32, 3, 625, DateTimeKind.Unspecified).AddTicks(4833), "Architecto et.", 21, 1, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 17, 38, 21, 187, DateTimeKind.Unspecified).AddTicks(2423), "Eos quis dolore impedit distinctio adipisci.", new DateTime(2022, 1, 25, 7, 59, 43, 275, DateTimeKind.Unspecified).AddTicks(370), "Enim odio.", 47, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 28, 19, 2, 24, 775, DateTimeKind.Unspecified).AddTicks(3225), "Autem aliquam ad accusamus eos.", new DateTime(2022, 2, 3, 3, 2, 23, 513, DateTimeKind.Unspecified).AddTicks(5680), "Laborum suscipit.", 22, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 17, 16, 33, 39, 62, DateTimeKind.Unspecified).AddTicks(1759), "Adipisci consequatur facere laudantium.", new DateTime(2021, 7, 27, 3, 6, 12, 707, DateTimeKind.Unspecified).AddTicks(447), "Et vitae.", 43, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 1, 6, 55, 3, 81, DateTimeKind.Unspecified).AddTicks(2713), "Repudiandae est perspiciatis at aut.", new DateTime(2022, 1, 17, 22, 30, 32, 807, DateTimeKind.Unspecified).AddTicks(575), "Nesciunt culpa.", 13, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 1, 1, 16, 45, 34, DateTimeKind.Unspecified).AddTicks(278), "Qui omnis cum deserunt est voluptatem et.", new DateTime(2021, 12, 30, 15, 49, 43, 716, DateTimeKind.Unspecified).AddTicks(4770), "Suscipit non.", 26, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 6, 18, 27, 22, 328, DateTimeKind.Unspecified).AddTicks(7371), "Et voluptatibus in cupiditate vel adipisci similique beatae qui fuga.", new DateTime(2021, 4, 7, 0, 27, 26, 809, DateTimeKind.Unspecified).AddTicks(8892), "Odit eum.", 44, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 15, 19, 46, 53, 859, DateTimeKind.Unspecified).AddTicks(2805), "Nihil optio aperiam sed eligendi autem consequatur quae facilis repudiandae.", new DateTime(2022, 7, 18, 5, 0, 7, 309, DateTimeKind.Unspecified).AddTicks(6960), "Pariatur quia.", 4, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 8, 16, 50, 19, 258, DateTimeKind.Unspecified).AddTicks(5528), "Voluptates aspernatur molestiae non.", new DateTime(2021, 5, 1, 2, 48, 14, 119, DateTimeKind.Unspecified).AddTicks(8102), "Aut sit.", 29, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 29, 21, 43, 55, 859, DateTimeKind.Unspecified).AddTicks(2506), "Earum aliquam similique molestias veniam.", new DateTime(2021, 6, 23, 4, 46, 45, 756, DateTimeKind.Unspecified).AddTicks(30), "Dolorem blanditiis.", 7, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 11, 20, 32, 20, 789, DateTimeKind.Unspecified).AddTicks(2634), "Exercitationem praesentium dolores assumenda aut repellendus doloribus quam qui.", new DateTime(2022, 2, 3, 15, 20, 19, 281, DateTimeKind.Unspecified).AddTicks(2810), "Velit veniam.", 37, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 11, 4, 8, 17, 585, DateTimeKind.Unspecified).AddTicks(9770), "Et quae delectus.", new DateTime(2021, 10, 28, 15, 19, 49, 969, DateTimeKind.Unspecified).AddTicks(5345), "Maiores ad.", 38, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 11, 22, 30, 53, 669, DateTimeKind.Unspecified).AddTicks(5151), "Possimus distinctio quis eos.", new DateTime(2022, 11, 21, 1, 41, 42, 380, DateTimeKind.Unspecified).AddTicks(6900), "Laborum architecto.", 25, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 9, 16, 30, 1, 539, DateTimeKind.Unspecified).AddTicks(2176), "In sequi eligendi commodi recusandae.", new DateTime(2021, 8, 26, 15, 47, 57, 87, DateTimeKind.Unspecified).AddTicks(3540), "Ipsum vel.", 30, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 13, 20, 10, 971, DateTimeKind.Unspecified).AddTicks(8690), "Sapiente totam fugit rerum exercitationem maxime veniam exercitationem ut.", new DateTime(2021, 9, 23, 4, 16, 34, 582, DateTimeKind.Unspecified).AddTicks(2018), "Maiores a.", 30, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 16, 1, 58, 5, 975, DateTimeKind.Unspecified).AddTicks(2529), "Tempore quo aut dolor.", new DateTime(2021, 10, 29, 18, 22, 20, 406, DateTimeKind.Unspecified).AddTicks(216), "Iste et.", 8, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 13, 57, 56, 37, DateTimeKind.Unspecified).AddTicks(4258), "Et qui totam fuga ut facilis corporis quaerat sequi saepe.", new DateTime(2022, 5, 25, 2, 29, 55, 554, DateTimeKind.Unspecified).AddTicks(8821), "Quisquam voluptatibus.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 23, 16, 24, 41, 655, DateTimeKind.Unspecified).AddTicks(7272), "Illo numquam dolorum fugit et aut omnis.", new DateTime(2021, 2, 26, 9, 54, 31, 976, DateTimeKind.Unspecified).AddTicks(3305), "Aut ipsa.", 42, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 1, 35, 49, 726, DateTimeKind.Unspecified).AddTicks(215), "Quisquam omnis sint at reprehenderit reprehenderit sint suscipit.", new DateTime(2021, 2, 7, 1, 50, 22, 524, DateTimeKind.Unspecified).AddTicks(4756), "Voluptas deleniti.", 44, 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 25, 21, 11, 37, 393, DateTimeKind.Unspecified).AddTicks(6049), "Voluptatem adipisci illo repellat dolore.", new DateTime(2021, 11, 3, 0, 8, 45, 638, DateTimeKind.Unspecified).AddTicks(3419), "Unde inventore.", 31, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 8, 17, 21, 56, 411, DateTimeKind.Unspecified).AddTicks(6588), "Quo ea similique quia.", new DateTime(2022, 9, 29, 18, 23, 7, 66, DateTimeKind.Unspecified).AddTicks(2938), "Eius mollitia.", 19, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 13, 13, 39, 15, 240, DateTimeKind.Unspecified).AddTicks(2144), "Qui pariatur sed ducimus fuga sunt officiis.", new DateTime(2022, 5, 16, 2, 15, 53, 128, DateTimeKind.Unspecified).AddTicks(6527), "Et rerum.", 12, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 4, 14, 47, 872, DateTimeKind.Unspecified).AddTicks(2852), "Necessitatibus id atque.", new DateTime(2021, 5, 17, 15, 21, 20, 441, DateTimeKind.Unspecified).AddTicks(7649), "Quis quia.", 18, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 10, 11, 13, 0, 819, DateTimeKind.Unspecified).AddTicks(7953), "Sit delectus exercitationem est incidunt tenetur.", new DateTime(2022, 4, 24, 8, 26, 20, 357, DateTimeKind.Unspecified).AddTicks(4723), "Repellat accusantium.", 47, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 11, 14, 51, 20, 464, DateTimeKind.Unspecified).AddTicks(8482), "Quisquam et architecto sint iusto consequatur pariatur commodi et unde.", new DateTime(2022, 1, 8, 2, 48, 56, 488, DateTimeKind.Unspecified).AddTicks(159), "Fuga impedit.", 37, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 8, 30, 17, 22, 37, 587, DateTimeKind.Unspecified).AddTicks(5652), "A unde velit porro amet est eum odio nihil dolorem.", new DateTime(2021, 5, 9, 1, 5, 45, 931, DateTimeKind.Unspecified).AddTicks(4464), "Commodi consequatur.", 23 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 29, 21, 17, 59, 929, DateTimeKind.Unspecified).AddTicks(6790), "Incidunt omnis aliquid quia.", new DateTime(2021, 4, 24, 13, 3, 6, 390, DateTimeKind.Unspecified).AddTicks(1903), "Voluptas aut.", 3, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 31, 17, 35, 38, 871, DateTimeKind.Unspecified).AddTicks(245), "Sit non voluptatibus accusantium.", new DateTime(2021, 2, 21, 3, 22, 58, 328, DateTimeKind.Unspecified).AddTicks(3525), "Natus quo.", 37, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 17, 27, 4, 312, DateTimeKind.Unspecified).AddTicks(2872), "Ut quia a magnam aliquam nam neque officiis fugit.", new DateTime(2021, 3, 18, 15, 50, 34, 65, DateTimeKind.Unspecified).AddTicks(6355), "Sit quia.", 8, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 24, 7, 30, 28, 333, DateTimeKind.Unspecified).AddTicks(2987), "Dolore ducimus ex praesentium.", new DateTime(2022, 8, 9, 2, 51, 23, 239, DateTimeKind.Unspecified).AddTicks(8916), "Eos adipisci.", 8, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 4, 13, 31, 19, 410, DateTimeKind.Unspecified).AddTicks(6668), "Quis nulla nostrum sapiente ut.", new DateTime(2022, 9, 16, 17, 58, 12, 5, DateTimeKind.Unspecified).AddTicks(2240), "Libero id.", 23, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 9, 11, 8, 31, 49, 645, DateTimeKind.Unspecified).AddTicks(5377), "Non pariatur voluptatem.", new DateTime(2022, 10, 19, 5, 34, 25, 671, DateTimeKind.Unspecified).AddTicks(4763), "Quis eius.", 33, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 20, 58, 18, 166, DateTimeKind.Unspecified).AddTicks(1690), "Omnis sed eos magni fugiat.", new DateTime(2021, 2, 17, 12, 48, 50, 562, DateTimeKind.Unspecified).AddTicks(3880), "Maxime officia.", 10, 1, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 12, 11, 18, 40, 13, 363, DateTimeKind.Unspecified).AddTicks(4572), "Voluptatibus blanditiis explicabo veniam sunt ipsam consequatur exercitationem.", new DateTime(2021, 2, 14, 14, 17, 56, 220, DateTimeKind.Unspecified).AddTicks(9023), "Ipsam quaerat.", 50, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 13, 20, 45, 33, 636, DateTimeKind.Unspecified).AddTicks(744), "Veritatis nostrum voluptas voluptatibus ratione dolor soluta rem perspiciatis illum.", new DateTime(2022, 2, 18, 23, 42, 32, 256, DateTimeKind.Unspecified).AddTicks(5182), "Possimus rerum.", 12, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 18, 13, 44, 6, 514, DateTimeKind.Unspecified).AddTicks(7595), "Aut omnis porro autem.", new DateTime(2021, 3, 13, 9, 2, 46, 879, DateTimeKind.Unspecified).AddTicks(4488), "Laboriosam aliquid.", 26, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 23, 21, 26, 52, 663, DateTimeKind.Unspecified).AddTicks(3384), "Ipsam optio inventore velit dolores qui quisquam incidunt corrupti aperiam.", new DateTime(2022, 6, 6, 4, 23, 10, 712, DateTimeKind.Unspecified).AddTicks(8060), "Aut sit.", 18, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 31, 20, 53, 16, 357, DateTimeKind.Unspecified).AddTicks(6545), "A et quisquam quibusdam in vel cupiditate.", new DateTime(2021, 12, 29, 11, 27, 24, 873, DateTimeKind.Unspecified).AddTicks(9948), "Soluta quos.", 2, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 6, 23, 23, 52, 519, DateTimeKind.Unspecified).AddTicks(8725), "Dignissimos consequatur consectetur rem.", new DateTime(2022, 3, 27, 10, 13, 40, 372, DateTimeKind.Unspecified).AddTicks(9638), "Et delectus.", 47, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 1, 4, 6, 21, DateTimeKind.Unspecified).AddTicks(5386), "Adipisci itaque perferendis odit qui autem odit.", new DateTime(2022, 11, 15, 21, 20, 5, 34, DateTimeKind.Unspecified).AddTicks(7932), "Aliquid ut.", 10, 2, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 21, 19, 7, 31, 503, DateTimeKind.Unspecified).AddTicks(33), "Similique sunt dolorum.", new DateTime(2021, 9, 27, 11, 33, 50, 544, DateTimeKind.Unspecified).AddTicks(4098), "Molestias eum.", 45, 1, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 19, 4, 43, 21, 881, DateTimeKind.Unspecified).AddTicks(9221), "Velit laudantium beatae quo et temporibus minus.", new DateTime(2021, 1, 30, 21, 10, 57, 574, DateTimeKind.Unspecified).AddTicks(539), "Est omnis.", 2, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 6, 13, 57, 486, DateTimeKind.Unspecified).AddTicks(1586), "Architecto et quas iusto vel temporibus laboriosam sit sint.", new DateTime(2022, 6, 19, 13, 35, 10, 136, DateTimeKind.Unspecified).AddTicks(7300), "Voluptas aut.", 42, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 30, 6, 42, 25, 934, DateTimeKind.Unspecified).AddTicks(5223), "Voluptatum quia laudantium minima explicabo aperiam ratione eos vitae voluptas.", new DateTime(2021, 11, 18, 10, 41, 50, 154, DateTimeKind.Unspecified).AddTicks(8015), "Id quibusdam.", 33, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 19, 15, 43, 58, 267, DateTimeKind.Unspecified).AddTicks(8631), "Nostrum odit ipsam dolor voluptatum ex cum ratione.", new DateTime(2022, 6, 15, 18, 34, 16, 397, DateTimeKind.Unspecified).AddTicks(2155), "Quis fugiat.", 43, 1, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 15, 14, 33, 52, 628, DateTimeKind.Unspecified).AddTicks(7196), "Qui doloribus distinctio neque voluptas.", new DateTime(2022, 10, 30, 10, 13, 0, 533, DateTimeKind.Unspecified).AddTicks(3813), "Vel porro.", 26, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 27, 3, 40, 41, 405, DateTimeKind.Unspecified).AddTicks(5218), "A incidunt qui quas est quisquam.", new DateTime(2022, 7, 1, 11, 31, 36, 145, DateTimeKind.Unspecified).AddTicks(5819), "Qui voluptate.", 36, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 19, 17, 55, 36, 406, DateTimeKind.Unspecified).AddTicks(3008), "Maxime consequatur atque optio.", new DateTime(2021, 2, 26, 22, 7, 42, 794, DateTimeKind.Unspecified).AddTicks(2677), "Ea quia.", 1, 2, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 23, 3, 54, 21, 89, DateTimeKind.Unspecified).AddTicks(5079), "Error fugiat repellat eius voluptatem occaecati modi voluptas ut quae.", new DateTime(2021, 7, 15, 18, 0, 21, 275, DateTimeKind.Unspecified).AddTicks(2473), "Iure est.", 48, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 18, 55, 56, 367, DateTimeKind.Unspecified).AddTicks(841), "Asperiores tempora hic reiciendis perferendis ipsum rerum deserunt.", new DateTime(2022, 1, 25, 5, 9, 29, 188, DateTimeKind.Unspecified).AddTicks(5221), "Repellendus ea.", 12, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 29, 10, 10, 40, 713, DateTimeKind.Unspecified).AddTicks(4997), "Quo earum itaque amet nemo.", new DateTime(2022, 11, 6, 15, 33, 37, 969, DateTimeKind.Unspecified).AddTicks(4878), "Laboriosam odit.", 44, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 10, 6, 6, 9, 50, 477, DateTimeKind.Unspecified).AddTicks(4175), "Voluptatem veritatis vitae voluptas modi quo nulla ipsam.", new DateTime(2022, 4, 18, 5, 18, 28, 618, DateTimeKind.Unspecified).AddTicks(1589), "Repudiandae inventore.", 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 6, 21, 15, 1, 12, 988, DateTimeKind.Unspecified).AddTicks(4342), "Aut quaerat quas consectetur necessitatibus velit.", new DateTime(2021, 4, 25, 0, 41, 27, 753, DateTimeKind.Unspecified).AddTicks(7432), "Eveniet aperiam.", 27 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 7, 4, 25, 315, DateTimeKind.Unspecified).AddTicks(4744), "Quas omnis fuga repellendus id veritatis enim beatae.", new DateTime(2022, 8, 22, 4, 19, 54, 763, DateTimeKind.Unspecified).AddTicks(3790), "Quos odio.", 39, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 13, 10, 19, 15, 282, DateTimeKind.Unspecified).AddTicks(5143), "Cupiditate ea assumenda et unde amet non tempore sunt.", new DateTime(2022, 1, 13, 5, 19, 7, 645, DateTimeKind.Unspecified).AddTicks(2124), "Rerum ipsum.", 16, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 15, 23, 47, 15, 615, DateTimeKind.Unspecified).AddTicks(6021), "Blanditiis adipisci molestiae quis omnis laborum totam voluptatibus accusantium.", new DateTime(2021, 12, 19, 9, 27, 9, 751, DateTimeKind.Unspecified).AddTicks(5159), "Veniam quidem.", 9, 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 2, 20, 7, 20, 220, DateTimeKind.Unspecified).AddTicks(5322), "Occaecati sint veniam dolores quae ullam sit eum.", new DateTime(2022, 9, 3, 16, 39, 38, 719, DateTimeKind.Unspecified).AddTicks(6794), "Ad suscipit.", 34, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 23, 10, 54, 36, 848, DateTimeKind.Unspecified).AddTicks(1148), "Et dignissimos et.", new DateTime(2022, 2, 15, 16, 0, 37, 889, DateTimeKind.Unspecified).AddTicks(7714), "Sit ipsum.", 43, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 5, 17, 17, 26, 127, DateTimeKind.Unspecified).AddTicks(5123), "Quia saepe at dolorum distinctio dolorem perspiciatis perferendis sed sint.", new DateTime(2022, 7, 31, 0, 46, 6, 756, DateTimeKind.Unspecified).AddTicks(4936), "Ipsam laboriosam.", 35, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 28, 2, 33, 29, 620, DateTimeKind.Unspecified).AddTicks(1436), "Quaerat qui cupiditate et vel id.", new DateTime(2021, 8, 11, 7, 48, 36, 381, DateTimeKind.Unspecified).AddTicks(2619), "Voluptates repudiandae.", 50, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 22, 18, 54, 29, 627, DateTimeKind.Unspecified).AddTicks(3391), "Aliquid sint sit non ipsam molestias autem quae ipsa quo.", new DateTime(2022, 6, 30, 15, 52, 52, 590, DateTimeKind.Unspecified).AddTicks(6070), "Magni illo.", 27, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 3, 4, 59, 1, 505, DateTimeKind.Unspecified).AddTicks(8013), "Sint animi nam vero officiis.", new DateTime(2022, 12, 30, 12, 19, 55, 974, DateTimeKind.Unspecified).AddTicks(4712), "Iure minima.", 24, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 13, 18, 12, 27, 894, DateTimeKind.Unspecified).AddTicks(9190), "Dolorem sit est cupiditate.", new DateTime(2021, 5, 30, 21, 48, 1, 958, DateTimeKind.Unspecified).AddTicks(8179), "Ut debitis.", 30, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 6, 21, 0, 43, 700, DateTimeKind.Unspecified).AddTicks(6835), "Et eum quia sit magni nobis laborum quas quis et.", new DateTime(2022, 12, 1, 12, 1, 21, 217, DateTimeKind.Unspecified).AddTicks(8321), "Quasi praesentium.", 33, 1, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 25, 5, 27, 11, 478, DateTimeKind.Unspecified).AddTicks(827), "Fuga ut ratione perspiciatis sequi et sed dolores accusantium cumque.", new DateTime(2021, 7, 21, 18, 7, 11, 512, DateTimeKind.Unspecified).AddTicks(720), "Velit cumque.", 5, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 42, 29, 463, DateTimeKind.Unspecified).AddTicks(6135), "Rerum consectetur rerum ea id odio sed.", new DateTime(2022, 3, 4, 17, 43, 53, 207, DateTimeKind.Unspecified).AddTicks(4951), "Corrupti id.", 37, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 27, 22, 45, 57, 471, DateTimeKind.Unspecified).AddTicks(8968), "Magni dolore eveniet odio distinctio officiis quasi atque adipisci in.", new DateTime(2022, 11, 15, 22, 50, 21, 614, DateTimeKind.Unspecified).AddTicks(6202), "Dolore quae.", 31, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 30, 19, 21, 39, 156, DateTimeKind.Unspecified).AddTicks(4076), "Sed eum blanditiis.", new DateTime(2021, 7, 14, 0, 11, 43, 560, DateTimeKind.Unspecified).AddTicks(1740), "Consectetur dolore.", 39, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 18, 8, 7, 43, 515, DateTimeKind.Unspecified).AddTicks(1553), "Consequatur labore possimus enim.", new DateTime(2022, 5, 12, 4, 3, 18, 312, DateTimeKind.Unspecified).AddTicks(5566), "Vitae nihil.", 8, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 17, 15, 37, 30, 57, DateTimeKind.Unspecified).AddTicks(9906), "Earum animi assumenda.", new DateTime(2021, 3, 30, 12, 9, 48, 242, DateTimeKind.Unspecified).AddTicks(9590), "Id qui.", 19, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 20, 6, 30, 21, 647, DateTimeKind.Unspecified).AddTicks(2007), "Voluptates vel ut voluptatem maiores eum et numquam quaerat.", new DateTime(2021, 10, 5, 14, 56, 7, 695, DateTimeKind.Unspecified).AddTicks(7857), "Rerum amet.", 30, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 30, 5, 16, 36, 579, DateTimeKind.Unspecified).AddTicks(6492), "Odit deserunt hic dolor quia doloremque maiores.", new DateTime(2021, 6, 21, 19, 10, 24, 989, DateTimeKind.Unspecified).AddTicks(7014), "Et nam.", 27, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 2, 16, 47, 7, 731, DateTimeKind.Unspecified).AddTicks(3848), "Itaque ut eum accusantium nostrum temporibus sit.", new DateTime(2022, 4, 20, 7, 36, 49, 179, DateTimeKind.Unspecified).AddTicks(1541), "Nihil temporibus.", 47, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 22, 7, 40, 14, 649, DateTimeKind.Unspecified).AddTicks(6658), "Sint porro odio eveniet pariatur labore sit.", new DateTime(2021, 10, 10, 0, 33, 13, 138, DateTimeKind.Unspecified).AddTicks(6171), "Et iure.", 19, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 5, 19, 45, 22, 3, DateTimeKind.Unspecified).AddTicks(3980), "Inventore fuga voluptas earum praesentium maxime repellat.", new DateTime(2022, 7, 29, 12, 49, 15, 682, DateTimeKind.Unspecified).AddTicks(4566), "Excepturi aut.", 29, 1, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 16, 1, 32, 50, 914, DateTimeKind.Unspecified).AddTicks(3568), "Rem quas impedit beatae quod occaecati impedit.", new DateTime(2022, 6, 14, 10, 31, 12, 997, DateTimeKind.Unspecified).AddTicks(2010), "Debitis et.", 6, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 21, 0, 38, 20, 636, DateTimeKind.Unspecified).AddTicks(354), "Quibusdam beatae est id et sit laboriosam id.", new DateTime(2021, 7, 12, 12, 30, 46, 275, DateTimeKind.Unspecified).AddTicks(3445), "Nisi consectetur.", 24, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 22, 5, 50, 35, 736, DateTimeKind.Unspecified).AddTicks(9629), "Non et eius quia sit molestiae nostrum.", new DateTime(2021, 4, 19, 2, 21, 28, 115, DateTimeKind.Unspecified).AddTicks(1032), "Accusantium porro.", 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 18, 6, 21, 39, 855, DateTimeKind.Unspecified).AddTicks(6059), "Ex non pariatur sapiente quas eligendi ut numquam non.", new DateTime(2021, 2, 5, 12, 34, 27, 9, DateTimeKind.Unspecified).AddTicks(8243), "Tempora deserunt.", 15, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "State" },
                values: new object[] { new DateTime(2019, 9, 14, 2, 30, 42, 146, DateTimeKind.Unspecified).AddTicks(2807), "Non sunt corporis beatae.", new DateTime(2021, 8, 24, 17, 14, 18, 797, DateTimeKind.Unspecified).AddTicks(5812), "Expedita et.", 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 6, 0, 55, 47, 37, DateTimeKind.Unspecified).AddTicks(6281), "Voluptates doloribus laborum ut voluptas quibusdam.", new DateTime(2021, 5, 28, 21, 46, 58, 995, DateTimeKind.Unspecified).AddTicks(4041), "Aut suscipit.", 43, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 6, 9, 56, 23, 71, DateTimeKind.Unspecified).AddTicks(90), "Quam dolorem neque aliquam qui nam velit.", new DateTime(2021, 9, 27, 22, 13, 48, 316, DateTimeKind.Unspecified).AddTicks(8656), "Dicta aperiam.", 22, 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 21, 5, 14, 35, 748, DateTimeKind.Unspecified).AddTicks(9686), "Facilis atque nesciunt non aspernatur.", new DateTime(2022, 3, 12, 4, 26, 0, 272, DateTimeKind.Unspecified).AddTicks(9199), "Et nemo.", 35, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 12, 7, 20, 8, 103, DateTimeKind.Unspecified).AddTicks(4736), "Hic quis incidunt eum impedit.", new DateTime(2022, 1, 29, 2, 21, 21, 242, DateTimeKind.Unspecified).AddTicks(7964), "Ex amet.", 29, 3, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 29, 2, 16, 11, 747, DateTimeKind.Unspecified).AddTicks(1922), "Nostrum nihil omnis sint eos.", new DateTime(2021, 10, 1, 1, 6, 17, 302, DateTimeKind.Unspecified).AddTicks(8932), "Numquam aut.", 4, 1, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 23, 21, 12, 13, 307, DateTimeKind.Unspecified).AddTicks(4497), "Rerum omnis et est neque cupiditate ipsa laborum veritatis aliquam.", new DateTime(2022, 5, 10, 21, 42, 42, 457, DateTimeKind.Unspecified).AddTicks(7837), "Delectus harum.", 13, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 1, 8, 21, 47, 52, 156, DateTimeKind.Unspecified).AddTicks(52), "Quisquam in dolorum beatae alias id non optio.", new DateTime(2022, 11, 13, 13, 36, 56, 370, DateTimeKind.Unspecified).AddTicks(4448), "Est enim.", 42 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 29, 3, 2, 50, 118, DateTimeKind.Unspecified).AddTicks(1142), "Nisi fugiat saepe repudiandae quas et sit eum optio.", new DateTime(2022, 1, 3, 5, 3, 40, 480, DateTimeKind.Unspecified).AddTicks(7765), "Est enim.", 45, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 7, 7, 37, 22, 990, DateTimeKind.Unspecified).AddTicks(8610), "Non sed quam beatae et.", new DateTime(2021, 1, 1, 8, 2, 9, 114, DateTimeKind.Unspecified).AddTicks(3126), "Molestiae nesciunt.", 34, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 13, 19, 37, 5, 500, DateTimeKind.Unspecified).AddTicks(8919), "Excepturi veniam laboriosam.", new DateTime(2022, 12, 6, 3, 13, 38, 754, DateTimeKind.Unspecified).AddTicks(3759), "Eum aut.", 45, 5, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 7, 7, 47, 26, 810, DateTimeKind.Unspecified).AddTicks(2335), "Et molestias fugiat mollitia facere ex id ipsa earum.", new DateTime(2021, 12, 20, 12, 16, 0, 867, DateTimeKind.Unspecified).AddTicks(9273), "Molestias quasi.", 4, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 17, 15, 13, 33, 405, DateTimeKind.Unspecified).AddTicks(5424), "Rerum incidunt sed ut tempore.", new DateTime(2021, 3, 11, 0, 21, 3, 561, DateTimeKind.Unspecified).AddTicks(1619), "Voluptatum voluptatem.", 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 17, 19, 59, 26, 620, DateTimeKind.Unspecified).AddTicks(8612), "Expedita iure eos ullam quod corporis enim dolor dolor sunt.", new DateTime(2021, 8, 16, 15, 19, 45, 15, DateTimeKind.Unspecified).AddTicks(1072), "Et fuga.", 43, 2, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 16, 8, 49, 29, 986, DateTimeKind.Unspecified).AddTicks(4517), "Impedit quidem provident accusantium molestiae est.", new DateTime(2021, 5, 2, 4, 10, 41, 151, DateTimeKind.Unspecified).AddTicks(6456), "Dolor cupiditate.", 40, 2, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 4, 1, 13, 39, 870, DateTimeKind.Unspecified).AddTicks(1521), "Ut dolores et debitis aspernatur ea.", new DateTime(2022, 1, 21, 6, 3, 4, 527, DateTimeKind.Unspecified).AddTicks(4080), "Nisi qui.", 2, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 8, 29, 18, 17, 0, 775, DateTimeKind.Unspecified).AddTicks(3177), "Id sint et repellendus enim natus dolore dolor ducimus ullam.", new DateTime(2022, 12, 7, 11, 28, 30, 21, DateTimeKind.Unspecified).AddTicks(6142), "Libero et.", 16, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 22, 5, 7, 2, 916, DateTimeKind.Unspecified).AddTicks(785), "Ipsa soluta quam consequatur ab et veritatis sunt.", new DateTime(2022, 11, 15, 4, 58, 13, 443, DateTimeKind.Unspecified).AddTicks(1441), "Est voluptas.", 45, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 26, 2, 31, 9, 649, DateTimeKind.Unspecified).AddTicks(4219), "Natus id dicta nihil possimus totam earum in ipsam aspernatur.", new DateTime(2022, 2, 26, 21, 16, 36, 146, DateTimeKind.Unspecified).AddTicks(7761), "Est maiores.", 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 29, 15, 22, 8, 617, DateTimeKind.Unspecified).AddTicks(558), "Voluptates rerum sunt vitae.", new DateTime(2022, 1, 25, 7, 26, 27, 993, DateTimeKind.Unspecified).AddTicks(6587), "Est minima.", 23, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 27, 15, 39, 2, 484, DateTimeKind.Unspecified).AddTicks(3350), "Distinctio illum in dicta placeat consectetur.", new DateTime(2022, 1, 12, 17, 38, 1, 762, DateTimeKind.Unspecified).AddTicks(3103), "Quia et.", 30, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 9, 58, 19, 646, DateTimeKind.Unspecified).AddTicks(1828), "Assumenda in ea voluptates quae pariatur praesentium minima non.", new DateTime(2022, 3, 21, 8, 59, 52, 955, DateTimeKind.Unspecified).AddTicks(418), "Architecto aut.", 26, 3, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 13, 12, 17, 12, 153, DateTimeKind.Unspecified).AddTicks(4250), "Itaque omnis iusto.", new DateTime(2022, 2, 9, 4, 27, 57, 785, DateTimeKind.Unspecified).AddTicks(8346), "Rem similique.", 12, 1, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 4, 2, 3, 42, 10, 998, DateTimeKind.Unspecified).AddTicks(4697), "Saepe voluptas earum ab.", new DateTime(2022, 1, 17, 22, 54, 32, 666, DateTimeKind.Unspecified).AddTicks(8834), "Sequi quibusdam.", 32, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 27, 9, 29, 57, 263, DateTimeKind.Unspecified).AddTicks(1473), "Molestiae quis molestiae impedit.", new DateTime(2022, 6, 13, 18, 2, 40, 757, DateTimeKind.Unspecified).AddTicks(6103), "Unde sit.", 2, 4, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 31, 7, 28, 6, 909, DateTimeKind.Unspecified).AddTicks(2619), "Deleniti qui et voluptas debitis in eligendi.", new DateTime(2021, 9, 26, 21, 16, 51, 826, DateTimeKind.Unspecified).AddTicks(6505), "Inventore sunt.", 39, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 12, 11, 50, 21, 297, DateTimeKind.Unspecified).AddTicks(5811), "Eligendi ducimus ut dolorem est voluptatem architecto explicabo.", new DateTime(2022, 8, 28, 13, 11, 56, 181, DateTimeKind.Unspecified).AddTicks(2553), "Blanditiis eos.", 5, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 18, 2, 32, 13, 983, DateTimeKind.Unspecified).AddTicks(7926), "Labore rerum omnis.", new DateTime(2022, 12, 15, 15, 6, 3, 448, DateTimeKind.Unspecified).AddTicks(8427), "Iusto est.", 16, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 2, 13, 2, 51, 731, DateTimeKind.Unspecified).AddTicks(5471), "Esse eum numquam temporibus architecto sapiente laborum sit laboriosam.", new DateTime(2022, 4, 2, 6, 22, 8, 287, DateTimeKind.Unspecified).AddTicks(7366), "Laudantium reiciendis.", 7, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 7, 13, 36, 51, 355, DateTimeKind.Unspecified).AddTicks(1677), "Voluptas numquam ipsa ullam earum architecto saepe eligendi tempore ut.", new DateTime(2022, 1, 26, 9, 37, 42, 654, DateTimeKind.Unspecified).AddTicks(6759), "Esse quod.", 50, 2, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 24, 6, 6, 26, 678, DateTimeKind.Unspecified).AddTicks(6413), "Vero recusandae rerum qui tempore.", new DateTime(2022, 3, 14, 3, 35, 57, 492, DateTimeKind.Unspecified).AddTicks(7718), "Sit voluptatem.", 16, 5, 0 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 22, 12, 31, 331, DateTimeKind.Unspecified).AddTicks(1492), "Fuga asperiores a et molestiae reiciendis fuga maxime est vitae.", new DateTime(2022, 1, 30, 21, 14, 16, 66, DateTimeKind.Unspecified).AddTicks(8216), "Repellat dolorem.", 27, 2, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 31, 20, 14, 8, 795, DateTimeKind.Unspecified).AddTicks(2186), "Esse inventore et quis perferendis eius enim.", new DateTime(2022, 3, 17, 19, 16, 58, 644, DateTimeKind.Unspecified).AddTicks(7238), "Neque asperiores.", 33, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 2, 3, 17, 27, 264, DateTimeKind.Unspecified).AddTicks(1943), "Maxime adipisci sint ratione doloribus accusamus commodi quas quos.", new DateTime(2021, 8, 15, 4, 30, 11, 496, DateTimeKind.Unspecified).AddTicks(9559), "Quam labore.", 6, 5, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2019, 4, 16, 13, 46, 55, 821, DateTimeKind.Unspecified).AddTicks(5562), "Aut voluptatem voluptas esse qui.", new DateTime(2022, 1, 25, 16, 53, 48, 844, DateTimeKind.Unspecified).AddTicks(2212), "Repellat cum.", 6 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 12, 24, 10, 30, 2, 926, DateTimeKind.Unspecified).AddTicks(1745), "Saepe voluptatem et beatae est.", new DateTime(2022, 6, 24, 17, 18, 4, 841, DateTimeKind.Unspecified).AddTicks(8319), "Omnis nostrum.", 5, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 20, 4, 21, 22, 202, DateTimeKind.Unspecified).AddTicks(6834), "Ut praesentium eos sit accusamus ut consequatur voluptatum rerum.", new DateTime(2021, 11, 29, 15, 47, 32, 120, DateTimeKind.Unspecified).AddTicks(5144), "Ullam officia.", 37, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 24, 19, 53, 20, 586, DateTimeKind.Unspecified).AddTicks(6042), "Non totam porro nostrum est voluptate est.", new DateTime(2021, 7, 16, 9, 10, 59, 191, DateTimeKind.Unspecified).AddTicks(8580), "Dolorem quibusdam.", 4, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 3, 3, 13, 55, 956, DateTimeKind.Unspecified).AddTicks(2661), "Doloribus quia vel natus fuga totam similique.", new DateTime(2022, 5, 3, 19, 22, 53, 371, DateTimeKind.Unspecified).AddTicks(48), "Ut voluptatum.", 43, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 11, 17, 13, 12, 495, DateTimeKind.Unspecified).AddTicks(6531), "A enim et.", new DateTime(2022, 12, 4, 5, 15, 13, 679, DateTimeKind.Unspecified).AddTicks(4965), "Iure non.", 28, 1, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 1, 8, 21, 45, 893, DateTimeKind.Unspecified).AddTicks(3706), "Libero esse aliquid et a dolorem rerum fuga eveniet eligendi.", new DateTime(2022, 5, 4, 6, 8, 30, 110, DateTimeKind.Unspecified).AddTicks(8198), "Non labore.", 50, 5 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 7, 25, 19, 46, 6, 538, DateTimeKind.Unspecified).AddTicks(6072), "Voluptate vero consequatur.", new DateTime(2022, 3, 26, 19, 12, 17, 964, DateTimeKind.Unspecified).AddTicks(9257), "Placeat qui.", 43, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 26, 2, 3, 20, 874, DateTimeKind.Unspecified).AddTicks(1401), "Voluptas nam nemo ducimus ab.", new DateTime(2021, 8, 12, 21, 5, 13, 96, DateTimeKind.Unspecified).AddTicks(5468), "Et explicabo.", 16, 3, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 16, 6, 19, 54, 621, DateTimeKind.Unspecified).AddTicks(7047), "Provident reprehenderit cupiditate dolore voluptatem illum itaque qui qui.", new DateTime(2022, 2, 25, 4, 35, 29, 450, DateTimeKind.Unspecified).AddTicks(7214), "Recusandae odit.", 34, 4, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 10, 24, 44, 191, DateTimeKind.Unspecified).AddTicks(6748), "Hic quia rerum hic modi iste.", new DateTime(2022, 6, 26, 13, 12, 42, 643, DateTimeKind.Unspecified).AddTicks(8576), "Cum aut.", 33, 1, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 15, 20, 23, 24, DateTimeKind.Unspecified).AddTicks(7735), "Dolorem aut accusamus omnis culpa vel aut.", new DateTime(2021, 1, 5, 8, 44, 17, 732, DateTimeKind.Unspecified).AddTicks(7128), "Molestiae minus.", 47, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 11, 4, 42, 37, 301, DateTimeKind.Unspecified).AddTicks(9328), "Ut quas numquam consequuntur velit voluptas.", new DateTime(2022, 7, 19, 3, 44, 46, 813, DateTimeKind.Unspecified).AddTicks(5411), "Voluptatem nam.", 13, 5, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 20, 4, 0, 51, 245, DateTimeKind.Unspecified).AddTicks(2484), "Vitae tempore eveniet minus dolorem.", new DateTime(2022, 4, 16, 22, 3, 25, 938, DateTimeKind.Unspecified).AddTicks(2096), "Qui distinctio.", 20, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 18, 2, 7, 34, 789, DateTimeKind.Unspecified).AddTicks(8774), "Et inventore consequuntur maiores qui et sed inventore sint.", new DateTime(2022, 5, 16, 23, 25, 55, 881, DateTimeKind.Unspecified).AddTicks(8433), "Eius unde.", 29, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 18, 20, 15, 17, 238, DateTimeKind.Unspecified).AddTicks(8453), "Possimus ad eos quia.", new DateTime(2022, 4, 28, 10, 14, 3, 461, DateTimeKind.Unspecified).AddTicks(6245), "Aspernatur repudiandae.", 40, 3, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 21, 4, 55, 53, 749, DateTimeKind.Unspecified).AddTicks(7685), "Quia sit quia voluptatem asperiores cum adipisci.", new DateTime(2022, 2, 28, 6, 54, 30, 841, DateTimeKind.Unspecified).AddTicks(2520), "Aliquid libero.", 33, 3 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 19, 9, 50, 4, 778, DateTimeKind.Unspecified).AddTicks(8803), "Ducimus nesciunt sunt ab architecto.", new DateTime(2021, 10, 19, 1, 17, 36, 100, DateTimeKind.Unspecified).AddTicks(6620), "Autem iure.", 14, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 11, 19, 7, 25, 30, 43, DateTimeKind.Unspecified).AddTicks(5749), "Fugiat deserunt cupiditate et aut iure et consequatur fugit ea.", new DateTime(2021, 3, 23, 8, 24, 33, 89, DateTimeKind.Unspecified).AddTicks(8919), "Earum eaque.", 49, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 4, 5, 43, 45, 289, DateTimeKind.Unspecified).AddTicks(7866), "Ut unde quisquam est qui facere velit.", new DateTime(2022, 5, 20, 3, 21, 33, 61, DateTimeKind.Unspecified).AddTicks(9802), "Sed quibusdam.", 33, 2 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 1, 21, 11, 15, 261, DateTimeKind.Unspecified).AddTicks(6315), "Aliquam quibusdam mollitia quas reiciendis omnis ut.", new DateTime(2022, 12, 31, 6, 4, 57, 903, DateTimeKind.Unspecified).AddTicks(6571), "Ducimus est.", 12, 1 });

            migrationBuilder.UpdateData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 2, 2, 22, 21, 45, 171, DateTimeKind.Unspecified).AddTicks(1303), "Nihil quibusdam id fuga.", new DateTime(2022, 2, 22, 3, 3, 12, 941, DateTimeKind.Unspecified).AddTicks(6558), "Fugit ut.", 7, 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 50, new DateTime(2019, 6, 6, 16, 39, 0, 832, DateTimeKind.Unspecified).AddTicks(702), new DateTime(2022, 6, 26, 19, 25, 42, 329, DateTimeKind.Unspecified).AddTicks(8134), "Doloribus sint ad eum magni ut dolor et rerum vel.", "Incredible Rubber Tuna", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 22, new DateTime(2019, 7, 4, 22, 30, 44, 302, DateTimeKind.Unspecified).AddTicks(4779), new DateTime(2022, 2, 22, 4, 15, 54, 568, DateTimeKind.Unspecified).AddTicks(9798), "Repudiandae nam qui cupiditate quos consequuntur.", "Rustic Concrete Soap", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 21, new DateTime(2019, 4, 26, 13, 39, 14, 444, DateTimeKind.Unspecified).AddTicks(7672), new DateTime(2021, 5, 20, 2, 45, 26, 781, DateTimeKind.Unspecified).AddTicks(7452), "Ea totam a voluptas suscipit provident ducimus rerum.", "Gorgeous Soft Ball", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2019, 2, 11, 21, 16, 37, 948, DateTimeKind.Unspecified).AddTicks(8452), new DateTime(2021, 10, 27, 2, 33, 47, 831, DateTimeKind.Unspecified).AddTicks(7956), "Facere vitae veritatis eligendi rerum illo et aut laborum.", "Tasty Rubber Chair", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 11, new DateTime(2019, 11, 7, 6, 32, 19, 106, DateTimeKind.Unspecified).AddTicks(2886), new DateTime(2022, 2, 18, 16, 25, 33, 615, DateTimeKind.Unspecified).AddTicks(8690), "Provident id sunt et nihil accusantium dolore nam id.", "Generic Soft Pants", 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 10, 12, 0, 26, 50, 882, DateTimeKind.Unspecified).AddTicks(77), "Ledner, Stokes and Frami" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 5, 29, 2, 48, 23, 564, DateTimeKind.Unspecified).AddTicks(5970), "Anderson Inc" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2017, 4, 17, 15, 27, 52, 475, DateTimeKind.Unspecified).AddTicks(1368), "Kerluke - Orn" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2011, 10, 28, 10, 57, 0, 737, DateTimeKind.Unspecified).AddTicks(9794), "O'Reilly - Kihn" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 1, 22, 21, 20, 29, 837, DateTimeKind.Unspecified).AddTicks(7481), "Berge LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2015, 5, 26, 3, 29, 18, 759, DateTimeKind.Unspecified).AddTicks(6229), "Rau LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 8, 23, 13, 49, 45, 69, DateTimeKind.Unspecified).AddTicks(1381), "Murphy LLC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 6, 22, 10, 49, 16, 535, DateTimeKind.Unspecified).AddTicks(8759), "Aufderhar - Weissnat" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2014, 5, 29, 3, 59, 18, 824, DateTimeKind.Unspecified).AddTicks(9648), "Mosciski, Muller and Legros" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2012, 9, 18, 22, 55, 24, 134, DateTimeKind.Unspecified).AddTicks(2856), "Pfannerstill Group" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 5, 22, 22, 6, 2, 956, DateTimeKind.Unspecified).AddTicks(1959), "Monserrate12@gmail.com", "Marlene", "Flatley", new DateTime(2014, 4, 17, 13, 31, 3, 346, DateTimeKind.Unspecified).AddTicks(9587), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 12, 9, 11, 20, 44, 518, DateTimeKind.Unspecified).AddTicks(6665), "Ima.Schoen1@gmail.com", "Barrett", "Roob", new DateTime(2013, 12, 16, 21, 41, 44, 272, DateTimeKind.Unspecified).AddTicks(595), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 5, 24, 7, 3, 20, 457, DateTimeKind.Unspecified).AddTicks(5434), "Cassandra4@hotmail.com", "Orpha", "Daugherty", new DateTime(2015, 5, 21, 11, 31, 43, 35, DateTimeKind.Unspecified).AddTicks(6305), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 7, 4, 13, 44, 54, 869, DateTimeKind.Unspecified).AddTicks(4525), "Willie_Moen37@yahoo.com", "Zion", "Lockman", new DateTime(2015, 9, 14, 12, 48, 2, 833, DateTimeKind.Unspecified).AddTicks(7873), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 1, 18, 50, 54, 835, DateTimeKind.Unspecified).AddTicks(9764), "Aubrey.Jakubowski@hotmail.com", "Burnice", "Price", new DateTime(2014, 10, 7, 8, 44, 7, 763, DateTimeKind.Unspecified).AddTicks(1298), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 8, 19, 7, 41, 19, 933, DateTimeKind.Unspecified).AddTicks(2400), "Boyd.Haag@hotmail.com", "Jamison", "Leannon", new DateTime(2017, 9, 21, 13, 38, 8, 485, DateTimeKind.Unspecified).AddTicks(270), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1996, 6, 4, 17, 33, 27, 511, DateTimeKind.Unspecified).AddTicks(7396), "Shania66@hotmail.com", "Brannon", "Champlin", new DateTime(2014, 2, 26, 0, 10, 20, 441, DateTimeKind.Unspecified).AddTicks(5900) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 11, 18, 10, 26, 25, 58, DateTimeKind.Unspecified).AddTicks(4293), "Elissa_Kshlerin@yahoo.com", "Garnet", "Yost", new DateTime(2015, 10, 30, 13, 16, 50, 656, DateTimeKind.Unspecified).AddTicks(8729), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 29, 0, 25, 6, 946, DateTimeKind.Unspecified).AddTicks(7884), "Palma_Goyette@yahoo.com", "Tod", "Mosciski", new DateTime(2016, 5, 12, 18, 28, 12, 977, DateTimeKind.Unspecified).AddTicks(3636), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 2, 2, 18, 46, 19, 397, DateTimeKind.Unspecified).AddTicks(4196), "Katlyn61@yahoo.com", "Xavier", "Cronin", new DateTime(2013, 7, 11, 0, 35, 38, 519, DateTimeKind.Unspecified).AddTicks(8184), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 6, 27, 14, 20, 16, 845, DateTimeKind.Unspecified).AddTicks(7314), "Darlene47@yahoo.com", "Kiara", "Reichert", new DateTime(2014, 7, 27, 8, 8, 56, 477, DateTimeKind.Unspecified).AddTicks(7980), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 11, 20, 10, 23, 42, 605, DateTimeKind.Unspecified).AddTicks(584), "Kobe_Larson@hotmail.com", "Abelardo", "Fahey", new DateTime(2015, 4, 14, 14, 6, 27, 12, DateTimeKind.Unspecified).AddTicks(338), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 11, 24, 15, 36, 13, 172, DateTimeKind.Unspecified).AddTicks(6458), "Daphnee25@hotmail.com", "Vinnie", "Renner", new DateTime(2017, 12, 13, 2, 37, 29, 767, DateTimeKind.Unspecified).AddTicks(9930), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 10, 7, 10, 12, 46, 263, DateTimeKind.Unspecified).AddTicks(7433), "Kendrick.Jones75@yahoo.com", "Alexis", "Wilderman", new DateTime(2012, 8, 19, 20, 55, 25, 618, DateTimeKind.Unspecified).AddTicks(6710), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 5, 9, 0, 47, 15, 584, DateTimeKind.Unspecified).AddTicks(4212), "Joshua.Jenkins59@hotmail.com", "Mason", "Schaden", new DateTime(2013, 4, 22, 22, 17, 55, 463, DateTimeKind.Unspecified).AddTicks(289) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 18, 3, 24, 21, 902, DateTimeKind.Unspecified).AddTicks(5005), "Remington_Hamill75@hotmail.com", "Dahlia", "Wiegand", new DateTime(2013, 9, 15, 19, 36, 50, 524, DateTimeKind.Unspecified).AddTicks(6608), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 3, 14, 17, 36, 10, 471, DateTimeKind.Unspecified).AddTicks(5096), "Gideon_Braun20@gmail.com", "Jodie", "Russel", new DateTime(2014, 1, 3, 9, 45, 22, 653, DateTimeKind.Unspecified).AddTicks(7608), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 23, 19, 2, 22, 45, DateTimeKind.Unspecified).AddTicks(8935), "Mckenzie_Nitzsche56@gmail.com", "Xander", "Olson", new DateTime(2015, 8, 11, 20, 15, 5, 51, DateTimeKind.Unspecified).AddTicks(6578), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 8, 1, 12, 24, 50, 135, DateTimeKind.Unspecified).AddTicks(2159), "Leo_Olson95@gmail.com", "Florida", "Lesch", new DateTime(2010, 10, 3, 19, 59, 40, 726, DateTimeKind.Unspecified).AddTicks(2525), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 28, 19, 7, 12, 113, DateTimeKind.Unspecified).AddTicks(4558), "Christelle.VonRueden36@gmail.com", "Sigmund", "Pagac", new DateTime(2012, 10, 25, 23, 30, 54, 242, DateTimeKind.Unspecified).AddTicks(6607), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 26, 22, 52, 59, 114, DateTimeKind.Unspecified).AddTicks(7983), "Adonis.Littel@gmail.com", "Kayley", "Larkin", new DateTime(2014, 5, 18, 17, 31, 55, 734, DateTimeKind.Unspecified).AddTicks(3688), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 6, 30, 0, 17, 41, 430, DateTimeKind.Unspecified).AddTicks(7145), "Alexandria70@gmail.com", "Manuel", "Gibson", new DateTime(2013, 4, 6, 20, 2, 24, 137, DateTimeKind.Unspecified).AddTicks(423), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 8, 8, 8, 40, 17, 946, DateTimeKind.Unspecified).AddTicks(9761), "Cheyenne.Fisher@hotmail.com", "Taya", "Gutmann", new DateTime(2015, 7, 29, 13, 44, 49, 685, DateTimeKind.Unspecified).AddTicks(4828), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 11, 17, 9, 58, 58, 521, DateTimeKind.Unspecified).AddTicks(2810), "Lance.Konopelski35@hotmail.com", "Russell", "Kessler", new DateTime(2016, 3, 31, 0, 59, 23, 153, DateTimeKind.Unspecified).AddTicks(2280), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 3, 17, 0, 25, 230, DateTimeKind.Unspecified).AddTicks(7787), "Geovanny_Baumbach57@hotmail.com", "Rylan", "Walker", new DateTime(2011, 7, 29, 0, 45, 17, 893, DateTimeKind.Unspecified).AddTicks(220), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 5, 15, 0, 55, 41, 827, DateTimeKind.Unspecified).AddTicks(349), "Wilford_Rutherford89@gmail.com", "Francisca", "Greenfelder", new DateTime(2011, 5, 18, 15, 46, 14, 456, DateTimeKind.Unspecified).AddTicks(3786), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 8, 17, 16, 8, 20, 594, DateTimeKind.Unspecified).AddTicks(2780), "Anika.Simonis@hotmail.com", "Leonardo", "Rodriguez", new DateTime(2010, 10, 31, 11, 22, 34, 406, DateTimeKind.Unspecified).AddTicks(6844), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 9, 19, 19, 24, 35, 909, DateTimeKind.Unspecified).AddTicks(9552), "Lenore.Klein@hotmail.com", "D'angelo", "Kilback", new DateTime(2010, 8, 9, 3, 8, 46, 63, DateTimeKind.Unspecified).AddTicks(2618), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 7, 3, 11, 9, 8, 977, DateTimeKind.Unspecified).AddTicks(9052), "Ressie_Bauch@hotmail.com", "Melvin", "Walsh", new DateTime(2016, 8, 29, 0, 33, 34, 957, DateTimeKind.Unspecified).AddTicks(4816), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 9, 19, 46, 45, 155, DateTimeKind.Unspecified).AddTicks(2434), "Reta_Bailey81@gmail.com", "Adrain", "O'Keefe", new DateTime(2011, 2, 21, 7, 6, 56, 100, DateTimeKind.Unspecified).AddTicks(9541), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 4, 11, 2, 21, 2, 383, DateTimeKind.Unspecified).AddTicks(3232), "Eugenia72@gmail.com", "Celestino", "Bashirian", new DateTime(2013, 7, 20, 11, 47, 59, 79, DateTimeKind.Unspecified).AddTicks(7420), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 13, 12, 24, 56, 142, DateTimeKind.Unspecified).AddTicks(2516), "Thea_Welch13@hotmail.com", "Al", "Reichert", new DateTime(2014, 2, 6, 16, 21, 45, 374, DateTimeKind.Unspecified).AddTicks(8673), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 17, 19, 7, 30, 954, DateTimeKind.Unspecified).AddTicks(1119), "Donavon.Denesik38@gmail.com", "Reilly", "Flatley", new DateTime(2012, 7, 7, 8, 6, 26, 725, DateTimeKind.Unspecified).AddTicks(3767), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 2, 11, 21, 23, 20, 729, DateTimeKind.Unspecified).AddTicks(1530), "Mertie_Reilly@gmail.com", "Jimmie", "Lind", new DateTime(2015, 4, 6, 8, 6, 0, 517, DateTimeKind.Unspecified).AddTicks(697), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 9, 19, 20, 8, 13, 853, DateTimeKind.Unspecified).AddTicks(1778), "Kole.Leuschke@gmail.com", "Fermin", "Yost", new DateTime(2014, 2, 2, 2, 21, 6, 374, DateTimeKind.Unspecified).AddTicks(6985), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 11, 23, 3, 11, 37, 919, DateTimeKind.Unspecified).AddTicks(910), "Deangelo.Langworth@gmail.com", "Javier", "McGlynn", new DateTime(2017, 3, 12, 22, 14, 30, 800, DateTimeKind.Unspecified).AddTicks(2778), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 8, 9, 14, 18, 13, 815, DateTimeKind.Unspecified).AddTicks(2166), "Claud66@hotmail.com", "Kylie", "Kunde", new DateTime(2012, 6, 19, 19, 7, 33, 384, DateTimeKind.Unspecified).AddTicks(2991), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 10, 1, 11, 16, 42, 918, DateTimeKind.Unspecified).AddTicks(2327), "Euna45@gmail.com", "Amani", "Daniel", new DateTime(2016, 9, 25, 11, 21, 31, 40, DateTimeKind.Unspecified).AddTicks(6506), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 5, 12, 14, 19, 28, 174, DateTimeKind.Unspecified).AddTicks(6622), "Sherman_Mosciski@hotmail.com", "Quincy", "Bergnaum", new DateTime(2017, 10, 17, 16, 29, 9, 69, DateTimeKind.Unspecified).AddTicks(886), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1990, 1, 23, 20, 8, 59, 142, DateTimeKind.Unspecified).AddTicks(3319), "Hermina19@gmail.com", "Brett", "Weimann", new DateTime(2011, 3, 12, 2, 2, 43, 308, DateTimeKind.Unspecified).AddTicks(7857) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 2, 2, 0, 15, 43, 148, DateTimeKind.Unspecified).AddTicks(5763), "Ezra55@gmail.com", "Meaghan", "Goldner", new DateTime(2017, 6, 15, 13, 1, 50, 613, DateTimeKind.Unspecified).AddTicks(3000), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 25, 11, 35, 8, 296, DateTimeKind.Unspecified).AddTicks(1297), "Cristobal78@yahoo.com", "Will", "Rau", new DateTime(2010, 5, 2, 17, 18, 51, 686, DateTimeKind.Unspecified).AddTicks(2364), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 12, 21, 20, 51, 26, 426, DateTimeKind.Unspecified).AddTicks(2484), "Garrison35@gmail.com", "Maia", "Glover", new DateTime(2016, 1, 28, 9, 11, 27, 413, DateTimeKind.Unspecified).AddTicks(8414), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 7, 17, 20, 14, 42, 126, DateTimeKind.Unspecified).AddTicks(8129), "Shayna42@hotmail.com", "Gavin", "Yundt", new DateTime(2011, 4, 6, 13, 23, 30, 599, DateTimeKind.Unspecified).AddTicks(6714), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 3, 3, 5, 22, 630, DateTimeKind.Unspecified).AddTicks(1086), "Kelvin_Abbott@gmail.com", "Michaela", "Klein", new DateTime(2013, 10, 22, 13, 2, 52, 310, DateTimeKind.Unspecified).AddTicks(4157), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 9, 9, 21, 50, 28, 166, DateTimeKind.Unspecified).AddTicks(3124), "George_Wyman63@yahoo.com", "Abel", "Weissnat", new DateTime(2013, 7, 25, 18, 56, 15, 877, DateTimeKind.Unspecified).AddTicks(6186), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1990, 7, 17, 10, 12, 50, 266, DateTimeKind.Unspecified).AddTicks(142), "Samson.Veum@gmail.com", "Raul", "Grady", new DateTime(2012, 9, 11, 22, 57, 57, 252, DateTimeKind.Unspecified).AddTicks(5300) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 5, 31, 2, 14, 32, 914, DateTimeKind.Unspecified).AddTicks(9140), "Kirk_Labadie@hotmail.com", "Kenton", "Beahan", new DateTime(2013, 10, 8, 8, 58, 48, 116, DateTimeKind.Unspecified).AddTicks(299), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 6, 11, 16, 1, 332, DateTimeKind.Unspecified).AddTicks(7111), "Twila48@yahoo.com", "Carmelo", "Larson", new DateTime(2015, 5, 11, 7, 38, 23, 135, DateTimeKind.Unspecified).AddTicks(8811), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 4, 9, 3, 11, 4, 390, DateTimeKind.Unspecified).AddTicks(4345), "Alize_Little@hotmail.com", "Joesph", "Bernhard", new DateTime(2014, 12, 25, 6, 30, 59, 777, DateTimeKind.Unspecified).AddTicks(6646), 10 });

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectTasks_Projects_ProjectId",
                table: "ProjectTasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectTasks_Users_PerformerId",
                table: "ProjectTasks",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
