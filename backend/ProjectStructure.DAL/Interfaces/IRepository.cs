using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Interfaces;

public interface IRepository<TEntity> where TEntity : BaseEntity
{
    Task<List<TEntity>> GetAll();

    Task<TEntity?> GetById(int id);

    Task Add(TEntity entity);

    Task Delete(TEntity entity);

    Task DeleteById(int id);

    Task Update(TEntity entity);
}