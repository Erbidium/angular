﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Interfaces;

public interface IUnitOfWork
{
    IRepository<Project> ProjectRepository { get; }
    IRepository<ProjectTask> ProjectTaskRepository { get; }
    IRepository<Team> TeamRepository { get; }
    IRepository<User> UserRepository { get; }
    Task<int> SaveChangesAsync();
}