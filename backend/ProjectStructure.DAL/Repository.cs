﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL;

public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
{
    private readonly ProjectsDbContext _context;

    public Repository(ProjectsDbContext context)
    {
        _context = context;
    }

    public Task<List<TEntity>> GetAll()
    {
        return _context.Set<TEntity>().ToListAsync();
    }

    public Task<TEntity?> GetById(int id)
    {
        return _context.Set<TEntity>().FindAsync(id).AsTask();
    }

    public async Task Add(TEntity entity)
    {
        await _context.Set<TEntity>().AddAsync(entity);
    }

    public async Task Delete(TEntity entity)
    {
        _context.Set<TEntity>().Remove(entity);
    }

    public async Task DeleteById(int id)
    {
        TEntity? entity = await GetById(id);
        await Delete(entity);
    }

    public async Task Update(TEntity entity)
    {
        _context.Set<TEntity>().Attach(entity);
        _context.Entry(entity).State = EntityState.Modified;
    }
}