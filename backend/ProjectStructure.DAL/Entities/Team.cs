﻿using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Entities;

public class Team : BaseEntity
{
    public string? Name { get; set; }
    public DateTime CreatedAt { get; set; }
}