﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL;

public class UnitOfWork : IUnitOfWork
{
    private readonly ProjectsDbContext _projectsDbContext;

    public UnitOfWork(ProjectsDbContext projectsDbContext)
    {
        _projectsDbContext = projectsDbContext;
        ProjectRepository = new Repository<Project>(projectsDbContext);
        ProjectTaskRepository = new Repository<ProjectTask>(projectsDbContext);
        TeamRepository = new Repository<Team>(projectsDbContext);
        UserRepository = new Repository<User>(projectsDbContext);
    }

    public IRepository<Project> ProjectRepository { get; }
    public IRepository<ProjectTask> ProjectTaskRepository { get; }
    public IRepository<Team> TeamRepository { get; }
    public IRepository<User> UserRepository { get; }

    public Task<int> SaveChangesAsync()
    {
        return _projectsDbContext.SaveChangesAsync();
    }
}