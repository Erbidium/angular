﻿using Bogus;
using ProjectStructure.DAL.Entities;
using ProjectStructure.Common.Enums;

namespace ProjectStructure.DAL.Context;

public static class DataGenerator
{
    public static List<Team> GenerateRandomTeams(int teamsNumber)
    {
        var teamId = 1;

        var teamsFake = new Faker<Team>()
            .RuleFor(t => t.Id, f => teamId++)
            .RuleFor(t => t.Name, f => f.Company.CompanyName())
            .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2010, 1, 1), new DateTime(2018, 1, 1)));

        return teamsFake.Generate(teamsNumber);
    }

    public static List<User> GenerateRandomUsers(int usersNumber, List<Team> teams)
    {
        var userId = 1;

        var usersFake = new Faker<User>()
            .RuleFor(u => u.Id, f => userId++)
            .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id)
            .RuleFor(u => u.FirstName, f => f.Name.FirstName())
            .RuleFor(u => u.LastName, f => f.Name.LastName())
            .RuleFor(u => u.Email, f => f.Internet.Email())
            .RuleFor(u => u.RegisteredAt, f => f.Date.Between(new DateTime(2010, 1, 1), new DateTime(2018, 1, 1)))
            .RuleFor(u => u.BirthDay, f => f.Date.Between(new DateTime(1990, 1, 1), new DateTime(2000, 1, 1)));

        return usersFake.Generate(usersNumber);
    }

    public static List<Project> GenerateRandomProjects(int projectsNumber, List<User> users, List<Team> teams)
    {
        var projectId = 1;

        var projectsFake = new Faker<Project>()
            .RuleFor(p => p.Id, f => projectId++)
            .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id)
            .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
            .RuleFor(p => p.Name, f => f.Commerce.ProductName())
            .RuleFor(p => p.Description, f => f.Lorem.Sentence())
            .RuleFor(u => u.CreatedAt, f => f.Date.Between(new DateTime(2019, 1, 1), new DateTime(2020, 1, 1)))
            .RuleFor(u => u.Deadline, f => f.Date.Between(new DateTime(2021, 1, 1), new DateTime(2023, 1, 1)));

        return projectsFake.Generate(projectsNumber);
    }

    public static List<ProjectTask> GenerateRandomProjectTasks(int projectTasksNumber, List<User> users,
        List<Project> projects)
    {
        var projectTaskId = 1;

        var projectTasksFake = new Faker<ProjectTask>()
            .RuleFor(t => t.Id, f => projectTaskId++)
            .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
            .RuleFor(t => t.PerformerId, f => f.PickRandom(users).Id)
            .RuleFor(t => t.Name, f => f.Lorem.Sentence(2))
            .RuleFor(t => t.Description, f => f.Lorem.Sentence())
            .RuleFor(t => t.State, f => f.PickRandom<TaskState>())
            .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2019, 1, 1), new DateTime(2020, 1, 1)))
            .RuleFor(t => t.FinishedAt, f => f.Date.Between(new DateTime(2021, 1, 1), new DateTime(2023, 1, 1)));

        return projectTasksFake.Generate(projectTasksNumber);
    }
}