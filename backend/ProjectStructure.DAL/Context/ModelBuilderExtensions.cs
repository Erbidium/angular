﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context;

public static class ModelBuilderExtensions
{
    public static void Configure(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Project>()
            .HasOne(p => p.Author)
            .WithMany()
            .HasForeignKey(p => p.AuthorId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Project>()
            .HasOne(p => p.Team)
            .WithMany()
            .HasForeignKey(p => p.TeamId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<User>()
            .HasOne(u => u.Team)
            .WithMany()
            .HasForeignKey(u => u.TeamId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<ProjectTask>()
            .HasOne(t => t.Project)
            .WithMany()
            .HasForeignKey(t => t.ProjectId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<ProjectTask>()
            .HasOne(t => t.Performer)
            .WithMany()
            .HasForeignKey(t => t.PerformerId)
            .OnDelete(DeleteBehavior.Restrict);


        modelBuilder.Entity<ProjectTask>()
            .Property(t => t.PerformerId)
            .HasColumnName("PerformerUserId");


        modelBuilder.Entity<User>()
            .Property(u => u.FirstName)
            .HasMaxLength(100);

        modelBuilder.Entity<User>()
            .Property(u => u.LastName)
            .HasMaxLength(100);

        modelBuilder.Entity<Team>()
            .Property(t => t.Name)
            .HasMaxLength(100);

        modelBuilder.Entity<Project>()
            .Property(p => p.Name)
            .HasMaxLength(100);

        modelBuilder.Entity<ProjectTask>()
            .Property(p => p.Name)
            .HasMaxLength(100);


        modelBuilder.Entity<ProjectTask>()
            .Property(t => t.CreatedAt)
            .IsRequired();

        modelBuilder.Entity<Team>()
            .Property(t => t.CreatedAt)
            .IsRequired();

        modelBuilder.Entity<ProjectTask>().ToTable("Tasks");
    }

    public static void Seed(this ModelBuilder modelBuilder)
    {
        var teams = DataGenerator.GenerateRandomTeams(10);
        var users = DataGenerator.GenerateRandomUsers(50, teams);
        var projects = DataGenerator.GenerateRandomProjects(5, users, teams);
        var projectTasks = DataGenerator.GenerateRandomProjectTasks(200, users, projects);

        modelBuilder.Entity<Team>().HasData(teams);
        modelBuilder.Entity<User>().HasData(users);
        modelBuilder.Entity<Project>().HasData(projects);
        modelBuilder.Entity<ProjectTask>().HasData(projectTasks);
    }
}