using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.DAL;

public class ProjectsDbContext : DbContext
{
    public DbSet<Project> Projects { get; private set; }
    public DbSet<ProjectTask> ProjectTasks { get; private set; }
    public DbSet<Team> Teams { get; private set; }
    public DbSet<User> Users { get; private set; }

    public ProjectsDbContext(DbContextOptions<ProjectsDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Configure();

        modelBuilder.Seed();
    }
}