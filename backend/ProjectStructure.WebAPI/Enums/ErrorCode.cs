﻿namespace ProjectStructure.WebAPI.Enums;

public enum ErrorCode
{
    General = 1,
    NotFound
}