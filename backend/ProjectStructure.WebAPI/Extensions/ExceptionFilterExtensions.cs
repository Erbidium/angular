﻿using System.Net;
using ProjectStructure.WebAPI.Enums;
using ProjectStructure_BLL.Exceptions;

namespace ProjectStructure.WebAPI.Extensions;

public static class ExceptionFilterExtensions
{
    public static (HttpStatusCode statusCode, ErrorCode errorCode) ParseException(this Exception exception)
    {
        return exception switch
        {
            NotFoundException _ => (HttpStatusCode.NotFound, ErrorCode.NotFound),
            _ => (HttpStatusCode.InternalServerError, ErrorCode.General),
        };
    }
}