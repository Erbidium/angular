﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure_BLL.MappingProfiles;
using ProjectStructure_BLL.Services;
using FluentValidation;
using ProjectStructure.WebAPI.Validators.Project;
using ProjectStructure.WebAPI.Validators.ProjectTask;
using ProjectStructure.WebAPI.Validators.Team;
using ProjectStructure.WebAPI.Validators.User;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure_BLL;

namespace ProjectStructure.WebAPI.Extensions;

public static class ServiceExtensions
{
    public static void RegisterCustomServices(this IServiceCollection services)
    {
        services.AddScoped<UserService>();
        services.AddScoped<ProjectService>();
        services.AddScoped<ProjectTaskService>();
        services.AddScoped<TeamService>();

        services.AddScoped<StructureLinqQueriesCreator>();
        services.AddScoped<LinqTaskService>();

        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<ProjectsDbContext>();
    }

    public static void RegisterCustomValidators(this IServiceCollection services)
    {
        services.AddSingleton<IValidator<UserDto>, UserDtoValidator>();
        services.AddSingleton<IValidator<NewUserDto>, NewUserDtoValidator>();
        services.AddSingleton<IValidator<UpdateUserDto>, UpdateUserDtoValidator>();

        services.AddSingleton<IValidator<ProjectDto>, ProjectDtoValidator>();
        services.AddSingleton<IValidator<NewProjectDto>, NewProjectDtoValidator>();
        services.AddSingleton<IValidator<UpdateProjectDto>, UpdateProjectDtoValidator>();

        services.AddSingleton<IValidator<ProjectTaskDto>, ProjectTaskDtoValidator>();
        services.AddSingleton<IValidator<NewProjectTaskDto>, NewProjectTaskDtoValidator>();
        services.AddSingleton<IValidator<UpdateProjectTaskDto>, UpdateProjectTaskDtoValidator>();

        services.AddSingleton<IValidator<TeamDto>, TeamDtoValidator>();
        services.AddSingleton<IValidator<NewTeamDto>, NewTeamDtoValidator>();
        services.AddSingleton<IValidator<UpdateTeamDto>, UpdateTeamDtoValidator>();
    }

    public static void RegisterAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<ProjectTaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
    }

    public static void ConfigureCustomValidationErrors(this IServiceCollection services)
    {
        services.Configure<ApiBehaviorOptions>(options =>
        {
            options.InvalidModelStateResponseFactory = context =>
            {
                var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => p.ErrorMessage)).ToList();
                var result = new
                {
                    Message = "Validation errors",
                    Errors = errors
                };

                return new BadRequestObjectResult(result);
            };
        });
    }
}