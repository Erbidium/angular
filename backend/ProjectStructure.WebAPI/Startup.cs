﻿using Microsoft.OpenApi.Models;
using ProjectStructure.WebAPI.Extensions;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.WebAPI.Filters;
using ProjectStructure.DAL;

namespace ProjectStructure.WebAPI;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        var migrationAssembly = typeof(ProjectsDbContext).Assembly.GetName().Name;
        services.AddDbContext<ProjectsDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionStrings:ProjectsDBConnection"],
                opt => opt.MigrationsAssembly(migrationAssembly)));

        services.AddControllers();

        services.AddSwaggerGen(c =>
        {
            c.CustomSchemaIds(type => type.ToString());
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProjectStructure.WebAPI", Version = "v1" });
        });

        services.RegisterAutoMapper();
        
        services.AddCors();

        services.RegisterCustomServices();
        services.RegisterCustomValidators();

        services
            .AddMvcCore(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)))
            .AddFluentValidation();

        services.ConfigureCustomValidationErrors();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure.WebAPI v1"));
        }
        
        app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

        app.UseExceptionHandler("/Error");

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}