﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure_BLL.Services;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProjectsController : ControllerBase
{
    private readonly ProjectService _projectService;

    public ProjectsController(ProjectService projectService)
    {
        _projectService = projectService;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<ProjectDto>>> Get()
    {
        return Ok(await _projectService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<ProjectDto>> GetById(int id)
    {
        return Ok(await _projectService.GetById(id));
    }

    [HttpPost]
    public async Task<ActionResult<ProjectDto>> Create([FromBody] NewProjectDto project)
    {
        return Ok(await _projectService.Create(project));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateProjectDto project)
    {
        return Ok(await _projectService.Update(project));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _projectService.Delete(id);
        return NoContent();
    }
}