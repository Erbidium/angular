﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure_BLL.Services;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TeamsController : ControllerBase
{
    private readonly TeamService _teamService;

    public TeamsController(TeamService teamService)
    {
        _teamService = teamService;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<TeamDto>>> Get()
    {
        return Ok(await _teamService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TeamDto>> GetById(int id)
    {
        return Ok(await _teamService.GetById(id));
    }

    [HttpPost]
    public async Task<ActionResult<TeamDto>> Create([FromBody] NewTeamDto team)
    {
        return Ok(await _teamService.Create(team));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateTeamDto team)
    {
        return Ok(await _teamService.Update(team));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _teamService.Delete(id);
        return NoContent();
    }
}