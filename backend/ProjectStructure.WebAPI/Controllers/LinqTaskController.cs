﻿using LinqTask.Lib.DTO;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure_BLL.Services;
using ProjectStructure.Common.DTO.LinqTask;

namespace ProjectStructure.WebAPI.Controllers;

[Route("api/linq-task")]
[ApiController]
public class LinqTaskController : ControllerBase
{
    private readonly LinqTaskService _linqTaskService;

    public LinqTaskController(LinqTaskService linqTaskService)
    {
        _linqTaskService = linqTaskService;
    }

    [HttpGet("number-user-tasks/{id}")]
    public async Task<ActionResult<NumberUserTasksDto>> GetNumberUserTasks(int id)
    {
        return Ok(await _linqTaskService.GetNumberUserTasks(id));
    }

    [HttpGet("tasks-assigned-to-user/{id}")]
    public async Task<ActionResult<TasksAssignedToUserDto>> GetTasksAssignedToUser(int id)
    {
        return Ok(await _linqTaskService.GetTasksAssignedToUser(id));
    }

    [HttpGet("finished-tasks-by-user-in-current-year/{id}")]
    public async Task<ActionResult<IEnumerable<FinishedTaskByUserInCurrentYearDto>?>>
        GetFinishedTasksByUserInCurrentYear(int id)
    {
        return Ok(await _linqTaskService.GetFinishedTasksByUserInCurrentYear(id));
    }

    [HttpGet("team-members")]
    public async Task<ActionResult<IEnumerable<TeamMembersDto>?>> GetTeamMembers()
    {
        return Ok(await _linqTaskService.GetTeamMembers());
    }

    [HttpGet("users-and-tasks")]
    public async Task<ActionResult<IEnumerable<UserAndTasksDto>?>> GetUsersAndTasks()
    {
        return Ok(await _linqTaskService.GetUsersAndTasks());
    }

    [HttpGet("user-statistics/{id}")]
    public async Task<ActionResult<UserStatisticsDto?>> GetUsersAndTasks(int id)
    {
        return Ok(await _linqTaskService.GetUserStatistics(id));
    }

    [HttpGet("project-statistics")]
    public async Task<ActionResult<IEnumerable<ProjectStatisticsDto>?>> GetProjectStatistics()
    {
        return Ok(await _linqTaskService.GetProjectStatistics());
    }
}