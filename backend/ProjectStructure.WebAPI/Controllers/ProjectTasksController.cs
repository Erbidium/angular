﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure_BLL.Services;
using ProjectStructure.Common.DTO.ProjectTask;

namespace ProjectStructure.WebAPI.Controllers;

[Route("api/Tasks")]
[ApiController]
public class ProjectTasksController : ControllerBase
{
    private readonly ProjectTaskService _projectTaskService;

    public ProjectTasksController(ProjectTaskService projectTaskService)
    {
        _projectTaskService = projectTaskService;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<ProjectTaskDto>>> Get()
    {
        return Ok(await _projectTaskService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<ProjectTaskDto>> GetById(int id)
    {
        return Ok(await _projectTaskService.GetById(id));
    }

    [HttpPost]
    public async Task<ActionResult<ProjectTaskDto>> Create([FromBody] NewProjectTaskDto projectTask)
    {
        return Ok(await _projectTaskService.Create(projectTask));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateProjectTaskDto projectTask)
    {
        return Ok(await _projectTaskService.Update(projectTask));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _projectTaskService.Delete(id);
        return NoContent();
    }

    [HttpGet("Unfinished/{id}")]
    public async Task<ActionResult<ProjectTaskDto>> GetUnfinishedTasksByUser(int id)
    {
        return Ok(await _projectTaskService.GetUnfinishedTasksByUser(id));
    }
}