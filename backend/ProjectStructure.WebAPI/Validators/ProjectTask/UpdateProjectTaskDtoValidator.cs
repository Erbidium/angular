﻿using FluentValidation;
using ProjectStructure.Common.DTO.ProjectTask;

namespace ProjectStructure.WebAPI.Validators.ProjectTask;

public class UpdateProjectTaskDtoValidator : AbstractValidator<UpdateProjectTaskDto>
{
    public UpdateProjectTaskDtoValidator()
    {
        RuleFor(t => t.Name)
            .MinimumLength(3)
            .WithMessage("Project task name should be minimum 3 character.")
            .When(t => t.Name is not null);

        RuleFor(t => t.Name)
            .MaximumLength(100)
            .WithMessage("Project task name should be maximum 100 characters.")
            .When(t => t.Name is not null);

        RuleFor(t => t.Description)
            .MinimumLength(10)
            .WithMessage("Project task description should be minimum 10 character.")
            .When(t => t.Description is not null);
    }
}