﻿using FluentValidation;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.Validators.Team;

public class UpdateTeamDtoValidator : AbstractValidator<UpdateTeamDto>
{
    public UpdateTeamDtoValidator()
    {
        RuleFor(t => t.Name)
            .MinimumLength(3)
            .WithMessage("Team name should be minimum 3 character.")
            .When(t => t.Name is not null);

        RuleFor(t => t.Name)
            .MaximumLength(100)
            .WithMessage("Team name should be maximum 100 characters.")
            .When(t => t.Name is not null);
    }
}