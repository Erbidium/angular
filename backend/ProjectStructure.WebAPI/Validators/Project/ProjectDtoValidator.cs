﻿using FluentValidation;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.WebAPI.Validators.Project;

public class ProjectDtoValidator : AbstractValidator<ProjectDto>
{
    public ProjectDtoValidator()
    {
        RuleFor(p => p.Name)
            .MinimumLength(3)
            .WithMessage("Project name should be minimum 3 character.")
            .When(p => p.Name is not null);

        RuleFor(t => t.Name)
            .MaximumLength(100)
            .WithMessage("Project name should be maximum 100 characters.")
            .When(p => p.Name is not null);

        RuleFor(p => p.Description)
            .MinimumLength(10)
            .WithMessage("Project description should be minimum 10 character.")
            .When(p => p.Description is not null);
    }
}