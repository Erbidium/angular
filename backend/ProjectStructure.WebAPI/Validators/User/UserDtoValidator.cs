﻿using FluentValidation;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.Validators.User;

public class UserDtoValidator : AbstractValidator<UserDto>
{
    public UserDtoValidator()
    {
        RuleFor(u => u.Email)
            .EmailAddress()
            .When(u => !string.IsNullOrEmpty(u.Email));

        RuleFor(u => u.FirstName)
            .MaximumLength(100)
            .WithMessage("First name should be maximum 100 characters.")
            .When(t => t.FirstName is not null);

        RuleFor(u => u.LastName)
            .MaximumLength(100)
            .WithMessage("Last name should be maximum 100 characters.")
            .When(t => t.LastName is not null);
    }
}