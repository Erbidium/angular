﻿using FluentValidation;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.Validators.User;

public class NewUserDtoValidator : AbstractValidator<NewUserDto>
{
    public NewUserDtoValidator()
    {
        RuleFor(u => u.Email)
            .EmailAddress()
            .When(u => !string.IsNullOrEmpty(u.Email));

        RuleFor(u => u.BirthDay)
            .NotEmpty()
            .WithMessage("BirthDay is mandatory");

        RuleFor(u => u.FirstName)
            .MaximumLength(100)
            .WithMessage("First name should be maximum 100 characters.")
            .When(t => t.FirstName is not null);

        RuleFor(u => u.LastName)
            .MaximumLength(100)
            .WithMessage("Last name should be maximum 100 characters.")
            .When(t => t.LastName is not null);
    }
}