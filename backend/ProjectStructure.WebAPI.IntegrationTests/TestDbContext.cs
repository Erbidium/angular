﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.WebAPI.IntegrationTests;

public class TestDbContext : ProjectsDbContext
{
    public TestDbContext(DbContextOptions<ProjectsDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Configure();
    }
}