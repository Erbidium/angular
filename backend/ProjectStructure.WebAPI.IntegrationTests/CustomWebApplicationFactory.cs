﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjectStructure.DAL;

namespace ProjectStructure.WebAPI.IntegrationTests;

public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureTestServices(services =>
        {
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                     typeof(DbContextOptions<ProjectsDbContext>));

            services.Remove(descriptor);

            string databaseName = Guid.NewGuid().ToString();

            services.AddScoped(_ =>
            {
                var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
                builder.UseInMemoryDatabase(databaseName);
                builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                builder.EnableSensitiveDataLogging();
                return builder.Options;
            });
            services.AddScoped<ProjectsDbContext, TestDbContext>();

            var sp = services.BuildServiceProvider();

            using var scope = sp.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var db = scopedServices.GetRequiredService<ProjectsDbContext>();

            db.Database.EnsureCreated();
        });
    }
}