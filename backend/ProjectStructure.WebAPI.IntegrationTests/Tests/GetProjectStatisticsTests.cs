using System.Text;
using AutoMapper;
using LinqTask.Lib.DTO;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Enums;
using ProjectLinq = LinqTask.Lib.Entities.Project;
using ProjectTaskLinq = LinqTask.Lib.Entities.ProjectTask;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class GetProjectStatisticsTests
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Startup> _factory;
    private readonly IMapper _mapper;

    public GetProjectStatisticsTests()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<ProjectTaskDto, ProjectTaskLinq>();
            cfg.CreateMap<ProjectDto, ProjectLinq>();
        });
        _mapper = new Mapper(configuration);
        _factory = new CustomWebApplicationFactory<Startup>();
        _client = _factory.CreateClient();
    }

    [Fact]
    public async void GetProjectStatistics_ReturnsCorrectResult()
    {
        var newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        var newProjectDto = new NewProjectDto
        {
            TeamId = 1,
            AuthorId = 1,
            Deadline = new DateTime(2023, 1, 1)
        };
        var projectResponse = await _client.PostAsync("api/Projects",
            new StringContent(JsonConvert.SerializeObject(newProjectDto), Encoding.UTF8, "application/json"));
        var project = JsonConvert.DeserializeObject<ProjectDto>(await projectResponse.Content.ReadAsStringAsync());

        var newProjectTaskDto1 = new NewProjectTaskDto
        {
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
        };
        var projectTask1Response = await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto1), Encoding.UTF8, "application/json"));
        var projectTask1 =
            JsonConvert.DeserializeObject<ProjectTaskDto>(await projectTask1Response.Content.ReadAsStringAsync());

        var newProjectTaskDto2 = new NewProjectTaskDto
        {
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done
        };
        var projectTask2Response = await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto2), Encoding.UTF8, "application/json"));
        var projectTask2 =
            JsonConvert.DeserializeObject<ProjectTaskDto>(await projectTask2Response.Content.ReadAsStringAsync());

        IEnumerable<ProjectStatisticsDto> expected = new[]
        {
            new ProjectStatisticsDto
            {
                Project = _mapper.Map<ProjectLinq>(project),
                LongestTask = _mapper.Map<ProjectTaskLinq>(projectTask1),
                ShortestTask = _mapper.Map<ProjectTaskLinq>(projectTask2),
                NumberOfUsersInProjectTeam = 1
            }
        };


        var actual = JsonConvert.DeserializeObject<IEnumerable<ProjectStatisticsDto>>(
            await _client.GetStringAsync("api/linq-task/project-statistics"));


        Assert.Equal(expected.Count(), actual.Count());
        Assert.Equal(expected.First().Project.Id, actual.First().Project.Id);
        Assert.Equal(expected.First().NumberOfUsersInProjectTeam, actual.First().NumberOfUsersInProjectTeam);
        Assert.Equal(expected.First().LongestTask.Id, actual.First().LongestTask.Id);
        Assert.Equal(expected.First().ShortestTask.Id, actual.First().ShortestTask.Id);
    }

    [Fact]
    public async void
        GetProjectStatistics_WhenTasksNumberMoreOrEqualThanThreeAndDescriptionIsLessThanTwentySymbols_ThenNumberOfUsersInProjectTeamIsNull()
    {
        var newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        var newProjectDto = new NewProjectDto
        {
            TeamId = 1,
            AuthorId = 1,
            Deadline = new DateTime(2023, 1, 1)
        };
        var projectResponse = await _client.PostAsync("api/Projects",
            new StringContent(JsonConvert.SerializeObject(newProjectDto), Encoding.UTF8, "application/json"));
        var project = JsonConvert.DeserializeObject<ProjectDto>(await projectResponse.Content.ReadAsStringAsync());

        var newProjectTaskDto1 = new NewProjectTaskDto
        {
            PerformerId = 1,
            ProjectId = 1,
            Name = "Long name.................",
            Description = "Long description.............",
            State = TaskState.Done,
        };
        await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto1), Encoding.UTF8, "application/json"));

        var newProjectTaskDto2 = new NewProjectTaskDto
        {
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name",
            Description = "Short description",
            State = TaskState.Done
        };
        await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto2), Encoding.UTF8, "application/json"));

        var newProjectTaskDto3 = new NewProjectTaskDto
        {
            PerformerId = 1,
            ProjectId = 1,
            Name = "Short name name",
            Description = "Short description",
            State = TaskState.Done
        };
        await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto3), Encoding.UTF8, "application/json"));


        var actual = JsonConvert.DeserializeObject<IEnumerable<ProjectStatisticsDto>>(
            await _client.GetStringAsync("api/linq-task/project-statistics"));


        Assert.Null(actual.First().NumberOfUsersInProjectTeam);
    }
}