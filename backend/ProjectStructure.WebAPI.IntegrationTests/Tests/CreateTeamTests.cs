using System.Net;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class CreateTeamTests
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Startup> _factory;

    public CreateTeamTests()
    {
        _factory = new CustomWebApplicationFactory<Startup>();
        _client = _factory.CreateClient();
    }

    [Fact]
    public async void CreateTeam_ThanResponseWithCode200AndCorrespondedBody()
    {
        NewTeamDto newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        var httpResponse = await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));
        var stringResponse = await httpResponse.Content.ReadAsStringAsync();
        var createdTeam = JsonConvert.DeserializeObject<TeamDto>(stringResponse);

        Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        Assert.Equal(newTeamDto.Name, createdTeam.Name);
    }

    [Fact]
    public async void CreateTeam_WhenTeamNameLessThanThreeSymbols_ThanResponseBadRequest()
    {
        NewTeamDto newTeamDto = new NewTeamDto
        {
            Name = "tt"
        };
        var httpResponse = await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
    }
}