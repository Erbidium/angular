﻿using System.Net;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class DeleteProjectTaskTests
{
    private readonly HttpClient _client;

    public DeleteProjectTaskTests()
    {
        var factory = new CustomWebApplicationFactory<Startup>();
        _client = factory.CreateClient();
    }

    [Fact]
    public async void DeleteProjectTask_ThanResponseWithCode204()
    {
        NewTeamDto newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        NewUserDto newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        }; 
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        NewProjectDto newProjectDto = new NewProjectDto
        {
            TeamId = 1,
            AuthorId = 1,
            Deadline = new DateTime(2023, 1, 1)
        }; 
        await _client.PostAsync("api/Projects",
            new StringContent(JsonConvert.SerializeObject(newProjectDto), Encoding.UTF8, "application/json"));

        NewProjectTaskDto newProjectTaskDto = new NewProjectTaskDto
        {
            ProjectId = 1,
            PerformerId = 1
        };
        await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTaskDto), Encoding.UTF8, "application/json"));

        int deleteProjectTaskId = 1;

        var httpResponse = await _client.DeleteAsync($"api/Tasks/{deleteProjectTaskId}");

        Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
    }

    [Fact]
    public async void DeleteProjectTask_WhenNotExistingProjectTask_ThanResponseWithCode404()
    {
        int deleteNotExistingProjectTaskId = 5;

        var httpResponse = await _client.DeleteAsync($"api/Tasks/{deleteNotExistingProjectTaskId}");

        Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
    }
}