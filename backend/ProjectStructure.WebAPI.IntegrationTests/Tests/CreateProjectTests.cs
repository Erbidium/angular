using System.Net;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class CreateProjectTests
{
    private readonly HttpClient _client;

    public CreateProjectTests()
    {
        var factory = new CustomWebApplicationFactory<Startup>();
        _client = factory.CreateClient();
    }

    [Fact]
    public async void CreateProject_ThanResponseWithCode200AndCorrespondedBody()
    {
        var newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        }; 
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        var newProjectDto = new NewProjectDto
        {
            TeamId = 1,
            AuthorId = 1,
            Name = "Project1",
            Description = "Description1",
            Deadline = new DateTime(2023, 1, 1)
        };
        string jsonInString = JsonConvert.SerializeObject(newProjectDto);


        var httpResponse = await _client.PostAsync("api/Projects",
            new StringContent(jsonInString, Encoding.UTF8, "application/json"));
        var stringResponse = await httpResponse.Content.ReadAsStringAsync();
        var createdProject = JsonConvert.DeserializeObject<ProjectDto>(stringResponse);

        Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        Assert.Equal(newProjectDto.Name, createdProject.Name);
        Assert.Equal(newProjectDto.Description, createdProject.Description);
        Assert.Equal(newProjectDto.Deadline, createdProject.Deadline);
    }

    [Fact]
    public async void CreateProject_WhenTeamIdIsNotSpecified_ThanResponseBadRequest()
    {
        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        
        var newProjectDto = new NewProjectDto
        {
            AuthorId = 1,
            Name = "Project1",
            Description = "Description1",
            Deadline = new DateTime(2023, 1, 1)
        };
        string jsonInString = JsonConvert.SerializeObject(newProjectDto);


        var httpResponse = await _client.PostAsync("api/Projects",
            new StringContent(jsonInString, Encoding.UTF8, "application/json"));

        Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
    }
}