﻿using System.Net;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class DeleteUserTests
{
    private readonly HttpClient _client;

    public DeleteUserTests()
    {
        var factory = new CustomWebApplicationFactory<Startup>();
        _client = factory.CreateClient();
    }

    [Fact]
    public async void DeleteUser_ThanResponseWithCode204()
    {
        var newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        int deleteUserId = 1;

        var httpResponse = await _client.DeleteAsync($"api/Users/{deleteUserId}");

        Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
    }

    [Fact]
    public async void DeleteUser_WhenNotExistingUser_ThanResponseWithCode404()
    {
        int deleteNotExistingUserId = 5;

        var httpResponse = await _client.DeleteAsync($"api/Users/{deleteNotExistingUserId}");

        Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
    }
}