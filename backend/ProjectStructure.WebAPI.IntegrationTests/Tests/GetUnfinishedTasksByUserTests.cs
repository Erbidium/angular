﻿using System.Net;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ProjectTask;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Enums;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests;

public class GetUnfinishedTasksByUserTests
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Startup> _factory;

    public GetUnfinishedTasksByUserTests()
    {
        _factory = new CustomWebApplicationFactory<Startup>();
        _client = _factory.CreateClient();
    }

    [Fact]
    public async void GetUnfinishedTasksByUser_ThanResponseWithCode200AndCorrespondedBody()
    {
        var newTeamDto = new NewTeamDto
        {
            Name = "Good team"
        };
        await _client.PostAsync("api/Teams",
            new StringContent(JsonConvert.SerializeObject(newTeamDto), Encoding.UTF8, "application/json"));

        var newUserDto = new NewUserDto
        {
            TeamId = 1,
            BirthDay = new DateTime(2000, 1, 1)
        };
        await _client.PostAsync("api/Users",
            new StringContent(JsonConvert.SerializeObject(newUserDto), Encoding.UTF8, "application/json"));

        var newProjectDto = new NewProjectDto
        {
            TeamId = 1,
            AuthorId = 1,
            Deadline = new DateTime(2023, 1, 1)
        };
        await _client.PostAsync("api/Projects",
            new StringContent(JsonConvert.SerializeObject(newProjectDto), Encoding.UTF8, "application/json"));

        var newProjectTask1Dto = new NewProjectTaskDto
        {
            Name = "Name 1",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.InProgress
        };
        var projectTask1Response = await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTask1Dto), Encoding.UTF8, "application/json"));
        var projectTask1 =
            JsonConvert.DeserializeObject<ProjectTaskDto>(await projectTask1Response.Content.ReadAsStringAsync());

        var newProjectTask2Dto = new NewProjectTaskDto
        {
            Name = "Name 2",
            PerformerId = 1,
            ProjectId = 1,
            State = TaskState.ToDo,
        };
        var projectTask2Response = await _client.PostAsync("api/Tasks",
            new StringContent(JsonConvert.SerializeObject(newProjectTask2Dto), Encoding.UTF8, "application/json"));
        var projectTask2 =
            JsonConvert.DeserializeObject<ProjectTaskDto>(await projectTask2Response.Content.ReadAsStringAsync());

        int userId = 1;
        IEnumerable<ProjectTaskDto> expected = new[]
        {
            projectTask1,
            projectTask2
        };

        var httpResponse = await _client.GetAsync($"api/Tasks/Unfinished/{userId}");
        var stringResponse = await httpResponse.Content.ReadAsStringAsync();
        var actual = JsonConvert.DeserializeObject<IEnumerable<ProjectTaskDto>>(stringResponse);

        Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        Assert.Equal(expected.Count(), actual.Count());
        Assert.All(expected,
            expectedProjectTask => actual.Any(actualProjectTask => actualProjectTask.Id == expectedProjectTask.Id));
    }

    [Fact]
    public async void GetUnfinishedTasksByUser_WhenNotExistingUser_ThanResponseWithCode404()
    {
        int notExistingUserId = 1;

        var httpResponse = await _client.GetAsync($"api/Tasks/Unfinished/{notExistingUserId}");

        Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
    }
}